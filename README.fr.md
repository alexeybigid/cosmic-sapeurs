# Cosmic Sapeurs 🇫🇷 🇬
(Version anglaise [ici](README.md).)

Cosmic-Sapeurs est un projet porté par [la brigade des sapeurs-pompiers de Paris](https://www.pompiersparis.fr/) et [Etalab](https://www.etalab.gouv.fr/) à travers [Programme d'Entrepreneurs d'Intérêt Général](https://www.etalab.gouv.fr/entrepreneurs-d-interet-general). Ensemble, nous avons développé un tableau de bord en temps réel indiquant où se trouvent les ressources (par exemple, les engins-pompes) et les besoins (par exemple les incendies).

Vous pouvez lire la description du projet sur le [site du programme EIG](https://entrepreneur-interet-general.etalab.gouv.fr/defis/2020/operations-18.html).

Ce dépôt héberge le code de l'application.
Le backend est en Python; le frontend en React / Tsx et Redux.

🤔 Pourquoi "cosmic-sapeurs"? Ça vient du livre [Cosmic Python](https://www.cosmicpython.com/), dont nous nous sommes inspirés pour concevoir notre architecture, de manière "clean". D'ailleurs, nous vous encourageons à le lire.

## Fonctionnalitées

<div align="center" >
<img src="readme-docs/fonctionnalites.png" alt="fonctionnalites" height="350px">
</div>

## Architecture logicielle
### Event-driven
Cette application est *pilotée par les événements*, ce qui signifie que nous «écoutons» (c'est-à-dire nous inscrivons, «connectons») à une file d'attente d'événements et nous mettons à jour l'application en conséquence.

Vous pouvez aller voir [ce chapitre](https://www.cosmicpython.com/book/part2.html) de Cosmic-Python sur le sujet. 
<br>

Plus concrêtement:   🔥 ➡ 📞 ➡ 🧑🏾‍💻 ➡  🚒 ➡ 🎙

- Vous appelez le 18 pour signaler un incendie. A la création de l'opération, nous recevons un événement *"operation_changed_status"* avec les informations pertinentes (identifiant d'opération, état, adresse, cause, date, niveau d'urgence ...)

- Un véhicule est affecté à l'opération. En quittant la gare, l'agent change le statut du véhicule et nous recevons un événement *"vehicle_changed_status"* avec les informations pertinentes (id du véhicule, statut, catégorie de véhicule, gare, opération affectée, date ...)

 <br>

Un événement est un objet avec un `uuid`, un `topic` et un objet `data`. Voir la définition d'un [EventEntity](https://gitlab.com/op18-enki/cosmic-sapeurs/-/blob/develop/backs/shared/shared/events/event_entity.py). 

<br> 

<div align="center">
<img src="readme-docs/architecture-story.png" alt="architecture example" >
</div>


### Services
There are 4 services (3 in [backs](./backs), 1 [front](./front). 
- **Interface utilisateur:** une application frontend construite en typescript / Redux / React. Docs [ici](./front/README.fr.md).
- **Backend Sapeurs:** une application python, c'est là que se passent les traitements métiers. Docs [ici](./backs/cover-ops/README.fr.md)
- **Convertisseur:** cette brique permet de convertir les données brut du système externe au format du domaine sapeur. Cette brique est une application python deployée indépendamment du backend. C'est le convertisseur qui stock les évenements bruts du système externe avant de les convertir.Docs [ici](./backs/converters/README.fr.md)
- **Replayers:** ce service permet de "rejouer" des évènements depuis des fichiers CSV, afin de simuler le système externe. Un évènement est un objet avec un uuid, un topic et une data serialisée. On peut choisir la vitesse de rejeu.  Docs [ici](./backs/replayers/README.fr.md)

<div align="center">
<img src="readme-docs/architecture-sapeurs.jpg" alt="architecture logicielle" >
</div>

### Contrat d'Interface
Le contrat d'interface est un utilitaire indépendant [dataclass-to-interface](./dataclass-to-interface)). Il garantit que le format des objets échangés entre le backend et le frontend n'est défini qu'une seule fois. Par conséquent, nous définissons nos objets comme une dataclass Python [ici](./backs/shared/shared/data_transfert_objects) et nous générons leurs interfaces Typescript correspondantes [là](./front/src/interfaces/generated).

Par exemple : 

Cette dataclass VehicleEventData :  
```python
@dataclass
class VehicleEventData(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    timestamp: str
    raw_vehicle_id: str
    raw_operation_id: str
    status: Literal[ "departed_to_intervention" , "arrived_on_intervention", "transport_to_hospital"]
    raw_status: str  
    home_area: Literal["CHPT" , "STOU" , "PARM" ]  
    role: Literal["vsav" , "ep" ]  
    vehicle_name: str  
```
... devient l'interface suivante: 
```typescript
interface VehicleEventData {
  timestamp: string;
  raw_vehicle_id: string;
  raw_operation_id?: string;
  status: "departed_to_intervention" | "arrived_on_intervention" | "transport_to_hospital"
  raw_status: string;
  home_area: "CHPT" | "STOU" | "PARM" 
  role: "vsav" | "ep" 
  vehicle_name: string;
}
```


### "Clean" organization within each service 

Si vous ouvrez l'un des trois services de backend, vous remarquerez l'organisation suivante :

- **DOMAIN**
    - **ports:** API de dépendance où nous définissons comment l'application interagit avec des "briques" externes. Par exemple, un port de dépôt aurait une méthode "add". La mise en œuvre réelle de ces "briques" se fait par des adaptateurs.
    Les ports sont testés dans tests/unit. 

    - **Use cases:** toutes les règles métier spécifiques à l'application sont divisées en une liste de cas d'usage, chacun ayant une seule responsabilité. Par exemple: stocker le nouvel événement dans un dépôt.
    Les use-cases sont testés dans tests/unit.  
    - ...
- **ADAPTATERS**:
    La mise en œuvre réelle des ports. Par exemple, un référentiel pourrait être implémenté avec la base de données Postgres, donc la méthode "add" serait un INSERT. 
    Les adapters sont testés dans tests/integration. 

- **ENTRYPOINTS**:
   - ./server.py: le script pour lancer l'application.
   - ./config.py: préparation des instances de l'application.
   Le serveur est testé dans tests/e2e. 



## Installation

Cloner le repo avec git :

```
git clone https://gitlab.com/op18-enki/cosmic-sapeurs
```

Si vous n'avez pas git, vous pouvez aussi télécharger le .zip [ici](https://gitlab.com/op18-enki/cosmic-sapeurs)

### Avec Docker

#### Prérequis

- Docker CE >= 1.13.0
- docker-compose >= 1.10.0

#### Étapes

Copiez le `front / .env.sample` dans` front / .env` et le fichier cstc.json. Notez que vous pouvez adapter les variables d'environnement et la carte geo-json, si nécessaire.

```
cp ./front/.env.sample ./front/.env
cp ./front/src/ui/map/empty_cstc.json ./front/src/ui/map/cstc.json
```
Construire et lancer les services : 

```
docker-compose up --build
```
Ouvrez la page http://localhost:4200/

Si vous souhaitez rejouer des événements, suivez les indications dans [la doc du replayers](./backs/replayers/README.md)

Notez qu'un dossier `docker-data` a été créé. Il contient les volumes que nous voulons rendre persistants. Cela va dans les deux sens: si vous avez des données dans ce dossier, le container Docker en aura également. Par conséquent, si vous souhaitez repartir d'un setup vierge, vous pouvez supprimer ce répertoire: 

```
rm -rf docker-data
```


### Sans Docker

Vous pouvez également lancer les backends et le frontend indépendamment, en vous référant
aux documentations respectives ([docs du replayers](/backs/replayers/README.fr.md), [docs du converters](/backs/converters/README.fr.md), [docs de l'API](/backs/cover-ops/README.fr.md), [frontend documentation](/front/README.fr.md)).


## Tests 

### Backs 
- Suivre les instructions d'installation de chaque application (afin de créer un environnement virtuel dans chaque dossier.)
- Lancer tous les tests: 
```
 cd ./backs 
 sh launch_all_tests.sh   
```

### Front 
```
cd ./front
npm install 
npm run test 
```
