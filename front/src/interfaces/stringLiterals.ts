import { Literals } from "interfaces/generated/Literals";

export type VehicleStatus = Literals["vehicleStatus"];
export type VehicleRole = Literals["vehicleRole"];
export type AvailabilityKind = Literals["availabilityKind"];
export type DomainTopic = Literals["domainTopic"];
export type OperationCause = Literals["operationCause"];
export type OmnibusBalanceKind = Literals["omnibusBalanceKind"];

type Area = Literals["area"];

// prettier-ignore
export const bsppAreaOptions: Area[] = ["STMR", "ANTO", "CHPY", "PLCL", "STCL", "CHLY", "POIS", "ROUS", "CHTO", "STOU", "PARM", "VIJF", "PIER", "SEVI", "CHPT", 
"PCDG", "LEVA", "AUBE", "VISG", "BOUL", "NOIS", "MALF", "SUCY", "VILC", "CHOI", "VITR", "STDE", "LACO", "MTRL", "STHO", "VIMB", "NEUI", 
"COBI", "CLIC", "PLAI", "AULN", "MALA", "BLME", "DRAN", "LAND", "MENI", "CHAR", "PANT", "BITC", "BLAN", "MTMA", "PROY", "AUTE", "DAUP", 
"MTGE", "GREN", "ISSY", "MEUD", "BGLR", "CLAM", "NATI", "CBVE", "PUTX", "GENN", "MASS", "ASNI", "IVRY", "RUEI", "VINC", "NANT", "COBE", 
"NOGT", "CRET", "TREM", "RUNG", "ORLY", "BOND", "BSLT"];

export const omnibusBalanceKindOptions: OmnibusBalanceKind[] = [
  "pse_on_inter_fire",
  "only_pse_available",
  "both_available",
  "pse_on_inter_san",
  "vsav_on_inter_san",
  "both_on_inter_san",
];

export type { Area };
