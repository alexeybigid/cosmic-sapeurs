export type {
  Area,
  VehicleRole,
  VehicleStatus,
  DomainTopic,
  AvailabilityKind,
  OperationCause,
  OmnibusBalanceKind,
} from "interfaces/stringLiterals";
export {
  bsppAreaOptions,
  omnibusBalanceKindOptions,
} from "interfaces/stringLiterals";
export type {
  OmnibusBalanceChangedEventData as OmnibusBalanceChangedEvent,
  Omnibus,
  OmnibusBalance,
} from "interfaces/generated/OmnibusBalanceChangedEventData";

export type { VehicleEventData as VehicleEvent } from "interfaces/generated/VehicleEventData";
export type {
  Availability,
  OngoingOpChangedEventData as OngoingOpChangedEvent,
  AvailabilityChangedEventData as AvailabilityChangedEvent,
} from "interfaces/generated/CoverOpsBecameReadyEventData";
