/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

/**
 * OmnibusBalance(both_available: int = 0, vsav_on_inter_san: int = 0, pse_on_inter_san: int = 0, pse_on_inter_fire: int = 0, both_on_inter_san: int = 0, only_pse_available: int = 0)
 */
export interface OmnibusBalance {
  both_available?: number;
  vsav_on_inter_san?: number;
  pse_on_inter_san?: number;
  pse_on_inter_fire?: number;
  both_on_inter_san?: number;
  only_pse_available?: number;
}
