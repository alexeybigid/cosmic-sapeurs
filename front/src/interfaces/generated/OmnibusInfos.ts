/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

/**
 * OmnibusInfos(partner_raw_vehicle_id: <function NewType.<locals>.new_type at 0x7fb3003055e0>)
 */
export interface OmnibusInfos {
  partner_raw_vehicle_id: string;
}
