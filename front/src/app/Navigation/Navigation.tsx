import { useSelector } from "app/redux-hooks";
import { routes, useRoute } from "app/Router";
import { UiTabs, LinkTab, UiAppBar } from "ui";
import { format } from "date-fns";
import { lastTimestampDateSelector } from "core-logic";
import { PubSubStatusChip } from "./PubSubStatusChip";

export const Navigation = () => {
  const route = useRoute();
  const pubSubStatus = useSelector((state) => state.interactions.pubSubStatus);
  const lastTimestampDate = useSelector(lastTimestampDateSelector);
  const isLoading = useSelector(
    (state) => state.cover.isFetchingCover || state.ops.isFetchingOps,
  );

  return (
    <UiAppBar isLoading={isLoading}>
      <UiTabs tabValue={route.name}>
        <LinkTab
          label="Couverture Opérationnelle"
          {...routes.coverOps().link}
          value={routes.coverOps().name}
        />
        <LinkTab
          label="Détail des disponibilitées"
          {...routes.detailedCover().link}
          value={routes.detailedCover().name}
        />
        {/* <LinkTab
          label="Détail des opérations"
          {...routes.detailedOps().link}
          value={routes.detailedOps().name}
        /> */}
      </UiTabs>
      <div className="flex-grow text-right pr-5">
        {format(lastTimestampDate, "dd/MM/yyyy  HH:mm")}
      </div>
      <PubSubStatusChip status={pubSubStatus} />
    </UiAppBar>
  );
};
