import { createRouter, defineRoute } from "type-route";
import { CoverOps } from "app/pages/CoverOps/CoverOps";
import { DetailedCover } from "app/pages/DetailedCover/DetailedCover";
// import { DetailedOps } from "app/pages/DetailedOps/DetailedOps";

export const { RouteProvider, useRoute, routes } = createRouter({
  coverOps: defineRoute(["/", "/cover-ops"]),
  detailedCover: defineRoute({}, () => `/detailed-cover`),
  detailedOps: defineRoute({}, () => `/detailed-ops`),
});

export const Router = () => {
  const route = useRoute();

  return (
    <>
      {route.name === "detailedCover" && <DetailedCover route={route} />}
      {route.name === "coverOps" && <CoverOps route={route} />}
      {/* {route.name === "detailedOps" && <DetailedOps route={route} />} */}
    </>
  );
};
