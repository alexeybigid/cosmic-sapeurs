import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { actions, AppThunk } from "core-logic";

const useSubscription = (
  subscribeThunk: () => AppThunk,
  unsubscribeThunk: () => AppThunk,
) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(subscribeThunk());
    return () => {
      dispatch(unsubscribeThunk());
    };
  }, []);
};

export const useSubscriptionToAvailabilityChangedEvents = () =>
  useSubscription(
    actions.subscribeToAvailabilityChangedEventsThunk,
    actions.unsubscribeFromAvailabilityChangedEventsThunk,
  );

export const useSubscriptionToOngoingOpChangedEvents = () =>
  useSubscription(
    actions.subscribeToOngoingOpChangedEventsThunk,
    actions.unsubscribeFromOngoingOpChangedEventsThunk,
  );

export const useSubscriptionToOmnibusBalanceChangedEvents = () =>
  useSubscription(
    actions.subscribeToOmnibusBalanceChangedEventsThunk,
    actions.unsubscribeFromOmnibusBalanceChangedEventsThunk,
  );

export const useBatchDispatchLoop = () =>
  useSubscription(
    actions.loopOverDispatchBatchThunk,
    actions.stopLoopOverDispatchBatchThunk,
  );
