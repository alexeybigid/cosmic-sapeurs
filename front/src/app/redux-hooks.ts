import { RootState } from "core-logic";
import {
  useSelector as useReactReduxSelector,
  TypedUseSelectorHook,
} from "react-redux";

export const useSelector: TypedUseSelectorHook<RootState> = useReactReduxSelector;
