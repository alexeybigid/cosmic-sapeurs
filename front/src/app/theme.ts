import { AvailabilityKind, OperationCause } from "interfaces";
import type { CoverOpsVehicleSubCategory } from "core-logic";

const commonColors = {
  // unavailable: "gray",
  recoverable: { color: "gray" },
};

export type CoverStyle = {
  mainColor: string;
  byKind: Omit<
    Record<AvailabilityKind, { color: string; stripes?: string }>,
    "unavailable"
  >;
  scale: string[];
};

const hexToRGBBuilder = (hexaColor: string) => (opacity = 1) => {
  const r = parseInt(hexaColor.slice(1, 3), 16);
  const g = parseInt(hexaColor.slice(3, 5), 16);
  const b = parseInt(hexaColor.slice(5, 7), 16);
  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};

type NumberOfColors = 3 | 4;

const makeScale = (
  hexaColor: string,
  numberOfColors?: NumberOfColors,
): string[] => {
  const hexToRgb = hexToRGBBuilder(hexaColor);
  switch (numberOfColors) {
    case 3:
      return [hexToRgb(0), hexToRgb(0.4), hexToRgb(1)];
    case 4:
    default:
      return [hexToRgb(0), hexToRgb(0.15), hexToRgb(0.6), hexToRgb(1)];
  }
};

const victimRescueMain = "#4CB9DB";
const pumpMain = "#ffc400";
export const omnibusBothAvailable = "#69C71E";
export const stripesColor = "#404040";

const makeSubCategoryStyle = (
  mainColor: string,
  numberOfColorInScale: NumberOfColors,
): CoverStyle => ({
  mainColor,
  byKind: {
    ...commonColors,
    in_service: { color: mainColor, stripes: stripesColor },
    available: { color: mainColor },
  },
  scale: makeScale(mainColor, numberOfColorInScale).reverse(),
});

export const coverStyles: Record<CoverOpsVehicleSubCategory, CoverStyle> = {
  sap: makeSubCategoryStyle(victimRescueMain, 4),
  ep: makeSubCategoryStyle(pumpMain, 3),
};

export type OpsStyle = {
  main: string;
  scale: string[];
};

const victimOpsMain = "#3A48C2";
const fireOpsMain = "#fa5e43";
const otherOpsMain = "#336633";

export const opsColors: Record<OperationCause, OpsStyle> = {
  victim: {
    main: victimOpsMain,
    scale: makeScale(victimOpsMain),
  },
  fire: {
    main: fireOpsMain,
    scale: makeScale(fireOpsMain, 3),
  },
  other: {
    main: otherOpsMain,
    scale: [],
  },
};
