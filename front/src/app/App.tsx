import {
  useBatchDispatchLoop,
  useSubscriptionToAvailabilityChangedEvents,
  useSubscriptionToOmnibusBalanceChangedEvents,
  useSubscriptionToOngoingOpChangedEvents,
} from "app/subscription-hooks";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { actions, Hours } from "core-logic";
import { Navigation } from "app/Navigation/Navigation";
import { Router } from "app/Router";

const useFetchCover = (hours: Hours) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchCoverThunk(hours));
  }, [hours]);
};

const useFetchOps = (hours: Hours) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchOpsThunk(hours));
  }, [hours]);
};

const useFetchOmnibus = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.fetchOmnibusThunk());
  }, []);
};

export const App = () => {
  const maxNumberOfHoursToShow = 4;

  useBatchDispatchLoop();
  useFetchOps(maxNumberOfHoursToShow);
  useFetchCover(maxNumberOfHoursToShow);
  useFetchOmnibus();
  useSubscriptionToAvailabilityChangedEvents();
  useSubscriptionToOngoingOpChangedEvents();
  useSubscriptionToOmnibusBalanceChangedEvents();

  return (
    <>
      <Navigation />
      <Router />
    </>
  );
};
