import { lastOnGoingOpEventByOperationIdSelector } from "core-logic";
import { Route } from "type-route";
import { useSelector } from "app/redux-hooks";
import { routes } from "app/Router";

import { SimpleTable } from "ui";
import * as R from "ramda";

type DetailedOpsProps = { route: Route<typeof routes.detailedOps> };

export const DetailedOps = (props: DetailedOpsProps) => {
  const lastOnGoingOpEventByOperationId = useSelector(
    lastOnGoingOpEventByOperationIdSelector,
  );

  const lastOnGoingOpEvents = R.values(lastOnGoingOpEventByOperationId);

  return (
    <div>
      <SimpleTable data={lastOnGoingOpEvents as any[]} />
    </div>
  );
};
