import { routes } from "app/Router";
import { availabilityTableInputSelector, vehiclesLabels } from "core-logic";
import { Route } from "type-route";
import { AvailabilityTable } from "ui/AvailabilityTable";
import { useSelector } from "app/redux-hooks";

type DetailedCoverProps = { route: Route<typeof routes.detailedCover> };

export const DetailedCover = (props: DetailedCoverProps) => {
  const data = useSelector(availabilityTableInputSelector);
  return <AvailabilityTable data={data} labels={vehiclesLabels} />;
};
