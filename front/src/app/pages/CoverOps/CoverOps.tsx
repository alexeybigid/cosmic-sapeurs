import { Route } from "type-route";
import { routes } from "app/Router";
import { FireRow } from "./FireRow";
import { VictimRow } from "./VictimRow";
import { OmnibusRow } from "./OmnibusRow";

import { HelloSapeurs } from "./HelloSapeurs";
import "app/app.css";

const chartHeight = "300px";

type CoverOpsProps = { route: Route<typeof routes.coverOps> };

export const CoverOps = (props: CoverOpsProps) => (
  <div className="app m-5">
    <VictimRow chartHeight={chartHeight} />
    <OmnibusRow chartHeight={chartHeight} />
    <FireRow chartHeight={chartHeight} />
    {process.env.NODE_ENV !== "production" && <HelloSapeurs />}
  </div>
);
