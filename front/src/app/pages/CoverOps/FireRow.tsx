import {
  actions,
  fireLineInputSelector,
  Hours,
  lastOngoingOpChangedEventByAreaByCauseSelector,
  lastTimestampDateSelector,
  epBarInputSelector,
  lastAvailabilityChangedEventByAreaByRoleSelector,
} from "core-logic";
import type { CoverOpsVehicleSubCategory } from "core-logic";
import type { AvailabilityKind, OperationCause } from "interfaces";
import { LineStyleByVariable, Map } from "ui";
import { useSelector } from "app/redux-hooks";
import { coverStyles } from "app/theme";
import { opsColors } from "app/theme";
import { AxisToStyle } from "ui/charts/line/lineChartHooks";
import { numberOfHoursToShowSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { useDispatch } from "react-redux";
import { ChartsRow } from "./ChartsRow";
import { LineAndBar, operationToLabel, vehicleToLabel } from "./LineAndBar";

type Fire = Extract<OperationCause, "fire">;
type Pump = Extract<CoverOpsVehicleSubCategory, "ep">;

const fireLineStyleByVariable: LineStyleByVariable<Fire | Pump> = {
  fire: {
    color: opsColors.fire.main,
    axis: "left",
  },
  ep: {
    color: coverStyles.ep.mainColor,
    axis: "right",
  },
};

const axisToStyle: AxisToStyle = {
  left: {
    color: opsColors.fire.main,
    yMax: 30,
  },
  right: { color: coverStyles.ep.mainColor, yMax: 120 },
};

const availablePseMapMax = 2;
const fireMapMax = 2;
const nTicks = 6;

type FireRowProps = {
  chartHeight: string;
};

export const FireRow = ({ chartHeight }: FireRowProps) => {
  const epBarInputs = useSelector(epBarInputSelector);
  const { ep: epCoverCountByArea = [] } = useSelector(
    lastAvailabilityChangedEventByAreaByRoleSelector,
  );
  const { fire: fireOpsCountByArea = [] } = useSelector(
    lastOngoingOpChangedEventByAreaByCauseSelector,
  );
  const fireLineInputs = useSelector(fireLineInputSelector);
  const hoursSliceToShow = useSelector(numberOfHoursToShowSelector);
  const lastTimestamp = useSelector(lastTimestampDateSelector);
  const dispatch = useDispatch();

  return (
    <ChartsRow
      elements={[
        <Map<"ongoingOps">
          data={fireOpsCountByArea}
          max={fireMapMax}
          legendPosition="bottom"
          fill="ongoingOps"
          colors={opsColors.fire.scale}
          title={`Nbr d'inter. ${operationToLabel["fire"]} \n en cours par cstc`}
        />,
        <LineAndBar<Pump | Fire>
          chartHeight={chartHeight}
          axisToStyle={axisToStyle}
          lineStyleByVariable={fireLineStyleByVariable}
          barChartData={epBarInputs}
          lineChartData={fireLineInputs}
          hoursToShow={hoursSliceToShow}
          nTicks={nTicks}
          vehicleSubCategory="ep"
          operationCause="fire"
          tMax={lastTimestamp}
          onSliderChange={(n: Hours) => {
            dispatch(actions.numberOfHoursToShowChanged(n));
          }}
          barLegendPosition="flex-end"
        />,
        <Map<AvailabilityKind>
          data={epCoverCountByArea}
          max={availablePseMapMax}
          fill="available"
          legendPosition="bottom"
          colors={coverStyles.ep.scale}
          title={`Nbr d'${vehicleToLabel["ep"]} disponibles par cstc`}
        />,
      ]}
    />
  );
};
