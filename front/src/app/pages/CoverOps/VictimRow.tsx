import {
  actions,
  CoverOpsVehicleSubCategory,
  Hours,
  lastAvailabilityChangedEventByAreaByRoleSelector,
  lastOngoingOpChangedEventByAreaByCauseSelector,
  lastTimestampDateSelector,
  victimLineInputSelector,
  sapBarInputSelector,
} from "core-logic";
import { AvailabilityKind, OperationCause } from "interfaces";
import { LineStyleByVariable, Map } from "ui";
import { useSelector } from "app/redux-hooks";
import { opsColors } from "app/theme";
import { coverStyles } from "app/theme";
import { AxisToStyle } from "ui/charts/line/lineChartHooks";
import { numberOfHoursToShowSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { useDispatch } from "react-redux";
import { LineAndBar, operationToLabel, vehicleToLabel } from "./LineAndBar";
import { ChartsRow } from "./ChartsRow";

type Victim = Extract<OperationCause, "victim">;
type Sap = Extract<CoverOpsVehicleSubCategory, "sap">;

const victimLineStyleByVariable: LineStyleByVariable<Victim | Sap> = {
  victim: {
    color: opsColors.victim.main,
    axis: "left",
  },
  sap: {
    color: coverStyles.sap.mainColor,
    axis: "right",
  },
};

const victimMapMax = 3;
const availableVsavMapMax = 3;
const nTicks = 6;

type VictimRowProps = {
  chartHeight: string;
};

const axisToStyle: AxisToStyle = {
  left: {
    color: opsColors.victim.main,
    yMax: 180,
  },
  right: { color: coverStyles.sap.mainColor, yMax: 180 },
};

export const VictimRow = ({ chartHeight }: VictimRowProps) => {
  const { victim: victimOpsCountByArea = [] } = useSelector(
    lastOngoingOpChangedEventByAreaByCauseSelector,
  );
  const { sap: sapRescueCoverCountByArea = [] } = useSelector(
    lastAvailabilityChangedEventByAreaByRoleSelector,
  );
  const vsavBarInput = useSelector(sapBarInputSelector);
  const victimLineInputs = useSelector(victimLineInputSelector);
  const hoursSliceToShow = useSelector(numberOfHoursToShowSelector);
  const lastTimestamp = useSelector(lastTimestampDateSelector);
  const dispatch = useDispatch();

  return (
    <ChartsRow
      elements={[
        <Map<"ongoingOps">
          data={victimOpsCountByArea}
          max={victimMapMax}
          fill="ongoingOps"
          legendPosition="top"
          colors={opsColors.victim.scale}
          title={`Nbr d'inter. ${operationToLabel["victim"]} en cours par cstc`}
        />,
        <LineAndBar<Victim | Sap>
          chartHeight={chartHeight}
          axisToStyle={axisToStyle}
          lineStyleByVariable={victimLineStyleByVariable}
          barChartData={vsavBarInput}
          lineChartData={victimLineInputs}
          hoursToShow={hoursSliceToShow}
          nTicks={nTicks}
          vehicleSubCategory="sap"
          operationCause="victim"
          tMax={lastTimestamp}
          onSliderChange={(n: Hours) => {
            dispatch(actions.numberOfHoursToShowChanged(n));
          }}
          barLegendPosition="start"
        />,
        <Map<AvailabilityKind>
          data={sapRescueCoverCountByArea}
          max={availableVsavMapMax}
          fill="available"
          legendPosition="top"
          colors={coverStyles.sap.scale}
          title={`Nbr de ${vehicleToLabel["sap"]} disponibles par cstc`}
        />,
      ]}
    />
  );
};
