import { ChartsRow } from "./ChartsRow";
import { OmnibusBarChartHorizontal } from "ui";
// import { OmnibusBarChart } from "ui";
import { labelAndColorByBalanceOption } from "app/pages/CoverOps/barChartStyles";
import { useSelector } from "app/redux-hooks";
import { omnibusBalanceSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { omnibusBalanceKindOptions } from "interfaces";

type OmnibusRowProps = {
  chartHeight: string;
};

export const OmnibusRow = ({ chartHeight }: OmnibusRowProps) => {
  const omnibusBarInput = useSelector(omnibusBalanceSelector);

  // const nTicks = 5;
  // const ymax = 50;

  return (
    <ChartsRow
      elements={[
        <div></div>,
        // <OmnibusBarChart
        //   LegendHover={(props: {
        //     label: string;
        //     variable: OmnibusBalanceKind;
        //   }) => <div>TODO on hover</div>}
        //   chartHeight={chartHeight}
        //   yMax={ymax}
        //   nTicks={nTicks}
        //   data={omnibusBarInput}
        //   omnibusBalanceOptions={omnibusBalanceKindOptions}
        //   colorByBalance={labelAndColorByBalanceOption}
        //   barLabel={"Répartition omnibus"}
        // />,
        <OmnibusBarChartHorizontal
          chartHeight={chartHeight}
          data={omnibusBarInput}
          omnibusBalanceOptions={omnibusBalanceKindOptions}
          colorByBalance={labelAndColorByBalanceOption}
          barLabel={"Répartition omnibus"}
        />,
        <div></div>,
      ]}
    />
  );
};
