type ChartsRowProps = {
  elements: [React.ReactNode, React.ReactNode, React.ReactNode];
};

export const ChartsRow = ({ elements }: ChartsRowProps) => {
  return (
    <div className="flex items-center justify-between">
      <div>{elements[0]}</div>
      <div style={{ flex: 2 }}>{elements[1]}</div>
      <div>{elements[2]}</div>
    </div>
  );
};
