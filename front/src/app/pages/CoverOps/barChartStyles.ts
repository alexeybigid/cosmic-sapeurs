import { coverStyles, omnibusBothAvailable, stripesColor } from "app/theme";
import { CoverOpsVehicleSubCategory } from "core-logic";
import { AvailabilityKind, OmnibusBalanceKind } from "interfaces";
import * as R from "ramda";
import { AvailabilityKindToDisplay, ColorsBy } from "ui";

type LabelOrStrongLabel =
  | { label: string; strongLabel?: string }
  | { label?: string; strongLabel: string };

const countKindToLabel: Record<AvailabilityKind, LabelOrStrongLabel> = {
  available: { strongLabel: "Disponible" },
  in_service: { label: "Sur intervention" },
  recoverable: { label: "Récupérable" },
  unavailable: { label: "Indisponible" },
};

export const createLabelAndColorByAvailability = (
  role: CoverOpsVehicleSubCategory,
): Record<
  AvailabilityKindToDisplay,
  LabelOrStrongLabel & { color: string; stripes?: string }
> =>
  R.mapObjIndexed(
    (coverColor, availabilityKind) => ({
      color: coverColor.color,
      stripes: coverColor.stripes,
      ...countKindToLabel[availabilityKind],
    }),
    coverStyles[role].byKind,
  );

export const labelAndColorByBalanceOption: ColorsBy<OmnibusBalanceKind> = {
  both_on_inter_san: {
    color: coverStyles.sap.mainColor,
    stripes: stripesColor,
    label: "EP3 et VSAV M sur inter",
  },
  vsav_on_inter_san: {
    color: coverStyles.sap.mainColor,
    label: "VSAV M sur inter, ",
    strongLabel: "EP3 dispo",
  },
  pse_on_inter_san: {
    color: coverStyles.sap.mainColor,
    label: "EP3 sur inter, ",
    strongLabel: "VSAV M dispo",
  },
  both_available: {
    color: omnibusBothAvailable,
    strongLabel: "Ensemble constitué dispo",
  },
  pse_on_inter_fire: {
    color: coverStyles.ep.mainColor,
    label: "Ensemble modulaire sur inter pompe",
    stripes: stripesColor,
  },
  only_pse_available: {
    color: coverStyles.ep.mainColor,
    label: "Ensemble modulaire soclé EP6",
  },
};
