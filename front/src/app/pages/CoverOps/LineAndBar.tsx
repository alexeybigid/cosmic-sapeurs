import { availabilityKindOptions } from "core-logic";
import type { Hours } from "core-logic";
import type { CoverOpsVehicleSubCategory } from "core-logic";
import type { AvailabilityKind, OperationCause } from "interfaces";
import {
  AvailabilityBarChart,
  BarInput,
  ChartTitle,
  LineInput,
  LineStyleByVariable,
  StreamingLineChart,
} from "ui";
import { coverStyles } from "app/theme";
import { opsColors } from "app/theme";
import type { AxisToStyle } from "ui/charts/line/lineChartHooks";
import { AvailabilityBarLegendHover } from "./AvailabilityBarLegendHover";
import { createLabelAndColorByAvailability } from "./barChartStyles";
import { CSSProperties } from "react";

type LineAndBarProps<T extends string> = {
  chartHeight: string;
  hoursToShow: Hours;
  nTicks: number;
  lineChartData: LineInput<T>[];
  barChartData: BarInput<AvailabilityKind>;
  axisToStyle: AxisToStyle;
  lineStyleByVariable: LineStyleByVariable<T>;
  vehicleSubCategory: CoverOpsVehicleSubCategory & T;
  operationCause: OperationCause & T;
  tMax?: Date;
  onSliderChange: (h: Hours) => void;
  barLegendPosition: CSSProperties["alignItems"];
};

export const vehicleToLabel: Record<CoverOpsVehicleSubCategory, string> = {
  ep: "EP",
  sap: "SAP",
};

export const operationToLabel: Record<OperationCause, string> = {
  victim: "SAV (3••)",
  fire: "Feux (1••)",
  other: "Autre",
};

export const LineAndBar = <T extends string>({
  hoursToShow,
  chartHeight,
  nTicks,
  lineChartData,
  barChartData,
  axisToStyle,
  lineStyleByVariable,
  vehicleSubCategory,
  operationCause,
  tMax,
  onSliderChange,
  barLegendPosition,
}: LineAndBarProps<T>) => (
  <div className="flex">
    <div className="relative">
      <ChartTitle color={opsColors[operationCause].main}>
        {`Nbr d'inter. ${operationToLabel[operationCause]}`}
      </ChartTitle>
      <StreamingLineChart
        onSliderChange={onSliderChange}
        chartHeight={chartHeight}
        data={lineChartData}
        hoursToShow={hoursToShow}
        tUnit="hour"
        nTicks={nTicks}
        variableToStyle={lineStyleByVariable}
        axisToStyle={axisToStyle}
        tMax={tMax}
      />
      <ChartTitle color={coverStyles[vehicleSubCategory].mainColor} isOnRight>
        {`Capacité ${vehicleToLabel[vehicleSubCategory]}`}
      </ChartTitle>
    </div>
    <AvailabilityBarChart
      LegendHover={(props: { label: string; variable: AvailabilityKind }) => (
        <AvailabilityBarLegendHover
          label={props.label}
          availabilityKind={props.variable}
          subCategory={vehicleSubCategory}
        />
      )}
      chartHeight={chartHeight}
      yMax={axisToStyle.right.yMax}
      nTicks={nTicks}
      data={barChartData}
      availabilityKindOptions={availabilityKindOptions}
      colorByAvailability={createLabelAndColorByAvailability(
        vehicleSubCategory,
      )}
      barLabel={vehicleToLabel[vehicleSubCategory]}
      legendPosition={barLegendPosition}
    />
  </div>
);
