import {
  CoverOpsVehicleSubCategory,
  subCategoryToCountByStatusByAvailabilityKind,
} from "core-logic";
import { AvailabilityKind } from "interfaces";
import { useSelector } from "react-redux";

type AvailabilityBarLegendHoverProps = {
  label: string;
  availabilityKind: AvailabilityKind;
  subCategory: CoverOpsVehicleSubCategory;
};

export const AvailabilityBarLegendHover = ({
  label,
  availabilityKind,
  subCategory,
}: AvailabilityBarLegendHoverProps) => {
  const countsByKind = useSelector(
    subCategoryToCountByStatusByAvailabilityKind[subCategory][availabilityKind],
  );

  const countsByKindEntries = Object.entries(countsByKind);

  return (
    <div>
      <p className="text-lg underline">{label}</p>
      {!countsByKindEntries.length ? (
        <span className="text-base">Aucun</span>
      ) : (
        <ul>
          {countsByKindEntries.map(([status, count]) => {
            return (
              <li key={status} className="text-base">
                {count} {status}
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};
