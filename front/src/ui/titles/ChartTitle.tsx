type ChartTitleProps = {
  children: string;
  color: string;
  isOnRight?: boolean;
};

export const ChartTitle = ({
  children,
  isOnRight,
  color = "grey",
}: ChartTitleProps) => (
  <p
    className="absolute text-sm font-normal leading-normal -top-6"
    style={{ color, right: isOnRight ? "-40px" : "" }}
  >
    {children}
  </p>
);
