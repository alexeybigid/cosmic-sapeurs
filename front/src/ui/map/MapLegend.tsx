import * as R from "ramda";

export type LegendPosition = "top" | "bottom";

type MapLegendProps = {
  title: string;
  colors: {
    color: string;
    label: string;
  }[];
  position: LegendPosition;
};

export const MapLegend = ({ colors, title, position }: MapLegendProps) => {
  const renderTitle = () => (
    <div className="w-36" style={{ color: R.last(colors)!.color }}>
      {title}
    </div>
  );
  const positionStyle =
    position === "top" ? "left-0 top-0" : "left-0 -bottom-0";
  return (
    <div
      className={`flex flex-col text-sm font-normal leading-normal text-gray-400 absolute ${positionStyle}`}
    >
      {position === "top" && renderTitle()}
      <div>
        <ul className="flex m-0 p-0">
          {colors.map(({ color, label }) => (
            <li
              key={color}
              className={"flex flex-col items-center content-end"}
            >
              <div
                className={"border border-gray-400 h-6 w-6"}
                style={{ backgroundColor: color }}
              />
              <div className={"text-sm"}>{label}</div>
            </li>
          ))}
        </ul>
      </div>
      {position === "bottom" && renderTitle()}
    </div>
  );
};
