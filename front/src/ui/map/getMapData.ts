import { ENV } from "environmentVariables";
import { Area, bsppAreaOptions } from "interfaces";

let csAreasGeoJSON;

const getJsonAreas = (features: { code: string }[]) => {
  return features.map((feature) => feature.code);
};

if (ENV.mapDataOrigin === "BSPP") {
  const areasGeoJSON = require("./cstc.json");
  const csFeaturesGeoJSON = areasGeoJSON.features.filter(
    (geo: any) => geo.emprise === "CSTC",
  );
  const json_areas = getJsonAreas(csFeaturesGeoJSON);

  // TODO : same as backend

  const isOnlyBsppAreas = json_areas.every((area) =>
    bsppAreaOptions.includes(area as Area),
  );
  if (!isOnlyBsppAreas) {
    throw new Error("GeoJson got CSTC code outside known BSPP areas");
  }

  csAreasGeoJSON = {
    type: areasGeoJSON.type,
    features: csFeaturesGeoJSON,
  };
} else {
  csAreasGeoJSON = require("./arrondissementsWithCodes.json");
}

export const areaGeoJSON = csAreasGeoJSON;
