import * as React from "react";
import { Area } from "interfaces";
import { Geographies, Geography, ComposableMap } from "react-simple-maps";
import { makeColorScale } from "utils";
import * as R from "ramda";
import "./map.css";

import { LegendPosition, MapLegend } from "./MapLegend";
import { areaGeoJSON } from "./getMapData";

export type MapInputSample<K extends string> = {
  counts: Partial<Record<K, number>>;
  area: string;
};

type Coordinates = [number, number];

type AreaGeography = {
  cie: string;
  code: Area;
  emprise: string;
  geometry: { type: string; coordinates: Coordinates[] };
  gis: "3" | "2" | "1";
  nom: string;
  point: { type: string; coordinates: Coordinates };
  rsmKey: string;
  svgPath: string;
  type: string;
};

export type MapProps<K extends string> = {
  data: MapInputSample<K>[];
  colors: string[];
  max: number;
  fill: K;
  title: string;
  legendPosition: LegendPosition;
};

export const Map = <K extends string>({
  data,
  colors,
  max,
  fill,
  title,
  legendPosition,
}: MapProps<K>) => {
  const { getColor, scale } = makeColorScale({ colors, max });

  return (
    <div className={"flex flex-col items-center width-420 relative"}>
      <MapLegend colors={scale} title={title} position={legendPosition} />
      <div className={"flex-4 w-full map-wrapper"}>
        <ComposableMap
          projectionConfig={{
            center: [2.4, 48.85],
            scale: 100000,
          }}
        >
          <Geographies geography={areaGeoJSON}>
            {({ geographies }: { geographies: AreaGeography[] }) =>
              geographies.map((geo, index) => {
                const inputSample = data.find(
                  (inputSample) => inputSample.area === geo.code,
                );

                const count = inputSample?.counts[fill];
                const color = R.isNil(count) ? "lightgray" : getColor(count!);

                return (
                  <Geography
                    name={geo.code}
                    key={geo.rsmKey}
                    geography={geo}
                    fill={color}
                    stroke={"gray"}
                    strokeWidth={".05em"}
                  />
                );
              })
            }
          </Geographies>
        </ComposableMap>
      </div>
    </div>
  );
};
