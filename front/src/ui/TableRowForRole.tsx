import { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {
  latestEnrichedVehicleEventByRawVehicleIdForRoleSelectors,
  vehiclesLabels,
} from "core-logic";
import { VehicleEvent, VehicleRole } from "interfaces";
import { UiDrawer } from "ui";

import * as R from "ramda";
import type { AvailabilityCellValue } from "core-logic";
import { useSelector } from "app/redux-hooks";
import { WithTimestamp } from "utils";
import { RedWhenLow } from "./RedWhenLow";

type Extend<T extends string, K extends string> = K extends T ? K : never;
type VehicleInfos =
  | Extend<keyof VehicleEvent, "home_area" | "vehicle_name" | "raw_status">
  | "since";

const drawerTableHeaders: Record<VehicleInfos, string> & WithTimestamp = {
  vehicle_name: "Immatriculation",
  home_area: "Affectation",
  raw_status: "Statut",
  timestamp: "Heure",
  since: "Dernière maj",
};

type TableRowForRoleProps = {
  valuesByColumn: Partial<Record<VehicleRole, AvailabilityCellValue>>;
};

export const TableRowForRole = ({ valuesByColumn }: TableRowForRoleProps) => {
  const valuesByColumnNotPartial = valuesByColumn as Record<
    VehicleRole,
    AvailabilityCellValue
  >;

  const roles = R.keys(valuesByColumnNotPartial);
  const fullWidthIfUniq = (roles: unknown[]) =>
    roles.length === 1 ? " w-full" : "";

  return (
    <TableContainer component={Paper}>
      <div className="flex">
        {roles.map((role) => {
          const cellValue = valuesByColumnNotPartial[role];
          const { value, theoretical, percentage } = cellValue;
          const fullWidthIfNeeded = fullWidthIfUniq(roles);
          return (
            <DrawerForRole role={role} className={fullWidthIfNeeded}>
              <div
                key={role}
                className={"hover:opacity-60" + fullWidthIfNeeded}
              >
                <Table
                  className="w-auto"
                  size="small"
                  aria-label="a dense table"
                >
                  <TableHead className="bg-gray-300 w-full">
                    <TableHeadCell cellValue={cellValue} key={role} role={role}>
                      {vehiclesLabels.roles[role]}
                    </TableHeadCell>
                  </TableHead>
                  <TableBody className="w-full">
                    <TableRow>
                      <TableCell align="center">
                        <RedWhenLow value={percentage}>
                          {value}/{theoretical}
                        </RedWhenLow>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center">
                        <RedWhenLow value={percentage}>
                          {R.isNil(percentage) ? "? %" : `${percentage}%`}
                        </RedWhenLow>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </div>
            </DrawerForRole>
          );
        })}
      </div>
    </TableContainer>
  );
};

type ArrowProps = {
  visible?: boolean;
};

const Increased = ({ visible = false }: ArrowProps) => (
  <div
    className="w-0 h-0 bg-fixed border-l-8 border-r-8 border-transparent"
    style={{
      visibility: visible ? "visible" : "hidden",
      borderBottom: "8px solid green",
    }}
  />
);

const Decreased = ({ visible = false }: ArrowProps) => (
  <div
    className="w-0 h-0 bg-fixed border-l-8 border-r-8 border-transparent"
    style={{
      visibility: visible ? "visible" : "hidden",
      borderTop: "8px solid red",
    }}
  />
);

type TableHeadCellProps = {
  children: React.ReactNode;
  cellValue: AvailabilityCellValue;
  role: VehicleRole;
  className?: string;
};

const TableHeadCell = ({
  children,
  cellValue,
  role,
  className,
}: TableHeadCellProps) => {
  const [moving, setMoving] = useState<"up" | "down" | null>(null);
  const [lastCellValue, setLastCellValue] = useState<AvailabilityCellValue>(
    cellValue,
  );

  useEffect(() => {
    if (!cellValue.value || cellValue.value === lastCellValue.value) {
    } else if (!lastCellValue.value) {
      setMoving("up");
    } else {
      setMoving(cellValue.value! > lastCellValue.value! ? "up" : "down");
    }
    setLastCellValue(cellValue);

    const timer = setTimeout(() => setMoving(null), 8000);

    return () => clearTimeout(timer);
  }, [cellValue.percentage, cellValue.theoretical, cellValue.value]);

  return (
    <TableCell
      align="center"
      style={{ padding: "2px 24px" }}
      className={className}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Increased visible={moving === "up"} />
        <RedWhenLow value={cellValue.percentage}>{children}</RedWhenLow>
        <Decreased visible={moving === "down"} />
      </div>
    </TableCell>
  );
};

type DrawerForRoleProps = {
  role: VehicleRole;
  children: React.ReactNode;
  className?: string;
};

const DrawerForRole = ({ role, children, className }: DrawerForRoleProps) => {
  // TODO : redux access should not be in a UI component...
  const vehiclesByIdForThisRole = useSelector(
    latestEnrichedVehicleEventByRawVehicleIdForRoleSelectors[role],
  );

  return (
    <UiDrawer
      data={R.values(vehiclesByIdForThisRole)}
      headers={drawerTableHeaders}
      title={vehiclesLabels.roles[role]}
      className={className}
    >
      {children}
    </UiDrawer>
  );
};
