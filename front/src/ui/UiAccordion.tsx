import Accordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
// import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { makeStyles, withStyles } from "@material-ui/core";
import { isLow, RedWhenLow } from "./RedWhenLow";

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(0),
  },
}))(MuiAccordionDetails);

const AccordionSummary = withStyles({
  content: {
    "&$expanded": {
      margin: 0,
    },
  },
  root: {
    "&$expanded": {
      minHeight: "inherit",
    },
  },
  expanded: {},
})(MuiAccordionSummary);

export type Total = {
  value: number;
  theoretical: number;
};

type UiAccordionProps = {
  children: React.ReactElement;
  title: string;
  total: Total;
  color: string;
};

const useStyle = makeStyles(() => ({
  root: ({ color }: { color: string }) => ({
    backgroundColor: `${color}66`,
  }),
}));

export const UiAccordion = ({
  children,
  title,
  total: { value, theoretical },
  color,
}: UiAccordionProps) => {
  const classes = useStyle({ color });

  const percentage = Math.round((theoretical ? value / theoretical : 0) * 100);
  const redBorderIfLow = `border-4 border-solid rounded ${
    isLow(percentage) ? "border-red-500" : "border-transparent"
  }`;

  return (
    <div className={redBorderIfLow}>
      <Accordion defaultExpanded>
        <AccordionSummary
          expandIcon={<span className="text-sm">v</span>}
          className={classes.root}
        >
          <div className="flex justify-between w-full">
            <div>{title}</div>
            <RedWhenLow value={percentage}>
              <div className="pl-2">{percentage}%</div>
            </RedWhenLow>
          </div>
        </AccordionSummary>
        <AccordionDetails>{children}</AccordionDetails>
      </Accordion>
    </div>
  );
};
