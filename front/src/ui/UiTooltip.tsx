import Tooltip from "@material-ui/core/Tooltip";
import { useState } from "react";

type UiTooltipProps = {
  content: React.ReactElement | string;
  children: React.ReactElement;
  disable?: boolean;
};

export const UiTooltip = ({ children, content, disable }: UiTooltipProps) => {
  return (
    <RenderWrapperOnHover
      WrapperComponent={Tooltip}
      wrapperProps={{
        title: content,
        placement: "top" as const,
        disableHoverListener: disable,
        children: children,
      }}
    />
  );
};

type WithChildren = { children: React.ReactElement };

type RenderWrapperOnHoverProps<P extends WithChildren> = {
  WrapperComponent: React.ComponentType<P>;
  wrapperProps: P;
};

const RenderWrapperOnHover = <P extends WithChildren>({
  WrapperComponent,
  wrapperProps,
}: RenderWrapperOnHoverProps<P>) => {
  const { children } = wrapperProps;
  const [isHovered, setIsHovered] = useState(false);

  const renderWrapped = () => (
    <div
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {children}
    </div>
  );

  return isHovered ? (
    <WrapperComponent {...wrapperProps} children={renderWrapped()} />
  ) : (
    renderWrapped()
  );
};
