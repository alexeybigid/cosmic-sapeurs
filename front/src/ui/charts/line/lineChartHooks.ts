import Chart, { TimeUnit } from "chart.js";
import { useChart } from "ui/charts/chartHooks";
import { useEffect } from "react";
import type {
  AxisPosition,
  LineInput,
  LineStyleByVariable,
} from "./lineChartUtils";
import { inputDataToChartJSData } from "./lineChartUtils";
import type { Hours } from "core-logic";

export type AxisToStyle = Record<AxisPosition, { color: string; yMax: number }>;

export type LineChartProps<T extends string> = {
  data: LineInput<T>[];
  tUnit?: TimeUnit;
  tMin?: string;
  tMax?: string;
  yMin?: number;
  nTicks: number;
  variableToStyle: LineStyleByVariable<T>;
  axisToStyle: AxisToStyle;
  hoursToShow: Hours;
};

export const useLineChartJS = <T extends string>({
  data,
  tUnit,
  tMin,
  tMax,
  yMin = 0,
  nTicks,
  variableToStyle,
  axisToStyle,
  hoursToShow,
}: LineChartProps<T>) => {
  const { chartRef, canvasRef } = useChart();

  const timeUnit =
    hoursToShow <= 1.5
      ? { unit: "minute" as const, unitStepSize: 15 }
      : { unit: tUnit };

  const chartJSData = inputDataToChartJSData(data, variableToStyle);

  useEffect(() => {
    if (canvasRef.current) {
      const leftYMax = axisToStyle.left.yMax;
      const rightYMax = axisToStyle.right.yMax;
      chartRef.current = new Chart(canvasRef.current, {
        type: "line",
        data: chartJSData,
        options: {
          maintainAspectRatio: false,
          legend: { display: false },
          animation: { duration: 0 },
          spanGaps: true, // draw lines between points with no or null data
          elements: {
            point: {
              radius: 0,
            },
          },
          scales: {
            xAxes: [
              {
                type: "time",
                time: {
                  ...timeUnit,
                  displayFormats: {
                    minute: "HH:mm",
                    hour: "HH:mm",
                  },
                },
                ticks: {
                  min: tMin,
                  max: tMax,
                  autoSkip: false,
                  maxRotation: 0,
                  minRotation: 0,
                  major: { enabled: true },
                },
                gridLines: {
                  zeroLineColor: "rgba(0, 0, 0, 0.1)",
                  zeroLineWidth: 1,
                },
              },
            ],
            yAxes: [
              {
                id: "left",
                ticks: {
                  max: leftYMax,
                  min: yMin,
                  fontColor: axisToStyle.left.color,
                  stepSize: leftYMax / nTicks,
                },
                position: "left",
              },
              {
                id: "right",
                ticks: {
                  max: rightYMax,
                  min: yMin,
                  fontColor: axisToStyle.right.color,
                  stepSize: rightYMax / nTicks,
                },
                position: "right",
              },
            ],
          },
        },
      });
    }
  }, [canvasRef.current]);

  useEffect(() => {
    if (canvasRef.current && chartRef.current) {
      const timeX = chartRef.current.options.scales?.xAxes![0]?.time;

      if (timeX) {
        chartRef.current.options.scales!.xAxes![0]!.time = {
          ...timeX,
          ...timeUnit,
        };
      }
    }
  }, [timeUnit]);

  return { canvasRef, chartRef };
};
