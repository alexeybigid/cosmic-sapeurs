import { TimeUnit } from "chart.js";
import { useEffect } from "react";
import { useLineChartJS } from "./lineChartHooks";
import type { AxisToStyle, LineChartProps } from "./lineChartHooks";
import {
  LineInput,
  LineStyleByVariable,
  useDebounce,
  useStateWatching,
} from "./lineChartUtils";
import { inputDataToChartJSData } from "./lineChartUtils";
import { subSeconds } from "date-fns";
import MuiSlider from "@material-ui/core/Slider";
import type { Hours } from "core-logic";
import { withStyles } from "@material-ui/core";

const Slider = withStyles((theme) => ({
  root: {
    color: theme.palette.grey[500],
    height: 8,
  },
  track: {
    backgroundColor: `${theme.palette.grey[300]}!important`,
  },
}))(MuiSlider);

export type StreamingLineChartProps<T extends string> = {
  chartHeight: string;
  data: LineInput<T>[];
  tUnit?: TimeUnit;
  yMin?: number;
  nTicks: number;
  variableToStyle: LineStyleByVariable<T>;
  axisToStyle: AxisToStyle;
  tMax?: Date;
  hoursToShow: Hours;
  onSliderChange: (n: number) => void;
};

export const StreamingLineChart = <T extends string>(
  props: StreamingLineChartProps<T>,
) => {
  const {
    tMax,
    hoursToShow,
    nTicks,
    data,
    variableToStyle,
    chartHeight,
    onSliderChange,
  } = props;

  const secondsToShow = hoursToShow * 3600;

  const initLineChartProps: LineChartProps<T> = {
    ...props,
    tMin: tMax && subSeconds(tMax, secondsToShow).toISOString(),
    tMax: tMax && tMax.toISOString(),
    nTicks,
    hoursToShow,
  };
  const { canvasRef, chartRef } = useLineChartJS(initLineChartProps);

  const updateXAxis = (chart: Chart, tMax: Date) => {
    chart.options.scales!.xAxes![0].time!.min = subSeconds(
      tMax,
      secondsToShow,
    ).toISOString();
    chart.options.scales!.xAxes![0].time!.max = tMax.toISOString();
  };

  useEffect(() => {
    if (!chartRef.current) return;
    chartRef.current.data = inputDataToChartJSData(data, variableToStyle);
    if (tMax) updateXAxis(chartRef.current, tMax);

    chartRef.current.update();
  }, [data, tMax]);

  useEffect(() => {
    if (!chartRef.current) return;
    if (tMax) updateXAxis(chartRef.current, tMax);
    chartRef.current.update();
  }, [hoursToShow]);

  const [sliderValue, setSliderValue] = useStateWatching(hoursToShow);

  useDebounce(sliderValue, onSliderChange, 150);

  const maxNumberOfHoursToShow = 4;
  const hoursStepSize = 0.5;

  return (
    <div
      className="w-full"
      style={{
        height: chartHeight,
        width: "450px",
      }}
    >
      <canvas ref={canvasRef} />
      <div className="px-8">
        {/* This slider is use with negative value, in order to have max on left side */}
        <Slider
          value={-sliderValue}
          valueLabelDisplay="auto"
          valueLabelFormat={(value: number) => {
            const opposedValue = -value;
            const hour = Math.trunc(opposedValue);
            const min = Math.round((opposedValue - hour) * 60);
            return `${hour}h${min ? min : ""}`;
          }}
          step={hoursStepSize}
          min={-maxNumberOfHoursToShow}
          max={-hoursStepSize}
          track="inverted"
          onChange={(e, newValue) => {
            if (typeof newValue === "object") return;
            setSliderValue(-newValue);
          }}
        />
      </div>
    </div>
  );
};
