import type { BarInput, ColorsBy } from "./barChartUtils";

import { Chart } from "chart.js";
import { useEffect } from "react";
import { useChart } from "ui/charts/chartHooks";
import { inputDataToHorizontalBarChartJSData } from "./barChartUtils";
import { OmnibusBalanceKind } from "interfaces";
import { LegendText } from "ui/charts/bar/LegendText";
import { UiTooltip } from "ui/UiTooltip";
import * as R from "ramda";
import { coverStyles } from "app/theme";
import { useUpdateChartOnDataChange } from "../chartHooks";
import { OmnibusBarLegendHover } from "app/pages/CoverOps/OmnibusBarLegendHover";

type OmnibusBarChartHorizontalProps = {
  chartHeight: string;
  data: BarInput<OmnibusBalanceKind>;
  omnibusBalanceOptions: OmnibusBalanceKind[];
  colorByBalance: ColorsBy<OmnibusBalanceKind>;
  barLabel: string;
};

const useHorizontalBarChart = ({
  data,
  omnibusBalanceOptions,
  colorByBalance,
  barLabel,
}: OmnibusBarChartHorizontalProps) => {
  const { chartRef, canvasRef } = useChart();
  const chartJSData = inputDataToHorizontalBarChartJSData({
    data,
    variableOptions: omnibusBalanceOptions,
    colorByVariable: colorByBalance,
    barLabel,
  });

  useEffect(() => {
    if (canvasRef.current) {
      chartRef.current = new Chart(canvasRef.current, {
        type: "horizontalBar",
        data: chartJSData,
        options: {
          legend: { display: false },
          hover: { mode: undefined },
          tooltips: { enabled: false },
          animation: { duration: 0 },
          scales: { xAxes: [{ ticks: { min: 0 } }] },
        },
      });
    }
  }, [canvasRef.current]);

  useUpdateChartOnDataChange(chartRef, chartJSData);

  return canvasRef;
};

export const OmnibusBarChartHorizontal = (
  props: OmnibusBarChartHorizontalProps,
) => {
  const canvasRef = useHorizontalBarChart(props);

  const rawData = props.data;

  const totalCount = R.values(rawData).reduce((acc, n) => acc! + (n ?? 0), 0);

  const sanAvailable =
    (rawData.both_available ?? 0) * 2 +
    (rawData.pse_on_inter_san ?? 0) +
    (rawData.vsav_on_inter_san ?? 0);

  const pumpAvailable =
    (rawData.both_available ?? 0) + (rawData.only_pse_available ?? 0);

  return (
    <div className="absolute z-10" style={{ right: "440px", top: "300px" }}>
      <div className="pr-4 text-md font-bold text-gray-600">
        {totalCount} Ensembles modulaire en service
        <p className="text-sm font-normal">
          Moyens disponible modulable en :{" "}
          <SanColor>{`${sanAvailable} cellules SAN`}</SanColor> {" ou "}
          <EpColor>{`${pumpAvailable} EP6`}</EpColor>
        </p>
      </div>
      <div className="flex">
        <div>
          <div className="pt-3 pb-8 text-right flex flex-col justify-between h-full">
            {R.keys(props.colorByBalance).map((omnibusBalanceKind) => {
              const { label, strongLabel } = props.colorByBalance[
                omnibusBalanceKind
              ];
              const count = rawData[omnibusBalanceKind];

              const fullLabel = `${label ?? ""} ${strongLabel ?? ""} `;

              return (
                <UiTooltip
                  key={fullLabel}
                  content={
                    <OmnibusBarLegendHover
                      label={fullLabel}
                      omnibusBalanceKind={omnibusBalanceKind}
                    />
                  }
                >
                  <LegendText
                    label={label}
                    strongLabel={strongLabel}
                    value={count}
                  />
                </UiTooltip>
              );
            })}
          </div>
        </div>
        <div>
          <canvas
            ref={canvasRef}
            style={{ height: props.chartHeight, width: "220px" }}
          />
        </div>
      </div>
    </div>
  );
};

type ColorTextProp = { children: string };

const SanColor = ({ children }: ColorTextProp) => (
  <span className="font-bold" style={{ color: coverStyles.sap.mainColor }}>
    {children}
  </span>
);

const EpColor = ({ children }: ColorTextProp) => (
  <span className="font-bold" style={{ color: coverStyles.ep.mainColor }}>
    {children}
  </span>
);
