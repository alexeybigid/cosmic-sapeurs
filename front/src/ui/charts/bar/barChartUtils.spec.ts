import { inputDataToBarChartJSData } from "./barChartUtils";
import type { BarChartJSData, ColorsBy } from "./barChartUtils";
import type { AvailabilityKind } from "interfaces";

const availabilityKindOptions: AvailabilityKind[] = [
  "available",
  "recoverable",
  "in_service",
];

const commonColorsAndLabels = {
  unavailable: { color: "lightgray", label: "indisponible" },
  recoverable: { color: "gray", label: "récupérable" },
};
const colorByAvailability: ColorsBy<AvailabilityKind> = {
  ...commonColorsAndLabels,
  available: { color: "green", label: "disponible" },
  in_service: { color: "lightgreen", label: "en service" },
};

describe("Bar Chart Utils", () => {
  describe("Convert input data sample to Chart.js data", () => {
    it("Returns empty datasets, when no data and empty role and count options", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          data: {},
          variableOptions: [],
          colorByVariable: {},
          barLabel: "foo",
        }),
        {
          datasets: [],
        },
      );
    });
    it("Returns empty datasets, when no data but role and count options", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          data: {},
          variableOptions: availabilityKindOptions,
          colorByVariable: colorByAvailability,
          barLabel: "foo",
        }),
        {
          datasets: [
            { variable: "available", data: [null] },
            { variable: "recoverable", data: [null] },
            { variable: "in_service", data: [null] },
          ],
        },
      );
    });

    it("Returns correct format, when one data sample is given with colors", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          data: { available: 10, recoverable: 4, in_service: 90 },
          variableOptions: availabilityKindOptions,
          colorByVariable: colorByAvailability,
          barLabel: "foo",
        }),

        {
          datasets: [
            {
              variable: "available",
              data: [10],
              backgroundColor: "green",
            },
            {
              variable: "recoverable",
              data: [4],
              backgroundColor: "gray",
            },
            {
              variable: "in_service",
              data: [90],
              backgroundColor: "lightgreen",
            },
          ],
          labels: ["foo"],
        },
      );
    });

    it("Returns correct format, when multiple data sample are given with colors", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          data: {
            available: 10,
            recoverable: 4,
            in_service: 90,
          },
          variableOptions: availabilityKindOptions,
          colorByVariable: colorByAvailability,
          barLabel: "foo",
        }),
        {
          datasets: [
            {
              variable: "available",
              data: [10],
              backgroundColor: "green",
            },
            {
              variable: "recoverable",
              data: [4],
              backgroundColor: "gray",
            },
            {
              variable: "in_service",
              data: [90],
              backgroundColor: "lightgreen",
            },
          ],
          labels: ["foo"],
        },
      );
    });
  });
  const expectConvertedDataToBe = (
    actual: BarChartJSData<AvailabilityKind>,
    expected: BarChartJSData<AvailabilityKind>,
  ) => {
    expect(actual).toMatchObject(expected);
  };
});
