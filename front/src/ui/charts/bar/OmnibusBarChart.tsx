import type { BarInput, ColorsBy } from "./barChartUtils";

import Chart from "chart.js";
import { useEffect } from "react";
import { useChart } from "ui/charts/chartHooks";
import { inputDataToBarChartJSData } from "./barChartUtils";
import * as R from "ramda";
import { OmnibusBalanceKind } from "interfaces";
import { WithLegendHover, LegendElement } from "ui/charts/bar/LegendElement";
import { useUpdateChartOnDataChange } from "../chartHooks";

export type OmnibusBarChartProps = WithLegendHover<OmnibusBalanceKind> & {
  chartHeight: string;
  data: BarInput<OmnibusBalanceKind>;
  omnibusBalanceOptions: OmnibusBalanceKind[];
  colorByBalance: ColorsBy<OmnibusBalanceKind>;
  yMax: number;
  barLabel: string;
  nTicks: number;
};

const useBarChart = ({
  data,
  omnibusBalanceOptions,
  colorByBalance,
  yMax,
  barLabel,
  nTicks,
}: OmnibusBarChartProps) => {
  const { chartRef, canvasRef } = useChart();
  const chartJSData = inputDataToBarChartJSData({
    data,
    variableOptions: omnibusBalanceOptions,
    colorByVariable: colorByBalance,
    barLabel,
  });

  useEffect(() => {
    if (canvasRef?.current) {
      chartRef.current = new Chart(canvasRef.current, {
        type: "bar",
        data: chartJSData,
        options: {
          maintainAspectRatio: false,
          animation: { duration: 0 },
          legend: { display: false },
          tooltips: { enabled: false },
          hover: { mode: undefined },
          responsive: true,
          scales: {
            xAxes: [
              {
                stacked: true,
              },
            ],
            yAxes: [
              {
                ticks: {
                  //   display: false,
                  max: yMax,
                  //   fontColor:
                  //     colorByBalance[availabilityKindOptions[0]].color,
                  stepSize: yMax / nTicks,
                },
                stacked: true,
              },
            ],
          },
        },
      });
    }
  }, [canvasRef.current]);

  useUpdateChartOnDataChange(chartRef, chartJSData);

  return { canvasRef, chartJSData };
};

export const OmnibusBarChart = (props: OmnibusBarChartProps) => {
  const { canvasRef, chartJSData } = useBarChart(props);
  const getValueForBalanceCount = (omnibusBalanceOption: OmnibusBalanceKind) =>
    chartJSData.datasets.find(
      ({ variable }) => variable === omnibusBalanceOption,
    );

  // const totalCount = chartJSData.datasets.reduce((acc, { data }) => {
  //   const toAdd = data[0] ?? 0;
  //   return acc + toAdd;
  // }, 0);

  return (
    <div
      className="flex items-center relative -my-20"
      style={{ marginLeft: "1000px" }}
    >
      <div
        className="absolute top-0 left-8 text-xl"
        style={{
          // color: R.last(R.values(props.colorByAvailability))?.color ?? "gray",
          color: "gray",
        }}
      >
        {/* {getValueForBalanceCount("available")?.data[0]} / {totalCount} */}
      </div>
      <div
        className="w-full"
        style={{ height: props.chartHeight, width: "120px" }}
      >
        <canvas ref={canvasRef} />
      </div>
      <div className="pl-4">
        {R.keys(props.colorByBalance).map((balanceOption) => {
          const { color, stripes, label, strongLabel } = props.colorByBalance[
            balanceOption
          ];
          const chartJsDataSample = getValueForBalanceCount(balanceOption);
          const value: number | undefined | null = chartJsDataSample?.data[0];

          return (
            <LegendElement
              variable={balanceOption}
              color={color}
              stripes={stripes}
              label={`${label} ${strongLabel}`}
              value={value}
              key={label}
              LegendHover={props.LegendHover}
            />
          );
        })}
      </div>
    </div>
  );
};
