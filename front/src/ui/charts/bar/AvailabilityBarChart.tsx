import Chart from "chart.js";
import { CSSProperties, useEffect } from "react";
import { useChart } from "ui/charts/chartHooks";
import { ColorsBy, inputDataToBarChartJSData } from "./barChartUtils";
import * as R from "ramda";
import { AvailabilityKind } from "interfaces";
import { WithLegendHover, LegendElement } from "ui/charts/bar/LegendElement";
import type { BarInput } from "./barChartUtils";
import { useUpdateChartOnDataChange } from "../chartHooks";

type WithoutUnavailable<T extends string> = Exclude<T, "unavailable">;

export type AvailabilityKindToDisplay = WithoutUnavailable<AvailabilityKind>;

export type AvailabilityBarChartProps = WithLegendHover<AvailabilityKind> & {
  chartHeight: string;
  data: BarInput<AvailabilityKindToDisplay>;
  availabilityKindOptions: AvailabilityKindToDisplay[];
  colorByAvailability: ColorsBy<AvailabilityKindToDisplay>;
  yMax: number;
  barLabel: string;
  nTicks: number;
  legendPosition: CSSProperties["alignItems"];
};

const useBarChart = ({
  data,
  availabilityKindOptions,
  colorByAvailability,
  yMax,
  barLabel,
  nTicks,
}: AvailabilityBarChartProps) => {
  const { chartRef, canvasRef } = useChart();
  const chartJSData = inputDataToBarChartJSData({
    data,
    variableOptions: availabilityKindOptions,
    colorByVariable: colorByAvailability,
    barLabel,
  });

  useEffect(() => {
    if (canvasRef?.current) {
      chartRef.current = new Chart(canvasRef.current, {
        type: "bar",
        data: chartJSData,
        options: {
          maintainAspectRatio: false,
          animation: { duration: 0 },
          legend: { display: false },
          tooltips: { enabled: false },
          hover: { mode: undefined },
          responsive: true,
          scales: {
            xAxes: [
              {
                stacked: true,
              },
            ],
            yAxes: [
              {
                ticks: {
                  display: false,
                  max: yMax,
                  fontColor:
                    colorByAvailability[availabilityKindOptions[0]].color,
                  stepSize: yMax / nTicks,
                },
                stacked: true,
              },
            ],
          },
        },
      });
    }
  }, [canvasRef.current]);

  useUpdateChartOnDataChange(chartRef, chartJSData);

  return { canvasRef, chartJSData };
};

export const AvailabilityBarChart = (props: AvailabilityBarChartProps) => {
  const { canvasRef, chartJSData } = useBarChart(props);
  const getValueForAvailabilityCount = (availabilityKind: AvailabilityKind) =>
    chartJSData.datasets.find(({ variable }) => variable === availabilityKind);

  const totalCount = chartJSData.datasets.reduce((acc, { data }) => {
    const toAdd = data[0] ?? 0;
    return acc + toAdd;
  }, 0);

  return (
    <div className="flex relative" style={{ alignItems: props.legendPosition }}>
      <div
        className="absolute top-0 left-8 text-xl"
        style={{
          // color: R.last(R.values(props.colorByAvailability))?.color ?? "gray",
          color: "gray",
        }}
      >
        {getValueForAvailabilityCount("available")?.data[0]} / {totalCount}
      </div>
      <div
        className="w-full"
        style={{ height: props.chartHeight, width: "120px" }}
      >
        <canvas ref={canvasRef} />
      </div>
      <div className="pl-4">
        {R.keys(props.colorByAvailability).map((availabilityKind) => {
          const {
            color,
            stripes,
            ...labelAndStrongLabel
          } = props.colorByAvailability[availabilityKind];
          const chartJsDataSample = getValueForAvailabilityCount(
            availabilityKind,
          );
          const value: number | undefined | null = chartJsDataSample?.data[0];

          return (
            <LegendElement
              variable={availabilityKind}
              color={color}
              stripes={stripes}
              value={value}
              LegendHover={props.LegendHover}
              key={labelAndStrongLabel.label ?? labelAndStrongLabel.strongLabel}
              {...labelAndStrongLabel}
            />
          );
        })}
      </div>
    </div>
  );
};
