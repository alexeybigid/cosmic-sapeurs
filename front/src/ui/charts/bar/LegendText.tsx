type LegendTextProps = {
  label?: string;
  strongLabel?: string;
  value?: number | string;
};

export const LegendText = ({ label, strongLabel, value }: LegendTextProps) => (
  <div className="py-1 text-xs font-light" key={`${label} ${strongLabel}`}>
    {label}
    <span className="font-bold text-sm">{strongLabel}</span>:{" "}
    {strongLabel ? <span className="font-bold text-sm">{value}</span> : value}
  </div>
);
