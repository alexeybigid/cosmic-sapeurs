import {
  Drawer,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import { isBefore } from "date-fns";
import { AvailabilityKind } from "interfaces";
import * as R from "ramda";
import React from "react";
import { rejectIsNil, sortByTimestamp, WithTimestamp } from "utils";

export type WithSince = {
  since: string;
};

type WithAvailabilityKind = {
  availability_kind?: AvailabilityKind;
};

type VehicleDrawerInput<K extends string> = { [key in K]?: any } & // TODO : remove this any
  WithTimestamp &
  WithSince &
  WithAvailabilityKind;

type UiDrawerProps<K extends string> = {
  children: React.ReactNode;
  data: (VehicleDrawerInput<K> | undefined)[];
  headers: VehicleDrawerInput<K>;
  title: string;
  className?: string;
};

const isNotUnavailable = <T extends WithAvailabilityKind>(element: T) =>
  element.availability_kind !== "unavailable";

const useStyles = makeStyles((theme) => ({
  drawer: {
    top: "48px",
  },
  unavailable: {
    color: theme.palette.grey[400],
  },
}));

export const UiDrawer = <K extends string>({
  children,
  data,
  headers,
  title,
  className,
}: UiDrawerProps<K>) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const compactData = rejectIsNil(data);
  const headersValues = R.values(headers);
  const classes = useStyles();
  return (
    <>
      <div
        className={`cursor-pointer ${className ?? ""}`}
        onClick={() => setIsOpen(true)}
      >
        {children}
      </div>
      <Drawer
        anchor={"right"}
        open={isOpen}
        onClose={() => setIsOpen(false)}
        classes={{
          paperAnchorRight: classes.drawer,
        }}
      >
        <div className="text-center p-2 bg-gray-50">{title}</div>
        <TableContainer component={Paper}>
          <Table stickyHeader size="small">
            <TableHead>
              <TableRow>
                {headersValues.map((value, index) => (
                  <TableCell key={index} align="right">
                    {value}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {R.concat(
                ...R.partition(
                  isNotUnavailable,
                  sortByTimestamp(compactData, "desc"),
                ),
              ).map((row, index) => {
                const columnToDisplayForThisRow = isBefore(
                  new Date(row.timestamp),
                  new Date("2020"),
                )
                  ? { ...row, timestamp: "", since: "" }
                  : row;
                return (
                  <TableRow key={index}>
                    {R.keys(headers).map((columnName) => {
                      return (
                        <TableCell
                          align="right"
                          key={columnName}
                          className={
                            row.availability_kind === "unavailable"
                              ? classes.unavailable
                              : ""
                          }
                        >
                          {columnToDisplayForThisRow[columnName]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Drawer>
    </>
  );
};
