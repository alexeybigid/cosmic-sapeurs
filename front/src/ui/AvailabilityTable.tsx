import type {
  VehicleSubCategory,
  VehicleCategory,
  VehicleLabels,
  AvailabilityCellValue,
} from "core-logic";
import { TableRowForRole } from "ui";
import { coverStyles, omnibusBothAvailable } from "app/theme";
import { VehicleRole } from "interfaces";
import * as R from "ramda";
import { UiAccordion } from "ui/UiAccordion";
import type { Total } from "ui/UiAccordion";

type SubCategoryData = Partial<Record<VehicleRole, AvailabilityCellValue>>;

export type AvailabilityTableData = Record<
  VehicleCategory,
  Partial<Record<VehicleSubCategory, SubCategoryData>>
>;

export type AvailabilityTableProps = {
  data: AvailabilityTableData;
  labels: VehicleLabels;
};

const calculateSubCategoryTotal = (subCategoryData: SubCategoryData) => {
  return R.values(subCategoryData).reduce<Total>(
    (acc, availabilityByRole) => {
      if (!availabilityByRole) return acc;
      const value = acc.value + (availabilityByRole?.value ?? 0);
      const theoretical =
        acc.theoretical + (availabilityByRole?.theoretical ?? 0);
      return {
        value,
        theoretical,
      };
    },
    { value: 0, theoretical: 0 },
  );
};

export const AvailabilityTable = ({ data, labels }: AvailabilityTableProps) => {
  const colorsByCategory: Record<VehicleCategory, string> = {
    omnibus: omnibusBothAvailable,
    fire: coverStyles.ep.mainColor,
    sanitary: coverStyles.sap.mainColor,
    command: "#960340",
    special: "#1300A1",
  };

  return (
    <div className="flex flex-wrap">
      {Object.entries(data).map(([category, categoryData]) => {
        const color = colorsByCategory[category as VehicleCategory];
        return (
          <div key={category} className="px-5 pt-5">
            <div className="text-center text-xl" style={{ color }}>
              {labels.categories[category as VehicleCategory]}
            </div>
            <div className="flex flex-wrap justify-items-start">
              {Object.entries(categoryData).map(
                ([subCategoryString, subCategoryData]) => {
                  const subCategory = subCategoryString as VehicleSubCategory;
                  const subCategoryName = labels.subCategories[subCategory];
                  const total = calculateSubCategoryTotal(subCategoryData!);

                  return (
                    <div className="m-2" key={subCategoryName}>
                      <UiAccordion
                        title={subCategoryName}
                        total={total}
                        color={color}
                      >
                        <TableRowForRole valuesByColumn={subCategoryData!} />
                      </UiAccordion>
                    </div>
                  );
                },
              )}
            </div>
          </div>
        );
      })}
    </div>
  );
};
