import { ReactNode } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import * as R from "ramda";

type DataElement = Record<string, ReactNode>
type SimpleTableProps = { data: DataElement[] }

export const SimpleTable = ({ data }: SimpleTableProps) => {
    const headers = R.keys(data[0])

    return <div>
        <TableContainer component={Paper}>
            <Table size="small" className="w-auto">
                <TableHead>
                    <TableRow>
                        {headers.map(header => <TableCell>{header}</TableCell>)}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((row, index) => (
                        <TableRow key={index}>
                            {headers.map(header => <TableCell component="th" scope="row">
                                {row[header]}
                            </TableCell>)}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    </div>;
}

