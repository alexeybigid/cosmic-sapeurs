import Chip from "@material-ui/core/Chip";

export type StatusChipProps = {
  label: string;
  color: string;
  textColor?: string;
};

export const StatusChip = ({ label, color, textColor }: StatusChipProps) => (
  <Chip
    label={label}
    variant="default"
    className="font-bold"
    style={{ backgroundColor: color, color: textColor }}
  />
);
