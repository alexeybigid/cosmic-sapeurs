import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

type UiButtonProps = {
  onClick: () => void;
  children: React.ReactNode;
  disabled?: boolean;
  isLoading?: boolean;
};

export const UiButton = ({
  onClick,
  children,
  disabled,
  isLoading,
}: UiButtonProps) => (
  <Button
    onClick={onClick}
    disabled={disabled ?? isLoading}
    color="primary"
    variant="contained"
    className="m-4 w-56"
    endIcon={isLoading && <CircularProgress color="inherit" size={20} />}
  >
    {children}
  </Button>
);
