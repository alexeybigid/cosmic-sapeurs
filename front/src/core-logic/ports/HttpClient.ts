import type { Hours } from "core-logic/useCases/cover-ops/interactions.slice";
import type {
  AvailabilityChangedEvent,
  OmnibusBalanceChangedEvent,
  OngoingOpChangedEvent,
} from "interfaces";

export interface HttpClient {
  helloSapeur: () => Promise<string>;
  fetchCover: (hours: Hours) => Promise<AvailabilityChangedEvent[]>;
  fetchOps: (hours: Hours) => Promise<OngoingOpChangedEvent[]>;
  fetchOmnibus: () => Promise<OmnibusBalanceChangedEvent | null>;
}
