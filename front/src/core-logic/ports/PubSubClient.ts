import type {
  AvailabilityChangedEvent,
  OngoingOpChangedEvent,
  DomainTopic,
  OmnibusBalanceChangedEvent,
} from "interfaces";

type WithTopicAndPayload<T extends string, P> = {
  topic: T;
  payload: P;
};

type Event<T extends DomainTopic, P> = WithTopicAndPayload<T, P>;

export type AppEvent =
  | Event<"availabilityChanged", AvailabilityChangedEvent>
  | Event<"ongoingOpChanged", OngoingOpChangedEvent>
  | Event<"omnibusBalanceChanged", OmnibusBalanceChangedEvent>;

export type NarrowEvent<T extends AppEvent["topic"]> = Extract<
  AppEvent,
  { topic: T }
>;

export interface PubSubClient {
  setInternalSocketEventsHandler: (cb: (e: any) => void) => void;

  unsubscribe: (topic: AppEvent["topic"]) => void;
  subscribe: <T extends AppEvent["topic"]>(
    topic: T,
    callback: (event: NarrowEvent<T>) => void,
  ) => void;
}
