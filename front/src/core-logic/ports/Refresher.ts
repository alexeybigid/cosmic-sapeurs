import * as R from "ramda";

export interface Refresher {
  addToLoop: (cb: () => void, callbackId: string) => void;
  removeFromLoop: (callbackId: string) => void;
}

export class IntervalRefresher implements Refresher {
  private currentLoop?: number;
  private callbacks: Record<string, () => void>;

  constructor(private loopDuration: number) {
    this.callbacks = {};
  }

  public addToLoop(cb: () => void, callBackId: string) {
    if (this.callbacks[callBackId])
      throw Error("This callbackId is already used");

    this.callbacks[callBackId] = cb;
    this.cancelLoop();
    this.startLoop();
  }

  public startLoop() {
    this.currentLoop = window.setInterval(() => {
      console.time("LOOP");
      R.values(this.callbacks).forEach((cb) => cb());
      console.timeEnd("LOOP");
    }, this.loopDuration);
  }

  public removeFromLoop(id: string) {
    delete this.callbacks[id];
  }

  public cancelLoop() {
    if (this.currentLoop) window.clearInterval(this.currentLoop);
  }
}

export class InstantOnceRefresher implements Refresher {
  public removeFromLoop() {
    this.cb = () => console.warn("Loop was canceled");
  }

  private cb = (): void => console.warn("Call back not provided in Refresher");

  public addToLoop(cb: () => void) {
    this.cb = cb;
  }

  public runLoop() {
    this.cb();
  }
}
