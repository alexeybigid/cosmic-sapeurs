import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  OmnibusBalance,
  OmnibusBalanceChangedEvent,
  Omnibus,
} from "interfaces";

type OmnibusState = {
  omnibusBalance: OmnibusBalance;
  details: Omnibus[];
};

export const omnibusSliceName = "omnibus";

const initialState: OmnibusState = {
  omnibusBalance: {
    both_available: 0,
    only_pse_available: 0,
    vsav_on_inter_san: 0,
    pse_on_inter_san: 0,
    both_on_inter_san: 0,
    pse_on_inter_fire: 0,
  },
  details: [],
};

const omnibusSlice = createSlice({
  name: omnibusSliceName,
  initialState,
  reducers: {
    omnibusBalanceChanged: (
      state,
      { payload }: PayloadAction<OmnibusBalanceChangedEvent | null>,
    ) => {
      if (payload) {
        state.omnibusBalance = payload.omnibus_balance;
        state.details = payload.details;
      } else {
        state = initialState;
      }
    },
  },
});

export const {
  reducer: omnibusReducer,
  actions: omnibusActions,
} = omnibusSlice;
