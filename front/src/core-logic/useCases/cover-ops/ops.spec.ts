import type { AppEvent } from "core-logic/ports/PubSubClient";
import { InstantOnceRefresher } from "core-logic/ports/Refresher";
import { InMemoryPubSubClient } from "core-logic/secondaryAdapters/InMemoryPubSubClient";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import { opsActions } from "./ops.slice";
import {
  subscribeToOngoingOpChangedEventsThunk,
  unsubscribeFromOngoingOpChangedEventsThunk,
} from "./thunks/subscribeToOngoingOpChangedEventsThunk";
import type { ReduxStore } from "core-logic/setup/store.config";
import { makeOngoingOpChangedEvent } from "core-logic/useCases/cover-ops/factories/makeOngoingOpChangedEvent";
import type { OngoingOpChangedEvent } from "interfaces";
import { loopOverDispatchBatchThunk } from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";

describe("Update Ops", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let pubSubClient: InMemoryPubSubClient;
  let renderingRefresher: InstantOnceRefresher;

  beforeEach(() => {
    const testStore = createTestStore();
    pubSubClient = testStore.pubSubClient;
    renderingRefresher = testStore.renderingRefresher;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to ongoing op changed event, then unsubscribes", async () => {
    const timestamp_0 = new Date().toISOString();
    const payload: OngoingOpChangedEvent = {
      timestamp: timestamp_0,
      raw_operation_id: "1",
      address_area: "CHPT" as const,
      cause: "fire" as const,
      area_ongoing_ops: 1,
      bspp_ongoing_ops: 3,
      status: "finished",
    };

    const incomingEvent: AppEvent = {
      topic: "ongoingOpChanged",
      payload,
    };

    await store.dispatch(loopOverDispatchBatchThunk());

    await store.dispatch(subscribeToOngoingOpChangedEventsThunk());

    publishEvent(incomingEvent);

    const expectedOngoingOpChangedEvent_0 = payload;

    expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [expectedOngoingOpChangedEvent_0],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });

    await store.dispatch(unsubscribeFromOngoingOpChangedEventsThunk());
    publishEvent(incomingEvent);

    expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [expectedOngoingOpChangedEvent_0],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });
  });

  it("Purges old ongoingOp changed events", async () => {
    const ongoingOp1 = makeOngoingOpChangedEvent({
      raw_operation_id: "1",
      timestamp: "2021-03-03T15:59:00.0",
    });
    const ongoingOp2 = makeOngoingOpChangedEvent({
      raw_operation_id: "2",
      timestamp: "2021-03-03T16:01:00.0",
    });
    const ongoingOp3 = makeOngoingOpChangedEvent({
      raw_operation_id: "3",
      timestamp: "2021-03-03T20:00:00.0",
    });

    const { store: _store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [ongoingOp1, ongoingOp2, ongoingOp3],
      },
    });
    const _expectStateToEqual = buildExpectStateToEqual(
      _store,
      _store.getState(),
    );

    await _store.dispatch(opsActions.purgeOldOngoingOps());

    _expectStateToEqual({
      ops: {
        ongoingOpChangedEvents: [ongoingOp2, ongoingOp3],
      },
    });
  });

  const publishEvent = (event: AppEvent) => {
    pubSubClient.publish(event);
    renderingRefresher.runLoop();
  };
});
