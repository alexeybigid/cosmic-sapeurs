import { AppThunk } from "../../../setup/store.config";
import { coverActions } from "../cover.slice";

const purgeOldAvailabilityEvents = "purgeOldAvailabilityEvents";

export const subscribeToAvailabilityChangedEventsThunk = (): AppThunk => async (
  dispatch,
  _,
  { pubSubClient, purgeRefresher, batchHandler },
) => {
  pubSubClient.subscribe("availabilityChanged", (event) => {
    batchHandler.addForDispatch(
      coverActions.availabilityChanged(event.payload),
    );
  });

  purgeRefresher.addToLoop(() => {
    dispatch(coverActions.purgeOldAvailabilityEvents());
  }, purgeOldAvailabilityEvents);
};

export const unsubscribeFromAvailabilityChangedEventsThunk = (): AppThunk => async (
  dispatch,
  _,
  { pubSubClient, purgeRefresher },
) => {
  pubSubClient.unsubscribe("availabilityChanged");
  purgeRefresher.removeFromLoop(purgeOldAvailabilityEvents);
};
