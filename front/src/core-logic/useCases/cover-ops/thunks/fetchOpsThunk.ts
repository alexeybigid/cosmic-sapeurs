import { AppThunk } from "core-logic/setup/store.config";
import { opsActions } from "core-logic/useCases/cover-ops/ops.slice";
import type { Hours } from "core-logic/useCases/cover-ops/interactions.slice";

export const fetchOpsThunk = (hours: Hours): AppThunk => async (
  dispatch,
  state,
  { httpClient },
) => {
  dispatch(opsActions.opsRequested());
  try {
    const availability_events = await httpClient.fetchOps(hours);
    dispatch(opsActions.opsFetched(availability_events));
  } catch (e) {
    dispatch(opsActions.opsCouldNotFetch(e.message));
  }
};
