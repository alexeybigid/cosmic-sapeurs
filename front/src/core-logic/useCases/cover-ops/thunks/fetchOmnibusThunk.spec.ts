import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { ReduxStore } from "core-logic/setup/store.config";
import { fetchOmnibusThunk } from "core-logic/useCases/cover-ops/thunks/fetchOmnibusThunk";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "core-logic/useCases/test.utils";
import { OmnibusBalanceChangedEvent } from "interfaces";

describe("Fetch Omnibus", () => {
  let store: ReduxStore;
  let httpClient: InMemoryHttpClient;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    httpClient = testStore.httpClient;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("reflects availabilities given received data", async () => {
    const omnibusBalanceChangedEvent: OmnibusBalanceChangedEvent = {
      timestamp: "2020-12-10",
      omnibus_balance: {
        both_available: 20,
        both_on_inter_san: 1,
        vsav_on_inter_san: 3,
        pse_on_inter_fire: 39,
      },
      details: [
        {
          area: "STOU",
          balance_kind: "only_pse_available",
          pse_id: "my_pse_id",
          vsav_id: "my_vsav_id",
        },
      ],
    };

    httpClient.setOmnibusResponse(omnibusBalanceChangedEvent);

    await store.dispatch(fetchOmnibusThunk());
    expectStateToEqual({
      omnibus: {
        omnibusBalance: omnibusBalanceChangedEvent.omnibus_balance,
        details: omnibusBalanceChangedEvent.details,
      },
    });
  });
});
