import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { ReduxStore } from "core-logic/setup/store.config";
import { opsActions } from "core-logic/useCases/cover-ops/ops.slice";
import { fetchOpsThunk } from "core-logic/useCases/cover-ops/thunks/fetchOpsThunk";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "core-logic/useCases/test.utils";
import { OngoingOpChangedEvent } from "interfaces";

describe("Fetch Ops", () => {
  let store: ReduxStore;
  let httpClient: InMemoryHttpClient;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    httpClient = testStore.httpClient;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("shows fetch is ongoing", async () => {
    await store.dispatch(opsActions.opsRequested());
    expectStateToEqual({ ops: { isFetchingOps: true } });
  });

  it("reflects availabilities given received data", async () => {
    const onGoingOpsEvent: OngoingOpChangedEvent = {
      address_area: "STOU",
      area_ongoing_ops: 12,
      bspp_ongoing_ops: 13,
      cause: "fire",
      raw_operation_id: "123",
      timestamp: "2020-01-01T12:00:00.1",
      status: "first_vehicle_affected",
    };

    httpClient.setOpsResponse([onGoingOpsEvent]);

    await store.dispatch(fetchOpsThunk(3));
    expectStateToEqual({
      ops: {
        isFetchingOps: false,
        ongoingOpChangedEvents: [onGoingOpsEvent],
      },
    });
  });

  it("warns when fetch fails", async () => {
    httpClient.setError("Could not fetch!");
    await store.dispatch(fetchOpsThunk(3));
    expectStateToEqual({
      ops: { fetchOpsError: "Could not fetch!", isFetchingOps: false },
    });
  });
});
