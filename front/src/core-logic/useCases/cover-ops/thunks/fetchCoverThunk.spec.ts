import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { RootState } from "core-logic/setup/root.reducer";
import { ReduxStore } from "core-logic/setup/store.config";
import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import { fetchCoverThunk } from "core-logic/useCases/cover-ops/thunks/fetchCoverThunk";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "core-logic/useCases/test.utils";
import type { AvailabilityChangedEvent, VehicleEvent } from "interfaces";

describe("Fetch Cover", () => {
  let store: ReduxStore;
  let httpClient: InMemoryHttpClient;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    httpClient = testStore.httpClient;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("shows fetch is ongoing", async () => {
    await store.dispatch(coverActions.coverRequested());
    expectStateToEqual({ cover: { isFetchingCover: true } });
  });

  it("reflects availabilities given received data", async () => {
    const latest_event_data: VehicleEvent = {
      home_area: "STOU",
      raw_status: "Yeah",
      raw_vehicle_id: "vehicle_id",
      role: "vsav_solo",
      status: "arrived_on_intervention",
      timestamp: "2021-03-03T12:00:00.0",
      vehicle_name: "VSAV 123",
      availability_kind: "in_service",
    };

    const availabilityChangedEvent: AvailabilityChangedEvent = {
      area_availability: { available: 0, in_service: 1, unavailable: 3 },
      bspp_availability: { unavailable: 2 },
      home_area: "STOU",
      raw_vehicle_id: "1",
      role: "epa_epsa",
      timestamp: "2020-01-01T12:00:00.1",
      latest_event_data,
    };

    httpClient.setCoverResponse([availabilityChangedEvent]);

    await store.dispatch(fetchCoverThunk(3));
    expectStateToEqual({
      cover: {
        isFetchingCover: false,
        availabilityChangedEvents: [availabilityChangedEvent],
      },
    });
  });

  it("warns when fetch fails", async () => {
    httpClient.setError("Could not fetch!");
    await store.dispatch(fetchCoverThunk(3));
    expectStateToEqual({
      cover: { fetchCoverError: "Could not fetch!", isFetchingCover: false },
    });
  });
});
