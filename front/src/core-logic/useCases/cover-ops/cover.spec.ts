import { InMemoryPubSubClient } from "../../secondaryAdapters/InMemoryPubSubClient";
import { ReduxStore } from "../../setup/store.config";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import {
  subscribeToAvailabilityChangedEventsThunk,
  unsubscribeFromAvailabilityChangedEventsThunk,
} from "./thunks/subscribeToAvailabilityChangedEventsThunk";
import { InstantOnceRefresher } from "../../ports/Refresher";
import { AppEvent } from "../../ports/PubSubClient";
import { AvailabilityChangedEvent, VehicleEvent } from "interfaces";
import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import { coverActions } from "core-logic/useCases/cover-ops/cover.slice";
import { loopOverDispatchBatchThunk } from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";

const latest_event_data: VehicleEvent = {
  home_area: "STOU",
  raw_status: "Yeah",
  raw_vehicle_id: "vehicle_id",
  role: "vsav_solo",
  status: "arrived_on_intervention",
  timestamp: "2021-03-03T12:00:00.0",
  vehicle_name: "VSAV 123",
  availability_kind: "in_service",
};

describe("Update Cover", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let pubSubClient: InMemoryPubSubClient;
  let renderingRefresher: InstantOnceRefresher;

  beforeEach(() => {
    const testStore = createTestStore();
    pubSubClient = testStore.pubSubClient;
    renderingRefresher = testStore.renderingRefresher;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to : availability changed events, then unsubscribes", async () => {
    const timestamp_0 = new Date().toISOString();
    const count_0 = 2;

    await store.dispatch(loopOverDispatchBatchThunk());
    await store.dispatch(subscribeToAvailabilityChangedEventsThunk());

    const payload: AvailabilityChangedEvent = {
      timestamp: timestamp_0,
      area_availability: {
        available: count_0,
        unavailable: 0,
        in_service: 0,
        recoverable: 0,
      },
      bspp_availability: {
        available: count_0,
        unavailable: 0,
        in_service: 0,
        recoverable: 0,
      },
      role: "vsav_solo",
      home_area: "STOU",
      raw_vehicle_id: "1",
      latest_event_data,
    };

    const incomingEvent: AppEvent = {
      topic: "availabilityChanged",
      payload,
    };

    pubSubClient.publish(incomingEvent);
    renderingRefresher.runLoop();

    const expectedVehicleAvailableCount_0 = payload;

    expectStateToEqual({
      cover: {
        availabilityChangedEvents: [expectedVehicleAvailableCount_0],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });

    await store.dispatch(unsubscribeFromAvailabilityChangedEventsThunk());

    pubSubClient.publish(incomingEvent);
    renderingRefresher.runLoop();

    expectStateToEqual({
      cover: {
        availabilityChangedEvents: [expectedVehicleAvailableCount_0],
      },
      interactions: {
        pubSubStatus: "open",
      },
    });
  });

  it("Purges old availability changed events", async () => {
    const availabilityVehicle1beforeDate = makeAvailabilityChangedEvent({
      raw_vehicle_id: "1",
      timestamp: "2021-03-03T12:00:00.0",
    });
    const availabilityVehicle2beforeDate = makeAvailabilityChangedEvent({
      raw_vehicle_id: "2",
      timestamp: "2021-03-03T11:00:00.0",
    });
    const availabilityVehicle1afterDate = makeAvailabilityChangedEvent({
      raw_vehicle_id: "1",
      timestamp: "2021-03-03T20:00:00.0",
    });

    const { store: _store } = createTestStore({
      cover: {
        availabilityChangedEvents: [
          availabilityVehicle1beforeDate,
          availabilityVehicle2beforeDate,
          availabilityVehicle1afterDate,
        ],
      },
    });
    const _expectStateToEqual = buildExpectStateToEqual(
      _store,
      _store.getState(),
    );

    await _store.dispatch(coverActions.purgeOldAvailabilityEvents());

    _expectStateToEqual({
      cover: {
        availabilityChangedEvents: [
          availabilityVehicle2beforeDate,
          availabilityVehicle1afterDate,
        ],
      },
    });
  });
});
