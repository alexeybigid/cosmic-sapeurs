import type { OngoingOpChangedEvent } from "interfaces";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { id, findBackwards } from "utils";
import {
  keepOnlyRecentEvents,
  pushAfterLatestTimestamp,
} from "core-logic/useCases/cover-ops/coverOps.utils";

type OpsState = {
  ongoingOpChangedEvents: OngoingOpChangedEvent[];
  isFetchingOps: boolean;
  fetchOpsError?: string;
};

export const opsSliceName = "ops";

const opsSlice = createSlice({
  name: opsSliceName,
  initialState: id<OpsState>({
    ongoingOpChangedEvents: [],
    isFetchingOps: false,
  }),
  reducers: {
    purgeOldOngoingOps: ({ ongoingOpChangedEvents, ...rest }) => {
      const events = keepOnlyRecentEvents(ongoingOpChangedEvents);
      return {
        ...rest,
        ongoingOpChangedEvents: events,
      };
    },
    ongoingOpChanged: (
      state,
      { payload }: PayloadAction<OngoingOpChangedEvent>,
    ) => {
      const [previousEvent, previousEventIndex] = findBackwards(
        state.ongoingOpChangedEvents,
        (evt) => evt.raw_operation_id === payload.raw_operation_id,
      );

      const perviousEventHasSameStatus =
        previousEvent?.bspp_ongoing_ops === payload.bspp_ongoing_ops;
      const removePreviousEvent = () =>
        state.ongoingOpChangedEvents.splice(previousEventIndex!, 1);

      if (perviousEventHasSameStatus) removePreviousEvent();
      pushAfterLatestTimestamp(state.ongoingOpChangedEvents, payload);
    },

    opsRequested: (state) => {
      state.isFetchingOps = true;
    },
    opsFetched: (state, action: PayloadAction<OngoingOpChangedEvent[]>) => {
      state.ongoingOpChangedEvents.push(...action.payload);
      state.isFetchingOps = false;
    },
    opsCouldNotFetch: (state, action: PayloadAction<string>) => {
      state.isFetchingOps = false;
      state.fetchOpsError = action.payload;
    },
  },
});

export const { reducer: opsReducer, actions: opsActions } = opsSlice;
