import type { AvailabilityChangedEvent } from "interfaces";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { id, sortByTimestamp, findBackwards, removeAtIndex } from "utils";
import * as R from "ramda";
import {
  getLatestForEachRawId,
  keepOnlyRecentEvents,
  pushAfterLatestTimestamp,
} from "core-logic/useCases/cover-ops/coverOps.utils";

type CoverState = {
  availabilityChangedEvents: AvailabilityChangedEvent[];
  isFetchingCover: boolean;
  fetchCoverError?: string;
};

export const coverSliceName = "cover";

const coverSlice = createSlice({
  name: coverSliceName,
  initialState: id<CoverState>({
    availabilityChangedEvents: [],
    isFetchingCover: false,
  }),
  reducers: {
    purgeOldAvailabilityEvents: ({ availabilityChangedEvents, ...rest }) => {
      const events = R.concat(
        getLatestForEachRawId(availabilityChangedEvents),
        keepOnlyRecentEvents(availabilityChangedEvents),
      );
      return {
        ...rest,
        availabilityChangedEvents: sortByTimestamp(
          R.uniqBy((e) => `${e.raw_vehicle_id}_${e.timestamp}`, events),
          "asc",
        ),
      };
    },
    availabilityChanged: (
      state,
      { payload }: PayloadAction<AvailabilityChangedEvent>,
    ) => {
      const [previousEvent, previousEventIndex] = findBackwards(
        state.availabilityChangedEvents,
        (evt) => evt.raw_vehicle_id === payload.raw_vehicle_id,
      );

      const perviousEventHasSameAvailability =
        previousEvent?.latest_event_data.availability_kind ===
        payload.latest_event_data.availability_kind;

      if (perviousEventHasSameAvailability)
        removeAtIndex(state.availabilityChangedEvents, previousEventIndex ?? 0);

      pushAfterLatestTimestamp(state.availabilityChangedEvents, payload);
    },

    coverRequested: (state) => {
      state.isFetchingCover = true;
    },
    coverFetched: (
      state,
      action: PayloadAction<AvailabilityChangedEvent[]>,
    ) => {
      state.availabilityChangedEvents.push(...action.payload);
      state.isFetchingCover = false;
    },
    coverCouldNotFetch: (state, action: PayloadAction<string>) => {
      state.isFetchingCover = false;
      state.fetchCoverError = action.payload;
    },
  },
});

export const {
  reducer: coverReducer,
  actions: coverActions,
} = coverSlice;
