import { createTestStore } from "core-logic/useCases/test.utils";
import { availabilityChangedEventsByRoleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByRoleSelector";
import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import type { AvailabilityChangedEvent, VehicleRole } from "interfaces";

describe("Availability changed events by role selector", () => {
  it("Filters availability changed events by their role", () => {
    const vsavAvailabilityEvent = makeAvailabilityChangedEvent({
      role: "vsav_solo",
    });
    const pse1AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "pse_solo",
    });
    const pse2AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "pse_solo",
      home_area: "CHPT",
    });

    const { store } = createTestStore({
      cover: {
        availabilityChangedEvents: [
          vsavAvailabilityEvent,
          pse1AvailabilityEvent,
          pse2AvailabilityEvent,
        ],
      },
    });

    const actual = availabilityChangedEventsByRoleSelector(store.getState());

    const expected: Partial<Record<VehicleRole, AvailabilityChangedEvent[]>> = {
      vsav_solo: [vsavAvailabilityEvent],
      pse_solo: [pse1AvailabilityEvent, pse2AvailabilityEvent],
    };
    expect(actual).toEqual(expected);
  });
});
