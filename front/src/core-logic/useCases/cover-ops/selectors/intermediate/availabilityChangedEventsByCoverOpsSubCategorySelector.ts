import { Availability, VehicleRole } from "interfaces";
import { createSelector } from "@reduxjs/toolkit";
import * as R from "ramda";
import type { AvailabilityChangedEvent } from "interfaces";
import { rolesPerCoverOpsSubCategory } from "core-logic/useCases/cover-ops/selectors/vehicleRoleClassification";
import { availabilityChangedEventsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";

const sumAvailabilityFor = (
  availabilityToAccess: "area_availability" | "bspp_availability",
) => (eventsByRole: AvailabilityChangedEvent[]): Availability =>
  eventsByRole.reduce(
    (acc, evt) => {
      if (!evt) return acc;
      return sumAvailabilities(acc, R.prop(availabilityToAccess, evt));
    },
    {
      available: 0,
      unavailable: 0,
      in_service: 0,
      recoverable: 0,
    },
  );

const combineAvailabilityChangedEventsByRole = (
  events: AvailabilityChangedEvent[],
): AvailabilityChangedEvent[] =>
  events.map((currentEvent, currentIndex) => {
    const eventsBefore = events.slice(0, currentIndex + 1);
    const eventsBeforeForCurrentArea = eventsBefore.filter(
      ({ home_area }) => home_area === currentEvent.home_area,
    );

    const cumulatedAreaAvailability = R.pipe(
      getLastByRole,
      sumAvailabilityFor("area_availability"),
    )(eventsBeforeForCurrentArea);

    const cumulatedBsppAvailability = R.pipe(
      getLastByRole,
      sumAvailabilityFor("bspp_availability"),
    )(eventsBefore);

    return {
      ...currentEvent,
      area_availability: cumulatedAreaAvailability,
      bspp_availability: cumulatedBsppAvailability,
    };
  });

const makeAvailabilityChangedEventsCombinedBySubCategorySelector = (
  roles: VehicleRole[],
) => {
  const combineAvailabilities = R.pipe<
    AvailabilityChangedEvent[],
    AvailabilityChangedEvent[],
    AvailabilityChangedEvent[]
  >(
    R.filter<AvailabilityChangedEvent>((evt) => roles.includes(evt.role)),
    combineAvailabilityChangedEventsByRole,
  );

  return createSelector(
    availabilityChangedEventsSelector,
    combineAvailabilities,
  );
};

const availabilityChangedEventsBySelectors = {
  ep: makeAvailabilityChangedEventsCombinedBySubCategorySelector(
    rolesPerCoverOpsSubCategory.ep ?? [],
  ),
  sap: makeAvailabilityChangedEventsCombinedBySubCategorySelector(
    rolesPerCoverOpsSubCategory.sap ?? [],
  ),
};

const optionalSum = (x?: number, y?: number) => (x ?? 0) + (y ?? 0);

const sumAvailabilities = (a: Availability, b: Availability) => ({
  available: optionalSum(a.available, b.available),
  unavailable: optionalSum(a.unavailable, b.unavailable),
  in_service: optionalSum(a.in_service, b.in_service),
  recoverable: optionalSum(a.recoverable, b.recoverable),
});

const getLastByRole = R.pipe<
  AvailabilityChangedEvent[],
  Record<string, AvailabilityChangedEvent[]>,
  AvailabilityChangedEvent[][],
  AvailabilityChangedEvent[]
>(
  R.groupBy((event) => event.role),
  R.values,
  R.map((events) => R.last(events)!),
);

export const availabilityChangedEventsByCoverOpsSubCategorySelector = createSelector(
  availabilityChangedEventsBySelectors.ep,
  availabilityChangedEventsBySelectors.sap,
  (epEvents, sapEvents) => ({ ep: epEvents, sap: sapEvents }),
);
