import { RootState } from "core-logic/setup/root.reducer";

export const availabilityChangedEventsSelector = (state: RootState) =>
  state.cover.availabilityChangedEvents;

export const ongoingOpChangedEventsSelector = (state: RootState) =>
  state.ops.ongoingOpChangedEvents;

export const omnibusBalanceSelector = (state: RootState) =>
  state.omnibus.omnibusBalance;

export const omnibusDetailsSelector = (state: RootState) =>
  state.omnibus.details;

export const numberOfHoursToShowSelector = (state: RootState) =>
  state.interactions.numberOfHoursToShow;
