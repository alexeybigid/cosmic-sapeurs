import { createSelector } from "@reduxjs/toolkit";
import { ongoingOpChangedEventsSelector } from "./rootSelectors";
import { groupBy } from "utils";
import type { OngoingOpChangedEvent, OperationCause } from "interfaces";

const groupByCause = groupBy<OngoingOpChangedEvent, OperationCause>(
  (event) => event.cause,
);

export const ongoingOpChangedEventsByRoleSelector = createSelector(
  ongoingOpChangedEventsSelector,
  groupByCause,
);
