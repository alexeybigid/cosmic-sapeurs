import * as R from "ramda";
import { createSelector } from "@reduxjs/toolkit";
import { ongoingOpChangedEventsSelector } from "./intermediate/rootSelectors";
import type { OngoingOpChangedEvent } from "interfaces";

const toLastEventByOperationId = R.pipe<
  OngoingOpChangedEvent[],
  Record<string, OngoingOpChangedEvent[]>,
  Record<string, OngoingOpChangedEvent>
>(
  R.groupBy((event) => event.raw_operation_id),
  R.mapObjIndexed((event) => R.last(event)!),
);

export const lastOnGoingOpEventByOperationIdSelector = createSelector(
  ongoingOpChangedEventsSelector,
  toLastEventByOperationId,
);
