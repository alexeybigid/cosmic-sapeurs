import { createTestStore } from "core-logic/useCases/test.utils";
import { makeOngoingOpChangedEvent } from "core-logic/useCases/cover-ops/factories/makeOngoingOpChangedEvent";
import { lastOnGoingOpEventByOperationIdSelector } from "core-logic/useCases/cover-ops/selectors/lastOnGoingOpEventByOperationIdSelector";
import type { OngoingOpChangedEvent } from "interfaces";

describe("lastOnGoinOpEventByOperationIdSelector", () => {
  it("Get last operation event", () => {
    const ope1Event = makeOngoingOpChangedEvent({
      timestamp: "2020-02-01",
      bspp_ongoing_ops: 30,
      area_ongoing_ops: 3,
      cause: "victim",
      address_area: "ASNI",
      raw_operation_id: "ope1",
      status: "ongoing",
    });

    const ope2Event = makeOngoingOpChangedEvent({
      timestamp: "2020-02-02",
      bspp_ongoing_ops: 30,
      area_ongoing_ops: 3,
      cause: "victim",
      address_area: "ASNI",
      raw_operation_id: "ope2",
      status: "ongoing",
    });
    const ope1LastEvent = makeOngoingOpChangedEvent({
      timestamp: "2020-02-03",
      bspp_ongoing_ops: 30,
      area_ongoing_ops: 3,
      cause: "victim",
      address_area: "ASNI",
      raw_operation_id: "ope1",
      status: "closed",
    });

    const { store } = createTestStore({
      ops: {
        ongoingOpChangedEvents: [ope1Event, ope2Event, ope1LastEvent],
      },
    });
    const actual = lastOnGoingOpEventByOperationIdSelector(store.getState());

    const expected: Record<string, OngoingOpChangedEvent> = {
      [ope1Event.raw_operation_id]: {
        timestamp: "2020-02-03",
        bspp_ongoing_ops: 30,
        area_ongoing_ops: 3,
        cause: "victim",
        address_area: "ASNI",
        raw_operation_id: "ope1",
        status: "closed",
      },
      [ope2Event.raw_operation_id]: {
        timestamp: "2020-02-02",
        bspp_ongoing_ops: 30,
        area_ongoing_ops: 3,
        cause: "victim",
        address_area: "ASNI",
        raw_operation_id: "ope2",
        status: "ongoing",
      },
    };

    expect(actual).toEqual(expected);
  });
});
