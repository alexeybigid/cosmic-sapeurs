import type { Availability, AvailabilityKind } from "interfaces";
import { createSelector } from "@reduxjs/toolkit";
import type { BarInput } from "ui/charts/bar/barChartUtils";
import { lastAvailabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityChangedEventsByCoverOpsSubCategorySelector";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";

const createBarInputSelectorForSubCategory = <
  S extends CoverOpsVehicleSubCategory
>(
  subCategory: S,
) =>
  createSelector(
    lastAvailabilityChangedEventsByCoverOpsSubCategorySelector,
    (
      lastAvailabilityChangedEventsByCoverOpsSubCategory,
    ): BarInput<AvailabilityKind> => {
      const bspp_availability: Availability = lastAvailabilityChangedEventsByCoverOpsSubCategory[
        subCategory
      ]?.bspp_availability ?? {
        available: 0,
        in_service: 0,
        recoverable: 0,
        unavailable: 0,
      };

      return bspp_availability;
    },
  );

export const sapBarInputSelector = createBarInputSelectorForSubCategory("sap");
export const epBarInputSelector = createBarInputSelectorForSubCategory("ep");

export const availabilityKindOptions: Exclude<
  AvailabilityKind,
  "unavailable"
>[] = ["available", "in_service", "recoverable"];
