import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "core-logic/setup/root.reducer";
import * as R from "ramda";

const timestampToDate = (timestamp: string): Date => new Date(timestamp);

const coverLastTimestampSelector = (state: RootState): string | undefined =>
  state.cover.availabilityChangedEvents[
    state.cover.availabilityChangedEvents.length - 1
  ]?.timestamp;

const opsLastTimestampSelector = (state: RootState): string | undefined =>
  R.last(state.ops.ongoingOpChangedEvents)?.timestamp;

export const lastTimestampDateSelector = createSelector(
  coverLastTimestampSelector,
  opsLastTimestampSelector,
  (coverLast, opsLast) => {
    if (!coverLast) {
      if (!opsLast) return new Date();
      return timestampToDate(opsLast);
    }
    // At this stage, coverLast is defined
    if (!opsLast) return timestampToDate(coverLast);

    const lastCoverDate = timestampToDate(coverLast);
    const lastOpsDate = timestampToDate(opsLast);

    if (lastCoverDate.getDate() > lastOpsDate.getDate()) return lastCoverDate;
    return lastOpsDate;
  },
);
