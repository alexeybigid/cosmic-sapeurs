import { createSelector } from "@reduxjs/toolkit";
import { formatDistance } from "date-fns";
import { AvailabilityKind, VehicleRole } from "interfaces";
import * as R from "ramda";
import { fr } from "date-fns/locale";
import {
  rolesPerCoverOpsSubCategory,
  vehiclesLabels,
} from "core-logic/useCases/cover-ops/selectors/vehicleRoleClassification";
import { availabilityKindOptions } from "core-logic/useCases/cover-ops/selectors/barInputSampleSelector";
import { availabilityChangedEventsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";

const availabilityChangedEventsByIdSelector = createSelector(
  availabilityChangedEventsSelector,
  (availabilityChangedEvents) => {
    const availabilityChangedEventsById = R.groupBy(
      (availabilityChangedEventData) => {
        return availabilityChangedEventData.raw_vehicle_id;
      },
      availabilityChangedEvents,
    );

    const latestAvailabilityChangedEventsById = R.mapObjIndexed(
      (events) => R.last(events)!, // in this case it can never be empty list because it is coming from a groupBy
      availabilityChangedEventsById,
    );

    return latestAvailabilityChangedEventsById;
  },
);

const makeEnrichedLatestVehicleEventByRawVehicleIdForRoleSelector = (
  role: VehicleRole,
) => {
  return createSelector(
    availabilityChangedEventsByIdSelector,
    (latestAvailabilityChangedEventsById) => {
      const latestAvailabilityChangedEventsByIdForRole = R.filter(
        (availabilityChangedEvent) => availabilityChangedEvent.role === role,
        latestAvailabilityChangedEventsById,
      );

      return R.mapObjIndexed((availabilityEvent) => {
        const latestEventData = availabilityEvent.latest_event_data;

        return {
          ...latestEventData,
          since: formatDistance(
            new Date(),
            new Date(latestEventData.timestamp),
            { locale: fr },
          ),
        };
      }, latestAvailabilityChangedEventsByIdForRole);
    },
  );
};

const makeLatestVehicleEventByRawVehicleIdForSubCategorySelector = (
  subCategory: CoverOpsVehicleSubCategory,
) => {
  return createSelector(
    availabilityChangedEventsByIdSelector,
    (latestAvailabilityChangedEventsById) => {
      return rolesPerCoverOpsSubCategory[subCategory].flatMap((role) => {
        const latestAvailabilityChangedEventsByIdForRole = R.filter(
          (availabilityChangedEvent) => availabilityChangedEvent.role === role,
          latestAvailabilityChangedEventsById,
        );
        return R.values(latestAvailabilityChangedEventsByIdForRole);
      });
    },
  );
};

export const latestEnrichedVehicleEventByRawVehicleIdForRoleSelectors = R.keys(
  vehiclesLabels.roles,
).reduce((acc, role) => {
  return {
    ...acc,
    [role]: makeEnrichedLatestVehicleEventByRawVehicleIdForRoleSelector(role),
  };
}, {} as Record<VehicleRole, ReturnType<typeof makeEnrichedLatestVehicleEventByRawVehicleIdForRoleSelector>>);

export const latestVehicleEventByRawVehicleIdForCoverOpsSubCategorySelectors = R.keys(
  rolesPerCoverOpsSubCategory,
).reduce((acc, subCategory) => {
  return {
    ...acc,
    [subCategory]: makeLatestVehicleEventByRawVehicleIdForSubCategorySelector(
      subCategory,
    ),
  };
}, {} as Record<CoverOpsVehicleSubCategory, ReturnType<typeof makeLatestVehicleEventByRawVehicleIdForSubCategorySelector>>);

export const makeCountByStatusForSubCategoryForAvailabilityKind = (
  subCategory: CoverOpsVehicleSubCategory,
  availabilityKind: AvailabilityKind,
) => {
  return createSelector(
    latestVehicleEventByRawVehicleIdForCoverOpsSubCategorySelectors[
      subCategory
    ],
    (latestAvailabilityChangedEventForSubCategory) => {
      const valuesForAvailabilityKind = R.filter(
        ({ latest_event_data }) =>
          latest_event_data.availability_kind === availabilityKind,
        latestAvailabilityChangedEventForSubCategory,
      );
      return R.countBy(({ latest_event_data }) => {
        return latest_event_data.raw_status;
      }, valuesForAvailabilityKind);
    },
  );
};

const makeSubCategoryToCountByStatusByAvailabilityKind = (
  subCategory: CoverOpsVehicleSubCategory,
) =>
  availabilityKindOptions.reduce((acc, availabilityKind) => {
    return {
      ...acc,
      [availabilityKind]: makeCountByStatusForSubCategoryForAvailabilityKind(
        subCategory,
        availabilityKind,
      ),
    };
  }, {} as Record<AvailabilityKind, ReturnType<typeof makeCountByStatusForSubCategoryForAvailabilityKind>>);

export const subCategoryToCountByStatusByAvailabilityKind = {
  ep: makeSubCategoryToCountByStatusByAvailabilityKind("ep"),
  sap: makeSubCategoryToCountByStatusByAvailabilityKind("sap"),
};
