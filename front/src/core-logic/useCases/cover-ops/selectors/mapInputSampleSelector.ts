import * as R from "ramda";
import { createSelector } from "@reduxjs/toolkit";
import {
  Area,
  AvailabilityKind,
  OperationCause,
  AvailabilityChangedEvent,
  OngoingOpChangedEvent,
} from "interfaces";
import { ongoingOpChangedEventsByRoleSelector } from "./intermediate/ongoingOpChangedEventsByRoleSelector";
import { availabilityChangedEventsByCoverOpsSubCategorySelector } from "./intermediate/availabilityChangedEventsByCoverOpsSubCategorySelector";
import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { Availability } from "interfaces/generated/Availability";
import { groupBy } from "utils";
import { MapInputSample } from "ui";

const groupValuesByHomeArea = (
  eventsByCoverOpsVehicleSubCategory: Record<
    CoverOpsVehicleSubCategory,
    AvailabilityChangedEvent[]
  >,
) =>
  R.mapObjIndexed(
    groupBy<AvailabilityChangedEvent, Area>(
      (availabilityChangedEvent) => availabilityChangedEvent.home_area,
    ),
  )(eventsByCoverOpsVehicleSubCategory);

const availabilitiesByAreaByCoverOpsSubCategorySelector = createSelector(
  availabilityChangedEventsByCoverOpsSubCategorySelector,
  groupValuesByHomeArea,
);

type CoverMapInputSample = MapInputSample<AvailabilityKind>;

type AvailabilityAndArea = { counts: Availability; area: Area };

const convertToArrayOfAvailabilityAndArea = R.pipe<
  Record<Area, AvailabilityChangedEvent[]>,
  Record<Area, AvailabilityAndArea>,
  Array<AvailabilityAndArea>
>(
  R.mapObjIndexed((availabilityChangedEvents) => {
    const { area_availability, home_area } =
      R.last(availabilityChangedEvents)! ?? ({} as AvailabilityChangedEvent);

    return { counts: area_availability, area: home_area };
  }),
  R.values,
);

export const lastAvailabilityChangedEventByAreaByRoleSelector = createSelector(
  availabilitiesByAreaByCoverOpsSubCategorySelector,
  (
    availabilitiesByAreaByRole,
  ): Partial<Record<CoverOpsVehicleSubCategory, CoverMapInputSample[]>> => {
    return R.mapObjIndexed(
      convertToArrayOfAvailabilityAndArea,
      availabilitiesByAreaByRole,
    );
  },
);

const ongoingOpsByAreaByRoleSelector = createSelector(
  ongoingOpChangedEventsByRoleSelector,
  (ongoingOpChangedEventsByRole) => {
    return R.mapObjIndexed(
      R.groupBy<OngoingOpChangedEvent>(
        (ongoingOpChangedEvent) => ongoingOpChangedEvent.address_area,
      ),
      ongoingOpChangedEventsByRole,
    ) as Record<OperationCause, Record<Area, OngoingOpChangedEvent[]>>;
  },
);

type OpsMapInputSample = MapInputSample<"ongoingOps">;

type OngoingOpCountAndArea = {
  counts: { ongoingOps: number | undefined };
  area: Area;
};

export const lastOngoingOpChangedEventByAreaByCauseSelector = createSelector(
  ongoingOpsByAreaByRoleSelector,
  (
    ongoingOpsCountByAreaByCause,
  ): Partial<Record<OperationCause, OpsMapInputSample[]>> => {
    const lastOngoingOpChangedEventByAreaByRole = R.mapObjIndexed(
      R.pipe<
        Record<Area, OngoingOpChangedEvent[]>,
        Record<Area, OngoingOpCountAndArea>,
        OngoingOpCountAndArea[]
      >(
        R.mapObjIndexed((ongoingOpChangedEvents) => {
          const { area_ongoing_ops, address_area } =
            R.last(ongoingOpChangedEvents) ?? {};
          return {
            counts: { ongoingOps: area_ongoing_ops },
            area: address_area!,
          };
        }),
        R.values,
      ),
      ongoingOpsCountByAreaByCause,
    );

    return lastOngoingOpChangedEventByAreaByRole;
  },
);
