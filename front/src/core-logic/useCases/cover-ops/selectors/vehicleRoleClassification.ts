import { CoverOpsVehicleSubCategory } from "core-logic/useCases/cover-ops/CoverOpsVehicleSubCategory";
import { VehicleRole } from "interfaces";

export type VehicleCategory =
  | "omnibus"
  | "sanitary"
  | "fire"
  | "special"
  | "command";

export type VehicleSubCategory =
  | "san"
  | "pump"
  | "mea"
  | "strongPower" // grande puissance
  | "ep"
  | "sap"
  | "medical"
  | "nautical"
  | "rsmu"
  | "nrbc"
  | "eld"
  | "cynoTech"
  | "imp"
  | "exhaustion" // Épuisement
  | "support" // Soutien
  | "cie"
  | "group" // Groupement
  | "brigade";

type VehicleRoleClassification = Record<
  VehicleCategory,
  Partial<Record<VehicleSubCategory, VehicleRole[]>>
>;

export const vehicleRoleClassification: VehicleRoleClassification = {
  omnibus: {
    san: ["vsav_omni", "pse_omni_san"],
    pump: ["pse_omni_pump"],
  },
  sanitary: {
    sap: ["vsav_solo", "vpsp"],
    medical: ["ar_bspp"],
  },
  command: {
    // cie: [],
    group: ["vpc_tac"],
    brigade: ["cmo_appui", "cmo_san"],
  },
  fire: {
    mea: ["bea", "epa_epsa"],
    strongPower: ["fa", "ca_ba", "fmogp"],
    ep: ["fpt", "fa", "fptl", "pse_solo", "ccr", "pst", "fptlhp"],
  },
  special: {
    nautical: ["vsis", "esav", "esavi"],
    rsmu: ["vrsd", "cesd"],
    nrbc: ["vrch"],
    eld: ["celd", "vrex"],
    cynoTech: ["vec", "vra"],
    imp: ["vimp"],
    exhaustion: ["vid", "vigi"],
    support: ["crac", "pev", "cd", "vrcp"],
  },
};

export const rolesPerCoverOpsSubCategory: Record<
  CoverOpsVehicleSubCategory,
  VehicleRole[]
> = {
  ep: vehicleRoleClassification.fire.ep ?? [],
  sap: vehicleRoleClassification.sanitary.sap ?? [],
};

export type VehicleLabels = {
  categories: Record<VehicleCategory, string>;
  subCategories: Record<VehicleSubCategory, string>;
  roles: Record<VehicleRole, string>;
};

export const vehiclesLabels: VehicleLabels = {
  categories: {
    omnibus: "MODULAIRE",
    fire: "INCENDIE",
    sanitary: "SANITAIRE",
    command: "COMMANDEMENT",
    special: "SPÉCIAUX",
  },
  subCategories: {
    san: "SAN",
    pump: "PSE pompe",
    mea: "MEA",
    strongPower: "Grande puissance",
    ep: "EP",
    sap: "SAP",
    medical: "Médicalisé",
    group: "GRPT",
    cie: "Cie",
    brigade: "Brigade",
    nautical: "Nautique",
    rsmu: "RSMU",
    nrbc: "NRBC",
    eld: "ELD",
    cynoTech: "CYNO - TECH",
    imp: "IMP",
    exhaustion: "Épuisement",
    support: "Soutien",
  },
  roles: {
    bea: "BEA",
    epa_epsa: "EPA / EPSA",
    ca_ba: "CA / BA",
    fa: "FA",
    fmogp: "FMOGP",
    fpt: "FPT",
    fptl: "FPTL",
    pse_solo: "PSE pompe",
    pse_omni_san: "EP3",
    pse_omni_pump: "PSE modulaire",
    vsav_solo: "VSAV hors omni",
    vsav_omni: "VSAV omni",

    ccr: "CCR",
    pst: "PST",
    fptlhp: "FPTL HP",
    vpsp: "VPSP (associatif)",
    ar_bspp: "AR BSPP",
    umh: "UMH",
    vpc_tac: "VPC TAC",
    cmo_san: "CMO SAN",
    cmo_appui: "CMO Appui",
    vsis: "VSIS",
    esav: "ESAV",
    esavi: "ESAVI",
    vrsd: "VRSD",
    cesd: "CESD",
    vrch: "VRCH",
    celd: "CELD",
    vrex: "VREX",
    vec: "VEC",
    vra: "VRA",
    vimp: "VIMP",
    vid: "VID",
    vigi: "VIGI",
    crac: "CRAC",
    pev: "PEV",
    cd: "CD",
    vrcp: "VRCP",
    other: "Autre",
  },
};
