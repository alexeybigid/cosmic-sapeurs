import { createTestStore } from "core-logic/useCases/test.utils";
import { makeAvailabilityChangedEvent } from "core-logic/useCases/cover-ops/factories/makeAvailabilityChangedEvent";
import { availabilityToLineInputSelector } from "core-logic/useCases/cover-ops/selectors/lineInputSampleSelector";

describe("Line Input Sample selector for availabilities", () => {
  it("Sums the availability counts for each sub-category", () => {
    const sap1AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "vsav_solo",
      timestamp: "2020-01-01T12:00:00",
      home_area: "STOU",
      bspp_availability: {
        available: 1,
        unavailable: 0,
        in_service: 0,
        recoverable: 0,
      },
    });
    const ep1AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "pse_solo",
      home_area: "STOU",
      timestamp: "2020-01-01T13:00:00",
      bspp_availability: {
        available: 3,
        unavailable: 0,
        in_service: 0,
        recoverable: 0,
      },
    });
    const ep2AvailabilityEvent = makeAvailabilityChangedEvent({
      role: "fptl",
      home_area: "STOU",
      timestamp: "2020-01-01T14:00:00",
      bspp_availability: {
        available: 8,
        unavailable: 0,
        in_service: 0,
        recoverable: 0,
      },
    });

    const { store } = createTestStore({
      cover: {
        availabilityChangedEvents: [
          sap1AvailabilityEvent,
          ep1AvailabilityEvent,
          ep2AvailabilityEvent,
        ],
      },
    });

    const actual = availabilityToLineInputSelector(store.getState());

    const expected = {
      sap: [
        {
          timestamp: sap1AvailabilityEvent.timestamp,
          value: 1,
          variable: "sap",
        },
      ],
      ep: [
        {
          timestamp: ep1AvailabilityEvent.timestamp,
          value: 3,
          variable: "ep",
        },
        {
          timestamp: ep2AvailabilityEvent.timestamp,
          value: 11,
          variable: "ep",
        },
      ],
    };
    expect(actual).toEqual(expected);
  });
});
