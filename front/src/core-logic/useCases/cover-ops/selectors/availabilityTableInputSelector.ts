import { createSelector } from "@reduxjs/toolkit";

import { AvailabilityTableData } from "ui";
import { lastAvailabilityChangedEventByRoleSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/lastAvailabilityChangedEventByRoleSelector";
import { vehicleRoleClassification } from "core-logic/useCases/cover-ops/selectors/vehicleRoleClassification";
import * as R from "ramda";
import type {
  Availability,
  VehicleRole,
  AvailabilityChangedEvent,
} from "interfaces";

export type AvailabilityCellValue = {
  value?: number;
  theoretical?: number;
  percentage?: number;
};

const calculatePourcentage = (value?: number, theoretical?: number) => {
  if (!theoretical || R.isNil(value)) return undefined;
  return Math.round((value / theoretical) * 100);
};

const getCellValue = (
  availabilityForRole?: Availability,
): AvailabilityCellValue => {
  const value = availabilityForRole?.available;
  const theoretical = availabilityForRole
    ? R.sum(Object.values(availabilityForRole)) -
      (availabilityForRole.unavailable ?? 0)
    : undefined;
  return {
    value: value,
    theoretical: theoretical,
    percentage: calculatePourcentage(value, theoretical),
  };
};

const getAvailabilityCellByVehicleRole = (
  lastAvailabilityChangedEventByRole: Partial<
    Record<VehicleRole, AvailabilityChangedEvent>
  >,
) => (roles: VehicleRole[]) =>
  roles.reduce(
    (acc, role) => ({
      ...acc,
      [role]: getCellValue(
        lastAvailabilityChangedEventByRole[role]?.bspp_availability,
      ),
    }),
    {} as Partial<Record<VehicleRole, AvailabilityCellValue>>,
  );

export const availabilityTableInputSelector = createSelector(
  lastAvailabilityChangedEventByRoleSelector,
  (lastAvailabilityChangedEventByRole): AvailabilityTableData => {
    const availabilityTableData = R.mapObjIndexed(
      (rolesBySubCategory) =>
        R.mapObjIndexed(
          getAvailabilityCellByVehicleRole(lastAvailabilityChangedEventByRole),
          rolesBySubCategory,
        ),
      vehicleRoleClassification,
    );

    return availabilityTableData;
  },
);
