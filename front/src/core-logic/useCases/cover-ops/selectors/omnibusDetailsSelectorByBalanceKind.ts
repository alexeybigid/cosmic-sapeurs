import { createSelector } from "@reduxjs/toolkit";
import { omnibusDetailsSelector } from "core-logic/useCases/cover-ops/selectors/intermediate/rootSelectors";
import type { OmnibusBalanceKind } from "interfaces";
import { omnibusBalanceKindOptions } from "interfaces";

const makeOmnibusDetailsSelectorForBalanceKind = (
  omnibusBalanceKind: OmnibusBalanceKind,
) =>
  createSelector(omnibusDetailsSelector, (omnibusDetails) =>
    omnibusDetails.filter(
      ({ balance_kind }) => balance_kind === omnibusBalanceKind,
    ),
  );

export const omnibusDetailsSelectorByBalanceKind = omnibusBalanceKindOptions.reduce(
  (acc, omnibusBalanceKind) => {
    return {
      ...acc,
      [omnibusBalanceKind]: makeOmnibusDetailsSelectorForBalanceKind(
        omnibusBalanceKind,
      ),
    };
  },
  {} as Record<
    OmnibusBalanceKind,
    ReturnType<typeof makeOmnibusDetailsSelectorForBalanceKind>
  >,
);
