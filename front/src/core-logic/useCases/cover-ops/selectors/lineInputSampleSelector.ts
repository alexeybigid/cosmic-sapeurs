import { OperationCause } from "interfaces";
import { LineInput } from "ui";
import { createSelector } from "@reduxjs/toolkit";
import { ongoingOpChangedEventsSelector } from "./intermediate/rootSelectors";
import { availabilityChangedEventsByCoverOpsSubCategorySelector } from "core-logic/useCases/cover-ops/selectors/intermediate/availabilityChangedEventsByCoverOpsSubCategorySelector";
import * as R from "ramda";
import type { AvailabilityChangedEvent } from "interfaces";

type EpAndSapLineInputSample = LineInput<"ep" | "sap">;

type ToEPAndSAPLineInput = (param: {
  ep: AvailabilityChangedEvent[];
  sap: AvailabilityChangedEvent[];
}) => { ep: EpAndSapLineInputSample[]; sap: EpAndSapLineInputSample[] };

const toEpAndSapLineInput: ToEPAndSAPLineInput = R.mapObjIndexed(
  (events, subCategory) =>
    events.map((event) => ({
      timestamp: event.timestamp,
      value: event.bspp_availability.available ?? null,
      variable: subCategory,
    })),
);

export const availabilityToLineInputSelector = createSelector(
  availabilityChangedEventsByCoverOpsSubCategorySelector,
  toEpAndSapLineInput,
);

export const ongoingOpToLineInputSelector = createSelector(
  ongoingOpChangedEventsSelector,
  (ongoingOpChangedEvents) =>
    ongoingOpChangedEvents.map<LineInput<OperationCause>>(
      ({ cause, bspp_ongoing_ops, timestamp }) => ({
        timestamp,
        value: bspp_ongoing_ops,
        variable: cause,
      }),
    ),
);

export const victimLineInputSelector = createSelector(
  availabilityToLineInputSelector,
  ongoingOpToLineInputSelector,
  ({ sap: sapAvailabilityLineInput }, ongoingOpLineInput) => {
    const victim = "victim" as const;
    const victimOngoingOpLineInput = ongoingOpLineInput.filter(
      (sample): sample is LineInput<typeof victim> =>
        sample.variable === victim,
    );
    return [
      ...(sapAvailabilityLineInput as LineInput<"sap">[]),
      ...victimOngoingOpLineInput,
    ];
  },
);

export const fireLineInputSelector = createSelector(
  availabilityToLineInputSelector,
  ongoingOpToLineInputSelector,
  ({ ep: epAvailabilityLineInput }, ongoingOpLineInput) => {
    const fire = "fire" as const;
    const fireOngoingOpLineInput = ongoingOpLineInput.filter(
      (sample): sample is LineInput<typeof fire> => sample.variable === fire,
    );
    return [
      ...(epAvailabilityLineInput as LineInput<"ep">[]),
      ...fireOngoingOpLineInput,
    ];
  },
);
