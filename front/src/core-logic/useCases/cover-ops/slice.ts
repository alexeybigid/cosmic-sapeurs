import type {
  AvailabilityChangedEvent,
  OngoingOpChangedEvent,
} from "interfaces";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { id, sortByTimestamp, findBackwards, removeAtIndex } from "utils";
import type { Flavor, WithTimestamp } from "utils";
import * as R from "ramda";
import { format, subHours } from "date-fns";

export type PubSubStatus = "notConnected" | "open" | "closed" | "error";

export type Hours = Flavor<number, "Hours">;

type CoverOpsState = {
  availabilityChangedEvents: AvailabilityChangedEvent[];
  ongoingOpChangedEvents: OngoingOpChangedEvent[];
  isFetchingCover: boolean;
  isFetchingOps: boolean;
  fetchCoverError?: string;
  fetchOpsError?: string;
  pubSubStatus: PubSubStatus;
  numberOfHoursToShow: Hours;
};

export const sliceName = "coverOps";

const isTimestampAfterLast = <T extends WithTimestamp>(
  events: T[],
  newEvent: T,
): boolean => {
  const lastEvent = R.last(events);
  if (!lastEvent) return true;
  return lastEvent.timestamp < newEvent.timestamp;
};

const pushAfterLatestTimestamp = <T extends WithTimestamp>(
  array: T[],
  newElement: T,
) => {
  const newElementToPush = isTimestampAfterLast(array, newElement)
    ? newElement
    : { ...newElement, timestamp: R.last(array)!.timestamp };
  array.push(newElementToPush);
};

const getLatestForEachRawId = (
  events: AvailabilityChangedEvent[],
): AvailabilityChangedEvent[] => {
  const eventsByRawVehicleId = R.groupBy((e) => e.raw_vehicle_id, events);
  return R.values(
    R.mapObjIndexed((events) => R.last(events)!, eventsByRawVehicleId),
  );
};

const maxNumberOfHoursToShow = 4;

const keepOnlyRecentEvents = <
  E extends AvailabilityChangedEvent | OngoingOpChangedEvent
>(
  events: E[],
): E[] => {
  const mostRecentEvent = R.last(events);
  if (!mostRecentEvent) return events;
  const fromDate = format(
    subHours(new Date(mostRecentEvent.timestamp), maxNumberOfHoursToShow),
    "yyyy-MM-dd'T'HH:mm:ss.SSSSSS",
  );
  return events.filter((e) => fromDate < e.timestamp);
};

const coverOpsSlice = createSlice({
  name: sliceName,
  initialState: id<CoverOpsState>({
    availabilityChangedEvents: [],
    ongoingOpChangedEvents: [],
    isFetchingCover: false,
    isFetchingOps: false,
    pubSubStatus: "notConnected",
    numberOfHoursToShow: 2,
  }),
  reducers: {
    purgeOldOngoingOps: ({ ongoingOpChangedEvents, ...rest }) => {
      const events = keepOnlyRecentEvents(ongoingOpChangedEvents);
      return {
        ...rest,
        ongoingOpChangedEvents: events,
      };
    },
    purgeOldAvailabilityEvents: ({ availabilityChangedEvents, ...rest }) => {
      const events = R.concat(
        getLatestForEachRawId(availabilityChangedEvents),
        keepOnlyRecentEvents(availabilityChangedEvents),
      );
      return {
        ...rest,
        availabilityChangedEvents: sortByTimestamp(
          R.uniqBy((e) => `${e.raw_vehicle_id}_${e.timestamp}`, events),
          "asc",
        ),
      };
    },
    availabilityChanged: (
      state,
      { payload }: PayloadAction<AvailabilityChangedEvent>,
    ) => {
      const [previousEvent, previousEventIndex] = findBackwards(
        state.availabilityChangedEvents,
        (evt) => evt.raw_vehicle_id === payload.raw_vehicle_id,
      );

      const perviousEventHasSameAvailability =
        previousEvent?.latest_event_data.availability_kind ===
        payload.latest_event_data.availability_kind;

      if (perviousEventHasSameAvailability)
        removeAtIndex(state.availabilityChangedEvents, previousEventIndex ?? 0);

      pushAfterLatestTimestamp(state.availabilityChangedEvents, payload);
    },
    ongoingOpChanged: (
      state,
      { payload }: PayloadAction<OngoingOpChangedEvent>,
    ) => {
      const [previousEvent, previousEventIndex] = findBackwards(
        state.ongoingOpChangedEvents,
        (evt) => evt.raw_operation_id === payload.raw_operation_id,
      );

      const perviousEventHasSameStatus =
        previousEvent?.bspp_ongoing_ops === payload.bspp_ongoing_ops;
      const removePreviousEvent = () =>
        state.ongoingOpChangedEvents.splice(previousEventIndex!, 1);

      if (perviousEventHasSameStatus) removePreviousEvent();
      pushAfterLatestTimestamp(state.ongoingOpChangedEvents, payload);
    },
    pubSubStatusUpdated: (state, action: PayloadAction<PubSubStatus>) => {
      state.pubSubStatus = action.payload;
    },

    coverRequested: (state) => {
      state.isFetchingCover = true;
    },
    coverFetched: (
      state,
      action: PayloadAction<AvailabilityChangedEvent[]>,
    ) => {
      state.availabilityChangedEvents.push(...action.payload);
      state.isFetchingCover = false;
    },
    coverCouldNotFetch: (state, action: PayloadAction<string>) => {
      state.isFetchingCover = false;
      state.fetchCoverError = action.payload;
    },

    opsRequested: (state) => {
      state.isFetchingOps = true;
    },
    opsFetched: (state, action: PayloadAction<OngoingOpChangedEvent[]>) => {
      state.ongoingOpChangedEvents.push(...action.payload);
      state.isFetchingOps = false;
    },
    opsCouldNotFetch: (state, action: PayloadAction<string>) => {
      state.isFetchingOps = false;
      state.fetchOpsError = action.payload;
    },
    numberOfHoursToShowChanged: (state, action: PayloadAction<Hours>) => {
      state.numberOfHoursToShow = action.payload;
    },
  },
});

export const {
  reducer: coverOpsReducer,
  actions: coverOpsActions,
} = coverOpsSlice;
