import { VehicleSubCategory } from "core-logic/useCases/cover-ops/selectors/vehicleRoleClassification";

export type CoverOpsVehicleSubCategory = Extract<
  VehicleSubCategory,
  "ep" | "sap"
>;

export const coverOpsVehicleSubCategoryOptions: CoverOpsVehicleSubCategory[] = [
  "sap",
  "ep",
];
