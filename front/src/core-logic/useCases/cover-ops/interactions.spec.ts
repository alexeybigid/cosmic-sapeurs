import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import type { ReduxStore } from "core-logic/setup/store.config";
import { interactionsActions } from "core-logic/useCases/cover-ops/interactions.slice";

describe("Ui interactions", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;

  beforeEach(() => {
    const testStore = createTestStore();
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  describe("Hours to show", () => {
    it("updates the number of hours to display", async () => {
      const payload = 7;
      await store.dispatch(
        interactionsActions.numberOfHoursToShowChanged(payload),
      );

      expectStateToEqual({
        interactions: {
          numberOfHoursToShow: payload,
        },
      });
    });
  });
});
