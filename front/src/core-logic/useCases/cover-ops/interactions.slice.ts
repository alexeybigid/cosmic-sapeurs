import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { id } from "utils";
import type { Flavor } from "utils";

export type PubSubStatus = "notConnected" | "open" | "closed" | "error";

export type Hours = Flavor<number, "Hours">;

type InteractionState = {
  pubSubStatus: PubSubStatus;
  numberOfHoursToShow: Hours;
};

export const interactionSliceName = "interactions";

const interactionsSlice = createSlice({
  name: interactionSliceName,
  initialState: id<InteractionState>({
    pubSubStatus: "notConnected",
    numberOfHoursToShow: 2,
  }),
  reducers: {
    pubSubStatusUpdated: (state, action: PayloadAction<PubSubStatus>) => {
      state.pubSubStatus = action.payload;
    },
    numberOfHoursToShowChanged: (state, action: PayloadAction<Hours>) => {
      state.numberOfHoursToShow = action.payload;
    },
  },
});

export const {
  reducer: interactionsReducer,
  actions: interactionsActions,
} = interactionsSlice;
