import { DeepPartial } from "@reduxjs/toolkit";
import type { OngoingOpChangedEvent } from "interfaces";
import * as R from "ramda";

const defaultOngoingOpChangedEvent: OngoingOpChangedEvent = {
  timestamp: "2021-03-10T20:00:00.0",
  address_area: "STOU",
  area_ongoing_ops: 3,
  bspp_ongoing_ops: 14,
  cause: "victim",
  raw_operation_id: "operation_id",
  status: "opened",
};

export const makeOngoingOpChangedEvent = (
  props: DeepPartial<OngoingOpChangedEvent>,
): OngoingOpChangedEvent => {
  return R.mergeDeepRight(defaultOngoingOpChangedEvent, props);
};
