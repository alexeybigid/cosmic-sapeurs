import { InMemoryPubSubClient } from "../../secondaryAdapters/InMemoryPubSubClient";
import { ReduxStore } from "../../setup/store.config";
import {
  buildExpectStateToEqual,
  createTestStore,
  ExpectStateToEqual,
} from "../test.utils";
import {
  subscribeToOmnibusBalanceChangedEventsThunk,
  unsubscribeFromOmnibusBalanceChangedEventsThunk,
} from "./thunks/subscribeToOmnibusBalanceChangedEventsThunk";
import { InstantOnceRefresher } from "../../ports/Refresher";
import { OmnibusBalanceChangedEvent } from "interfaces";
import { loopOverDispatchBatchThunk } from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";

describe("Update Cover", () => {
  let store: ReduxStore;
  let expectStateToEqual: ExpectStateToEqual;
  let pubSubClient: InMemoryPubSubClient;
  let renderingRefresher: InstantOnceRefresher;

  beforeEach(() => {
    const testStore = createTestStore();
    pubSubClient = testStore.pubSubClient;
    renderingRefresher = testStore.renderingRefresher;
    store = testStore.store;
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to : omnibus balance changed events, then unsubscribes", async () => {
    await store.dispatch(loopOverDispatchBatchThunk());
    await store.dispatch(subscribeToOmnibusBalanceChangedEventsThunk());
    const payloadWhileSubscribed: OmnibusBalanceChangedEvent = {
      timestamp: new Date().toISOString(),
      omnibus_balance: {
        both_available: 19,
        both_on_inter_san: 23,
        pse_on_inter_fire: 0,
        pse_on_inter_san: 10,
        vsav_on_inter_san: 3,
      },
      details: [
        {
          area: "ANTO",
          balance_kind: "both_available",
          pse_id: "my_pse_id",
          vsav_id: "my_vsav_id",
        },
      ],
    };

    pubSubClient.publish({
      topic: "omnibusBalanceChanged",
      payload: payloadWhileSubscribed,
    });
    renderingRefresher.runLoop();

    expectStateToEqual({
      omnibus: {
        omnibusBalance: payloadWhileSubscribed.omnibus_balance,
        details: payloadWhileSubscribed.details,
      },
      interactions: {
        pubSubStatus: "open",
      },
    });

    await store.dispatch(unsubscribeFromOmnibusBalanceChangedEventsThunk());

    const payloadNotSubscribed: OmnibusBalanceChangedEvent = {
      timestamp: new Date().toISOString(),
      omnibus_balance: {
        both_available: 19,
        both_on_inter_san: 23,
        pse_on_inter_fire: 0,
        pse_on_inter_san: 10,
        vsav_on_inter_san: 9,
      },
      details: [
        {
          area: "STOU",
          balance_kind: "only_pse_available",
          pse_id: "other_pse_id",
          vsav_id: "other_vsav_id",
        },
      ],
    };

    pubSubClient.publish({
      topic: "omnibusBalanceChanged",
      payload: payloadNotSubscribed,
    });
    renderingRefresher.runLoop();

    expectStateToEqual({
      omnibus: {
        omnibusBalance: payloadWhileSubscribed.omnibus_balance,
        details: payloadWhileSubscribed.details,
      },
      interactions: {
        pubSubStatus: "open",
      },
    });
  });
});
