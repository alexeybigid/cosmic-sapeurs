import { id } from "utils";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type HelloSapeurState = {
  message?: string;
  isFetching: boolean;
  error?: string;
};

export const sliceName = "helloSapeur";

const helloSapeurSlice = createSlice({
  name: sliceName,
  initialState: id<HelloSapeurState>({
    isFetching: false,
  }),
  reducers: {
    helloSapeurRequested: (state) => ({ ...state, isFetching: true }),
    helloSapeurFetched: (
      state,
      action: PayloadAction<string>,
    ): HelloSapeurState => ({
      ...state,
      isFetching: false,
      message: action.payload,
    }),
    helloSapeurFailed: (
      state,
      action: PayloadAction<string>,
    ): HelloSapeurState => ({
      ...state,
      isFetching: false,
      error: action.payload,
    }),
  },
});

export const {
  reducer: helloSapeurReducer,
  actions: helloSapeurActions,
} = helloSapeurSlice;
