import { ActualBatchHandler } from "core-logic/ports/BatchHandler";
import { IntervalRefresher } from "core-logic/ports/Refresher";
import { ActualHttpClient } from "core-logic/secondaryAdapters/ActualHttpClient";
import { InMemoryHttpClient } from "core-logic/secondaryAdapters/InMemoryHttpClient";
import { InMemoryPubSubClient } from "core-logic/secondaryAdapters/InMemoryPubSubClient";
import { WebSocketPubSubClient } from "core-logic/secondaryAdapters/WebSocketPubSubClient";
import { ENV } from "environmentVariables";
import { configureReduxStore } from "./store.config";

type GetStoreProps = {
  httpClientKind: "IN_MEMORY" | "HTTP";
  pubSubClientKind: "IN_MEMORY" | "WEB_SOCKET";
};

// In development, you might want to change speed to match the replayer's one
const speed: number = 1;

export const getStore = ({
  httpClientKind,
  pubSubClientKind,
}: GetStoreProps) => {
  const httpClient =
    httpClientKind === "IN_MEMORY"
      ? new InMemoryHttpClient(500)
      : new ActualHttpClient();

  const pubSubClient =
    pubSubClientKind === "IN_MEMORY"
      ? new InMemoryPubSubClient()
      : new WebSocketPubSubClient(`ws://${ENV.backendHost}/api/ws`);

  return configureReduxStore({
    httpClient,
    pubSubClient,
    renderingRefresher: new IntervalRefresher(5000),
    purgeRefresher: new IntervalRefresher((3600 * 1000) / speed),
    batchHandler: new ActualBatchHandler(),
  });
};
