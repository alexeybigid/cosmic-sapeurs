import { combineReducers } from "@reduxjs/toolkit";
import {
  interactionSliceName,
  interactionsReducer,
} from "core-logic/useCases/cover-ops/interactions.slice";
import {
  coverSliceName,
  coverReducer,
} from "core-logic/useCases/cover-ops/cover.slice";
import {
  opsSliceName,
  opsReducer,
} from "core-logic/useCases/cover-ops/ops.slice";
import {
  helloSapeurReducer,
  sliceName as helloSapeurName,
} from "core-logic/useCases/helloSapeur/slice";
import {
  omnibusSliceName,
  omnibusReducer,
} from "core-logic/useCases/cover-ops/omnibus.slice";

export const rootReducer = combineReducers({
  [helloSapeurName]: helloSapeurReducer,
  [interactionSliceName]: interactionsReducer,
  [opsSliceName]: opsReducer,
  [coverSliceName]: coverReducer,
  [omnibusSliceName]: omnibusReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
