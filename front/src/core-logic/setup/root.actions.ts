import { helloSapeurThunk } from "core-logic/useCases/helloSapeur/helloSapeur.thunk";
import {
  subscribeToAvailabilityChangedEventsThunk,
  unsubscribeFromAvailabilityChangedEventsThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToAvailabilityChangedEventsThunk";
import {
  subscribeToOngoingOpChangedEventsThunk,
  unsubscribeFromOngoingOpChangedEventsThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToOngoingOpChangedEventsThunk";
import {
  subscribeToOmnibusBalanceChangedEventsThunk,
  unsubscribeFromOmnibusBalanceChangedEventsThunk,
} from "core-logic/useCases/cover-ops/thunks/subscribeToOmnibusBalanceChangedEventsThunk";
import {
  loopOverDispatchBatchThunk,
  stopLoopOverDispatchBatchThunk,
} from "core-logic/useCases/cover-ops/thunks/dispatchBatchThunk";
import { fetchCoverThunk } from "core-logic/useCases/cover-ops/thunks/fetchCoverThunk";
import { fetchOpsThunk } from "core-logic/useCases/cover-ops/thunks/fetchOpsThunk";
import { fetchOmnibusThunk } from "core-logic/useCases/cover-ops/thunks/fetchOmnibusThunk";

import {
  interactionsActions,
  Hours,
} from "core-logic/useCases/cover-ops/interactions.slice";

export const actions = {
  helloSapeurThunk,

  numberOfHoursToShowChanged: (hours: Hours) =>
    interactionsActions.numberOfHoursToShowChanged(hours),

  subscribeToAvailabilityChangedEventsThunk,
  unsubscribeFromAvailabilityChangedEventsThunk,

  subscribeToOngoingOpChangedEventsThunk,
  unsubscribeFromOngoingOpChangedEventsThunk,

  subscribeToOmnibusBalanceChangedEventsThunk,
  unsubscribeFromOmnibusBalanceChangedEventsThunk,

  loopOverDispatchBatchThunk,
  stopLoopOverDispatchBatchThunk,

  fetchCoverThunk,
  fetchOpsThunk,
  fetchOmnibusThunk,
};
