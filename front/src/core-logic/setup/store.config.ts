import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import type { Action, ThunkAction, DeepPartial } from "@reduxjs/toolkit";
import { enableBatching } from "redux-batched-actions";

import type { HttpClient } from "core-logic/ports/HttpClient";
import type { PubSubClient } from "core-logic/ports/PubSubClient";
import type { RootState } from "core-logic/setup/root.reducer";
import type { Refresher } from "core-logic/ports/Refresher";
import { interactionsActions } from "core-logic/useCases/cover-ops/interactions.slice";
import { rootReducer } from "./root.reducer";
import { BatchHandler } from "core-logic/ports/BatchHandler";

export type Dependencies = {
  httpClient: HttpClient;
  pubSubClient: PubSubClient;
  renderingRefresher: Refresher;
  purgeRefresher: Refresher;
  batchHandler: BatchHandler;
};

export const configureReduxStore = (
  dependencies: Dependencies,
  preloadedState?: DeepPartial<RootState>,
) => {
  const store = configureStore({
    reducer: enableBatching(rootReducer),
    middleware: getDefaultMiddleware({
      thunk: {
        extraArgument: dependencies,
      },
    }),
    // middleware: [
    //   ...(ENV.logActionsDuration ? [logActionsDurationMiddleware] : []),
    //   ...getDefaultMiddleware({
    //     thunk: {
    //       extraArgument: dependencies,
    //     },
    //   }),
    // ],
    preloadedState,
  });

  dependencies.pubSubClient.setInternalSocketEventsHandler(
    (e: { type: string }) => {
      console.log("PubSub Connection Event : ", e);
      switch (e.type) {
        case "close":
          store.dispatch(interactionsActions.pubSubStatusUpdated("closed"));
          return;
        case "error":
          store.dispatch(interactionsActions.pubSubStatusUpdated("error"));
          return;
        case "open":
          store.dispatch(interactionsActions.pubSubStatusUpdated("open"));
          return;
        default:
          console.warn("No redux action to handle this event");
          return;
      }
    },
  );

  return store;
};

export type ReduxStore = ReturnType<typeof configureReduxStore>;

export type AppThunk = ThunkAction<
  void,
  RootState,
  Dependencies,
  Action<string>
>;
