import * as R from "ramda";
import {
  AppEvent,
  NarrowEvent,
  PubSubClient,
} from "core-logic/ports/PubSubClient";

type MessageHandler = (event: MessageEvent) => void;
type MessageHandlersByTopic = Partial<
  Record<AppEvent["topic"], MessageHandler>
>;

export class WebSocketPubSubClient implements PubSubClient {
  constructor(private socketUrl: string) {}

  private socket?: WebSocket;
  private messageHandlersByTopic: MessageHandlersByTopic = {};

  private _internalSocketEventsHandler?: (e: any) => void;

  public setInternalSocketEventsHandler(cb: (e: any) => void) {
    this._internalSocketEventsHandler = cb;
  }

  subscribe<T extends AppEvent["topic"]>(
    topic: T,
    callback: (event: NarrowEvent<T>) => void,
  ): void {
    if (!R.isNil(this.messageHandlersByTopic[topic])) {
      console.warn("You have already subscribed to this topic");
      return;
    }

    if (!this.socket) {
      const socket = new WebSocket(this.socketUrl);

      const handler = (event: { type: string }) => {
        this._internalSocketEventsHandler?.(event);
      };

      socket.addEventListener("close", handler);
      socket.addEventListener("error", handler);

      socket.addEventListener("open", (openEvent) => {
        this._internalSocketEventsHandler?.(openEvent);
        this.subscribeToMessage(socket, topic, callback);
      });

      this.socket = socket;
      return;
    }

    this.subscribeToMessage(this.socket, topic, callback);
  }

  public unsubscribe(topic: AppEvent["topic"]) {
    if (!this.socket) {
      console.warn(
        `No sockets are connected, so you cannot unsubscribe from ${topic}`,
      );
      return;
    }
    const handlerForTopic = this.messageHandlersByTopic[topic];
    if (!handlerForTopic) {
      console.warn(
        `Tried to unsubscribe from ${topic} but it was not subscribed in the first place`,
      );
      return;
    }
    this.socket.removeEventListener("message", handlerForTopic);
  }

  private subscribeToMessage<T extends AppEvent["topic"]>(
    socket: WebSocket,
    topic: T,
    callback: (event: NarrowEvent<T>) => void,
  ) {
    const messageHandler = (event: MessageEvent) => {
      const eventData = JSON.parse(event.data);
      if (eventData.topic === topic) callback(eventData);
    };

    socket.addEventListener("message", messageHandler);

    this.messageHandlersByTopic[topic] = messageHandler;
  }

  send(command: unknown) {
    if (!this.socket) throw new Error("No websocket connections open");
    this.socket.send(JSON.stringify(command));
  }
}
