import { Flavor } from "./flavor";

export type DateString = Flavor<string, "DateString">;
