import pandas as pd

from shared.sapeurs_events import Event

__all__ = ["convert_int64_to_string_in_df", "event_to_series", "series_to_event"]


def event_to_series(event: Event) -> pd.Series:
    event_dict = event.as_dict()
    return pd.Series(event_dict)


# def series_to_event(series: pd.Series) -> Event:
#     event_dict = series.to_dict()
#     return Event(
#         topic=event_dict["topic"],
#         uuid=event_dict["uuid"],
#         data=event_dict["data"],
#         dispatched_at=event_dict.get("dispatched_at"),
#     )


# def series_to_event(series: pd.Series) -> Event:
#     data_dict = dict(series[~series.index.isin(["uuid", "topic"])])
#     if type(data_dict["timestamp"]) == pd.Timestamp:
#         data_dict["timestamp"] = from_timestamp(data_dict["timestamp"])

#     topic = series.topic
#     if topic not in topic_to_types:
#         raise ValueError(f"Unknown topic {topic}")
#     EventDataType = topic_to_types.get(topic)["data"]
#     EventType = topic_to_types.get(topic)["event"]
#     data_field_names = [field.name for field in topic_to_types[topic]["fields"]]
#     data_kwargs = {field_name: data_dict[field_name] for field_name in data_field_names}
#     data = EventDataType(**data_kwargs)
#     return EventType(
#         topic=series.topic,
#         uuid=series.uuid,
#         data=data,
#     )


# topic_to_types: Dict[CoverOpsDomainTopic, DictWithKeysDataEvent] = {
#     "vehicle_changed_status": {
#         "data": VehicleEventData,
#         "event": VehicleEvent,
#         "fields": list(fields(VehicleEventData)),
#     },
#     "operation_changed_status": {
#         "data": OperationEventData,
#         "event": OperationEvent,
#         "fields": [
#             field
#             for field in list(fields(OperationEventData))
#             if field.name != "is_ongoing"
#         ],
#     },
# }


# class UnknownTopic(Exception):
#     pass


# def make_event_as_dict_to_entity(EntityClass):
#     def series_to_entity(event_as_dict: TypedDict) -> EventEntity:
#         # series_as_dict = (
#         #     series.to_dict()
#         # )  # {uuid: .., topic: .., data: .., dispatched_at: .. }

#         # data_dict = event_as_dict.get("data")

#         # data = EventDataClass.from_dict(data_dict)

#         return EntityClass.from_dict(event_as_dict)

#     return series_to_entity


# def series_to_entity(series: pd.Series) -> EventEntity:
#     series_as_dict = (
#         series.to_dict()
#     )  # {uuid: .., topic: .., data: .., dispatched_at: .. }

#     # TODO: is it really usefull to give the topic in args?
#     assert topic == series_as_dict.get("topic")
#     data_dict = series_as_dict.get("data")

#     EventDataType = topic_to_types.get(topic)["data"]
#     EntityType = topic_to_entity.get(topic)

#     data = EventDataType(**data_dict)

#     return EntityType(uuid=series.uuid, data=data)


# def convert_int64_to_string_in_df(
#     df: pd.DataFrame,
# ):
#     for column in df:
#         if df[column].dtype == "int64":
#             df[column] = df[column].astype(str)
