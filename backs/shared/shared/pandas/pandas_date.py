import pandas as pd

from shared.helpers.date import DateStr, date_format


def to_timestamp(date: DateStr) -> pd.Timestamp:
    return pd.Timestamp(date).tz_localize(None)


def from_timestamp(timestamp: pd.Timestamp) -> DateStr:
    string_timestamp = timestamp.strftime(date_format)
    return DateStr(string_timestamp)
