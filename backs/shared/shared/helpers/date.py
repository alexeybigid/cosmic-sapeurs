import re
from datetime import datetime, timedelta
from typing import NewType

from dateutil import tz
from marshmallow.exceptions import ValidationError
from marshmallow.fields import Field

from shared.exceptions import InvalidEventFormatError

date_format = "%Y-%m-%dT%H:%M:%S.%f"
datestr_regex = r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})T(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})(\.(?P<decimal>\d{1,6}))?"


class DateStrField(Field):
    def _serialize(self, value, attr, obj, **kwargs):
        return str(value)

    def _deserialize(self, value, attr, data, **kwargs):
        if value == "None":
            return None
        if isinstance(value, str):
            if is_datestr_format(value):
                return DateStr(value)
            else:
                raise ValidationError(
                    f"Could not convert value {value} to DateStr: Expects format {date_format}, got {value}"
                )
        else:
            raise ValidationError(
                f"Could not convert value {value} to DateStr: Expects type str, got {type(value)}"
            )


DateStr = NewType("DateStr", str)


class InvalidDateFormat(Exception):
    pass


def _remove_space(string: str) -> str:
    return string.replace(" ", "")


def is_datestr_format(date: str) -> bool:
    try:
        datetime.strptime(date, date_format)
        return True
    except:
        return False


def from_datetime(date: datetime) -> DateStr:
    return DateStr(date.strftime(date_format))


def to_datetime(date: DateStr) -> datetime:
    return datetime.strptime(date, date_format)


def from_string(date_as_string: str, format: str = "%Y-%m-%d %H:%M:%S") -> DateStr:
    """Convert from string date with specified format to DateStr"""
    date = datetime.strptime(_remove_space(date_as_string), _remove_space(format))
    return from_datetime(date)


def to_string(date: DateStr, format: str = "%Y-%m-%d %H:%M:%S") -> str:
    """Convert from DateStr to string with specified format"""
    return to_datetime(date).strftime(format)


def from_string_with_tz(date: str, format="%Y-%m-%dT%H:%M:%S.%f%z") -> DateStr:
    date_strptime = datetime.strptime(date, format)
    local_tz = tz.gettz("Europe/Paris")
    local_datestr = from_datetime(date_strptime.astimezone(local_tz))
    return local_datestr


def from_regex(date: str, regex: str) -> DateStr:
    """Make sure regex has following named groups : year, month, day, hour, minute, second, and eventually decimal"""
    m = re.search(regex, date)
    if m is None:
        raise InvalidDateFormat(f"Received incorrect string {date}, expected {regex}.")
    try:
        decimal = m.group("decimal")
    except IndexError:
        decimal = None

    return DateStr(
        date_format.replace("%Y", m.group("year"))
        .replace("%m", m.group("month"))
        .replace("%d", m.group("day"))
        .replace("%H", m.group("hour"))
        .replace("%M", m.group("minute"))
        .replace("%S", m.group("second"))
        .replace("%f", decimal or "000000")
    )


def sub_milliseconds(date: DateStr, ms: int) -> DateStr:
    return from_datetime(to_datetime(date) - timedelta(milliseconds=ms))
