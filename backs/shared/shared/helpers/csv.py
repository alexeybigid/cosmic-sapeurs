import csv
from pathlib import Path
from typing import List


def writerow(csv_path: Path, row: List):
    with csv_path.open("a") as f:
        writer = csv.writer(f)
        writer.writerow(row)


def reset_csv_file(csv_path: Path):
    try:
        csv_path.unlink()
    except FileNotFoundError:
        pass
