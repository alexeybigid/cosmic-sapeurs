import os
from typing import Union
from shared.helpers.prepare_get_env_variable import prepare_get_env_variable


class Logger:
    def __init__(self):
        get_env_variable = prepare_get_env_variable(os.environ.get)
        self.mode = get_env_variable(
            "LOGGER_MODE", ["ALL", "INFO", "WARN", "NONE"], "ALL"
        )

    def info(self, *argv: Union[str, object]):
        if self.mode in ["ALL", "INFO"]:
            print(*argv)

    def warn(self, *argv: Union[str, object]):
        if self.mode in ["ALL", "WARN"]:
            print(*argv)


logger = Logger()
