import asyncio
from functools import wraps
from time import time
from typing import Callable, List, Literal, Optional

from shared.exceptions import SapeursError
from shared.helpers.logger import logger


def timeit(func_name: str = "function", min_duration: Optional[float] = None):
    def _timeit_decorator(func: Callable):
        @wraps(func)
        def _timeit_wrapper(*args, **kwargs):
            start = time()
            try:
                return func(*args, **kwargs)
            finally:
                duration = (time() - start) * 1000
                if min_duration is None or duration > min_duration:
                    print(f"\n[{func_name}] Execution took {duration} ms")

        return _timeit_wrapper

    return _timeit_decorator


def async_timeit(func_name: str = "function"):
    """See : https://gist.github.com/Integralist/77d73b2380e4645b564c28c53fae71fb"""

    def _timeit_decorator(func):
        async def process(func, *args, **params):
            if asyncio.iscoroutinefunction(func):
                return await func(*args, **params)
            else:
                return func(*args, **params)

        async def helper(*args, **params):
            start = time()
            result = await process(func, *args, **params)

            # Test normal function route...
            # result = await process(lambda *a, **p: print(*a, **p), *args, **params)

            print(f"[{func_name}] Async execution took {(time() - start) * 1000} ms")
            return result

        return helper

    return _timeit_decorator


ExceptionMode = Literal["PASS", "RAISE"]
exception_mode_options: List[ExceptionMode] = ["PASS", "RAISE"]


def make_exception_logger(exc_mode: ExceptionMode) -> Callable:
    def exception_logger():
        def _exception_decorator(function):
            """
            A decorator that wraps the passed in function and logs
            exceptions should one occur
            """

            @wraps(function)
            def wrapper(*args, **kwargs):
                try:
                    return function(*args, **kwargs)
                except Exception as e:
                    # log the exception
                    err = f"There was an exception in  {function.__name__} : \n "
                    err += str(e)
                    logger.warn(err)
                    if exc_mode == "RAISE":
                        raise

            return wrapper

        return _exception_decorator

    return exception_logger
