from typing import Callable, List, Optional, TypeVar


class UnexpectedEnvironmentVariableException(Exception):
    pass


GenericVariable = TypeVar("GenericVariable", bound=str)


def prepare_get_env_variable(os_environ_get: Callable):
    def get_env_variable(
        variable_name: str,
        variable_options: List[GenericVariable],
        default: Optional[str] = None,
    ) -> GenericVariable:
        variable: Optional[GenericVariable] = os_environ_get(variable_name) or default
        if not variable in variable_options:
            raise UnexpectedEnvironmentVariableException(
                f"\n \n Got {variable} for {variable_name}, expected one of {variable_options}"
            )
        return variable

    return get_env_variable
