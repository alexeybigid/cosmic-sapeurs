import json
from dataclasses import asdict
from typing import Dict, TypeVar


def dataclass_to_dict(dataclass) -> Dict:
    if hasattr(dataclass, "asdict"):
        return dataclass.asdict()
    return asdict(dataclass)


def dumps_dataclass(dataclass) -> str:
    return json.dumps(dataclass_to_dict(dataclass))
