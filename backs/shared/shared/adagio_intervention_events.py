from dataclasses import dataclass, field
from typing import Dict, Literal, Optional, Union

from marshmallow.validate import Regexp

from shared.adagio_date import adagio_etat_intervention_regex
from shared.adagio_topics import (
    TopicAdagioInterventionChangedStatus,
    TopicAdagioInterventionCreated,
    TopicAdagioInterventionReinforced,
)
from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import OperationCause
from shared.external_event import ExternalEvent
from shared.marshmallow_validate import marshmallow_validate, with_from_dict_classmethod

AdagioLibelleStatutIntervention = Literal[
    "Début",
    "Envoi",
    "Réception",
    "Validation manuelle",
    "Validation automatique",
    "Annulation",
    "Fin",
    "En cours",
    "Rapports  rédigés",
    "Cloture automatique",
    "Cloture intervention",
    "Cloture opération",
]


@dataclass
class Cstc:
    Libelle: Area


@dataclass
class Adresse:
    IdObjetGeo: Optional[int]
    CodeTypeReseau: Optional[str]
    CodeTypeAdresse: Optional[str]
    Commentaire: Optional[str]
    X: Optional[float]
    Y: Optional[float]
    LibelleComplet: Optional[str]
    TypeInfrastructure: Optional[str]
    CategorieFonctionnelle: Optional[str]


@dataclass
class Emetteur:
    Libelle: str


AdagioProcedure = Literal["R", "O", "B", "H", "V"]


@dataclass
class OdeResume:
    LibelleMotif: str
    Delestage: Optional[int]
    Procedure: AdagioProcedure
    Precurseur: Optional[bool]
    AlerteCoordination: Optional[bool]
    RenfortMoyenDeleste: Optional[bool]
    RenfortMoyenNonDeleste: Optional[bool]
    Emetteur: Optional[Emetteur]
    EtatIntervention: str  # timestamp with format "09/12/2020 17:35:24"
    # = field(
    #    metadata={"marshmallow_field": Regexp(adagio_etat_intervention_regex)}
    # )  # "09/12/2020 17:35:24"
    Cstc: Cstc
    Motif: str  # regex "{1|2|3|4}11"
    NbEnginsDepart: Optional[int]
    ListeEnginsDepart: str  # "\nMMA sélectionnés : EPA BSPP_1_BSLT-PSE_1_BSLT-VLR BSPP_1_STHO-FA_1_LAND-\nObservations : MMA manquants: \n\n\nObservations sur le motif d'alerte: CT: Zone de Defense - Ile de France / BSPP Admin - NA: NORMAL",
    NbEnginsPartis: Optional[int]
    NbEnginsPresentes: Optional[int]
    Adresse: Optional[Adresse]
    IdIntervention: int


@dataclass
class Intervention:
    ObservationMotifAlerte: Optional[str]
    ObservationAdresse: Optional[str]
    ObservationMMA: Optional[str]
    ObservationECTS: Optional[str]
    IdIntervention: int
    RenseignementComplementaire: Optional[str]


@dataclass
class StatutIntervention:
    IdStatutIntervention: int
    AbregeStatutIntervention: str
    LibelleStatutIntervention: AdagioLibelleStatutIntervention


@dataclass
class AdagioInterventionChangedStatusEventData:
    IdHistoriqueIntervention: int
    StatutIntervention: StatutIntervention
    Intervention: Intervention
    GroupeHoraireModificationStatut: str  # format : "2020-12-15T10:38:23.4534206+01:00"
    GroupeHoraireModificationStatutGMT: str


@with_from_dict_classmethod()
@dataclass
class AdagioInterventionCreatedEventData:
    OdeResume: OdeResume
    Intervention: Optional[Intervention]


@with_from_dict_classmethod()
@dataclass
class AdagioInterventionReinforcedEventData(AdagioInterventionCreatedEventData):
    pass


@with_from_dict_classmethod()
@dataclass
class AdagioInterventionCreatedEvent(ExternalEvent):
    data: AdagioInterventionCreatedEventData
    topic: TopicAdagioInterventionCreated = "adagio_intervention_created"


@with_from_dict_classmethod()
@dataclass
class AdagioInterventionReinforcedEvent(ExternalEvent):
    data: AdagioInterventionReinforcedEventData
    topic: TopicAdagioInterventionReinforced = "adagio_intervention_reinforced"


@with_from_dict_classmethod()
@dataclass
class AdagioInterventionChangedStatusEvent(ExternalEvent):
    data: AdagioInterventionChangedStatusEventData
    topic: TopicAdagioInterventionChangedStatus = "adagio_intervention_changed_status"


AdagioInterventionEvent = Union[
    AdagioInterventionCreatedEvent,
    AdagioInterventionReinforcedEvent,
    AdagioInterventionChangedStatusEvent,
]


# TODO : complete libelleMotifToCause with the mapping from raw cause to sapeurs cause
libelleMotifToCause: Dict[str, OperationCause] = {"Feu de ...": "fire"}


adagio_inter_created_date_format = "%d/%m/%Y %H:%M:%S"  # "09/12/2020 17:35:24"
adagio_inter_reinforced_date_format = adagio_inter_created_date_format
