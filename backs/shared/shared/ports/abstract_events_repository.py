import abc
from typing import Optional

from shared.events.event_entity import EventEntity


class AbstractEventsRepository(abc.ABC):
    def add(self, event) -> None:
        self._add(event)

    @abc.abstractmethod
    def _add(self, event_entity: EventEntity) -> None:
        raise NotImplementedError


class AlreadyExistingEventUuid(Exception):
    pass


class AbstractSapeursEventsRepository(AbstractEventsRepository):
    def add(self, event: EventEntity) -> None:
        if self._match_uuid(event.uuid):
            raise AlreadyExistingEventUuid(f"Duplicated uuid : {event.uuid}")
        self._add(event)

    @abc.abstractmethod
    def _match_uuid(self, uuid: str) -> Optional[EventEntity]:
        raise NotImplementedError
