from dataclasses import asdict, dataclass, field
from typing import Any, Dict, List, Optional, Type, TypeVar

from shared.cover_ops_topics import (
    CoverOpsDomainTopic,
    TopicAvailabilityChanged,
    TopicOngoingOpChanged,
)
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.marshmallow_validate import with_from_dict_classmethod

from .cover_ops_topics import (
    TopicConvertersCatchesUpAccumulatedEvents,
    TopicConvertersInitializedLastStatusOfAllVehicles,
    TopicOmnibusBalanceChanged,
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
)
from .data_transfert_objects.operation_event_data import OperationEventData
from .data_transfert_objects.vehicle_event_data import VehicleEventData
from .helpers.date import DateStr, DateStrField

_T_SchemaClass = TypeVar("_T_SchemaClass")


@dataclass
class Event:
    uuid: str
    data: Any  # = None
    topic: CoverOpsDomainTopic
    # dispatched_at: Optional[DateStr] = None
    dispatched_at: Optional[DateStr] = field(
        metadata={
            "marshmallow_field": DateStrField(),
        },  # Custom marshmallow field
        default_factory=lambda: None,
    )

    def as_dict(self):
        return {
            "dispatched_at": str(self.dispatched_at),
            "topic": self.topic,
            "uuid": self.uuid,
            "data": asdict(self.data),
        }

    @classmethod
    def from_dict(cls: Type[_T_SchemaClass], input: Dict) -> _T_SchemaClass:
        raise NotImplementedError


operation_changed_status: TopicOperationChangedStatus = "operation_changed_status"


@with_from_dict_classmethod()
@dataclass
class OperationEvent(Event):
    data: OperationEventData
    topic: TopicOperationChangedStatus = operation_changed_status


vehicle_changed_status: TopicVehicleChangedStatus = "vehicle_changed_status"


@with_from_dict_classmethod()
@dataclass
class VehicleEvent(Event):
    data: VehicleEventData
    topic: TopicVehicleChangedStatus = vehicle_changed_status


@with_from_dict_classmethod()
@dataclass
class ConvertersVehiclesInitEvent(Event):
    data: List[Event]
    topic: TopicConvertersInitializedLastStatusOfAllVehicles = (
        "converters_initialized_last_status_of_all_vehicles"
    )

    def as_dict(self):
        return {
            "dispatched_at": str(self.dispatched_at),
            "topic": self.topic,
            "uuid": self.uuid,
            "data": [event.as_dict() for event in self.data],
        }


@with_from_dict_classmethod()
@dataclass
class ConvertersCatchesUpEvent(Event):
    data: List[Event]
    topic: TopicConvertersCatchesUpAccumulatedEvents = (
        "converters_catches_up_accumulated_events"
    )

    def as_dict(self):
        return {
            "dispatched_at": str(self.dispatched_at),
            "topic": self.topic,
            "uuid": self.uuid,
            "data": [event.as_dict() for event in self.data],
        }


@dataclass
class AvailabilityChangedEvent(Event):
    data: AvailabilityChangedEventData
    topic: TopicAvailabilityChanged = "availabilityChanged"


@dataclass
class OngoingOpChangedEvent(Event):
    data: OngoingOpChangedEventData
    topic: TopicOngoingOpChanged = "ongoingOpChanged"


@dataclass
class OmnibusBalanceChangedEvent(Event):
    data: OmnibusBalanceChangedEventData
    topic: TopicOmnibusBalanceChanged = "omnibusBalanceChanged"
