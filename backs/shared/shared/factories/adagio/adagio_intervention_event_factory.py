from typing import Optional

from shared.adagio_intervention_events import (
    AdagioInterventionChangedStatusEvent,
    AdagioInterventionChangedStatusEventData,
    AdagioInterventionCreatedEvent,
    AdagioInterventionCreatedEventData,
    AdagioInterventionReinforcedEvent,
    AdagioInterventionReinforcedEventData,
    AdagioProcedure,
    Adresse,
    Cstc,
    Emetteur,
    Intervention,
    OdeResume,
    StatutIntervention,
)
from shared.data_transfert_objects.area import Area
from shared.helpers.date import DateStr, to_string


def make_adagio_intervention_created_event_data(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    libelle_motif: str = None,
    etat_intervention: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    adagio_procedure: AdagioProcedure = None,
) -> AdagioInterventionCreatedEventData:
    id_intervention = id_intervention or 12019366
    if etat_intervention is None:
        if timestamp is None:
            etat_intervention = "09/12/2020 17:35:24"
        else:
            etat_intervention = to_string(timestamp, format="%d/%m/%Y %H:%M:%S")
    return AdagioInterventionCreatedEventData(
        OdeResume=OdeResume(
            LibelleMotif=libelle_motif or "Feu de ... ",
            Delestage=0,
            Procedure=adagio_procedure or "R",
            Precurseur=False,
            AlerteCoordination=False,
            RenfortMoyenDeleste=False,
            RenfortMoyenNonDeleste=False,
            Emetteur=Emetteur(Libelle="blondela"),
            EtatIntervention=etat_intervention,
            Cstc=Cstc(Libelle=cstc or "STHO"),
            Motif=motif or "111",
            NbEnginsDepart=4,
            ListeEnginsDepart="\nMMA sélectionnés =EPA BSPP_1_BSLT-PSE_1_BSLT-VLR BSPP_1_STHO-FA_1_LAND-\nObservations =MMA manquants: \n\n\nObservations sur le motif d'alerte: CT: Zone de Defense - Ile de France / BSPP Admin - NA: NORMAL",
            NbEnginsPartis=0,
            NbEnginsPresentes=0,
            Adresse=Adresse(
                IdObjetGeo=4278312,
                CodeTypeReseau="ROUTIER",
                CodeTypeAdresse="ADR_VP",
                Commentaire="",
                X=649833.5,
                Y=6863363.0,
                LibelleComplet=address or "1 RUE DE L'ELYSEE 75008 8EME ARRONDISSEMENT",
                TypeInfrastructure="",
                CategorieFonctionnelle="RUE",
            ),
            IdIntervention=id_intervention,
        ),
        Intervention=Intervention(
            ObservationMotifAlerte=None,
            ObservationAdresse=None,
            ObservationMMA=None,
            ObservationECTS=None,
            IdIntervention=id_intervention,
            RenseignementComplementaire="",
        ),
    )


def make_adagio_intervention_reinforced_event_data(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    libelle_motif: str = None,
    etat_intervention: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    adagio_procedure: AdagioProcedure = None,
) -> AdagioInterventionReinforcedEventData:
    id_intervention = id_intervention or 12019366
    if etat_intervention is None:
        if timestamp is None:
            etat_intervention = "09/12/2020 17:35:24"
        else:
            etat_intervention = to_string(timestamp, format="%d/%m/%Y %H:%M:%S")
    return AdagioInterventionReinforcedEventData(
        OdeResume=OdeResume(
            LibelleMotif=libelle_motif or "Feu de ... ",
            Delestage=0,
            Procedure=adagio_procedure or "R",
            Precurseur=False,
            AlerteCoordination=False,
            RenfortMoyenDeleste=False,
            RenfortMoyenNonDeleste=False,
            Emetteur=Emetteur(Libelle="blondela"),
            EtatIntervention=etat_intervention,
            Cstc=Cstc(Libelle=cstc or "STHO"),
            Motif=motif or "111",
            NbEnginsDepart=4,
            ListeEnginsDepart="\nMMA sélectionnés =EPA BSPP_1_BSLT-PSE_1_BSLT-VLR BSPP_1_STHO-FA_1_LAND-\nObservations =MMA manquants: \n\n\nObservations sur le motif d'alerte: CT: Zone de Defense - Ile de France / BSPP Admin - NA: NORMAL",
            NbEnginsPartis=0,
            NbEnginsPresentes=0,
            Adresse=Adresse(
                IdObjetGeo=4278312,
                CodeTypeReseau="ROUTIER",
                CodeTypeAdresse="ADR_VP",
                Commentaire="",
                X=649833.5,
                Y=6863363.0,
                LibelleComplet=address or "1 RUE DE L'ELYSEE 75008 8EME ARRONDISSEMENT",
                TypeInfrastructure="",
                CategorieFonctionnelle="RUE",
            ),
            IdIntervention=id_intervention,
        ),
        Intervention=Intervention(
            ObservationMotifAlerte=None,
            ObservationAdresse=None,
            ObservationMMA=None,
            ObservationECTS=None,
            IdIntervention=id_intervention,
            RenseignementComplementaire="",
        ),
    )


def make_adagio_intervention_changed_status_event_data(
    id_intervention: int = None,
    libelle_statut_intervention: Optional[str] = None,
    groupe_horaire_modification_statut: str = None,
    timestamp: Optional[DateStr] = None,
) -> AdagioInterventionChangedStatusEventData:
    if groupe_horaire_modification_statut is None:
        if timestamp is None:
            groupe_horaire_modification_statut = "2020-12-15T10:38:23.4534206+01:00"
        else:
            groupe_horaire_modification_statut = (
                to_string(timestamp, format="%Y-%m-%dT%H:%M:%S.%f") + "+01:00"
            )

    return AdagioInterventionChangedStatusEventData(
        IdHistoriqueIntervention=27,
        StatutIntervention=StatutIntervention(
            IdStatutIntervention=4,
            AbregeStatutIntervention="VAM",
            LibelleStatutIntervention=libelle_statut_intervention
            or "Validation manuelle",
        ),
        Intervention=Intervention(
            ObservationMotifAlerte=None,
            ObservationAdresse=None,
            ObservationMMA=None,
            ObservationECTS=None,
            IdIntervention=id_intervention or 12434,
            RenseignementComplementaire="",
        ),
        GroupeHoraireModificationStatut=groupe_horaire_modification_statut
        or "2020-12-15T10:38:23.4534206+01:00",
        GroupeHoraireModificationStatutGMT="2020-12-15T09:38:23.4534206Z",
    )


def make_adagio_intervention_created_event(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    libelle_motif: str = None,
    etat_intervention: str = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
    adagio_procedure: AdagioProcedure = None,
) -> AdagioInterventionCreatedEvent:
    data = make_adagio_intervention_created_event_data(
        id_intervention=id_intervention,
        cstc=cstc,
        motif=motif,
        libelle_motif=libelle_motif,
        etat_intervention=etat_intervention,
        timestamp=timestamp,
        address=address,
        adagio_procedure=adagio_procedure,
    )
    return AdagioInterventionCreatedEvent(
        topic="adagio_intervention_created", data=data
    )


def make_adagio_intervention_reinforced_event(
    id_intervention: int = None,
    cstc: Area = None,
    motif: str = None,
    libelle_motif: str = None,
    etat_intervention: Optional[str] = None,
    timestamp: Optional[DateStr] = None,
    address: str = None,
) -> AdagioInterventionReinforcedEvent:
    return AdagioInterventionReinforcedEvent(
        topic="adagio_intervention_reinforced",
        data=make_adagio_intervention_reinforced_event_data(
            id_intervention=id_intervention,
            cstc=cstc,
            motif=motif,
            libelle_motif=libelle_motif,
            etat_intervention=etat_intervention,
            timestamp=timestamp,
            address=address,
        ),
    )


def make_adagio_intervention_changed_status_event(
    id_intervention: int = None,
    libelle_statut_intervention: str = None,
    groupe_horaire_modification_statut: str = None,
    timestamp: Optional[DateStr] = None,
) -> AdagioInterventionChangedStatusEvent:

    return AdagioInterventionChangedStatusEvent(
        data=make_adagio_intervention_changed_status_event_data(
            id_intervention=id_intervention,
            libelle_statut_intervention=libelle_statut_intervention,
            groupe_horaire_modification_statut=groupe_horaire_modification_statut,
            timestamp=timestamp,
        ),
        topic="adagio_intervention_changed_status",
    )
