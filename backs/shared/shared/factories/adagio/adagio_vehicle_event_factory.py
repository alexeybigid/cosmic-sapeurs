from typing import Optional

from shared.adagio_vehicle_events import (
    AdagioAbregeFamilleMMA,
    AdagioAbregeStatutOperationnel,
    AdagioArea,
    AdagioVehicleChangedStatusEvent,
    AdagioVehicleChangedStatusEventData,
    CodeMotif,
    InformationsOmnibus,
)
from shared.data_transfert_objects.area import Area


def make_adagio_vehicle_event_data(
    id_intervention: int,
    id_mma: Optional[int] = None,
    abrege_famille_mma_originelle: Optional[AdagioAbregeFamilleMMA] = None,
    abrege_famille_mma_operationnelle: Optional[AdagioAbregeFamilleMMA] = None,
    timestamp: Optional[str] = None,
    libelle_affectation_operationnelle: Optional[AdagioArea] = None,
    libelle_statut_operationnel: Optional[str] = None,
    abrege_statut_operationnel: Optional[AdagioAbregeStatutOperationnel] = None,
    immatriculation_bspp: Optional[str] = None,
    omnibus: Optional[bool] = None,
    omnibus_id_mma: Optional[int] = None,
) -> AdagioVehicleChangedStatusEventData:
    return AdagioVehicleChangedStatusEventData(
        codeMotif=CodeMotif(
            ObtenirOuDefinirCodeCodeMotif="", ObtenirOuDefinirLibelleCodeMotif=""
        ),
        IdMMA=id_mma or 725,
        Libelle_GTA="EPA BSPP_1_BSLT",
        IdFamilleMMAOriginelle=10,
        IdFamilleMMAOperationnelle=10,
        AbregeFamilleMMAOriginelle=abrege_famille_mma_originelle or "EPA BSPP",
        AbregeFamilleMMAOperationnelle=abrege_famille_mma_operationnelle or "EPA BSPP",
        LibelleFamilleMMAOperationnelle="Échelle pivotante Automatique 30 m BSPP ",
        IdMMAAppartenance=2,
        AbregeMMAAppartenance=None,
        LibelleMMAAppartenance=None,
        IdMMAPosition=0,
        LibelleMMAPosition=None,
        ImmatriculationBSPP=immatriculation_bspp or "EPA 103",
        ImmatriculationAdministrative="EPA 103 ",
        X=0,
        Y=0,
        LibelleAffectationOperationnelle=libelle_affectation_operationnelle or "STOU",
        LibelleAffectationAdministrative="BSLT",
        IdStatutOperationnel=103,
        AbregeStatutOperationnel=abrege_statut_operationnel or "DISPO",
        LibelleStatutOperationnel=libelle_statut_operationnel
        or "generated_LibelleStatutOperationnel",
        DateStatutOperationnelMMA=timestamp or "2020-12-15T17:46:09.85",
        IdMMASelection=-1,
        AdresseIntervention="Pas de lien Intervention",
        Cstc="BSLT",
        IdIntervention=id_intervention,
        EnDelestage=False,
        Omnibus=omnibus or False,
        Associe=False,
        InterventionEncours=False,
        DeclassementHorsOmnibus=False,
        InformationsOmnibus=InformationsOmnibus(
            OmnibusEnExecution=True,
            DeclassementTraite=False,
            ReclassementImpossible=False,
            IdMMAOmnibus=omnibus_id_mma or 333,
            ImmatriculationAdministrativeOmnibus="PS 208",
            ImmatriculationBSPPOmnibus="PS 208",
            IdMMADeclasse=omnibus_id_mma or 333,
            ImmatriculationAdministrativeDeclasse="PS 208",
            ImmatriculationBSPPDeclasse="PS 208",
        )
        if omnibus
        else None,
    )


def make_adagio_vehicle_event(
    id_intervention: int,
    id_mma: Optional[int] = None,
    abrege_famille_mma_operationnelle: Optional[AdagioAbregeFamilleMMA] = None,
    timestamp: Optional[str] = None,
    libelle_affectation_operationnelle: Optional[AdagioArea] = None,
    libelle_statut_operationnel: Optional[str] = None,
    abrege_statut_operationnel: Optional[AdagioAbregeStatutOperationnel] = None,
    immatriculation_bspp: Optional[str] = None,
    omnibus: Optional[bool] = None,
    omnibus_id_mma: Optional[int] = None,
) -> AdagioVehicleChangedStatusEvent:
    data = make_adagio_vehicle_event_data(
        id_mma=id_mma,
        abrege_famille_mma_operationnelle=abrege_famille_mma_operationnelle,
        timestamp=timestamp,
        libelle_affectation_operationnelle=libelle_affectation_operationnelle,
        id_intervention=id_intervention,
        libelle_statut_operationnel=libelle_statut_operationnel,
        abrege_statut_operationnel=abrege_statut_operationnel,
        immatriculation_bspp=immatriculation_bspp,
        omnibus=omnibus,
        omnibus_id_mma=omnibus_id_mma,
    )
    return AdagioVehicleChangedStatusEvent(
        topic="adagio_vehicle_changed_status", data=data
    )
