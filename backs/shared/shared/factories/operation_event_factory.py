import random
from typing import Union

from faker import Faker

from shared.data_transfert_objects.area import Area, bspp_area_options
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationEventData,
    OperationStatus,
    Procedure,
    RawOperationId,
    operation_just_opened_or_reinforced,
    operation_status_options,
)
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import OperationEvent

fake = Faker()


default_timestamp = DateStr("2020-01-01T12:00:00.0")


def make_operation_event_data(
    timestamp: DateStr = None,
    raw_operation_id: Union[RawOperationId, str] = None,
    status: OperationStatus = None,
    address: str = None,
    address_area: Area = None,
    cause: OperationCause = None,
    raw_cause: str = None,
    procedure: Procedure = None,
) -> OperationEventData:
    if status and status not in operation_status_options:
        raise ValueError(f"Provided status not in {operation_status_options}")
    address_area = address_area or "STOU"
    raw_operation_id = RawOperationId(
        raw_operation_id or str(fake.pyint(max_value=100))
    )
    raw_cause = raw_cause or "Some emergency description"
    status = status or "opened"
    operation_is_just_opened_or_reinforced = (
        status in operation_just_opened_or_reinforced
    )
    operation_event_data = OperationEventData(
        timestamp=timestamp or default_timestamp,
        raw_operation_id=raw_operation_id,
        status=status,
        address=address if operation_is_just_opened_or_reinforced else None,
        address_area=address_area if operation_is_just_opened_or_reinforced else None,
        cause=cause if operation_is_just_opened_or_reinforced else None,
        raw_cause=raw_cause if operation_is_just_opened_or_reinforced else None,
        procedure=procedure,
    )
    return operation_event_data


def make_operation_event(
    dispatched_at: DateStr = None,
    uuid: str = None,
    # Following args are for make_operation_event_data
    timestamp: DateStr = None,
    raw_operation_id: Union[RawOperationId, str] = None,
    status: OperationStatus = None,
    address: str = None,
    address_area: Area = None,
    cause: OperationCause = None,
    raw_cause: str = None,
    procedure: Procedure = None,
) -> OperationEvent:
    operation_event_data = make_operation_event_data(
        timestamp=timestamp,
        raw_operation_id=raw_operation_id,
        status=status,
        address=address,
        address_area=address_area,
        cause=cause,
        raw_cause=raw_cause,
        procedure=procedure,
    )
    uuid = uuid or uuid4()
    return OperationEvent(
        dispatched_at=dispatched_at,
        uuid=uuid,
        data=operation_event_data,
    )
