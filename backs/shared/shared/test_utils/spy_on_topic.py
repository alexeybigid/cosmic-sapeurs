from typing import Any, List

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.event_bus import InMemoryEventBus


def spy_on_topic(event_bus: InMemoryEventBus, topic: CoverOpsDomainTopic) -> List[Any]:
    published_events = []

    async def spy(e):
        published_events.append(e)

    event_bus.subscribe(topic, spy)
    return published_events
