from typing import List, Literal, Union

TopicAdagioVehicleChangedStatus = Literal["adagio_vehicle_changed_status"]
topic_adagio_vehicle_changed_status: TopicAdagioVehicleChangedStatus = (
    "adagio_vehicle_changed_status"
)

TopicAdagioInterventionCreated = Literal["adagio_intervention_created"]
TopicAdagioInterventionReinforced = Literal["adagio_intervention_reinforced"]
TopicAdagioInterventionChangedStatus = Literal["adagio_intervention_changed_status"]


TopicAdagioEvent = Union[
    TopicAdagioVehicleChangedStatus,
    TopicAdagioInterventionCreated,
    TopicAdagioInterventionReinforced,
    TopicAdagioInterventionChangedStatus,
]
topic_adagio_event_options: List[TopicAdagioEvent] = [
    "adagio_vehicle_changed_status",
    "adagio_intervention_created",
    "adagio_intervention_reinforced",
    "adagio_intervention_changed_status",
]
