import abc
from dataclasses import asdict, dataclass
from typing import Dict, Generic, Type, TypeVar, Union

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData

GenericEventData = TypeVar("GenericEventData")

_T_SchemaClass = TypeVar("_T_SchemaClass")


@dataclass
class EventEntity(abc.ABC, Generic[GenericEventData]):
    data: GenericEventData
    uuid: str
    topic: str

    def as_dict(self):
        return {"uuid": self.uuid, "topic": self.topic, "data": asdict(self.data)}

    @classmethod
    def from_dict(cls: Type[_T_SchemaClass], input: Dict) -> _T_SchemaClass:
        raise NotImplementedError


EventData = Union[
    VehicleEventData,
    AvailabilityChangedEventData,
    OperationEventData,
    OngoingOpChangedEventData,
]
