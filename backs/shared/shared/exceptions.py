import re
import typing


class SapeursError(Exception):
    """Base class for all sapeurs-related errors."""


class InvalidEventFormatError(SapeursError):
    def __init__(self, message: typing.Union[str, typing.List, typing.Dict], **kwargs):
        self.messages = [message] if isinstance(message, (str, bytes)) else message
        super().__init__(message)

    def __str__(self):
        p = re.compile(r"(Must be one of:) .*(\])")
        messages_as_str = p.sub(r"\1 ... \2", str(self.messages))
        return messages_as_str


class InvalidDateFormat(SapeursError):
    pass
