from typing import List, Literal, Union

TopicConvertersCatchesUpAccumulatedEvents = Literal[
    "converters_catches_up_accumulated_events"
]
TopicConvertersInitializedLastStatusOfAllVehicles = Literal[
    "converters_initialized_last_status_of_all_vehicles"
]
TopicVehicleChangedStatus = Literal["vehicle_changed_status"]
TopicOperationChangedStatus = Literal["operation_changed_status"]
TopicAvailabilityChanged = Literal["availabilityChanged"]
TopicOmnibusBalanceChanged = Literal["omnibusBalanceChanged"]
TopicOngoingOpChanged = Literal["ongoingOpChanged"]


ListenedByFrontCoverOpsDomainTopic = Union[
    TopicAvailabilityChanged, TopicOngoingOpChanged, TopicOmnibusBalanceChanged
]

# TODO : Rename ExternalTopic into StatusChangedTopic
ExternalTopic = Union[TopicVehicleChangedStatus, TopicOperationChangedStatus]
external_topic_options: List[ExternalTopic] = [
    "vehicle_changed_status",
    "operation_changed_status",
]

CoverOpsDomainTopic = Union[
    ListenedByFrontCoverOpsDomainTopic,
    TopicVehicleChangedStatus,
    TopicOperationChangedStatus,
    TopicConvertersCatchesUpAccumulatedEvents,
    TopicConvertersInitializedLastStatusOfAllVehicles,
]
