from dataclasses import dataclass
from typing import Any, List, Literal, Optional

from shared.adagio_topics import TopicAdagioVehicleChangedStatus
from shared.external_event import ExternalEvent
from shared.marshmallow_validate import with_from_dict_classmethod

# fmt: off

AdagioAbregeStatutOperationnel = Literal["ACQ", "PARTI", "SLL", "MESSAGE", "MESS URG", "TRANS HOP",
"ARR HOP", "DISPO", "INDISPO", "RENTRE", "REC COURS", "FEU CIRC", "MTRE FEU", "F. ETEINT", "PRES ZDI", "QTTELIEUX",
"POURS REC", "FEUPOUBVP", "FEUVHCLVP", "VIC REF T", "VIC DCD", "SMUR SLL", "TRANSHOPH", "PRESSPONT", "QTTE HOP",
"VICTPLACE", "AR SLL", "DSM SSL", "POL SLL", "GEND SLL", "EDF SLL", "GDF SLL", "DDE SLL", "SVIL SLL", "ELU SLL",
"VOIRIESLL", "CRS SLL", "PROC SLL", "ACCUEIL", "DCAA", "D.POL GEN", "D. SMUR", "CCS", "SPORT", "PRISE COS", "SPONTANNEE",
"RECO ER", "ECOUTE", "IRMCS", "IRMNTI1", "IRMNTI2", "IA", "DDESTAT", "IS", "IO", "ID", "S", "AS", "IAD", "IDLT", "IPD", "IEPO",
"ICAO", "II", "IREM", "IMEG", "IRMHL", "IMPER", "IGEST", "IDLTR", "IDLTC", "IDLTA", "ISDIS", "VUAMED", "ITT", "SOS"]

AdagioAbregeFamilleMMA = Literal["VSAV BSPP", "PSE", "PST", "FA", "VRA", "VEC", "FPT BSPP", "EPSA", "EPA BSPP", "EPAN", 
"VID", "CRAC", "PEV", "CD BSPP", "VLR BSPP", "VRCH BSPP", "VIRB", "CA", "CESD", "VSIS", "VPB", "CSO", "VAG", "VIMP", "DEP",
"ESAVI", "VPC GFIS", "VPC GIS", "VRM", "VAS", "AR", "DAP", "VSTI", "PRM", "FPT SDIS", "FPTL SDIS", "LOUV OFF", "LOUV CGI", 
"LOUV EQUI", "CAR", "SP", "CRF", "CSP", "OHFOM", "SFCB", "FNPC", "VSAV SDIS", "BEAA BSPP", "UMPS", "CFS", "SPCT", "VES", "VRCP",
"VAB", "SPM", "VSI", "SPF", "VSR SDIS", "VPC SDIS", "SPT", "VIVP", "CG", "FSR SDIS", "EPC SDIS", "EPA SDIS", "EPS SDIS",
"EPS 32", "EPS 30", "EPS 24", "EPS 18", "VSAV NRBC", "HELI", "VE2I", "VIGI", "CB", "CBL", "VATI", "SPGL", "TP", "UMH", "VBAL",
"FMMA TEST", "FPT SSLIA", "VSAV SSLIA", "SMU ADP", "ESAV", "CCF SDIS", "VRCH 20 77", "VTU SDIS", "VRCH SDIS", "CDév SDIS", "CCIHR SDIS",
"FMOGP SDIS", "FPTHR SDIS", "VDA SDIS", "VLR SDIS", "BEA SDIS", "CDHR SDIS", "VTP SDIS", "VLHR SDIS", "VFS SDIS", "VCYNO SDIS", "VPL SDIS",
"VPCE SDIS", "VGELD SDIS", "VGRIMP SDIS", "SSIAP", "VPC SAMU", "VPC CFS", "VPC CRF", "VPC FNPC", "VPC OHFOM", "VPC SFCB", "VPC UMPSA",
"CCGC SDIS", "DA", "CCR BSPP", "CDT BSPP", "CDT GIS1", "CDT GIS2", "CDT GIS3", "VID SAV", "VIM", "FPTL SSLIA", "CDT GAS", "CDT GFIS",
"MIR", "VER", "VPS", "CEE", "BP SDIS", "VRSD", "VLIS", "SPTT", "FFSS", "VSA 2", "VRCP SDIS", "DEP SDIS", "MEA SDIS", "VPMA SDIS",
"VAS SDIS", "VLHC SDIS", "CMIC SDIS", "VCH SDIS", "VACH SDIS", "VICH SDIS", "VRAD SDIS", "CERAD SDIS", "CMIR SDIS", "CMDR SDIS", "UMD SDIS",
"CAR SDE 1 SDIS", "CESD SDIS", "SDE 1 SDIS", "VPI BALA", "VSAV BALA", "DAP BALA", "VID BALA", "VLR BALA", "FPTL BSPP", "CMO APP",
"CMO SAN", "VLM", "VRB", "CELD", "VLTT", "VAC", "VL ECMO", "UMH 75", "UMH NECK", "UMH 77", "UMH 78", "UMH 91", "UMH 92", "UMH 93", 
"UMH 94", "UMH 95", "UMH NECKPED", "UMH LARIB", "UMH PITIE", "UMH DIEU", "UMH DEBREPED", "UMH GARC", "UMH BEAUJ", "UMH BECLPED", "UMH STDE", 
"UMH BOBI", "UMH MONTF", "UMH AULN", "UMH MTRLPED", "UMH MONDOR", "UMH VISG", "CRO", "VART", "G. CAI", "FMOGP BSPP", "VSAC", "VLM SAMU 75",
"VLM SAMU 92", "VLM SAMU 93", "VLM SAMU 94", "VTBA", "VLHP", "G.RETEX", "VRSD SDIS", "VSMG", "CRAC SDIS", "CCR SDIS", "CECGC SDIS",
"SRCGC SDIS", "VREX", "VLI", "CM18", "VAERO", "FPTL HP", "MIR ", "VAS_SDIS"]


AdagioArea = Literal["STMR","ANTO","CHPY","PLCL","STCL","CHLY","POIS","ROUS","CHTO","STOU","PARM","VIJF",
"PIER","SEVI","CHPT","PCDG","LEVA","AUBE","VISG","BOUL","NOIS","MALF","SUCY","VILC","CHOI","VITR","STDE",
"LACO","MTRL","STHO","VIMB","NEUI","COBI","CLIC","PLAI","AULN","MALA","BLME","DRAN","LAND","MENI","CHAR",
"PANT","BITC","BLAN","MTMA","PROY","AUTE","DAUP","MTGE","GREN","ISSY","MEUD","BGLR","CLAM","NATI","CBVE",
"PUTX","GENN","MASS","ASNI","IVRY","RUEI","VINC","NANT","COBE","NOGT","CRET","TREM","RUNG","ORLY","BOND",
"BSLT","BES", "CEP", "COU", "ENG", "ERA", "FRA", "GLG", "GOU", "MEV", "MLC", "MCY", "NLV", "SUR", "TAV",
"ARG", "VLB", "EVRY","MEREV","VERLG","RIS", "MENNE","CHALO","VIRY","MAISS","BMT", "CEV", "CHM", "AIN", 
"PER", "HER", "LIA", "VIG", "PRE", "NEU", "GON", "VIA", "DOM", "GRA", "EAU", "MAR", "CHA", "BEL", "BEZ", 
"LOU", "SAN", "ROI", "OSN", "MER", "WISOU","ANGER","ATHIS","BALIN","BALLA","BOUTI","CHILY","CUTTE","DRAVE",
"EPYNO","GIF", "JUVIS","LONGJ","MILLY","MONTG","MONTL","PUISE","SAVIG","VDY", "MONDE","CERNY","ETAMP","MASSY",
"SOS", "BIEVR","ULIS","SACLA","LISSE","ETREC","BREUI","ARPAJ","MAROL","LARDY","STCHE","PUSAY","SGB", "LIMOU",
"DOURD","BRETI","MARCO","BRUYE","PALAI","CORBE","MPS", "MLB", "SLG", "CHE", "PSY", "AUB", "CSH", "HOD",
"MES", "LOU", "MOA", "CLV", "MTS", "RAM", "VES", "BRE", "STA", "CYR", "BON", "SEP", "GRC", "CSB", "GUY",
"MAL", "LIM", "ACH", "PLA", "GGV", "ABL", "CRO", "CSC", "ESS", "MAG", "VRS", "VRS", "MLF", "CHA", "VIR",
"BOI", "VLY", "TRI", "SGL", "HOI", "VIL", "MAR", "LMX", "VRN", "SPT", "CPS", "CNS", "CSI", "CSL", "CLM",
"CLC", "DML", "DMG", "DMD", "FRM", "FTN", "JLC", "LCR", "LFG", "LFJ", "LGP", "LGM", "CTB", "LRB", "MSR",
"MLN", "MTM", "MSC", "NGS", "CHL", "FTB", "TPT", "LGS", "CTL", "GGR", "VLX", "MUX", "BLR", "MLR", "VLP",
"BSS", "FRB", "LZO", "PVS", "SPL", "EGV", "MMT", "MRL", "RBS", "BMG", "BRM", "BCR", "MTR", "NMS", "OZF",
"PTC", "SFP", "SGM", "SLT", "TNB", "VRM", "VLP", "VLG", "VLS", "RZB", 
# other LSO (not on geojson map but still in BSPP count)
"CCT","CASJ","CASC","MONN","CCT","SEVR","EMGAS","CASS","JOIN","NBCP","NBCL","NBCR","BALA",
# other LSL (not on geojson map but still in BSPP count)
"STEC","SAMU","CCL1","CCL2","CCL3","CIR","CFC","LVV","STEC","POUC","CDS2","CMAI","GPOR","CSC","LIME"]
# fmt: on


@dataclass
class CodeMotif:
    ObtenirOuDefinirCodeCodeMotif: str  # TODO ??
    ObtenirOuDefinirLibelleCodeMotif: str


@dataclass
class InformationsOmnibus:
    OmnibusEnExecution: Optional[bool]
    DeclassementTraite: Optional[bool]
    ReclassementImpossible: Optional[bool]
    IdMMAOmnibus: int
    ImmatriculationAdministrativeOmnibus: Optional[str]
    ImmatriculationBSPPOmnibus: Optional[str]
    ImmatriculationBSPPOmnibus: Optional[str]
    IdMMADeclasse: Optional[int]
    ImmatriculationAdministrativeDeclasse: Optional[str]
    ImmatriculationBSPPDeclasse: Optional[str]
    EstOmnibusEpm6Classique: bool


@dataclass
class AdagioVehicleChangedStatusEventData:
    codeMotif: Optional[CodeMotif]
    IdMMA: int
    Libelle_GTA: Optional[str]
    IdFamilleMMAOriginelle: int
    AbregeFamilleMMAOriginelle: AdagioAbregeFamilleMMA
    IdFamilleMMAOperationnelle: int
    AbregeFamilleMMAOperationnelle: AdagioAbregeFamilleMMA
    LibelleFamilleMMAOperationnelle: str
    IdMMAAppartenance: int
    AbregeMMAAppartenance: Optional[str]
    LibelleMMAAppartenance: Optional[str]
    IdMMAPosition: int
    LibelleMMAPosition: Optional[str]
    ImmatriculationBSPP: str
    ImmatriculationAdministrative: str
    X: int
    Y: int
    LibelleAffectationOperationnelle: AdagioArea
    LibelleAffectationAdministrative: Optional[str]  # TODO should be Optional[Area]
    IdStatutOperationnel: int
    AbregeStatutOperationnel: AdagioAbregeStatutOperationnel
    LibelleStatutOperationnel: str
    DateStatutOperationnelMMA: str
    IdMMASelection: int  # -1 eventually
    AdresseIntervention: Optional[str]
    Cstc: Optional[str]  # Optional[AdagioArea]
    IdIntervention: int  # -1 if selected
    EnDelestage: bool
    Omnibus: bool
    Associe: bool
    InterventionEncours: bool
    DeclassementHorsOmnibus: bool
    InformationsOmnibus: Optional[InformationsOmnibus]


adagio_vehicle_topic_options: List[TopicAdagioVehicleChangedStatus] = [
    "adagio_vehicle_changed_status"
]


@with_from_dict_classmethod()
@dataclass
class AdagioVehicleChangedStatusEvent(ExternalEvent):
    data: AdagioVehicleChangedStatusEventData
    topic: TopicAdagioVehicleChangedStatus = "adagio_vehicle_changed_status"
