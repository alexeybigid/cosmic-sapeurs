from typing import Dict, Union

from shared.exceptions import InvalidEventFormatError
from shared.helpers.logger import logger
from shared.sapeurs_events import (
    ConvertersCatchesUpEvent,
    ConvertersVehiclesInitEvent,
    Event,
    OperationEvent,
    VehicleEvent,
    operation_changed_status,
    vehicle_changed_status,
)


def ConvertersVehiclesInitEvent_from_dict(
    event_as_dict: Dict,
) -> ConvertersVehiclesInitEvent:
    return ConvertersVehiclesInitEvent(
        uuid=event_as_dict["uuid"],
        data=[
            vehicle_or_operation_event_dict_to_event(nested_event_as_dict)
            for nested_event_as_dict in event_as_dict["data"]
        ],
    )


def ConvertersCatchesUpEvent_from_dict(event_as_dict: Dict) -> ConvertersCatchesUpEvent:
    return ConvertersCatchesUpEvent(
        uuid=event_as_dict["uuid"],
        data=[
            vehicle_or_operation_event_dict_to_event(nested_event_as_dict)
            for nested_event_as_dict in event_as_dict["data"]
        ],
    )


def vehicle_or_operation_event_dict_to_event(event_as_dict: Dict) -> Event:
    topic = event_as_dict["topic"]
    if topic == vehicle_changed_status:
        return VehicleEvent.from_dict(event_as_dict)
    elif topic == operation_changed_status:
        return OperationEvent.from_dict(event_as_dict)
    else:
        message = f"Received unknown topic {topic}, expected {[vehicle_changed_status, operation_changed_status]}"
        logger.warn(message)
        raise InvalidEventFormatError(message)


def sapeurs_event_dict_to_event(
    event_as_dict: Dict,
) -> Event:
    topic = event_as_dict["topic"]
    if topic in [vehicle_changed_status, operation_changed_status]:
        return vehicle_or_operation_event_dict_to_event(event_as_dict)
    elif topic == "converters_catches_up_accumulated_events":
        return ConvertersCatchesUpEvent_from_dict(event_as_dict)
    elif topic == "converters_initialized_last_status_of_all_vehicles":
        print("ConvertersVehiclesInitEvent_from_dict")
        return ConvertersVehiclesInitEvent_from_dict(event_as_dict)
    else:
        message = f"Received dict with unknown topic {topic}."
        logger.warn(message)
        raise InvalidEventFormatError(message)
