from dataclasses import dataclass

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
    RawOperationId,
)
from shared.helpers.date import DateStr


@dataclass
class OngoingOpChangedEventData(JsonSchemaMixin, allow_additional_props=False):
    timestamp: DateStr
    bspp_ongoing_ops: int
    area_ongoing_ops: int
    cause: OperationCause
    address_area: Area
    raw_operation_id: RawOperationId
    status: OperationStatus

    def asdict(self):
        return {
            "timestamp": str(self.timestamp),
            "bspp_ongoing_ops": self.bspp_ongoing_ops,
            "area_ongoing_ops": self.area_ongoing_ops,
            "cause": self.cause,
            "address_area": self.address_area,
            "raw_operation_id": str(self.raw_operation_id),
            "status": self.status,
        }
