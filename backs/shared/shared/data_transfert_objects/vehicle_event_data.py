from dataclasses import asdict, dataclass, field
from typing import Dict, List, Literal, NewType, Optional, Type, TypeVar, Union

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.helpers.date import DateStr, DateStrField
from shared.marshmallow_validate import marshmallow_validate

AvailabilityKind = Literal[
    "available", "unavailable", "recoverable", "in_service", "unavailable_omnibus"
]
vehicle_availability_kind_options: List[AvailabilityKind] = [
    "available",
    "unavailable",
    "recoverable",
    "in_service",
    "unavailable_omnibus",
]

VehicleStatus = Literal[
    "departed_to_intervention",
    "arrived_on_intervention",
    "transport_to_hospital",
    "left_intervention",
    "arrived_at_hospital",
    "left_hospital",
    "information_message",
    "recoverable_within_15_minutes",  # sport
    "selected",  # présentation spontannée, sélection / instance de départ
    "waiting",
    "broken",
    "unavailable_lacks_staff",  # manque personnel
    "unavailable_omnibus",  # indispo délestage,
    "unavailable_offloading",  # indispo délestage
    "cancelled",
    "set_aside",  # indispo 1er départ
    "realocated",  # monté en garde
    "undefined",  # or unused
    "misc_unavailable",
    "misc_available",
    "arrived_at_home",
]
vehicle_status_options: List[VehicleStatus] = [
    "departed_to_intervention",
    "arrived_on_intervention",
    "transport_to_hospital",
    "left_intervention",
    "arrived_at_hospital",
    "left_hospital",
    "recoverable_within_15_minutes",
    "selected",
    "waiting",
    "broken",
    "unavailable_lacks_staff",  # manque personnel
    "unavailable_omnibus",  # indispo délestage,
    "unavailable_offloading",  # indispo délestage
    "cancelled",
    "set_aside",
    "realocated",
    "undefined",
    "misc_unavailable",
    "misc_available",
    "arrived_at_home",
    "information_message",
]

vehicle_status_to_availability_kind: Dict[VehicleStatus, AvailabilityKind] = {
    "departed_to_intervention": "in_service",
    "left_intervention": "in_service",
    "arrived_on_intervention": "in_service",
    "transport_to_hospital": "in_service",
    "arrived_at_hospital": "in_service",
    "left_hospital": "in_service",
    "recoverable_within_15_minutes": "recoverable",
    "selected": "in_service",  # présentation spontannée, sélection / instance de départ
    "waiting": "in_service",
    "broken": "unavailable",
    "unavailable_lacks_staff": "unavailable",  # manque personnel ?? Is it omnibus. TODO : ask.
    "unavailable_omnibus": "unavailable_omnibus",  # omnibus ? TODO : Eric^2 ?
    "unavailable_offloading": "recoverable",  # indispo délestage
    "cancelled": "available",
    "set_aside": "recoverable",  # indispo délestage, indispo 1er départ
    "realocated": "recoverable",  # monté en garde
    "undefined": "unavailable",  # or unused
    "misc_unavailable": "unavailable",
    "misc_available": "available",
    "arrived_at_home": "available",
    "information_message": "in_service",
}

# VehicleRole = Literal[
#     "vsav_solo",
#     "pse",
#     "other",
#     "faca",
#     "epa",
#     "epc",
#     "epsa",
# ]

# fmt: off
VehicleRole = Literal[
    "bea", "epa_epsa", "ca_ba", "fmogp", "fa", "fpt", "fptl", "pse_solo", "ccr", "pst", "fptlhp",
    "vsav_solo", "vpsp", "ar_bspp", "umh", "cmo_san", "cmo_appui", "vsis", "esav", "esavi", "vrsd", "cesd", "vrch", "celd",
    "vrex", "vec", "vra", "vimp", "vid", "vigi", "crac", "pev", "cd", "vrcp", "vpc_tac", 
    "other",
    "vsav_omni", "pse_omni_san", "pse_omni_pump"

]

# TODO : VL à gérer :
# "vl_infirmier", "vl_cdg", "vl_ogc", "vl_opc", "vl_osg", "vl_garde_environnement", "vl_colg", "vl_cob"

# TODO : non utilisés:
# "bars", "bumd", "prom", "bpe", "mpr", "mpe",


vehicle_role_options: List[VehicleRole] = [
   "bea", "epa_epsa", "ca_ba", "fmogp", "fa", "fpt", "fptl", "pse_solo", "ccr", "pst", "fptlhp",
    "vsav_solo", "vpsp", "ar_bspp", "umh", "cmo_san", "cmo_appui", "vsis", "esav", "esavi", "vrsd", "cesd", "vrch", "celd",
    "vrex", "vec", "vra", "vimp", "vid", "vigi", "crac", "pev", "cd", "vrcp", "vpc_tac",
    "vsav_omni", "pse_omni_san", "pse_omni_pump"
]
# fmt: on

RawVehicleId = NewType("RawVehicleId", str)


def vehicle_is_available(status: VehicleStatus) -> bool:
    return vehicle_status_to_availability_kind.get(status) == "available"


@dataclass
class OmnibusInfos(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    partner_raw_vehicle_id: RawVehicleId


@dataclass
class VehicleEventData(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    # timestamp: DateStr
    timestamp: DateStr = field(
        metadata={"marshmallow_field": DateStrField()}  # Custom marshmallow field
    )  # from "Operation-Software"
    raw_vehicle_id: RawVehicleId  # 'Operation-Software'  vehicle ID
    raw_operation_id: Optional[RawOperationId]  # 'Operation-Software'  intervention ID
    status: VehicleStatus
    raw_status: str  # Status label from 'Operation-Software'
    home_area: Area  # Reference on a partitioned map of where the vehicle belongs
    role: VehicleRole
    vehicle_name: str  # Immatriculation
    omnibus: Optional[OmnibusInfos] = None

    def asdict(self):
        return {**asdict(self), "availability_kind": self.availability_kind}

    @property
    def is_available(self) -> bool:
        return vehicle_is_available(self.status)

    @property
    def availability_kind(self) -> AvailabilityKind:
        return vehicle_status_to_availability_kind[self.status]

    @classmethod
    def from_dict(
        cls,
        input: Dict,
        skip_marshmallow_validation: bool = False,
    ) -> "VehicleEventData":
        if skip_marshmallow_validation:
            omnibus_dict = input["omnibus"]
            cls(
                timestamp=input["timestamp"],
                home_area=input["home_area"],
                omnibus=OmnibusInfos(**omnibus_dict) if omnibus_dict else None,
                raw_operation_id=input["raw_operation_id"],
                raw_vehicle_id=input["raw_vehicle_id"],
                raw_status=input["raw_status"],
                status=input["status"],
                vehicle_name=input["vehicle_name"],
                role=input["role"],
            )
        return marshmallow_validate(
            SchemaClass=cls,
            dict_candidate={
                k: v
                for (k, v) in input.items()
                if k not in ["availability_kind", "is_available"]
            },
        )
