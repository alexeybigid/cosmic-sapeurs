# fmt: off
from typing import List, Literal

Area = Literal["STMR", "ANTO", "CHPY", "PLCL", "STCL", "CHLY", "POIS", "ROUS", "CHTO", "STOU", "PARM", "VIJF", "PIER", "SEVI", "CHPT", 
"PCDG", "LEVA", "AUBE", "VISG", "BOUL", "NOIS", "MALF", "SUCY", "VILC", "CHOI", "VITR", "STDE", "LACO", "MTRL", "STHO", "VIMB", "NEUI", 
"COBI", "CLIC", "PLAI", "AULN", "MALA", "BLME", "DRAN", "LAND", "MENI", "CHAR", "PANT", "BITC", "BLAN", "MTMA", "PROY", "AUTE", "DAUP", 
"MTGE", "GREN", "ISSY", "MEUD", "BGLR", "CLAM", "NATI", "CBVE", "PUTX", "GENN", "MASS", "ASNI", "IVRY", "RUEI", "VINC", "NANT", "COBE", 
"NOGT", "CRET", "TREM", "RUNG", "ORLY", "BOND", "BSLT", 
"LSO", "LSL", "SDIS"]

bspp_area_options : List[Area] = ["STMR", "ANTO", "CHPY", "PLCL", "STCL", "CHLY", "POIS", "ROUS", "CHTO", "STOU", "PARM", "VIJF", "PIER", "SEVI", "CHPT", 
"PCDG", "LEVA", "AUBE", "VISG", "BOUL", "NOIS", "MALF", "SUCY", "VILC", "CHOI", "VITR", "STDE", "LACO", "MTRL", "STHO", "VIMB", "NEUI", 
"COBI", "CLIC", "PLAI", "AULN", "MALA", "BLME", "DRAN", "LAND", "MENI", "CHAR", "PANT", "BITC", "BLAN", "MTMA", "PROY", "AUTE", "DAUP", 
"MTGE", "GREN", "ISSY", "MEUD", "BGLR", "CLAM", "NATI", "CBVE", "PUTX", "GENN", "MASS", "ASNI", "IVRY", "RUEI", "VINC", "NANT", "COBE", 
"NOGT", "CRET", "TREM", "RUNG", "ORLY", "BOND", "BSLT", "LSO", "LSL"]

# fmt: on
