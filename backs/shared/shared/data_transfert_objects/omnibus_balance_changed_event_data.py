from dataclasses import dataclass
from typing import List, Literal

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.helpers.date import DateStr

OmnibusBalanceKind = Literal[
    "both_available",
    "vsav_on_inter_san",
    "pse_on_inter_san",
    "both_on_inter_san",
    "pse_on_inter_fire",
    "only_pse_available",
]

omnibus_balance_kind_options: List[OmnibusBalanceKind] = [
    "both_available",
    "vsav_on_inter_san",
    "pse_on_inter_san",
    "both_on_inter_san",
    "pse_on_inter_fire",
    "only_pse_available",
]


@dataclass
class OmnibusBalance(JsonSchemaMixin, allow_additional_props=False):
    both_available: int = 0  # x1
    vsav_on_inter_san: int = 0  # y1
    pse_on_inter_san: int = 0  # y2
    pse_on_inter_fire: int = 0  # z1
    both_on_inter_san: int = 0  # z2
    only_pse_available: int = 0  # x2


@dataclass
class OmnibusDetail(JsonSchemaMixin, allow_additional_props=False):
    vsav_id: RawVehicleId
    pse_id: RawVehicleId
    area: Area
    balance_kind: OmnibusBalanceKind


@dataclass()
class OmnibusBalanceChangedEventData(JsonSchemaMixin, allow_additional_props=False):
    timestamp: DateStr
    omnibus_balance: OmnibusBalance
    details: List[OmnibusDetail]
