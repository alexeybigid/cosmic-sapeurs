from dataclasses import dataclass

from dataclasses_jsonschema import JsonSchemaMixin

from shared.cover_ops_topics import ListenedByFrontCoverOpsDomainTopic
from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceKind,
)
from shared.data_transfert_objects.operation_event_data import OperationCause
from shared.data_transfert_objects.vehicle_event_data import (
    AvailabilityKind,
    VehicleRole,
    VehicleStatus,
)


@dataclass
class Literals(JsonSchemaMixin, allow_additional_props=False):
    vehicleRole: VehicleRole
    vehicleStatus: VehicleStatus
    operationCause: OperationCause
    availabilityKind: AvailabilityKind
    area: Area
    domainTopic: ListenedByFrontCoverOpsDomainTopic
    omnibusBalanceKind: OmnibusBalanceKind
