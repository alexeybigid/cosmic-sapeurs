from dataclasses import dataclass
from typing import List

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.helpers.dataclass import dataclass_to_dict


@dataclass
class CoverOpsBecameReadyEventData(JsonSchemaMixin, allow_additional_props=False):
    cover: List[AvailabilityChangedEventData]
    ops: List[OngoingOpChangedEventData]

    def asdict(self):
        return {
            "cover": list(map(dataclass_to_dict, self.cover)),
            "ops": list(map(dataclass_to_dict, self.ops)),
        }
