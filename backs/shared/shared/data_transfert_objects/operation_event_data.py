from dataclasses import dataclass
from typing import List, Literal, NewType, Optional, Union

from dataclasses_jsonschema import JsonSchemaMixin

from shared.data_transfert_objects.area import Area
from shared.helpers.date import DateStr

OperationCause = Literal["victim", "fire", "other"]
operation_cause_options: List[OperationCause] = ["victim", "fire", "other"]
Procedure = Literal["red", "orange", "white", "external_mission", "green"]

RawOperationId = NewType("RawOperationId", str)

OperationRelevantStatus = Literal[
    "opened",
    "first_vehicle_affected",
    "all_vehicles_released",
]
OperationStatus = Literal[
    "reinforced",
    "validated",
    "finished",
    "closed",
    "opened",
    "ongoing",
    "received",
    "sent",
    "first_vehicle_affected",
    "all_vehicles_released",
]


operation_relevant_status_options: List[OperationStatus] = [
    "opened",
    "first_vehicle_affected",
    "all_vehicles_released",
]
operation_status_options: List[OperationStatus] = [
    "opened",
    "reinforced",
    "validated",
    "finished",
    "closed",
    "first_vehicle_affected",
    "all_vehicles_released",
]

operation_closed: OperationStatus = "closed"
operation_just_opened: OperationRelevantStatus = "opened"
operation_has_first_affected_vehicle: OperationRelevantStatus = "first_vehicle_affected"
operation_has_all_vehicles_released: OperationRelevantStatus = "all_vehicles_released"

operation_just_opened_or_reinforced: List[OperationStatus] = ["opened", "reinforced"]


@dataclass
class OperationEventData(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    timestamp: DateStr  # From 'Operation-Software'
    raw_operation_id: RawOperationId  # 'Operation-Software'  vehicle ID
    status: OperationStatus
    address: Optional[str]
    address_area: Optional[
        Area
    ]  # Reference on a partitioned map of where the address belongs
    cause: Optional[OperationCause]
    raw_cause: Optional[str]  # Cause label from 'Operation-Software'
    procedure: Optional[Procedure]
