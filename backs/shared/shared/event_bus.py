import abc
import asyncio
from collections import defaultdict
from typing import Any, Callable, Coroutine, Dict, Generic, List, TypeVar

from shared.helpers.clock import AbstractClock
from shared.sapeurs_events import Event

EventCallback = Callable[[Any], Coroutine[Any, Any, Any]]
GenericTopic = TypeVar("GenericTopic")


class AbstractEventBus(abc.ABC, Generic[GenericTopic]):
    def __init__(self, clock: AbstractClock) -> None:
        self.clock = clock

    @abc.abstractclassmethod
    def subscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        raise NotImplementedError()


class InMemoryEventBus(AbstractEventBus, Generic[GenericTopic]):
    def __init__(self, clock: AbstractClock) -> None:
        super().__init__(clock=clock)
        Subscriptions = Dict[GenericTopic, List[EventCallback]]
        self._subscriptions: Subscriptions = defaultdict(lambda: [])

    def unsubscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        filtered = list(filter(lambda cb: cb != callback, self._subscriptions[topic]))
        self._subscriptions[topic] = filtered

    def subscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        self._subscriptions[topic].append(callback)

    async def publish(self, event: Event) -> None:
        event = self.set_dispatched_at(event)
        if self._subscriptions[event.topic]:
            await asyncio.wait(
                [
                    asyncio.create_task(callback(event))
                    for callback in self._subscriptions[event.topic]
                ]
            )

    def set_dispatched_at(self, event: Event) -> Event:
        event.dispatched_at = self.clock.get_now()
        return event
