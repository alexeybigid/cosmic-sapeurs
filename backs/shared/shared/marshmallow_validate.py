from typing import Dict, Type, TypeVar

import marshmallow_dataclass
from marshmallow import Schema
from marshmallow.exceptions import ValidationError

from shared.exceptions import InvalidEventFormatError


def marshmallow_validate(dict_candidate: Dict, SchemaClass=None):
    schema = marshmallow_dataclass.class_schema(SchemaClass)()
    try:
        return schema.load(dict_candidate)
    except ValidationError as marshmallow_validation_error:
        raise InvalidEventFormatError(marshmallow_validation_error.messages)


def with_from_dict_classmethod():
    """Decorator to add `from_dict` classmethod to dataclass

    Example :

    @with_from_dict_classmethod()
    @dataclass
    class MyClass:
        a: int

    my_class_from_dict = MyClass.from_dict({"a": 1})

    >> print(my_class_from_dict)
    MyClass(a=1)

    """
    _T_SchemaClass = TypeVar("_T_SchemaClass")

    def decorator(cls):
        @classmethod
        def from_dict(
            cls: Type[_T_SchemaClass],
            input: Dict,
        ) -> _T_SchemaClass:
            return marshmallow_validate(SchemaClass=cls, dict_candidate=input)

        setattr(cls, "from_dict", from_dict)

        return cls

    return decorator
