from shared.data_transfert_objects.sapeurs_routes import SapeursRoutes

sapeurs_routes = SapeursRoutes(
    converted_event="converted_event",
    hello="hello",
    get_cover="cover",
    get_ops="ops",
    get_omnibus="omnibus",
    prefix="api",
    ping="ping",
)


# converters
route_external_vehicle_event = "adagio_vehicle"
route_external_operation_event = "adagio_intervention"
route_get_last_status_of_all_vehicles = "last-status-of-all-vehicles"


def make_url(*argv: str) -> str:
    route = "/".join(argv)
    if route[0] != "/":
        route = "/" + route
    return route
