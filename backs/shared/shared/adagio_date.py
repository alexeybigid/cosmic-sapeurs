from dataclasses import dataclass
from typing import Dict, Union

from shared.adagio_topics import TopicAdagioEvent
from shared.cover_ops_topics import ExternalTopic
from shared.helpers.date import datestr_regex


@dataclass
class DateInfos:
    date_path_in_data: str
    date_format_regex_in_data: str


TopicsToReplay = Union[ExternalTopic, TopicAdagioEvent]

adagio_etat_intervention_regex = r"(?P<day>\d{2})\/(?P<month>\d{2})\/(?P<year>\d{4}) (?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})"
adagio_groupe_horaire_modification_statut = datestr_regex
adagio_date_statut_operationnel_mma = datestr_regex

topic_to_date_infos: Dict[TopicsToReplay, DateInfos] = {
    "vehicle_changed_status": DateInfos(
        date_path_in_data="timestamp",
        date_format_regex_in_data=datestr_regex,
    ),
    "operation_changed_status": DateInfos(
        date_path_in_data="timestamp",
        date_format_regex_in_data=datestr_regex,
    ),
    "adagio_vehicle_changed_status": DateInfos(
        date_path_in_data="DateStatutOperationnelMMA",
        date_format_regex_in_data=adagio_date_statut_operationnel_mma,
    ),
    "adagio_intervention_created": DateInfos(
        date_path_in_data="OdeResume.EtatIntervention",
        date_format_regex_in_data=adagio_etat_intervention_regex,
    ),
    "adagio_intervention_reinforced": DateInfos(
        date_path_in_data="OdeResume.EtatIntervention",
        date_format_regex_in_data=adagio_etat_intervention_regex,
    ),
    "adagio_intervention_changed_status": DateInfos(
        date_path_in_data="GroupeHoraireModificationStatut",
        date_format_regex_in_data=adagio_groupe_horaire_modification_statut,
    ),
}
