from setuptools import find_packages, setup

packages = find_packages(exclude=[])

setup(
    name="shared",
    install_requires=["dataclasses_jsonschema", "marshmallow_dataclass", "marshmallow"],
    version="0.1",
    packages=packages,  # ["."],
)
