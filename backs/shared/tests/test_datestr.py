from shared.adagio_date import adagio_etat_intervention_regex
from shared.helpers.date import DateStr, datestr_regex, from_regex


def test_from_regex_with_datestr_format():
    raw_date = "2021-01-28T09:22:19.1998766+01:00"
    expected_datestr = DateStr("2021-01-28T09:22:19.199876")
    assert from_regex(raw_date, datestr_regex) == expected_datestr


def test_from_regex_with_datestr_format_no_decimal():
    raw_date = "2021-01-27T22:31:03"
    expected_datestr = DateStr("2021-01-27T22:31:03.000000")
    assert from_regex(raw_date, datestr_regex) == expected_datestr


def test_from_regex_with_adagio_etat_intervention_format():
    raw_date = "27/01/2021 18:37:01"
    expected_datestr = DateStr("2021-01-27T18:37:01.000000")
    assert from_regex(raw_date, adagio_etat_intervention_regex) == expected_datestr
