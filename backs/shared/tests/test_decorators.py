import pytest

from shared.helpers.decorators import make_exception_logger

raise_exception_logger = make_exception_logger("RAISE")
pass_exception_logger = make_exception_logger("PASS")


def test_decorator_raises():
    @raise_exception_logger()
    def zero_divide():
        1 / 0

    with pytest.raises(Exception):
        zero_divide()


def test_decorator_passes():
    @pass_exception_logger()
    def zero_divide():
        1 / 0

    zero_divide()
