python-dotenv
aiohttp
aiohttp_cors
dataclasses_jsonschema
tqdm
requests
redis

# dev/test
pytest
pytest-watch
pytest-aiohttp
pytest-cov
faker
black
isort

# data
pandas