import json

from cover_ops.entrypoints.server import route_converted_event

# fixture, appears like it was unused, but it is !
from tests.utils.server_fixture import make_server_fixture

from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


async def test_publish_to_bus_and_save_vehicle_created_event(server_fixture):
    vehicle_changed_status_event_as_dict = {
        "data": {
            "raw_vehicle_id": "1",
            "raw_operation_id": "3",
            "role": "vsav_solo",
            "raw_status": "parti",
            "status": "departed_to_intervention",
            "timestamp": "2020-12-10T12:30:00.853",
            "home_area": "STOU",
            "vehicle_name": "VSAV 102",
        },
        "uuid": "some_vehicle_id",
        "topic": "vehicle_changed_status",
    }

    published_events = spy_on_topic(config.domain_event_bus, "vehicle_changed_status")

    resp = await server_fixture.post(
        route_converted_event,
        data=json.dumps(vehicle_changed_status_event_as_dict),
    )

    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}

    assert len(published_events) == 1
    assert published_events[0].uuid == "some_vehicle_id"
    assert len(config.vehicle_events_repo.df_with_flatten_data) == 1
