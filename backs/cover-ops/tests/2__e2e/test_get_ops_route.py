from tests.utils.server_fixture import make_server_fixture

from shared.routes import make_url, sapeurs_routes

server_fixture, config = make_server_fixture()


async def test_get_ops_route(server_fixture):
    resp = await server_fixture.get(
        path=make_url(sapeurs_routes.prefix, sapeurs_routes.get_ops),
        data='{"hours": 3}',
    )
    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == []
