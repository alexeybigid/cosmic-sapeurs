import asyncio

from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)
from tests.utils.prepare_update_ops import prepare_update_ops

from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.factories.operation_event_factory import make_operation_event
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import OperationEvent


def test_event_ongoing_op_is_updated():
    (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        domain_event_bus,
    ) = prepare_update_ops()

    domain_event_bus.subscribe(
        topic="operation_changed_status", callback=update_ops.execute
    )

    def on_event_assert_ongoing_op_entity_in_repo(
        event: OperationEvent,
        expected_on_going_op_event_data: OngoingOpChangedEventData,
    ):
        custom_uuid.set_next_uuid(uuid4())
        asyncio.run(domain_event_bus.publish(event))

        assert (
            ongoing_ops_events_repo.ongoing_op_events[-1].data
            == expected_on_going_op_event_data
        )

    # dispatched_at = DateStr("2020-11-30T12:00:00.0")
    # custom_clock.set_next_date(dispatched_at)

    # Operation 1 opened
    operation_id_1 = RawOperationId("operation_1")
    timestamp_1 = DateStr("2020-10-02T12:00:00.0")
    victim_1_opened = make_operation_event(
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_id_1,
        status="opened",
        timestamp=timestamp_1,
    )

    expected_opened_ongoing_op = make_ongoing_ops_changed_event_data(
        bspp_ongoing_ops=0,
        area_ongoing_ops=0,
        address_area="STOU",
        cause="victim",
        timestamp=timestamp_1,
        status="opened",
        raw_operation_id=operation_id_1,
    )

    on_event_assert_ongoing_op_entity_in_repo(
        victim_1_opened, expected_opened_ongoing_op
    )

    # Operation 1 validated
    operation_id_1 = RawOperationId("operation_1")
    timestamp_2 = DateStr("2020-10-02T12:02:00.0")
    victim_1_validated = make_operation_event(
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_id_1,
        status="validated",
        timestamp=timestamp_2,
    )

    on_event_assert_ongoing_op_entity_in_repo(
        victim_1_validated, expected_opened_ongoing_op
    )
