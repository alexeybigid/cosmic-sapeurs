import asyncio

from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_entity,
)
from tests.utils.prepare_update_cover import prepare_update_cover

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import VehicleEvent


def test_event_availability_is_updated():
    (
        _,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        _,
    ) = prepare_update_cover()
    domain_event_bus.subscribe(
        topic="vehicle_changed_status", callback=update_cover.execute
    )

    def on_event_assert_availability(
        event: VehicleEvent, expected_availability: AvailabilityChangedEventEntity
    ):
        custom_uuid.set_next_uuid(uuid4())
        asyncio.run(domain_event_bus.publish(event))

        assert (
            availability_events_repo.last_availability_event.data
            == expected_availability.data
        )

    dispatched_at = DateStr("2020-11-30T12:00:00.0")
    custom_clock.set_next_date(dispatched_at)

    # First event
    timestamp_1 = DateStr("2020-10-02T12:00:00.0")
    victim_rescue_1_became_available_event = make_vehicle_event(
        role="vsav_solo",
        raw_vehicle_id="1",
        status="misc_available",
        timestamp=timestamp_1,
        home_area="STOU",
    )

    on_event_assert_availability(
        victim_rescue_1_became_available_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_1,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="STOU",
            area_availability=Availability(available=1),
            bspp_availability=Availability(available=1),
            raw_vehicle_id="1",
            latest_event_data=victim_rescue_1_became_available_event.data,
        ),
    )

    # Second event : vehicle #2 as pump became available
    timestamp_2 = DateStr("2020-10-01T13:00:00.0")
    pump_2_became_available_event = make_vehicle_event(
        role="pse_solo",
        raw_vehicle_id="2",
        home_area="STOU",
        status="misc_available",
        timestamp=timestamp_2,
    )
    on_event_assert_availability(
        pump_2_became_available_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_2,
            uuid=uuid4(),
            role="pse_solo",
            home_area="STOU",
            area_availability=Availability(available=1),
            bspp_availability=Availability(available=1),
            raw_vehicle_id="2",
            latest_event_data=pump_2_became_available_event.data,
        ),
    )

    # Third event : vehicle #1 as victim_rescue was selected
    timestamp_3 = DateStr("2020-10-01T13:03")
    victim_rescue_5_was_selected_event = make_vehicle_event(
        role="vsav_solo",
        raw_vehicle_id="1",
        home_area="STOU",
        status="selected",
        timestamp=timestamp_3,
    )
    on_event_assert_availability(
        victim_rescue_5_was_selected_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_3,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="STOU",
            area_availability=Availability(in_service=1),
            bspp_availability=Availability(in_service=1),
            raw_vehicle_id="1",
            latest_event_data=victim_rescue_5_was_selected_event.data,
        ),
    )

    # Fourth event : vehicle #3 as other is new and in sport
    timestamp_4 = DateStr("2020-10-01T14:00")
    other_3_is_new_and_in_sport_event = make_vehicle_event(
        role="other",
        raw_vehicle_id="3",
        home_area="STOU",
        status="recoverable_within_15_minutes",
        timestamp=timestamp_4,
    )

    initialLength = len(availability_events_repo._availability_events)
    assert initialLength == 3
    custom_uuid.set_next_uuid(uuid4())
    asyncio.run(domain_event_bus.publish(other_3_is_new_and_in_sport_event))

    assert len(availability_events_repo._availability_events) == initialLength

    # Fifth event :
    timestamp_5 = DateStr("2020-10-01T20:09")
    victim_rescue_5_was_selected_event = make_vehicle_event(
        role="vsav_solo",
        home_area="STOU",
        raw_vehicle_id="1",
        status="arrived_at_home",
        timestamp=timestamp_5,
    )

    on_event_assert_availability(
        victim_rescue_5_was_selected_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_5,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="STOU",
            area_availability=Availability(available=1),
            bspp_availability=Availability(available=1),
            raw_vehicle_id="1",
            latest_event_data=victim_rescue_5_was_selected_event.data,
        ),
    )

    # Sixth event :
    timestamp_6 = DateStr("2020-10-01T20:09")
    victim_rescue_5_was_selected_event = make_vehicle_event(
        role="vsav_solo",
        home_area="CHPT",
        raw_vehicle_id="5",
        status="arrived_at_home",
        timestamp=timestamp_6,
    )
    on_event_assert_availability(
        victim_rescue_5_was_selected_event,
        make_availability_changed_event_entity(
            timestamp=timestamp_6,
            uuid=uuid4(),
            role="vsav_solo",
            home_area="CHPT",
            area_availability=Availability(available=1),
            bspp_availability=Availability(available=2),
            raw_vehicle_id="5",
            latest_event_data=victim_rescue_5_was_selected_event.data,
        ),
    )
