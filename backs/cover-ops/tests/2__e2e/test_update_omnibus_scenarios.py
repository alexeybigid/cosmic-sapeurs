import asyncio
from typing import List, Optional

from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_vehicle_events_repository import (
    ResyncCacheFromVehicleEventsRepository,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover
from cover_ops.entrypoints.subscribe_to_domain_events import (
    subscribe_to_vehicle_changed_status,
)
from tests.utils.prepare_update_omnibus import *

from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusBalanceChangedEventData,
)
from shared.helpers.uuid import RealUuid
from shared.sapeurs_events import VehicleEvent


def prepare_scenarios(initial_vehicle_events: List[VehicleEventEntity]):
    (
        update_omnibus,
        vehicle_cache,
        omnibus_repo,
        published_events,
        domain_event_bus,
    ) = prepare_update_omnibus()
    vehicle_events_repo = InMemoryVehicleEventsRepository()
    vehicle_events_repo.vehicle_events = initial_vehicle_events

    resync_cache_from_vehicles = ResyncCacheFromVehicleEventsRepository(
        vehicle_events_repo=vehicle_events_repo,
        vehicle_cache=vehicle_cache,
        uuid=RealUuid(),
    )

    update_cover = UpdateCover(
        domain_event_bus=domain_event_bus,
        availability_events_repo=InMemoryAvailabilityEventsRepository(),
        vehicle_cache=vehicle_cache,
        uuid=RealUuid(),
    )

    add_events_to_repositories = AddEventsToRepositories(
        operation_events_repo=InMemoryOperationEventsRepository(),
        vehicle_events_repo=vehicle_events_repo,
    )

    subscribe_to_vehicle_changed_status(
        domain_event_bus, update_cover, update_omnibus, add_events_to_repositories
    )

    resync_cache_from_vehicles.execute()

    def on_event_assert_omnibus_balance(
        event: VehicleEvent,
        expected_balance: Optional[OmnibusBalance],
        expected_details: Optional[List[OmnibusDetail]],
    ):
        asyncio.run(domain_event_bus.publish(event))

        if not expected_balance or not expected_details:
            return

        expected_omnibus_event_data = OmnibusBalanceChangedEventData(
            timestamp=event.data.timestamp,
            omnibus_balance=expected_balance,
            details=expected_details,
        )

        assert published_events[-1].data == expected_omnibus_event_data
        assert omnibus_repo.omnibus_events[-1].data == expected_omnibus_event_data
        assert vehicle_cache.get_omnibus_balance() == expected_balance

    return on_event_assert_omnibus_balance


def test_omnibus_is_updated_on_vehicle_changed_status():

    on_event_assert_omnibus_balance = prepare_scenarios(
        initial_vehicle_events=[
            make_vsav_event_entity("arrived_at_home"),
            make_pse_event_entity("arrived_at_home", role="pse_omni_pump"),
        ]
    )

    on_event_assert_omnibus_balance(
        vsav_affected_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[],
    )

    on_event_assert_omnibus_balance(
        pse_downgraded_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=+1,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "vsav_on_inter_san",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_to_inter_san_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=1,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "both_on_inter_san",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        vsav_back_home_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=1,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "pse_on_inter_san",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_as_vsav_home_event,
        OmnibusBalance(
            both_available=1,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "both_available",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_to_inter_fire_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[],
    )

    on_event_assert_omnibus_balance(
        vsav_lacks_staff_omnibus_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=1,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "pse_on_inter_fire",
            )
        ],
    )

    on_event_assert_omnibus_balance(
        pse_as_pse_home_event,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=1,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id,
                pse_raw_vehicle_id,
                "STOU",
                "only_pse_available",
            )
        ],
    )


vsav_1_id = RawVehicleId("vsav_1")
vsav_2_id = RawVehicleId("vsav_2")
pse_1_id = RawVehicleId("pse_1")
pse_2_id = RawVehicleId("pse_2")

vsav_1_omnibus_at_home = make_vehicle_event_entity(
    raw_vehicle_id=vsav_1_id,
    status="arrived_at_home",
    role="vsav_omni",
    omnibus=OmnibusInfos(partner_raw_vehicle_id=pse_1_id),
)


pse_1_omnibus_and_at_home = make_vehicle_event_entity(
    raw_vehicle_id=pse_1_id,
    status="arrived_at_home",
    role="pse_omni_pump",
    omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
)


def test_omnibus_couple_changes_vsav_while_both_home():
    vsav_2_at_home = make_vehicle_event_entity(
        raw_vehicle_id=vsav_2_id, status="arrived_at_home", omnibus=None
    )

    on_event_assert_omnibus_balance = prepare_scenarios(
        initial_vehicle_events=[
            vsav_2_at_home,
            vsav_1_omnibus_at_home,
            pse_1_omnibus_and_at_home,
        ]
    )

    vsav_1_detached = make_vehicle_event(
        raw_vehicle_id=vsav_1_id,
        role="vsav_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    vsav_2_attached = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="misc_unavailable",
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_1_id,
        ),
    )
    vsav_2_attached_and_ready = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="arrived_at_home",
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_1_id,
        ),
    )

    vsav_2_is_affected = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="selected",
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_1_id,
        ),
    )

    pse_1_downgraded = make_vehicle_event(
        raw_vehicle_id=pse_1_id,
        status="arrived_at_home",
        role="pse_omni_san",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_2_id),
    )

    on_event_assert_omnibus_balance(
        vsav_1_detached,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[],
    )
    on_event_assert_omnibus_balance(vsav_2_attached, None, None)

    on_event_assert_omnibus_balance(
        vsav_2_attached_and_ready,
        OmnibusBalance(
            both_available=1,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_2_id,
                pse_1_id,
                "STOU",
                "both_available",
            )
        ],
    )
    on_event_assert_omnibus_balance(
        vsav_2_is_affected,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[],
    )

    on_event_assert_omnibus_balance(
        pse_1_downgraded,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=1,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_2_id,
                pse_1_id,
                "STOU",
                "vsav_on_inter_san",
            )
        ],
    )


def test_omnibus_couple_changes_pse_while_both_home():

    pse_2_at_home = make_vehicle_event_entity(
        raw_vehicle_id=pse_2_id, status="arrived_at_home", omnibus=None
    )

    on_event_assert_omnibus_balance = prepare_scenarios(
        initial_vehicle_events=[
            pse_2_at_home,
            vsav_1_omnibus_at_home,
            pse_1_omnibus_and_at_home,
        ]
    )

    pse_1_detached = make_vehicle_event(
        raw_vehicle_id=pse_1_id,
        role="pse_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    pse_2_attached = make_vehicle_event(
        raw_vehicle_id=vsav_2_id,
        status="misc_unavailable",
        role="pse_omni_pump",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
    )
    pse_2_attached_and_ready = make_vehicle_event(
        raw_vehicle_id=pse_2_id,
        status="arrived_at_home",
        role="pse_omni_pump",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
    )

    pse_2_is_affected = make_vehicle_event(
        raw_vehicle_id=pse_2_id,
        status="selected",
        role="pse_omni_san",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=vsav_1_id),
    )

    on_event_assert_omnibus_balance(
        pse_1_detached,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[],
    )

    on_event_assert_omnibus_balance(pse_2_attached, None, None)

    on_event_assert_omnibus_balance(
        pse_2_attached_and_ready,
        OmnibusBalance(
            both_available=1,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_1_id,
                pse_2_id,
                "STOU",
                "both_available",
            )
        ],
    )
    on_event_assert_omnibus_balance(
        pse_2_is_affected,
        OmnibusBalance(
            both_available=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=1,
            both_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_details=[
            OmnibusDetail(
                vsav_1_id,
                pse_2_id,
                "STOU",
                "pse_on_inter_san",
            )
        ],
    )
