import json
from pathlib import Path

from cover_ops.entrypoints.server import route_converted_event
from tests.utils.remove_folder_content import remove_folder_content
from tests.utils.server_fixture import make_server_fixture

from shared.helpers.uuid import uuid4
from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


async def test_handle_converters_catches_up_accumulated_events_event(
    server_fixture,
):
    remove_folder_content(Path("data"))
    vehicle_uuid = "lolo_" + uuid4()
    operation_uuid = "lulu_" + uuid4()
    event_as_dict = {
        "topic": "converters_catches_up_accumulated_events",
        "uuid": "some_catching_up_event_id",
        "data": [
            {
                "topic": "vehicle_changed_status",
                "uuid": vehicle_uuid,
                "data": {
                    "raw_vehicle_id": "1",
                    "raw_operation_id": "3",
                    "role": "vsav_solo",
                    "vehicle_name": "VSAV 103",
                    "raw_status": "parti",
                    "status": "departed_to_intervention",
                    "timestamp": "2020-12-10T12:30:00.853",
                    "home_area": "STOU",
                },
            },
            {
                "topic": "operation_changed_status",
                "uuid": operation_uuid,
                "data": {
                    "timestamp": "",
                    "raw_operation_id": "2",
                    "status": "opened",
                    "address": "161 rue guillaume tell",
                    "address_area": "CHPT",
                    "cause": "victim",
                    "raw_cause": "Feu de ...",
                },
            },
        ],
    }

    published_events = spy_on_topic(
        config.domain_event_bus, "converters_catches_up_accumulated_events"
    )

    data = json.dumps(event_as_dict)

    resp = await server_fixture.post(route_converted_event, data=data)
    assert resp.status == 200

    resp_json = await resp.json()

    assert resp_json == {"success": "true"}
    assert len(published_events) == 1
    assert published_events[0].uuid == "some_catching_up_event_id"
    assert len(config.vehicle_events_repo.df_with_flatten_data) == 1
    assert config.vehicle_events_repo.df_with_flatten_data.iloc[0].uuid == vehicle_uuid

    df_with_flatten_data = config.operation_events_repo.df_with_flatten_data
    assert len(df_with_flatten_data) == 2

    first_operation_event = df_with_flatten_data.iloc[0]
    assert first_operation_event.status == "first_vehicle_affected"

    second_operation_event = df_with_flatten_data.iloc[1]
    assert second_operation_event.status == "opened"
    assert second_operation_event.uuid == operation_uuid
