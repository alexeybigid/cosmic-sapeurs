import json

# fixture, appears like it was unused, but it is !
from pathlib import Path

from cover_ops.entrypoints.server import route_converted_event
from tests.utils.remove_folder_content import remove_folder_content
from tests.utils.server_fixture import make_server_fixture

from shared.helpers.uuid import uuid4
from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


async def test_handle_converters_initialized_last_status_of_all_vehicles_event(
    server_fixture,
):
    remove_folder_content(Path("data"))
    event_as_dict = {
        "topic": "converters_initialized_last_status_of_all_vehicles",
        "uuid": "some_initialization_event_id",
        "data": [
            {
                "topic": "vehicle_changed_status",
                "uuid": "lili_" + uuid4(),
                "data": {
                    "raw_vehicle_id": "1",
                    "raw_operation_id": "3",
                    "role": "vsav_solo",
                    "vehicle_name": "VSAV 103",
                    "raw_status": "parti",
                    "status": "departed_to_intervention",
                    "timestamp": "2020-12-10T12:30:00.853",
                    "home_area": "STOU",
                },
            }
        ],
    }

    published_events = spy_on_topic(
        config.domain_event_bus, "converters_initialized_last_status_of_all_vehicles"
    )

    data = json.dumps(event_as_dict)
    resp = await server_fixture.post(route_converted_event, data=data)
    assert resp.status == 200

    resp_json = await resp.json()

    assert resp_json == {"success": "true"}
    assert len(published_events) == 1
    assert published_events[0].uuid == "some_initialization_event_id"
    assert len(config.vehicle_events_repo.df_with_flatten_data) == 1
