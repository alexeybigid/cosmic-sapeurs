import json

from cover_ops.entrypoints.server import route_converted_event

# fixture, appears like it was unused, but it is !
from tests.utils.server_fixture import make_server_fixture

from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


async def test_publish_to_bus_and_save_operation_created_event(server_fixture):

    operation_event_as_dict = {
        "topic": "operation_changed_status",
        "uuid": "some_operation_id",
        "data": {
            "timestamp": "",
            "raw_operation_id": "2",
            "status": "opened",
            "address": "161 rue guillaume tell",
            "address_area": "CHPT",
            "cause": "victim",
            "raw_cause": "Feu de ...",
        },
    }

    published_events = spy_on_topic(config.domain_event_bus, "operation_changed_status")

    data = json.dumps(operation_event_as_dict)
    resp = await server_fixture.post(route_converted_event, data=data)
    resp_json = await resp.json()

    assert resp.status == 200
    assert resp_json == {"success": "true"}
    assert len(published_events) == 1
    assert published_events[0].uuid == "some_operation_id"
    assert len(config.operation_events_repo.df_with_flatten_data) == 1
