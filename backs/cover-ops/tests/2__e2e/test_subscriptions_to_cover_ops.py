import json

from tests.utils.server_fixture import make_server_fixture

from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.routes import make_url, sapeurs_routes
from shared.test_utils.spy_on_topic import spy_on_topic

server_fixture, config = make_server_fixture()


route_handle_converted_events = make_url(
    sapeurs_routes.prefix, sapeurs_routes.converted_event
)


async def test_subscriptions_to_cover_ops(server_fixture):

    operation_changed_status_published_events = spy_on_topic(
        config.domain_event_bus, "operation_changed_status"
    )
    ongoing_op_changed_published_events = spy_on_topic(
        config.domain_event_bus, "ongoingOpChanged"
    )
    # Operation 1 opened
    operation_1_id = RawOperationId("operation_1")
    timestamp_opening = DateStr("2021-01-02T12:00:00")
    operation_1_opened = make_operation_event(
        status="opened",
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_opening,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(operation_1_opened),
    )
    assert resp.status == 200

    assert len(operation_changed_status_published_events) == 1
    assert len(ongoing_op_changed_published_events) == 1
    assert ongoing_op_changed_published_events[-1].data == OngoingOpChangedEventData(
        bspp_ongoing_ops=0,
        area_ongoing_ops=0,
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="opened",
        timestamp=timestamp_opening,
    )

    # Vehicle A became selected
    timestamp_vehicle_selected = DateStr("2021-01-02T12:01:00.0")
    vehicle_A_selected = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_A"),
        status="selected",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=None,
        timestamp=timestamp_vehicle_selected,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(vehicle_A_selected),
    )
    assert len(ongoing_op_changed_published_events) == 1

    # Vehicle A departed to intervention
    timestamp_vehicle_selected = DateStr("2021-01-02T12:01:00.0")
    vehicle_A_selected = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_A"),
        status="departed_to_intervention",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_selected,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(vehicle_A_selected),
    )
    assert resp.status == 200

    assert len(ongoing_op_changed_published_events) == 2
    assert ongoing_op_changed_published_events[-1].data == OngoingOpChangedEventData(
        bspp_ongoing_ops=1,
        area_ongoing_ops=1,
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="first_vehicle_affected",
        timestamp=timestamp_vehicle_selected,
    )
    # Vehicle B also selected to same operation
    timestamp_vehicle_selected = DateStr("2021-01-02T12:03:00.0")
    vehicle_B_selected = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_B"),
        status="arrived_on_intervention",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_selected,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(vehicle_B_selected),
    )
    assert resp.status == 200

    assert len(ongoing_op_changed_published_events) == 2

    # Vehicle A became available
    timestamp_vehicle_A_available = DateStr("2021-01-02T13:05:00.0")
    vehicle_A_selected = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_A"),
        status="arrived_at_home",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_A_available,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(vehicle_A_selected),
    )
    assert resp.status == 200

    assert len(ongoing_op_changed_published_events) == 2

    # Vehicle B became available
    timestamp_vehicle_B_available = DateStr("2021-01-02T14:03:00.0")
    vehicle_B_selected = make_vehicle_event(
        raw_vehicle_id=RawVehicleId("vehicle_B"),
        status="misc_available",
        role="vsav_solo",
        home_area="CHPT",
        raw_operation_id=operation_1_id,
        timestamp=timestamp_vehicle_B_available,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(vehicle_B_selected),
    )
    assert resp.status == 200

    assert len(ongoing_op_changed_published_events) == 3

    assert ongoing_op_changed_published_events[-1].data == OngoingOpChangedEventData(
        bspp_ongoing_ops=0,
        area_ongoing_ops=0,
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_1_id,
        status="all_vehicles_released",
        timestamp=timestamp_vehicle_B_available,
    )

    # Operation 1 all vehicle released (again...) - Should not really happen, but who knows.
    timestamp_closing = DateStr("2021-01-02T15:00:00")
    operation_1_closed = make_operation_event(
        status="all_vehicles_released",
        cause="victim",
        address_area="STOU",
        raw_operation_id=RawOperationId("operation_1"),
        timestamp=timestamp_closing,
    ).as_dict()

    resp = await server_fixture.post(
        route_handle_converted_events,
        data=json.dumps(operation_1_closed),
    )
    assert len(ongoing_op_changed_published_events) == 3
