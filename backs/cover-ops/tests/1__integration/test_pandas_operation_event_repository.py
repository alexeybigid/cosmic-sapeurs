import json
from dataclasses import asdict
from pathlib import Path
from typing import Dict, List, Optional

import pandas as pd
from cover_ops.adapters.pandas.pandas_operation_events_repository import (
    PandasOperationEventsRepository,
)
from cover_ops.helpers.factories.operation_event_entity_factory import (
    make_operation_event_entity,
)
from cover_ops.helpers.mkdir_if_relevant import mkdir_if_relevant
from tests.utils.assert_helpers import assert_list_events_have_uuid
from tests.utils.prepare_csv_from_kwargs import prepare_csv_from_kwargs

from shared.data_transfert_objects.operation_event_data import (
    RawOperationId,
    operation_closed,
    operation_has_all_vehicles_released,
    operation_has_first_affected_vehicle,
    operation_just_opened,
)
from shared.helpers.date import DateStr

parent_path = Path("tests/1__integration/temp_data")
mkdir_if_relevant(parent_path)
csv_path = parent_path / "operation_event_entities.csv"


def prepare_pandas_operation_events_repository(
    events_kwargs: Optional[List[Dict]] = None,
):
    prepare_csv_from_kwargs(
        csv_path=csv_path,
        factory=make_operation_event_entity,
        events_kwargs=events_kwargs,
    )
    pandas_operation_events_repository = PandasOperationEventsRepository(
        csv_path=csv_path
    )
    return pandas_operation_events_repository


def test_can_add_to_pandas_operation_events_repository():
    pandas_operation_events_repository = prepare_pandas_operation_events_repository()
    operation_event_entity = make_operation_event_entity(
        status="validated",
        raw_operation_id="3",
        timestamp=DateStr("2020-01-01T12:00:00.0"),
        uuid="9b25eabb-fbae-4eb9-90f8-2db3f77d30a6",
    )
    pandas_operation_events_repository.add(operation_event_entity)
    df = pd.read_csv(csv_path)
    last_row = df.iloc[-1]
    assert not last_row.empty
    assert last_row.uuid == operation_event_entity.uuid
    assert last_row.topic == operation_event_entity.topic
    assert json.loads(last_row.data) == asdict(operation_event_entity.data)


operation_id_1 = RawOperationId("operation_1")
operation_id_2 = RawOperationId("operation_2")
operation_id_3 = RawOperationId("operation_3")
operation_id_4 = RawOperationId("operation_4")


def test_get_events_for_id_with_status_in_when_empty():
    operation_events_repo = prepare_pandas_operation_events_repository()
    assert operation_events_repo.get_events_for_id_with_status_in(operation_id_1) == []


def test_get_events_for_id_with_status_in():
    operation_events_repo = prepare_pandas_operation_events_repository(
        [
            {
                "raw_operation_id": operation_id_1,
                "status": "opened",
                "cause": "victim",
                "address_area": "CHPT",
                "uuid": "operation_1_opened",
            },
            {
                "raw_operation_id": operation_id_1,
                "status": "validated",
                "uuid": "operation_1_validated",
            },
            {
                "raw_operation_id": operation_id_2,
                "status": "closed",
                "uuid": "operation_2_closed",
            },
            {
                "raw_operation_id": operation_id_2,
                "status": "all_vehicles_released",
                "cause": "victim",
                "address_area": "CHPT",
                "uuid": "operation_2_released",
            },
        ]
    )

    assert_list_events_have_uuid(
        operation_events_repo.get_events_for_id_with_status_in(
            operation_id_1, statuses=["opened"]
        ),
        ["operation_1_opened"],
    )

    assert_list_events_have_uuid(
        operation_events_repo.get_events_for_id_with_status_in(
            operation_id_1, statuses=["closed", "validated"]
        ),
        ["operation_1_validated"],
    )
    assert_list_events_have_uuid(
        operation_events_repo.get_events_for_id_with_status_in(
            operation_id_2, statuses=["all_vehicles_released", "closed"]
        ),
        ["operation_2_released", "operation_2_closed"],
    )


def test_get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status_if_empty():
    operation_events_repo = prepare_pandas_operation_events_repository()
    assert (
        operation_events_repo.get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status()
        == []
    )


def test_get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status():
    operation_events_repo = prepare_pandas_operation_events_repository(
        [
            {
                "raw_operation_id": operation_id_1,
                "status": operation_just_opened,
                "cause": "victim",
                "address_area": "CHPT",
                "uuid": "operation_1_opened",
            },
            {
                "raw_operation_id": operation_id_1,
                "status": "validated",
                "uuid": "operation_1_validated",
            },
            {
                "raw_operation_id": operation_id_2,
                "status": operation_closed,
                "uuid": "operation_2_closed",
            },
            {
                "raw_operation_id": operation_id_4,
                "status": operation_just_opened,
                "uuid": "operation_4_opened",
            },
            {
                "raw_operation_id": operation_id_3,
                "status": operation_has_first_affected_vehicle,
                "uuid": "operation_3_has_first_vehicle",
            },
            {
                "raw_operation_id": operation_id_4,
                "status": operation_has_first_affected_vehicle,
                "uuid": "operation_4_has_first_vehicle",
            },
            {
                "raw_operation_id": operation_id_4,
                "status": operation_has_all_vehicles_released,
                "uuid": "operation_4_has_all_vehicles_released",
            },
        ]
    )
    result = (
        operation_events_repo.get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status()
    )
    assert len(result) == 2
    result[0].uuid == "operation_1_opened"
    result[1].uuid == "operation_3_has_first_vehicle"
