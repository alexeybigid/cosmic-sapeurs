from cover_ops.adapters.redis_cache.redis_operation_cache import RedisOperationCache
from cover_ops.adapters.redis_cache.redis_utils import make_redis_instance
from cover_ops.domain.ports.operation_cache import OperationStoredInfos
from cover_ops.domain.ports.operation_events_repository import (
    OngoingOpsCountByCauseByArea,
)
from redis import Redis
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.operation_event_data import RawOperationId


def prepare_redis_cache():
    r = make_redis_instance()
    r.flushdb()
    operation_cache = RedisOperationCache(redis_instance=r)
    return operation_cache


def test_initialize_operation_cache():
    operation_cache = prepare_redis_cache()
    assert (
        operation_cache.get_ongoing_ops_count_for_area_for_cause("PARM", "victim") == 0
    )
    assert operation_cache.get_ongoing_ops_counts_for_cause("victim") == [0] * len(
        bspp_area_options
    )


operation_8 = RawOperationId("8")


def test_operation_cache_set_and_get_stored_infos_for_operation_id():
    operation_cache = prepare_redis_cache()
    given_operation_stored_infos_for_operation_id = OperationStoredInfos(
        status="first_vehicle_affected",
        latest_ongoing_op_changed_event_data=make_ongoing_ops_changed_event_data(
            raw_operation_id=operation_8, status="first_vehicle_affected"
        ),
    )
    operation_cache.set_operation_stored_infos_for_operation_id(
        operation_8, given_operation_stored_infos_for_operation_id
    )
    assert (
        operation_cache.get_operation_stored_infos_for_operation_id(operation_8)
        == given_operation_stored_infos_for_operation_id
    )


def test_operation_cache_remove_stored_infos_for_operation_id():
    operation_cache = prepare_redis_cache()
    operation_cache.set_operation_stored_infos_for_operation_id(
        operation_8, OperationStoredInfos("all_vehicles_released", None)
    )
    operation_cache.remove_operation_stored_infos_for_operation_id(operation_8)
    assert (
        operation_cache.get_operation_stored_infos_for_operation_id(operation_8) is None
    )


def test_operation_cache_set_ongoing_ops_count_for_causes_for_areas():
    operation_cache = prepare_redis_cache()
    given_ongoing_ops_count_for_causes_for_areas: OngoingOpsCountByCauseByArea = {
        "victim": {"STOU": 1},
        "fire": {"STOU": 2, "CHPT": 8},
    }
    operation_cache.set_ongoing_ops_count_for_causes_for_areas(
        given_ongoing_ops_count_for_causes_for_areas
    )
    assert (
        operation_cache.get_ongoing_ops_count_for_area_for_cause("STOU", "victim") == 1
    )
    assert operation_cache.get_ongoing_ops_count_for_area_for_cause("STOU", "fire") == 2
    assert operation_cache.get_ongoing_ops_count_for_area_for_cause("CHPT", "fire") == 8
    assert set(operation_cache.get_ongoing_ops_counts_for_cause("fire")) == {0, 2, 8}


def test_operation_cache_get_all_operation_stored_infos():
    operation_cache = prepare_redis_cache()
    stored_infos_op_8 = OperationStoredInfos(
        status="first_vehicle_affected",
        latest_ongoing_op_changed_event_data=make_ongoing_ops_changed_event_data(
            raw_operation_id=operation_8, status="first_vehicle_affected"
        ),
    )
    operation_9 = RawOperationId("operation_9")
    stored_infos_op_9 = OperationStoredInfos(
        status="all_vehicles_released",
        latest_ongoing_op_changed_event_data=make_ongoing_ops_changed_event_data(
            raw_operation_id=operation_9,
            status="all_vehicles_released",
        ),
    )
    operation_cache.set_operation_stored_infos_for_operation_id(
        operation_8, stored_infos_op_8
    )
    operation_cache.set_operation_stored_infos_for_operation_id(
        operation_9, stored_infos_op_9
    )

    actual_all_operation_stored_infos = operation_cache.get_all_operation_stored_infos()
    assert len(actual_all_operation_stored_infos) == 2
    assert stored_infos_op_8 in actual_all_operation_stored_infos
    assert stored_infos_op_9 in actual_all_operation_stored_infos
