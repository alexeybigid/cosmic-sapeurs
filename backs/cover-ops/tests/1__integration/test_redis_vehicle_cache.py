from cover_ops.adapters.redis_cache.redis_utils import make_redis_instance
from cover_ops.adapters.redis_cache.redis_vehicle_cache import RedisVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusDetail,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.factories.vehicle_event_factory import make_vehicle_event_data


def prepare_redis_cache() -> RedisVehicleCache:
    r = make_redis_instance()
    r.flushdb()
    vehicle_cache = RedisVehicleCache(redis_instance=r)
    return vehicle_cache


vehicle_1_id = RawVehicleId("1")


def test_initialize_vehicle_cache():
    vehicle_cache = prepare_redis_cache()
    assert (
        vehicle_cache.get_latest_vehicle_event_data_for_vehicle_id(vehicle_1_id) == None
    )
    assert vehicle_cache.get_omnibus_balance() == OmnibusBalance()
    assert vehicle_cache.get_availability_stored_infos_for_area_for_role(
        "STOU", "vsav_solo"
    ) == AvailabilityStoredInfos(availability=Availability(), latest_event_data=None)
    assert vehicle_cache.get_bspp_availability_stored_infos_for_role(
        "vsav_solo"
    ) == AvailabilityStoredInfos(availability=Availability(), latest_event_data=None)


def test_set_and_get_latest_vehicle_event_data_for_vehicle_id():
    vehicle_cache = prepare_redis_cache()
    vehicle_1_data = make_vehicle_event_data(raw_vehicle_id=vehicle_1_id)

    vehicle_cache.set_latest_vehicle_event_data_for_vehicle_id(vehicle_1_data)
    assert (
        vehicle_cache.get_latest_vehicle_event_data_for_vehicle_id(vehicle_1_id)
        == vehicle_1_data
    )


def test_set_and_get_omnibus_balance():
    vehicle_cache = prepare_redis_cache()
    actual_omnibus_balance = OmnibusBalance(pse_on_inter_fire=2, both_available=89)
    vehicle_cache.set_omnibus_balance(actual_omnibus_balance)
    assert vehicle_cache.get_omnibus_balance() == actual_omnibus_balance


def test_set_and_get_availability_stored_infos_for_area_for_role():
    vehicle_cache = prepare_redis_cache()
    area = "STOU"
    role = "vsav_solo"
    availability_stored_info = AvailabilityStoredInfos(
        availability=Availability(recoverable=3),
        latest_event_data=make_vehicle_event_data(),
    )

    vehicle_cache.set_availability_stored_infos_for_area_for_role(
        area, role, availability_stored_info
    )
    assert (
        vehicle_cache.get_availability_stored_infos_for_area_for_role(area, role)
        == availability_stored_info
    )


def test_set_and_get_bspp_availability_stored_infos_for_role():
    vehicle_cache = prepare_redis_cache()

    role = "vsav_solo"
    availability_stored_info = AvailabilityStoredInfos(
        availability=Availability(recoverable=3),
        latest_event_data=make_vehicle_event_data(),
    )
    vehicle_cache.set_bspp_availability_stored_infos_for_role(
        role, availability_stored_info
    )
    assert (
        vehicle_cache.get_bspp_availability_stored_infos_for_role(role)
        == availability_stored_info
    )


def test_get_latest_vehicle_event_datas_for_role_for_area():
    vehicle_cache = prepare_redis_cache()

    data_vsav_stou_0 = make_vehicle_event_data(
        role="vsav_solo", home_area="STOU", raw_vehicle_id=RawVehicleId("vsav_stou_0")
    )
    data_vsav_stou_1 = make_vehicle_event_data(
        role="vsav_solo", home_area="STOU", raw_vehicle_id=RawVehicleId("vsav_stou_1")
    )
    data_pse_stou = make_vehicle_event_data(
        role="pse_solo", home_area="STOU", raw_vehicle_id=RawVehicleId("pse_stou_0")
    )
    data_vsav_parm = make_vehicle_event_data(
        role="vsav_solo", home_area="PARM", raw_vehicle_id=RawVehicleId("vsav_parm_0")
    )

    for data in [data_vsav_stou_0, data_vsav_stou_1, data_pse_stou, data_vsav_parm]:
        vehicle_cache.set_latest_vehicle_event_data_for_vehicle_id(data)

    latest_vehicle_event_datas_for_area_for_role = (
        vehicle_cache.get_latest_vehicle_event_datas_for_area_for_role(
            "STOU", "vsav_solo"
        )
    )

    assert len(latest_vehicle_event_datas_for_area_for_role) == 2
    assert data_vsav_stou_0 in latest_vehicle_event_datas_for_area_for_role
    assert data_vsav_stou_1 in latest_vehicle_event_datas_for_area_for_role


def test_get_latest_vehicle_event_datas_for_raw_operation_id():
    vehicle_cache = prepare_redis_cache()

    data_vehicle_0_operation_0 = make_vehicle_event_data(
        raw_operation_id=RawOperationId("operation_0"),
        raw_vehicle_id=RawVehicleId("vehicle_0"),
    )
    data_vehicle_1_operation_0 = make_vehicle_event_data(
        raw_operation_id=RawOperationId("operation_0"),
        raw_vehicle_id=RawVehicleId("vehicle_1"),
    )
    data_vehicle_2_operation_1 = make_vehicle_event_data(
        raw_operation_id=RawOperationId("operation_1"),
        raw_vehicle_id=RawVehicleId("vehicle_2"),
    )

    data_vehicle_2_operation_2 = make_vehicle_event_data(
        raw_operation_id=RawOperationId("operation_2"),
        raw_vehicle_id=RawVehicleId("vehicle_2"),
    )

    for data in [
        data_vehicle_0_operation_0,
        data_vehicle_1_operation_0,
        data_vehicle_2_operation_1,
        data_vehicle_2_operation_2,
    ]:
        vehicle_cache.set_latest_vehicle_event_data_for_vehicle_id(data)

    latest_vehicle_event_datas_for_operation_0 = (
        vehicle_cache.get_vehicle_event_data_for_operation_id(
            RawOperationId("operation_0")
        )
    )

    assert len(latest_vehicle_event_datas_for_operation_0) == 2
    assert data_vehicle_0_operation_0 in latest_vehicle_event_datas_for_operation_0
    assert data_vehicle_1_operation_0 in latest_vehicle_event_datas_for_operation_0

    assert vehicle_cache.r.smembers("raw_operation_id:operation_1") == set()


def test_add_omnibus_then_get_omnibus_details_then_removes_omnibus():
    vehicle_cache = prepare_redis_cache()
    vsav_id = RawVehicleId("vsav_id")
    omni1_detail = OmnibusDetail(
        area="STOU",
        balance_kind="vsav_on_inter_san",
        pse_id=RawVehicleId("pse_id"),
        vsav_id=vsav_id,
    )
    omni2_detail = OmnibusDetail(
        area="PARM",
        balance_kind="pse_on_inter_san",
        pse_id=RawVehicleId("pse_2_id"),
        vsav_id=RawVehicleId("vsav_2_id"),
    )
    vehicle_cache.add_omnibus(omnibus_detail=omni1_detail)
    vehicle_cache.add_omnibus(omnibus_detail=omni2_detail)

    omni1_retrieved = vehicle_cache.get_omnibus_given_vsav_id(vsav_id=vsav_id)
    assert omni1_retrieved == omni1_detail

    details = vehicle_cache.get_omnibus_details()
    assert len(details) == 2
    assert omni1_detail in details
    assert omni2_detail in details

    vehicle_cache.remove_omnibus_given_vsav_id(vsav_id=vsav_id)
    details_after_remove = vehicle_cache.get_omnibus_details()
    assert details_after_remove == [omni2_detail]
