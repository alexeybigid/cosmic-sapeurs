import json
import os
from dataclasses import asdict
from pathlib import Path
from typing import Dict, List, Optional

import pandas as pd
import pytest
from cover_ops.adapters.pandas.pandas_vehicle_events_repository import (
    PandasVehicleEventsRepository,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_entity,
)
from cover_ops.helpers.mkdir_if_relevant import mkdir_if_relevant
from tests.utils.assert_helpers import assert_list_events_have_uuid
from tests.utils.prepare_csv_from_kwargs import prepare_csv_from_kwargs

from shared.data_transfert_objects.vehicle_event_data import OmnibusInfos, RawVehicleId
from shared.ports.abstract_events_repository import AlreadyExistingEventUuid

parent_path = Path("tests/1__integration/temp_data")
mkdir_if_relevant(parent_path)
csv_path = parent_path / "vehicle_event_entities.csv"

initial_vehicle_events_kwargs = [
    {
        "raw_vehicle_id": "1",
        "status": "selected",
        "role": "vsav_solo",
        "home_area": "STOU",
        "uuid": "vsav_1_stou_selected",
    },
    {
        "raw_vehicle_id": "2",
        "status": "arrived_on_intervention",
        "role": "pse_omni_san",
        "home_area": "STOU",
        "uuid": "pse_2_stou_inter",
        "omnibus": {"partner_raw_vehicle_id": "8"},
    },
    # pse_omni_san 2 becomes a pse_omni_pump
    {
        "raw_vehicle_id": "2",
        "status": "arrived_on_intervention",
        "role": "pse_omni_pump",
        "home_area": "STOU",
        "uuid": "pse_2_stou_inter",
        "omnibus": {"partner_raw_vehicle_id": "8"},
    },
    {
        "raw_vehicle_id": "2",
        "status": "arrived_at_home",
        "role": "pse_omni_pump",
        "home_area": "STOU",
        "uuid": "pse_2_stou_home",
        "omnibus": {"partner_raw_vehicle_id": "8"},
    },
    {
        "raw_vehicle_id": "3",
        "status": "misc_available",
        "role": "pse_solo",
        "home_area": "STOU",
        "uuid": "pse_3_stou_available",
    },
    #  pse 3 changes status and area !!
    {
        "raw_vehicle_id": "3",
        "status": "broken",
        "role": "pse_solo",
        "home_area": "CHPT",
        "uuid": "pse_3_chpt_broken",
    },
    # From SDIS should be ignored
    {
        "raw_vehicle_id": "4",
        "status": "broken",
        "role": "pse_solo",
        "home_area": "SDIS",
        "uuid": "pse_4_sdis_broken",
    },
    {
        "raw_vehicle_id": "5",
        "status": "misc_available",
        "role": "vsav_solo",
        "home_area": "STOU",
        "uuid": "vsav_5_stou_available",
    },
    {
        "raw_vehicle_id": "6",
        "status": "misc_available",
        "role": "vsav_solo",
        "home_area": "PARM",
        "uuid": "vsav_6_parm_available",
    },
    # Role other should be ignored
    {
        "raw_vehicle_id": "7",
        "status": "misc_available",
        "role": "other",
        "home_area": "CHPT",
        "uuid": "other_7_chpt_available",
    },
]


def prepare_pandas_vehicle_events_repository(
    events_kwargs: Optional[List[Dict]] = None,
):
    prepare_csv_from_kwargs(
        csv_path=csv_path,
        events_kwargs=events_kwargs,
        factory=make_vehicle_event_entity,
    )
    pandas_vehicle_events_repository = PandasVehicleEventsRepository(csv_path=csv_path)
    return pandas_vehicle_events_repository


def test_creates_csv_if_provided_csv_path_does_not_exists():
    # remove csv
    initially_no_file_csv_path = (
        Path("tests/1__integration/temp_data") / "initially_no_file_csv_path.csv"
    )
    if os.path.exists(initially_no_file_csv_path):
        os.remove(initially_no_file_csv_path)
    _ = PandasVehicleEventsRepository(csv_path=initially_no_file_csv_path)
    df = pd.read_csv(initially_no_file_csv_path)
    assert df.empty


def test_can_add_to_pandas_vehicle_events_repository():
    pandas_vehicle_events_repository = prepare_pandas_vehicle_events_repository()
    vehicle_event_entity = make_vehicle_event_entity()
    pandas_vehicle_events_repository.add(vehicle_event_entity)
    df = pd.read_csv(csv_path)
    # convert_int64_to_string_in_df(df)
    last_row = df.iloc[-1]
    assert not last_row.empty

    assert last_row.uuid == vehicle_event_entity.uuid
    assert last_row.topic == vehicle_event_entity.topic
    assert json.loads(last_row.data) == asdict(vehicle_event_entity.data)


def test_cannot_add_to_csv_if_already_exists():
    pandas_vehicle_events_repository = prepare_pandas_vehicle_events_repository()
    vehicle_event_entity = make_vehicle_event_entity()

    with pytest.raises(AlreadyExistingEventUuid):
        # add the same event twice
        pandas_vehicle_events_repository.add(vehicle_event_entity)
        pandas_vehicle_events_repository.add(vehicle_event_entity)


def test_get_latest_event_by_raw_vehicle_id_when_empty():
    pandas_vehicle_events_repository = prepare_pandas_vehicle_events_repository()
    assert pandas_vehicle_events_repository.get_latest_event_by_raw_vehicle_id() == {}


def test_get_latest_event_by_raw_vehicle_id():
    pandas_vehicle_events_repository = prepare_pandas_vehicle_events_repository(
        initial_vehicle_events_kwargs
    )

    actual_latest_event_by_raw_vehicle_id = (
        pandas_vehicle_events_repository.get_latest_event_by_raw_vehicle_id()
    )
    assert actual_latest_event_by_raw_vehicle_id[
        RawVehicleId("2")
    ].data.omnibus == OmnibusInfos(partner_raw_vehicle_id=RawVehicleId("8"))

    assert_list_events_have_uuid(
        list(actual_latest_event_by_raw_vehicle_id.values()),
        [
            "vsav_1_stou_selected",
            "pse_2_stou_home",
            "pse_3_chpt_broken",
            "pse_4_sdis_broken",
            "vsav_5_stou_available",
            "vsav_6_parm_available",
            "other_7_chpt_available",
        ],
    )


def test_list_latest_events_by_availability_by_role_by_area():
    pandas_vehicle_events_repository = prepare_pandas_vehicle_events_repository(
        initial_vehicle_events_kwargs
    )
    (
        events_by_availability_by_area_by_role,
        events_by_availability_by_role,
    ) = (
        pandas_vehicle_events_repository.list_latest_events_by_availability_by_role_by_area_and_for_bspp()
    )

    assert_list_events_have_uuid(
        events_by_availability_by_area_by_role["STOU"]["vsav_solo"]["in_service"],
        ["vsav_1_stou_selected"],
    )
    assert_list_events_have_uuid(
        events_by_availability_by_area_by_role["PARM"]["vsav_solo"]["available"],
        ["vsav_6_parm_available"],
    )
    assert_list_events_have_uuid(
        events_by_availability_by_area_by_role["STOU"]["vsav_solo"]["available"],
        ["vsav_5_stou_available"],
    )
    assert_list_events_have_uuid(
        events_by_availability_by_area_by_role["STOU"]["pse_omni_pump"]["available"],
        ["pse_2_stou_home"],
    )
    assert_list_events_have_uuid(
        events_by_availability_by_area_by_role["CHPT"]["pse_solo"]["unavailable"],
        ["pse_3_chpt_broken"],
    )

    assert_list_events_have_uuid(
        events_by_availability_by_role["pse_solo"]["unavailable"],
        ["pse_3_chpt_broken"],
    )
    assert_list_events_have_uuid(
        events_by_availability_by_role["vsav_solo"]["available"],
        ["vsav_5_stou_available", "vsav_6_parm_available"],
    )
