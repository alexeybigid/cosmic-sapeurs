from pathlib import Path

from cover_ops.adapters.csv_events_repositories import (
    CsvAvailabilityEventsRepository,
    CsvOngoingOpEventsRepository,
    writerow,
)
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_entity,
)
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_op_changed_event_entity,
)

from shared.helpers.csv import reset_csv_file

folder_path = Path("tests/1__integration/temp_data")
csv_path = folder_path / "repo.csv"


def test_csv_availability_events_repository_can_add_and_retrieve():
    reset_csv_file(csv_path)
    csv_availability_events_repository = CsvAvailabilityEventsRepository(
        csv_path=csv_path
    )
    entity_0 = make_availability_changed_event_entity()
    csv_availability_events_repository.add(entity_0)
    actual_entities_from_csv = csv_availability_events_repository._from_csv()
    assert actual_entities_from_csv == [entity_0]

    csv_availability_events_repository_from_existing_csv = (
        CsvAvailabilityEventsRepository(csv_path=csv_path)
    )
    assert csv_availability_events_repository_from_existing_csv.availability_events == [
        entity_0
    ]


def test_csv_availability_events_repository_passes_if_wrong_topic():
    reset_csv_file(csv_path)
    writerow(csv_path, ["uuid", "topic", "data"])
    writerow(csv_path, ["some_uuid", "some_wrong_topic", "some_data"])
    csv_availability_events_repository_from_existing_csv = (
        CsvAvailabilityEventsRepository(csv_path=csv_path)
    )
    csv_availability_events_repository_from_existing_csv.availability_events == []


def test_csv_ongoing_events_repository_can_add_and_retrieve():
    reset_csv_file(csv_path)
    csv_ongoing_op_events_repository = CsvOngoingOpEventsRepository(csv_path=csv_path)
    entity_0 = make_ongoing_op_changed_event_entity()
    csv_ongoing_op_events_repository.add(entity_0)
    actual_entities_from_csv = csv_ongoing_op_events_repository._from_csv()
    assert actual_entities_from_csv == [entity_0]

    csv_ongoing_op_events_repository_from_existing_csv = CsvOngoingOpEventsRepository(
        csv_path=csv_path
    )
    assert csv_ongoing_op_events_repository_from_existing_csv.ongoing_op_events == [
        entity_0
    ]
