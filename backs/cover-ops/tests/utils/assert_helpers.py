from typing import List


def assert_subset_of(subset=dict, expected=dict):
    for key, value in expected.items():
        assert (
            value == subset[key]
        ), f"key : {key} \n expected is: {value} \n value was: {subset[key]}"


def assert_list_events_have_uuid(
    actual_events: List,  # TODO : type this.
    expected_uuids: List[str],
):
    assert len(actual_events) == len(expected_uuids)
    assert set([event.uuid for event in actual_events]) == set(expected_uuids)
