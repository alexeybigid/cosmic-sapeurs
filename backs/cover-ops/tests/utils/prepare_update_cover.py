from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import InMemoryVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_vehicle_events_repository import (
    ResyncCacheFromVehicleEventsRepository,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover

from shared.helpers.clock import CustomClock
from shared.helpers.uuid import CustomUuid, RealUuid
from shared.test_utils.spy_on_topic import spy_on_topic


def prepare_update_cover(vehicle_events_repo: InMemoryVehicleEventsRepository = None):
    custom_clock = CustomClock()
    vehicle_events_repo = vehicle_events_repo or InMemoryVehicleEventsRepository()
    availability_events_repo = InMemoryAvailabilityEventsRepository()
    vehicle_cache = InMemoryVehicleCache()
    domain_event_bus = DomainEventBus(custom_clock)
    custom_uuid = CustomUuid()
    update_cover = UpdateCover(
        availability_events_repo=availability_events_repo,
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
        vehicle_cache=vehicle_cache,
    )
    ResyncCacheFromVehicleEventsRepository(
        vehicle_events_repo, vehicle_cache, custom_uuid
    ).execute()
    published_events = spy_on_topic(domain_event_bus, "availabilityChanged")
    return (
        vehicle_events_repo,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        published_events,
    )
