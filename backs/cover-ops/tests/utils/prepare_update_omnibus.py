import asyncio
from typing import List, Literal, Optional

from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.domain.ports.omnibus_events_repository import (
    InMemoryOmnibusEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import InMemoryVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_vehicle_events_repository import (
    ResyncCacheFromVehicleEventsRepository,
)
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_entity,
)

from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusBalanceChangedEventData,
    OmnibusDetail,
)
from shared.data_transfert_objects.vehicle_event_data import (
    OmnibusInfos,
    RawVehicleId,
    VehicleStatus,
)
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.clock import CustomClock
from shared.helpers.uuid import CustomUuid, RealUuid
from shared.sapeurs_events import VehicleEvent
from shared.test_utils.spy_on_topic import spy_on_topic

vsav_raw_vehicle_id = RawVehicleId("vsav_190")
pse_raw_vehicle_id = RawVehicleId("ps_237")


def prepare_update_omnibus(
    initial_vehicle_entities: Optional[List[VehicleEventEntity]] = None,
):
    clock = CustomClock()
    domain_event_bus = DomainEventBus(clock=clock)
    vehicle_repo = InMemoryVehicleEventsRepository()
    omnibus_repo = InMemoryOmnibusEventsRepository()
    custom_uuid = CustomUuid()
    vehicle_cache = InMemoryVehicleCache()
    update_omnibus = UpdateOmnibus(
        omnibus_events_repo=omnibus_repo,
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
        vehicle_cache=vehicle_cache,
    )
    if initial_vehicle_entities:
        vehicle_repo.vehicle_events = initial_vehicle_entities
    ResyncCacheFromVehicleEventsRepository(
        vehicle_repo, vehicle_cache, RealUuid()
    ).execute()

    published_events = spy_on_topic(domain_event_bus, "omnibusBalanceChanged")
    return (
        update_omnibus,
        vehicle_cache,
        omnibus_repo,
        published_events,
        domain_event_bus,
    )


def execute_update_omnibus_with_initialized_balance_assert_result(
    initial_vehicle_events: List[VehicleEvent],
    vehicle_event: VehicleEvent,
    expected_omnibus_balance: Optional[OmnibusBalance],
    expected_omnibus_details: Optional[List[OmnibusDetail]],
):
    (update_omnibus, _, omnibus_repo, published_events, _,) = prepare_update_omnibus(
        [
            VehicleEventEntity(data=event.data, uuid=event.uuid)
            for event in initial_vehicle_events
        ]
    )

    asyncio.run(update_omnibus.execute(vehicle_event))

    if expected_omnibus_balance is None or expected_omnibus_details is None:
        assert len(published_events) == 0
        return

    expected_omnibus_event_data = OmnibusBalanceChangedEventData(
        timestamp=vehicle_event.data.timestamp,
        omnibus_balance=expected_omnibus_balance,
        details=expected_omnibus_details,
    )
    assert len(published_events) == 1

    assert published_events[0].data == expected_omnibus_event_data
    assert omnibus_repo.omnibus_events[0].data == expected_omnibus_event_data


def make_vsav_event(status: VehicleStatus):
    return make_vehicle_event(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_raw_vehicle_id,
        ),
        status=status,
    )


def make_vsav_event_entity(status: VehicleStatus) -> VehicleEventEntity:
    return make_vehicle_event_entity(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_raw_vehicle_id,
        ),
        status=status,
    )


def make_pse_event(
    status: VehicleStatus, role: Literal["pse_omni_san", "pse_omni_pump"]
):
    return make_vehicle_event(
        raw_vehicle_id=pse_raw_vehicle_id,
        role=role,
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=vsav_raw_vehicle_id,
        ),
        status=status,
    )


def make_pse_event_entity(
    status: VehicleStatus, role: Literal["pse_omni_san", "pse_omni_pump"]
):
    return make_vehicle_event_entity(
        raw_vehicle_id=pse_raw_vehicle_id,
        role=role,
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=vsav_raw_vehicle_id,
        ),
        status=status,
    )


vsav_affected_event = make_vsav_event("selected")
pse_as_pse_home_event = make_pse_event("arrived_at_home", "pse_omni_pump")
pse_as_vsav_home_event = make_pse_event("arrived_at_home", "pse_omni_san")
vsav_lacks_staff_omnibus_event = make_vsav_event(status="unavailable_omnibus")
pse_to_inter_san_event = make_pse_event("departed_to_intervention", "pse_omni_san")
pse_to_inter_fire_event = make_pse_event("departed_to_intervention", "pse_omni_pump")
vsav_back_home_event = make_vsav_event("arrived_at_home")
pse_back_home_event = make_pse_event("arrived_at_home", "pse_omni_san")
pse_downgraded_event = make_pse_event(status="arrived_at_home", role="pse_omni_san")
pse_affected_as_pump_event = make_pse_event(status="selected", role="pse_omni_pump")
