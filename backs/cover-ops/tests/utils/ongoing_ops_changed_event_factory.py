from cover_ops.domain.entities.event_entities import OngoingOpChangedEventEntity
from faker import Faker

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
    RawOperationId,
)
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4

fake = Faker()

default_timestamp = DateStr("2020-10-31T12:00:00.0")


def make_ongoing_ops_changed_event_data(
    timestamp: DateStr = None,
    bspp_ongoing_ops: int = None,
    area_ongoing_ops: int = None,
    cause: OperationCause = None,
    address_area: Area = None,
    raw_operation_id: str = None,
    status: OperationStatus = None,
) -> OngoingOpChangedEventData:
    bspp_ongoing_ops = bspp_ongoing_ops if bspp_ongoing_ops is not None else 5
    area_ongoing_ops = area_ongoing_ops if area_ongoing_ops is not None else 2
    cause = cause or "victim"
    address_area = address_area or "CHPT"
    raw_operation_id = RawOperationId(
        raw_operation_id if raw_operation_id else "default_operation_id"
    )
    return OngoingOpChangedEventData(
        timestamp=timestamp or default_timestamp,
        bspp_ongoing_ops=bspp_ongoing_ops,
        area_ongoing_ops=area_ongoing_ops,
        cause=cause,
        address_area=address_area,
        raw_operation_id=raw_operation_id,
        status=status or "opened",
    )


def make_ongoing_op_changed_event_entity(
    uuid: str = None,
    *,
    # Following args are for make_ongoing_ops_changed_event_data
    timestamp: DateStr = None,
    bspp_ongoing_ops: int = None,
    area_ongoing_ops: int = None,
    cause: OperationCause = None,
    address_area: Area = None,
    raw_operation_id: str = None,
    status: OperationStatus = None,
) -> OngoingOpChangedEventEntity:
    data = make_ongoing_ops_changed_event_data(
        timestamp=timestamp,
        bspp_ongoing_ops=bspp_ongoing_ops,
        area_ongoing_ops=area_ongoing_ops,
        cause=cause,
        address_area=address_area,
        raw_operation_id=raw_operation_id,
        status=status,
    )
    uuid = uuid or uuid4()
    return OngoingOpChangedEventEntity(uuid=uuid, data=data)
