from typing import Optional, Union

from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from faker import Faker

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import (
    Availability,
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
)
from shared.factories.vehicle_event_factory import make_vehicle_event_data
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4
from shared.sapeurs_events import AvailabilityChangedEvent

fake = Faker()

default_timestamp = DateStr("2020-10-31T12:00:00.0")


def make_availability_changed_event_data(
    timestamp: DateStr = None,
    area_availability: Availability = None,
    bspp_availability: Availability = None,
    role: VehicleRole = None,
    home_area: Area = None,
    raw_vehicle_id: Union[RawVehicleId, str] = None,
    latest_event_data: VehicleEventData = None,
    previous_raw_operation_id: Optional[RawOperationId] = None,
) -> AvailabilityChangedEventData:
    area_availability = area_availability or Availability()
    bspp_availability = bspp_availability or Availability()
    home_area = home_area or "STOU"
    role = role or "vsav_solo"
    timestamp = timestamp or default_timestamp
    raw_vehicle_id = RawVehicleId(raw_vehicle_id or "default_vehicle")
    return AvailabilityChangedEventData(
        timestamp=timestamp,
        area_availability=area_availability,
        home_area=home_area,
        role=role,
        bspp_availability=bspp_availability,
        raw_vehicle_id=raw_vehicle_id,
        previous_raw_operation_id=previous_raw_operation_id,
        latest_event_data=latest_event_data
        or make_vehicle_event_data(
            role=role,
            home_area=home_area,
            timestamp=timestamp,
            raw_vehicle_id=raw_vehicle_id,
        ),
    )


def make_availability_changed_event_entity(
    uuid: str = None,
    *,
    # Following args are for make_vehicle_event_data
    timestamp: DateStr = None,
    area_availability: Availability = None,
    bspp_availability: Availability = None,
    role: VehicleRole = None,
    home_area: Area = None,
    raw_vehicle_id: Union[RawVehicleId, str] = None,
    latest_event_data: VehicleEventData = None,
    previous_raw_operation_id: Optional[RawOperationId] = None,
) -> AvailabilityChangedEventEntity:
    data = make_availability_changed_event_data(
        timestamp=timestamp,
        area_availability=area_availability,
        bspp_availability=bspp_availability,
        role=role,
        home_area=home_area,
        raw_vehicle_id=raw_vehicle_id,
        latest_event_data=latest_event_data,
        previous_raw_operation_id=previous_raw_operation_id,
    )
    uuid = uuid or uuid4()
    return AvailabilityChangedEventEntity(uuid=uuid, data=data)


def make_availability_changed_event(
    uuid: str = None,
    *,
    # Following args are for make_vehicle_event_data
    timestamp: DateStr = None,
    area_availability: Availability = None,
    bspp_availability: Availability = None,
    role: VehicleRole = None,
    home_area: Area = None,
    raw_vehicle_id: Union[RawVehicleId, str] = None,
    latest_event_data: VehicleEventData = None,
    previous_raw_operation_id: Optional[RawOperationId] = None,
    dispatched_at: Optional[DateStr] = None,
) -> AvailabilityChangedEvent:
    data = make_availability_changed_event_data(
        timestamp=timestamp,
        area_availability=area_availability,
        bspp_availability=bspp_availability,
        role=role,
        home_area=home_area,
        raw_vehicle_id=raw_vehicle_id,
        latest_event_data=latest_event_data,
        previous_raw_operation_id=previous_raw_operation_id,
    )
    uuid = uuid or uuid4()
    return AvailabilityChangedEvent(uuid=uuid, data=data, dispatched_at=dispatched_at)
