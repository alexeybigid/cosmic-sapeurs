import json
from pathlib import Path
from typing import Callable, Dict, List, Optional

import pandas as pd
from cover_ops.adapters.pandas.pandas_helpers import event_entity_to_series

from shared.helpers.csv import reset_csv_file
from shared.helpers.date import DateStr


def reset_csv(csv_path: Path):
    reset_csv_file(csv_path)
    pd.DataFrame(columns=["uuid", "topic", "data"]).to_csv(csv_path, index=False)


def prepare_csv_from_kwargs(
    csv_path: Path, factory: Callable, events_kwargs: Optional[List[Dict]] = None
):
    reset_csv(csv_path)
    if events_kwargs is not None:
        event_entity_list = [
            factory(timestamp=DateStr(f"2021-01-01T1{k}:00:00.000000"), **event_kwargs)
            for k, event_kwargs in enumerate(events_kwargs)
        ]
        df = pd.DataFrame(
            [event_entity_to_series(event_entity) for event_entity in event_entity_list]
        )
        df.data = df.data.map(json.dumps)
        df[["uuid", "topic", "data"]].to_csv(csv_path, index=False)
