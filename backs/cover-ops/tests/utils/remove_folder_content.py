import os
import shutil
from pathlib import Path


def remove_folder_content(path: Path):
    if os.path.exists(path):
        shutil.rmtree(path)
    os.mkdir(path)
