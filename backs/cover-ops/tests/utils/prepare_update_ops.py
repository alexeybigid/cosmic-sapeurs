from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.ongoing_ops_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_cache import InMemoryOperationCache
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.services.operation_event_service import OperationEventService
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_operation_events_repository import (
    ResyncCacheFromOperationEventsRepository,
)
from cover_ops.domain.use_cases.update_ops import UpdateOps

from shared.helpers.clock import CustomClock
from shared.helpers.uuid import CustomUuid
from shared.test_utils.spy_on_topic import spy_on_topic


def prepare_update_ops(operation_events_repo: InMemoryOperationEventsRepository = None):
    custom_clock = CustomClock()
    operation_events_repo = operation_events_repo or InMemoryOperationEventsRepository()
    ongoing_ops_events_repo = InMemoryOngoingOpEventsRepository()
    domain_event_bus = DomainEventBus(custom_clock)
    custom_uuid = CustomUuid()
    operation_cache = InMemoryOperationCache()
    operation_event_service = OperationEventService(
        operation_cache=operation_cache, operation_events_repo=operation_events_repo
    )
    update_ops = UpdateOps(
        ongoing_op_events_repo=ongoing_ops_events_repo,
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
        operation_event_service=operation_event_service,
    )
    ResyncCacheFromOperationEventsRepository(
        operation_events_repo, operation_event_service
    ).execute()
    published_events = spy_on_topic(domain_event_bus, "ongoingOpChanged")

    return (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        domain_event_bus,
    )
