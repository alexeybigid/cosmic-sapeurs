from pathlib import Path

from cover_ops.entrypoints.config import Config
from cover_ops.entrypoints.environment_variables import EnvironmentVariables
from tests.utils.remove_folder_content import remove_folder_content


def prepare_test_config():
    test_env = EnvironmentVariables(
        exception_mode="RAISE",
        processed_events_repositories="CSV",
        cache="REDIS",
    )
    remove_folder_content(Path("tests/1__integration/temp_data"))
    config = Config(env=test_env, csv_base_path=Path("tests/1__integration/temp_data"))
    return config
