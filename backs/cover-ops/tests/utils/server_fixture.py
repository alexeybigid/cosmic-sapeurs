import pytest
from cover_ops.entrypoints.server import make_app
from tests.utils.prepare_test_config import prepare_test_config


def make_server_fixture():
    config = prepare_test_config()

    @pytest.fixture
    def server_fixture(loop, aiohttp_client):
        """Start server for end-to-end tests"""
        app, _ = make_app(config)
        return loop.run_until_complete(aiohttp_client(app))

    return server_fixture, config
