import pytest
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_entity,
)

from shared.ports.abstract_events_repository import AlreadyExistingEventUuid


def test_can_add_to_vehicle_events_repository():
    vehicle_events_repository = InMemoryVehicleEventsRepository()
    vehicle_event_entity = make_vehicle_event_entity()
    vehicle_events_repository.add(vehicle_event_entity)

    assert vehicle_events_repository.get_all() == [vehicle_event_entity]


def test_cannot_add_if_already_exists():
    vehicle_events_repository = InMemoryVehicleEventsRepository()
    vehicle_event_entity = make_vehicle_event_entity()

    with pytest.raises(AlreadyExistingEventUuid):
        # add the same event two times
        vehicle_events_repository.add(vehicle_event_entity)
        vehicle_events_repository.add(vehicle_event_entity)
