from cover_ops.domain.ports.ongoing_ops_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_cache import (
    InMemoryOperationCache,
    OperationStoredInfos,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_ongoing_ops_repository_from_operation_cache import (
    ResyncOngoingOpsRepositoryFromOperationCache,
)
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)

from shared.helpers.uuid import CustomUuid


def test_resync_ongoing_ops_repository_from_operation_cache():
    custom_uuid = CustomUuid()
    operation_cache = InMemoryOperationCache()
    ongoing_op_events_repo = InMemoryOngoingOpEventsRepository()
    resync_ongoing_ops_repository_from_operation_cache = (
        ResyncOngoingOpsRepositoryFromOperationCache(
            uuid=custom_uuid,
            ongoing_op_events_repo=ongoing_op_events_repo,
            operation_cache=operation_cache,
        )
    )
    custom_uuid.set_next_uuid("opened_victim_4")
    ongoing_op_event_data = make_ongoing_ops_changed_event_data(
        raw_operation_id="4",
        status="opened",
        cause="victim",
        address_area="STOU",
        area_ongoing_ops=2,
        bspp_ongoing_ops=2,
    )

    operation_cache._operation_stored_infos_by_operation_id[
        ongoing_op_event_data.raw_operation_id
    ] = OperationStoredInfos(
        status="opened",
        latest_ongoing_op_changed_event_data=ongoing_op_event_data,
    )

    resync_ongoing_ops_repository_from_operation_cache.execute()

    assert len(ongoing_op_events_repo.ongoing_op_events) == 1
    assert ongoing_op_events_repo.ongoing_op_events[0].uuid == "opened_victim_4"
