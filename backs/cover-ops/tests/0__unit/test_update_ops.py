import asyncio
from typing import Any, cast

from cover_ops.domain.entities.event_entities import (
    OngoingOpChangedEventEntity,
    OperationEventEntity,
)
from cover_ops.domain.ports.operation_cache import OperationStoredInfos
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from tests.utils.ongoing_ops_changed_event_factory import (
    make_ongoing_ops_changed_event_data,
)
from tests.utils.prepare_update_ops import prepare_update_ops

from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.factories.operation_event_factory import (
    make_operation_event,
    make_operation_event_data,
)
from shared.helpers.date import DateStr

operation_id_1 = RawOperationId("operation_1")
timestamp_open = DateStr("2020-10-01T12:00")
timestamp_affected = DateStr("2020-10-01T12:30")
timestamp_released = DateStr("2020-10-01T13:00")
ongoing_op_uuid = "ongoing_op_uuid"

operation_victim_1_stou_opened = make_operation_event(
    uuid="operation_uuid",
    status="opened",
    cause="victim",
    timestamp=timestamp_open,
    address_area="STOU",
    raw_operation_id=operation_id_1,
)

operation_victim_1_stou_first_vehicle_affected = make_operation_event(
    status="first_vehicle_affected",
    cause=None,
    address_area=None,
    raw_operation_id=operation_id_1,
    timestamp=timestamp_affected,
)

operation_victim_1_stou_all_vehicles_released = make_operation_event(
    status="all_vehicles_released",
    cause=None,
    address_area=None,
    raw_operation_id=operation_id_1,
    timestamp=timestamp_released,
)

operation_victim_1_stou_closed = make_operation_event(
    status="closed",
    cause=None,
    address_area=None,
    raw_operation_id=operation_id_1,
    timestamp=timestamp_released,
)


def test_only_add_to_repo_events_with_irrelevant_status():
    (
        ongoing_ops_events_repo,
        update_ops,
        _,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops()
    for status in ["validated", "finished", "reinforced"]:
        event_with_irrelevent_status = make_operation_event(status="validated")
        asyncio.run(update_ops.execute(event=event_with_irrelevent_status))
        assert len(published_events) == 0
        assert len(ongoing_ops_events_repo.ongoing_op_events) == 0
        assert operation_cache._operation_stored_infos_by_operation_id == {}


def test_operation_just_opened():
    (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops()

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_opened))

    assert len(published_events) == 1
    expected_ongoing_ops_changed_event_data = OngoingOpChangedEventData(
        timestamp=timestamp_open,
        bspp_ongoing_ops=0,
        area_ongoing_ops=0,
        cause="victim",
        address_area="STOU",
        raw_operation_id=operation_id_1,
        status="opened",
    )

    # Event well published
    published_event = published_events[0]
    assert published_event.uuid == ongoing_op_uuid
    assert published_event.data == expected_ongoing_ops_changed_event_data

    # Event added in repo
    assert ongoing_ops_events_repo.ongoing_op_events == [
        OngoingOpChangedEventEntity(
            data=expected_ongoing_ops_changed_event_data, uuid=ongoing_op_uuid
        )
    ]

    # Cache updated
    assert operation_cache._operation_stored_infos_by_operation_id == {
        operation_id_1: OperationStoredInfos(
            status="opened",
            latest_ongoing_op_changed_event_data=expected_ongoing_ops_changed_event_data,
        )
    }
    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 0


def test_first_vehicle_affected_to_opened_operation_that_is_still_in_cache():
    (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops()

    operation_cache._ongoing_ops_count_by_cause_by_area = cast(
        Any,
        {
            "victim": {"STOU": 5, "PARM": 3},
            "fire": {"CHPT": 1},
        },
    )

    operation_cache.set_operation_stored_infos_for_operation_id(
        operation_id_1,
        OperationStoredInfos(
            status="opened",
            latest_ongoing_op_changed_event_data=make_ongoing_ops_changed_event_data(
                address_area="STOU",
                bspp_ongoing_ops=8,
                area_ongoing_ops=5,
                cause="victim",
                raw_operation_id=operation_id_1,
                status="opened",
                timestamp=timestamp_open,
            ),
        ),
    )

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    expected_ongoing_ops_changed_event_data = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        bspp_ongoing_ops=9,
        area_ongoing_ops=6,
        timestamp=timestamp_affected,
        raw_operation_id=operation_id_1,
        status="first_vehicle_affected",
        cause="victim",
    )

    # Event published
    published_event = published_events[0]

    assert published_event.data == expected_ongoing_ops_changed_event_data

    # Event added in repo
    assert ongoing_ops_events_repo.ongoing_op_events[-1].uuid == ongoing_op_uuid
    assert (
        ongoing_ops_events_repo.ongoing_op_events[-1].data
        == expected_ongoing_ops_changed_event_data
    )

    # Cache updated
    assert operation_cache._operation_stored_infos_by_operation_id == {
        operation_id_1: OperationStoredInfos(
            status="first_vehicle_affected",
            latest_ongoing_op_changed_event_data=expected_ongoing_ops_changed_event_data,
        )
    }
    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 6


def test_first_vehicle_affected_to_opened_operation_in_repo_but_not_in_cache():
    operation_events_repo = InMemoryOperationEventsRepository()
    operation_events_repo.operation_events = [
        OperationEventEntity(
            data=operation_victim_1_stou_opened.data,
            uuid=operation_victim_1_stou_opened.uuid,
        )
    ]

    (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops(operation_events_repo=operation_events_repo)

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    expected_ongoing_ops_changed_event_data = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        bspp_ongoing_ops=1,
        area_ongoing_ops=1,
        timestamp=timestamp_affected,
        raw_operation_id=operation_id_1,
        status="first_vehicle_affected",
        cause="victim",
    )
    # Event published
    published_event = published_events[0]
    assert published_event.data == expected_ongoing_ops_changed_event_data

    # Event added in repo
    assert ongoing_ops_events_repo.ongoing_op_events[-1].uuid == ongoing_op_uuid
    assert (
        ongoing_ops_events_repo.ongoing_op_events[-1].data
        == expected_ongoing_ops_changed_event_data
    )

    # Cache updated
    assert operation_cache._operation_stored_infos_by_operation_id == {
        operation_id_1: OperationStoredInfos(
            status="first_vehicle_affected",
            latest_ongoing_op_changed_event_data=expected_ongoing_ops_changed_event_data,
        )
    }
    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 1


def test_all_vehicle_released_from_operation_that_is_in_cache():
    (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops()

    operation_cache._ongoing_ops_count_by_cause_by_area = cast(
        Any,
        {
            "victim": {"STOU": 5, "PARM": 3},
            "fire": {"CHPT": 1},
        },
    )

    operation_cache.set_operation_stored_infos_for_operation_id(
        operation_id_1,
        OperationStoredInfos(
            status="opened",
            latest_ongoing_op_changed_event_data=make_ongoing_ops_changed_event_data(
                address_area="STOU",
                bspp_ongoing_ops=8,
                area_ongoing_ops=5,
                cause="victim",
                status="opened",
                raw_operation_id=operation_id_1,
            ),
        ),
    )

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_all_vehicles_released))
    expected_ongoing_ops_changed_event_data = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        bspp_ongoing_ops=7,
        area_ongoing_ops=4,
        timestamp=timestamp_released,
        raw_operation_id=operation_id_1,
        status="all_vehicles_released",
        cause="victim",
    )
    # Event published
    published_event = published_events[0]
    assert published_event.data == expected_ongoing_ops_changed_event_data

    # Event added in repo
    assert ongoing_ops_events_repo.ongoing_op_events[-1].uuid == ongoing_op_uuid
    assert (
        ongoing_ops_events_repo.ongoing_op_events[-1].data
        == expected_ongoing_ops_changed_event_data
    )

    # Cache updated
    assert operation_id_1 not in operation_cache._operation_stored_infos_by_operation_id

    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 4


def test_all_vehicles_released_from_operation_in_repo_but_not_in_cache():
    operation_events_repo = InMemoryOperationEventsRepository()
    operation_events_repo.operation_events = [
        OperationEventEntity(
            data=operation_victim_1_stou_opened.data,
            uuid=operation_victim_1_stou_opened.uuid,
        ),
        OperationEventEntity(
            data=operation_victim_1_stou_first_vehicle_affected.data,
            uuid=operation_victim_1_stou_first_vehicle_affected.uuid,
        ),
    ]

    (
        ongoing_ops_events_repo,
        update_ops,
        custom_uuid,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops(operation_events_repo=operation_events_repo)

    # assert resync should not add to repo
    assert len(ongoing_ops_events_repo.ongoing_op_events) == 0

    custom_uuid.set_next_uuid(ongoing_op_uuid)
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_all_vehicles_released))
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_closed))

    expected_ongoing_ops_changed_event_data = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        bspp_ongoing_ops=0,
        area_ongoing_ops=0,
        timestamp=timestamp_released,
        raw_operation_id=operation_id_1,
        status="all_vehicles_released",
        cause="victim",
    )
    # Event published
    published_event = published_events[0]
    assert published_event.data == expected_ongoing_ops_changed_event_data

    # Event added in repo
    assert ongoing_ops_events_repo.ongoing_op_events[-1].uuid == ongoing_op_uuid
    assert (
        ongoing_ops_events_repo.ongoing_op_events[-1].data
        == expected_ongoing_ops_changed_event_data
    )

    # Cache updated
    assert operation_id_1 not in operation_cache._operation_stored_infos_by_operation_id
    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 0


def test_ops_does_not_fail_when_first_operation_status_is_first_vehicle_affected():
    (
        ongoing_ops_events_repo,
        update_ops,
        _,
        _,
        published_events,
        _,
    ) = prepare_update_ops()
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    assert len(ongoing_ops_events_repo.ongoing_op_events) == 0
    assert len(published_events) == 0


def test_ops_does_not_fail_when_first_operation_status_is_all_vehicles_released():
    (
        ongoing_ops_events_repo,
        update_ops,
        _,
        _,
        published_events,
        _,
    ) = prepare_update_ops()
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_all_vehicles_released))
    assert len(ongoing_ops_events_repo.ongoing_op_events) == 0
    assert len(published_events) == 0


def test_ops_when_all_vehicles_released_twice():
    operation_events_repo = InMemoryOperationEventsRepository()
    operation_events_repo.operation_events = [
        OperationEventEntity(
            uuid="opened",
            data=make_operation_event_data(
                raw_operation_id=operation_id_1,
                status="opened",
                cause="victim",
                address_area="STOU",
            ),
        ),
    ]

    (
        ongoing_ops_events_repo,
        update_ops,
        _,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops(operation_events_repo=operation_events_repo)
    operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] = 8

    asyncio.run(update_ops.execute(event=operation_victim_1_stou_all_vehicles_released))
    # <-- Adding manually the entity to the repo, since it's now an independent use case
    operation_events_repo.add(
        OperationEventEntity(
            data=operation_victim_1_stou_all_vehicles_released.data,
            uuid=operation_victim_1_stou_all_vehicles_released.uuid,
        )
    )
    # -->
    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 7
    assert len(published_events) == 1
    assert len(ongoing_ops_events_repo.ongoing_op_events) == 1

    # All vehicles released twice : should not decrement twice !
    operation_victim_1_stou_all_vehicles_released_twice = make_operation_event(
        status="all_vehicles_released",
        cause=None,
        address_area=None,
        raw_operation_id=operation_id_1,
        timestamp=timestamp_released,
    )
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_all_vehicles_released_twice)
    )

    # <-- Adding manually the entity to the repo, since it's now an independent use case
    operation_events_repo.add(
        OperationEventEntity(
            data=operation_victim_1_stou_all_vehicles_released_twice.data,
            uuid=operation_victim_1_stou_all_vehicles_released_twice.uuid,
        )
    )
    # -->
    assert operation_cache._ongoing_ops_count_by_cause_by_area["victim"]["STOU"] == 7
    assert len(published_events) == 1
    assert len(ongoing_ops_events_repo.ongoing_op_events) == 1


def test_ops_incremented_lately_when_first_affected_received_before_opening():
    (
        ongoing_ops_events_repo,
        update_ops,
        _,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops()
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    assert operation_id_1 in operation_cache._operation_stored_infos_by_operation_id
    asyncio.run(update_ops.execute(event=operation_victim_1_stou_opened))

    assert len(ongoing_ops_events_repo.ongoing_op_events) == 1
    assert len(published_events) == 1
    expected_ongoing_ops_changed_event_data = make_ongoing_ops_changed_event_data(
        address_area="STOU",
        bspp_ongoing_ops=1,
        area_ongoing_ops=1,
        timestamp=timestamp_open,
        raw_operation_id=operation_id_1,
        status="opened",
        cause="victim",
    )
    assert published_events[0].data == expected_ongoing_ops_changed_event_data


def test_ops_not_opened_but_vehicle_affected_and_then_released():
    (
        ongoing_ops_events_repo,
        update_ops,
        _,
        operation_cache,
        published_events,
        _,
    ) = prepare_update_ops()
    asyncio.run(
        update_ops.execute(event=operation_victim_1_stou_first_vehicle_affected)
    )
    assert operation_id_1 in operation_cache._operation_stored_infos_by_operation_id

    asyncio.run(update_ops.execute(event=operation_victim_1_stou_all_vehicles_released))

    assert operation_id_1 not in operation_cache._operation_stored_infos_by_operation_id
    assert len(published_events) == 0
