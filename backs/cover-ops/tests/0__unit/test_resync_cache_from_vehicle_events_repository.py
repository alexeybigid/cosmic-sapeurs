from cover_ops.domain.ports.vehicle_cache import InMemoryVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    AvailabilityStoredInfos,
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_vehicle_events_repository import (
    ResyncCacheFromVehicleEventsRepository,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_entity,
)
from tests.utils.assert_helpers import assert_subset_of

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
)
from shared.data_transfert_objects.vehicle_event_data import OmnibusInfos, RawVehicleId
from shared.helpers.uuid import RealUuid


def test_resync_cache_from_vehicle_events_repository():
    vehicle_events_repo = InMemoryVehicleEventsRepository()
    vehicle_cache = InMemoryVehicleCache()
    resync_cache_from_vehicle_events_repository = (
        ResyncCacheFromVehicleEventsRepository(
            vehicle_events_repo=vehicle_events_repo,
            vehicle_cache=vehicle_cache,
            uuid=RealUuid(),
        )
    )
    event_vehicle_1_STOU_0 = make_vehicle_event_entity(
        raw_vehicle_id="1",
        status="left_hospital",
        role="vsav_solo",
        home_area="STOU",
        vehicle_name="vehicle_1_STOU",
    )
    event_vehicle_2_PARM_0 = make_vehicle_event_entity(
        raw_vehicle_id="2",
        status="arrived_on_intervention",
        role="pse_solo",
        home_area="PARM",
        vehicle_name="vehicle_2_PARM",
    )

    event_vehicle_3_STOU_0 = make_vehicle_event_entity(
        raw_vehicle_id="3",
        status="arrived_at_home",
        role="pse_solo",
        home_area="STOU",
        vehicle_name="vehicle_3_STOU",
    )

    event_vehicle_1_STOU_1 = make_vehicle_event_entity(
        raw_vehicle_id="1",
        status="arrived_at_home",
        role="vsav_solo",
        home_area="STOU",
        vehicle_name="vehicle_1_STOU",
    )

    event_vehicle_4_STOU_0 = make_vehicle_event_entity(
        raw_vehicle_id="4",
        status="arrived_at_home",
        role="vsav_omni",
        home_area="STOU",
        vehicle_name="vehicle_4_STOU",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=RawVehicleId("5"),
        ),
    )

    event_vehicle_5_STOU_0 = make_vehicle_event_entity(
        raw_vehicle_id="5",
        status="arrived_at_home",
        role="pse_omni_san",
        home_area="STOU",
        vehicle_name="vehicle_5_STOU",
        omnibus=OmnibusInfos(partner_raw_vehicle_id=RawVehicleId("5")),
    )
    vehicle_events_repo.vehicle_events = [
        event_vehicle_1_STOU_0,
        event_vehicle_2_PARM_0,
        event_vehicle_3_STOU_0,
        event_vehicle_1_STOU_1,
        event_vehicle_4_STOU_0,
        event_vehicle_5_STOU_0,
    ]

    resync_cache_from_vehicle_events_repository.execute()

    assert vehicle_cache._latest_vehicle_event_data_by_vehicle_id == {
        "1": event_vehicle_1_STOU_1.data,
        "2": event_vehicle_2_PARM_0.data,
        "3": event_vehicle_3_STOU_0.data,
        "4": event_vehicle_4_STOU_0.data,
        "5": event_vehicle_5_STOU_0.data,
    }
    availabilities_by_area_by_role = vehicle_cache._availabilities_by_area_by_role

    assert_subset_of(
        availabilities_by_area_by_role["STOU"],
        {
            "vsav_solo": AvailabilityStoredInfos(
                availability=Availability(available=1),
                latest_event_data=event_vehicle_1_STOU_1.data,
            ),
            "vsav_omni": AvailabilityStoredInfos(
                availability=Availability(available=1),
                latest_event_data=event_vehicle_4_STOU_0.data,
            ),
            "pse_solo": AvailabilityStoredInfos(
                availability=Availability(available=1),
                latest_event_data=event_vehicle_3_STOU_0.data,
            ),
            "pse_omni_san": AvailabilityStoredInfos(
                availability=Availability(available=1),
                latest_event_data=event_vehicle_5_STOU_0.data,
            ),
        },
    )

    bspp_availabilities_by_role = vehicle_cache._bspp_availabilities_by_role

    assert bspp_availabilities_by_role["vsav_solo"] == AvailabilityStoredInfos(
        availability=Availability(available=1)
    )
    assert bspp_availabilities_by_role["vsav_omni"] == AvailabilityStoredInfos(
        availability=Availability(available=1)
    )
    assert bspp_availabilities_by_role["pse_solo"] == AvailabilityStoredInfos(
        availability=Availability(available=1, in_service=1)
    )
    assert bspp_availabilities_by_role["pse_omni_san"] == AvailabilityStoredInfos(
        availability=Availability(available=1)
    )
    # TODO
    assert vehicle_cache.get_omnibus_balance() == OmnibusBalance(both_available=1)
