from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.use_cases.transfert_events_batch_to_repositories import (
    TransfertEventsBatchToRepositories,
)

from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event


def test_initialize_event_entities_with_correct_vehicle():
    vehicle_events_repo = InMemoryVehicleEventsRepository()
    operation_events_repo = InMemoryOperationEventsRepository()
    initialize_event_entities = TransfertEventsBatchToRepositories(
        vehicle_events_repo=vehicle_events_repo,
        operation_events_repo=operation_events_repo,
    )
    event_0 = make_vehicle_event(uuid="vehicle_0")
    event_1 = make_vehicle_event(uuid="vehicle_1")
    event_2 = make_operation_event(uuid="operation_2")
    initialize_event_entities.execute([event_0, event_1, event_2])

    assert len(vehicle_events_repo.vehicle_events) == 2
    assert len(operation_events_repo.operation_events) == 1
    assert vehicle_events_repo.vehicle_events[0].uuid == "vehicle_0"
    assert vehicle_events_repo.vehicle_events[1].uuid == "vehicle_1"
    assert operation_events_repo.operation_events[0].uuid == "operation_2"
