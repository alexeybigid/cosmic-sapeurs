import asyncio

from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_entity,
)
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_data,
    make_availability_changed_event_entity,
)
from tests.utils.prepare_update_cover import prepare_update_cover

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.test_utils.spy_on_topic import spy_on_topic

operation_id_1 = RawOperationId("operation_1")
operation_id_2 = RawOperationId("operation_2")
vehicle_id_A = RawVehicleId("vehicle_A")
vehicle_id_B = RawVehicleId("vehicle_B")


def test_vehicle_event_gets_stored_and_cover_updated_event_gets_published():
    (
        vehicle_event_repo,
        vehicle_cache,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        _,
    ) = prepare_update_cover()

    published_events = spy_on_topic(domain_event_bus, "availabilityChanged")
    timestamp = DateStr("2020-10-01T12:00:00.0")
    dispatched_at = DateStr("2020-11-30T12:00:00.0")
    custom_clock.set_next_date(dispatched_at)

    incoming_vehicle_event = make_vehicle_event(
        uuid="vehicle_uuid",
        status="selected",
        role="vsav_solo",
        timestamp=timestamp,
        dispatched_at=dispatched_at,
        home_area="STOU",
        raw_vehicle_id="1",
    )

    cover_uuid = "cover_uuid"

    expected_cover_updated_event_entity = make_availability_changed_event_entity(
        timestamp=timestamp,
        uuid=cover_uuid,
        role="vsav_solo",
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=1),
        home_area="STOU",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event.data,
    )

    custom_uuid.set_next_uuid(cover_uuid)

    asyncio.run(update_cover.execute(event=incoming_vehicle_event))

    assert len(published_events) == 1
    published_event = published_events[0]
    assert published_event.uuid == cover_uuid
    assert published_event.dispatched_at == dispatched_at
    assert published_event.data.area_availability == Availability(in_service=1)

    assert availability_repo.availability_events == [
        expected_cover_updated_event_entity
    ]


def test_vehicle_event_gets_stored_and_cover_updated_event_gets_published_when_repo_has_data():

    old_timestamp_1 = DateStr("2021-01-01T11:00:00.0")
    old_event_entity_1 = make_vehicle_event_entity(
        raw_vehicle_id=vehicle_id_A,
        status="arrived_at_home",
        role="vsav_solo",
        home_area="STOU",
        timestamp=old_timestamp_1,
        raw_operation_id=operation_id_1,
    )
    old_timestamp_2 = DateStr("2021-01-01T12:00:00.0")
    old_event_entity_2 = make_vehicle_event_entity(
        raw_vehicle_id=vehicle_id_A,
        status="departed_to_intervention",
        role="vsav_solo",
        home_area="STOU",
        timestamp=old_timestamp_2,
        raw_operation_id=operation_id_2,
    )
    vehicle_event_repo = InMemoryVehicleEventsRepository()
    vehicle_event_repo.vehicle_events = [old_event_entity_1, old_event_entity_2]
    (
        vehicle_events_repo,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        published_events,
    ) = prepare_update_cover(vehicle_events_repo=vehicle_event_repo)

    # assert resync does not influence availability events repo
    assert len(availability_events_repo.availability_events) == 0

    timestamp = DateStr("2020-10-01T12:00:00.0")
    dispatched_at = DateStr("2020-11-30T12:00:00.0")

    incoming_vehicle_event = make_vehicle_event(
        dispatched_at=dispatched_at,
        uuid="vehicle_uuid",
        status="selected",
        role="vsav_solo",
        raw_vehicle_id=vehicle_id_B,
        timestamp=timestamp,
        home_area="STOU",
        raw_operation_id=None,
    )

    expected_availability = Availability(recoverable=0, in_service=2)

    cover_uuid = "cover_uuid"
    expected_cover_updated_event_entity = make_availability_changed_event_entity(
        timestamp=timestamp,
        uuid=cover_uuid,
        role="vsav_solo",
        area_availability=expected_availability,
        bspp_availability=expected_availability,
        home_area="STOU",
        raw_vehicle_id=vehicle_id_B,
        latest_event_data=incoming_vehicle_event.data,
        previous_raw_operation_id=None,
    )
    custom_uuid.set_next_uuid(cover_uuid)

    asyncio.run(update_cover.execute(event=incoming_vehicle_event))

    assert len(availability_events_repo.availability_events) == 1

    assert (
        availability_events_repo.availability_events[-1]
        == expected_cover_updated_event_entity
    )


def test_cover_correctly_updated_when_availability_kind_remains_unchanged():
    (
        vehicle_events_repo,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        published_events,
    ) = prepare_update_cover()
    timestamp = DateStr("2020-10-01T12:00:00.0")
    incoming_vehicle_event_1 = make_vehicle_event(
        status="selected",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp,
        home_area="STOU",
    )
    incoming_vehicle_event_2 = make_vehicle_event(
        status="arrived_at_hospital",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp,
        home_area="STOU",
    )
    expected_availability = Availability(in_service=1)

    asyncio.run(update_cover.execute(event=incoming_vehicle_event_1))
    custom_uuid.set_next_uuid("uuid_2")
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_2))

    assert len(availability_events_repo.availability_events) == 2

    availability_event_data_0 = availability_events_repo.availability_events[0].data
    availability_event_data_1 = availability_events_repo.availability_events[1].data

    assert availability_event_data_0.area_availability == expected_availability
    assert availability_event_data_1.area_availability == expected_availability

    assert availability_event_data_0.latest_event_data.status == "selected"
    assert availability_event_data_1.latest_event_data.status == "arrived_at_hospital"


def test_receives_event_with_role_other():
    _, _, _, update_cover, _, _, _, _ = prepare_update_cover()

    incoming_vehicle_event = make_vehicle_event(role="other", raw_vehicle_id="1")
    assert asyncio.run(update_cover.execute(event=incoming_vehicle_event)) == None


def test_cover_correctly_updated_from_different_areas():
    (
        vehicle_events_repo,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        published_events,
    ) = prepare_update_cover()
    timestamp = DateStr("2020-10-01T12:00:00.0")
    incoming_vehicle_event_1 = make_vehicle_event(
        status="selected",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp,
        home_area="PARM",
    )

    incoming_vehicle_event_sdis = make_vehicle_event(
        status="arrived_at_hospital",
        role="vsav_solo",
        raw_vehicle_id="sdis",
        timestamp=timestamp,
        home_area="SDIS",
    )

    incoming_vehicle_event_3 = make_vehicle_event(
        status="arrived_at_hospital",
        role="vsav_solo",
        raw_vehicle_id="3",
        timestamp=timestamp,
        home_area="ANTO",
    )

    custom_uuid.set_next_uuid("uuid_1")
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_1))
    custom_uuid.set_next_uuid("uuid_sdis")
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_sdis))
    custom_uuid.set_next_uuid("uuid_3")
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_3))

    assert availability_events_repo.availability_events[
        0
    ].data == make_availability_changed_event_data(
        timestamp=timestamp,
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=1),
        home_area="PARM",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_1.data,
    )
    assert availability_events_repo.availability_events[
        1
    ].data == make_availability_changed_event_data(
        timestamp=timestamp,
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=2),
        home_area="ANTO",
        role="vsav_solo",
        raw_vehicle_id="3",
        latest_event_data=incoming_vehicle_event_3.data,
    )


def test_cover_correctly_updated_when_vehicle_changes_home_area_same_status():
    (
        vehicle_events_repo,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        published_events,
    ) = prepare_update_cover()
    parm_timestamp = DateStr("2020-10-01T12:00:00.0")
    incoming_vehicle_event_parm = make_vehicle_event(
        status="arrived_on_intervention",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=parm_timestamp,
        home_area="PARM",
    )
    anto_timestamp = DateStr("2020-10-01T13:00:00.0")
    incoming_vehicle_event_anto = make_vehicle_event(
        status="arrived_on_intervention",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=anto_timestamp,
        home_area="ANTO",
    )

    custom_uuid.set_next_uuids(
        [
            "uuid_parm",
            "uuid_anto",
            "uuid_anto_compensation",
        ]
    )

    asyncio.run(update_cover.execute(event=incoming_vehicle_event_parm))
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_anto))

    assert len(published_events) == 3
    assert len(availability_events_repo.availability_events) == 3

    expected_vsav_arrived_at_parm_event_data = make_availability_changed_event_data(
        timestamp=parm_timestamp,
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=1),
        home_area="PARM",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_parm.data,
    )
    expected_vsav_arrived_at_anto_event_data = make_availability_changed_event_data(
        timestamp=anto_timestamp,
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=2),
        home_area="ANTO",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_anto.data,
    )
    expected_vsav_left_parm_event_data = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:59:59.999000"),
        area_availability=Availability(in_service=0),
        bspp_availability=Availability(in_service=1),
        home_area="PARM",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_parm.data,
    )
    expected_event_data = [
        expected_vsav_arrived_at_parm_event_data,
        expected_vsav_left_parm_event_data,
        expected_vsav_arrived_at_anto_event_data,
    ]
    assert [
        event.data for event in availability_events_repo.availability_events
    ] == expected_event_data

    assert [event.data for event in published_events] == expected_event_data

    availabilities_by_area_by_role = vehicle_cache._availabilities_by_area_by_role

    assert (
        availabilities_by_area_by_role["ANTO"]["vsav_solo"]
    ).availability == Availability(in_service=1)

    assert (
        availabilities_by_area_by_role["PARM"]["vsav_solo"]
    ).availability == Availability(in_service=0)

    assert vehicle_cache._bspp_availabilities_by_role[
        "vsav_solo"
    ].availability == Availability(in_service=1)


def test_cover_correctly_updated_when_vehicle_changes_role_same_status():
    (
        vehicle_events_repo,
        vehicle_cache,
        availability_events_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        custom_clock,
        published_events,
    ) = prepare_update_cover()

    incoming_pse_timestamp = DateStr("2020-10-01T12:00:00.0")
    incoming_vehicle_event_pse = make_vehicle_event(
        status="arrived_at_home",
        role="pse_solo",
        raw_vehicle_id="1",
        timestamp=incoming_pse_timestamp,
        home_area="STOU",
    )

    incoming_vsav_timestamp = DateStr("2020-10-01T12:05:00.0")
    incoming_vehicle_event_vsav = make_vehicle_event(
        status="arrived_at_home",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=incoming_vsav_timestamp,
        home_area="STOU",
    )

    asyncio.run(update_cover.execute(event=incoming_vehicle_event_pse))
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_vsav))

    assert len(availability_events_repo.availability_events) == 3

    expected_incoming_pse = make_availability_changed_event_data(
        timestamp=incoming_pse_timestamp,
        area_availability=Availability(available=1),
        bspp_availability=Availability(available=1),
        home_area="STOU",
        role="pse_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_pse.data,
    )
    expected_incoming_vsav = make_availability_changed_event_data(
        timestamp=incoming_vsav_timestamp,
        area_availability=Availability(available=1),
        bspp_availability=Availability(available=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_vsav.data,
    )

    expected_compensated_pse = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:04:59.999000"),
        area_availability=Availability(available=0),
        bspp_availability=Availability(available=0),
        home_area="STOU",
        role="pse_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_pse.data,
    )

    expected_event_data = [
        expected_incoming_pse,
        expected_compensated_pse,
        expected_incoming_vsav,
    ]
    assert [
        event.data for event in availability_events_repo.availability_events
    ] == expected_event_data

    assert [event.data for event in published_events] == expected_event_data

    availabilities_STOU_vsav = vehicle_cache._availabilities_by_area_by_role["STOU"][
        "vsav_solo"
    ]
    assert availabilities_STOU_vsav.availability == Availability(available=1)

    assert (
        availabilities_STOU_vsav.latest_event_data == incoming_vehicle_event_vsav.data
    )

    assert vehicle_cache._bspp_availabilities_by_role[
        "pse_solo"
    ].availability == Availability(available=0)

    assert (
        vehicle_cache._latest_vehicle_event_data_by_vehicle_id[RawVehicleId("1")]
        == incoming_vehicle_event_vsav.data
    )


def test_cover_correctly_updated_when_vehicle_changes_role_and_status():
    (
        _,
        vehicle_cache,
        availability_repo,
        update_cover,
        domain_event_bus,
        custom_uuid,
        _,
        _,
    ) = prepare_update_cover()
    published_events = spy_on_topic(domain_event_bus, "availabilityChanged")
    timestamp_vsav = DateStr("2020-10-01T12:00:00.0")

    incoming_vehicle_event_vsav = make_vehicle_event(
        status="arrived_at_home",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_vsav,
        home_area="STOU",
    )
    timestamp_pse = DateStr("2020-10-01T12:08:00.0")
    incoming_vehicle_event_pse = make_vehicle_event(
        status="departed_to_intervention",
        role="pse_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_pse,
        home_area="STOU",
    )

    asyncio.run(update_cover.execute(event=incoming_vehicle_event_vsav))
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_pse))

    assert len(published_events) == 3
    assert len(availability_repo.availability_events) == 3

    expected_availability_vsav = make_availability_changed_event_data(
        timestamp=timestamp_vsav,
        area_availability=Availability(available=1),
        bspp_availability=Availability(available=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_vsav.data,
    )
    expected_availability_pse = make_availability_changed_event_data(
        timestamp=timestamp_pse,
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=1),
        home_area="STOU",
        role="pse_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_pse.data,
    )
    expected_availability_vsav_compensated = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:07:59.999000"),
        area_availability=Availability(available=0),
        bspp_availability=Availability(available=0),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_vsav.data,
    )

    expected_event_data = [
        expected_availability_vsav,
        expected_availability_vsav_compensated,
        expected_availability_pse,
    ]
    assert [
        event.data for event in availability_repo.availability_events
    ] == expected_event_data

    assert [event.data for event in published_events] == expected_event_data

    availabilities_STOU_vsav = vehicle_cache._availabilities_by_area_by_role["STOU"][
        "vsav_solo"
    ]
    assert availabilities_STOU_vsav.availability == Availability(available=0)

    assert vehicle_cache._bspp_availabilities_by_role[
        "pse_solo"
    ].availability == Availability(in_service=1)

    assert (
        vehicle_cache._latest_vehicle_event_data_by_vehicle_id[RawVehicleId("1")]
        == incoming_vehicle_event_pse.data
    )


def test_cover_correctly_updated_when_vehicle_changes_area_and_status():
    (
        _,
        vehicle_cache,
        availability_repo,
        update_cover,
        _,
        custom_uuid,
        _,
        published_events,
    ) = prepare_update_cover()

    timestamp_stou = DateStr("2020-10-01T12:00:00.0")
    incoming_vehicle_event_stou = make_vehicle_event(
        status="arrived_at_home",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_stou,
        home_area="STOU",
    )

    timestamp_chpt = DateStr("2020-10-01T12:03:00.0")
    incoming_vehicle_event_chpt = make_vehicle_event(
        status="departed_to_intervention",
        role="vsav_solo",
        raw_vehicle_id="1",
        timestamp=timestamp_chpt,
        home_area="CHPT",
    )

    asyncio.run(update_cover.execute(event=incoming_vehicle_event_stou))
    asyncio.run(update_cover.execute(event=incoming_vehicle_event_chpt))

    expected_availability_stou = make_availability_changed_event_data(
        timestamp=timestamp_stou,
        area_availability=Availability(available=1),
        bspp_availability=Availability(available=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_stou.data,
    )
    expected_availability_chpt = make_availability_changed_event_data(
        timestamp=timestamp_chpt,
        area_availability=Availability(in_service=1),
        bspp_availability=Availability(in_service=1, available=1),
        home_area="CHPT",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_chpt.data,
    )
    expected_availability_stou_compensated = make_availability_changed_event_data(
        timestamp=DateStr("2020-10-01T12:02:59.999000"),
        area_availability=Availability(available=0),
        bspp_availability=Availability(in_service=1),
        home_area="STOU",
        role="vsav_solo",
        raw_vehicle_id="1",
        latest_event_data=incoming_vehicle_event_stou.data,
    )

    expected_event_data = [
        expected_availability_stou,
        expected_availability_stou_compensated,
        expected_availability_chpt,
    ]
    assert [
        event.data.timestamp for event in availability_repo.availability_events
    ] == [
        "2020-10-01T12:00:00.0",
        "2020-10-01T12:02:59.999000",
        "2020-10-01T12:03:00.0",
    ]
    assert [
        event.data for event in availability_repo.availability_events
    ] == expected_event_data

    assert [event.data for event in published_events] == expected_event_data

    assert vehicle_cache._availabilities_by_area_by_role["STOU"][
        "vsav_solo"
    ].availability == Availability(available=0)
    assert vehicle_cache._availabilities_by_area_by_role["CHPT"][
        "vsav_solo"
    ].availability == Availability(in_service=1)
    assert vehicle_cache._bspp_availabilities_by_role[
        "vsav_solo"
    ].availability == Availability(in_service=1)

    assert (
        vehicle_cache._latest_vehicle_event_data_by_vehicle_id[RawVehicleId("1")]
        == incoming_vehicle_event_chpt.data
    )
