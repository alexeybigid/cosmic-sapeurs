from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import InMemoryVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_availability_repository_from_vehicle_cache import (
    ResyncAvailabilityRepositoryFromVehicleCache,
)
from cover_ops.helpers.factories.vehicle_event_entity_factory import (
    make_vehicle_event_data,
)

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.helpers.uuid import CustomUuid


def test_resync_availability_repository_from_vehicle_cache():
    custom_uuid = CustomUuid()
    custom_uuid.set_next_uuid("vehicle_4_home")
    vehicle_cache = InMemoryVehicleCache()
    availability_events_repo = InMemoryAvailabilityEventsRepository()
    resync_availability_repository_from_vehicle_cache = (
        ResyncAvailabilityRepositoryFromVehicleCache(
            uuid=custom_uuid,
            availability_events_repo=availability_events_repo,
            vehicle_cache=vehicle_cache,
        )
    )

    vehicle_event_data = make_vehicle_event_data(
        raw_vehicle_id="4",
        status="arrived_at_home",
        role="vsav_solo",
        home_area="STOU",
        vehicle_name="vehicle_4_STOU",
    )

    vehicle_cache._availabilities_by_area_by_role["STOU"][
        "vsav_solo"
    ] = AvailabilityStoredInfos(
        availability=Availability(available=1),
        latest_event_data=vehicle_event_data,
    )
    vehicle_cache._bspp_availabilities_by_role["vsav_solo"] = AvailabilityStoredInfos(
        availability=Availability(available=1),
        latest_event_data=vehicle_event_data,
    )
    vehicle_cache._latest_vehicle_event_data_by_vehicle_id[
        vehicle_event_data.raw_vehicle_id
    ] = vehicle_event_data

    resync_availability_repository_from_vehicle_cache.execute()

    assert len(availability_events_repo.availability_events) == 1
    assert availability_events_repo.availability_events[0].uuid == "vehicle_4_home"
