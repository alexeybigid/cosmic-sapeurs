import asyncio

from tests.utils.prepare_update_omnibus import *

from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
)
from shared.data_transfert_objects.vehicle_event_data import OmnibusInfos
from shared.factories.vehicle_event_factory import make_vehicle_event


def test_vehicle_not_omnibus():

    (
        update_omnibus,
        _,
        omnibus_repo,
        published_events,
        _,
    ) = prepare_update_omnibus()
    event = make_vehicle_event(omnibus=None)
    asyncio.run(update_omnibus.execute(event))
    assert len(omnibus_repo.omnibus_events) == 0
    assert len(published_events) == 0


def test_pse_affected_as_pse():
    initial_vehicle_events = [vsav_lacks_staff_omnibus_event, pse_as_pse_home_event]
    vehicle_event = pse_affected_as_pump_event

    expected_omnibus_balance = OmnibusBalance(
        both_available=0,
        both_on_inter_san=0,
        vsav_on_inter_san=0,
        pse_on_inter_san=0,
        pse_on_inter_fire=1,
        only_pse_available=0,
    )
    expected_omnibus_details = [
        OmnibusDetail(
            vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "pse_on_inter_fire"
        )
    ]
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        vehicle_event=vehicle_event,
        expected_omnibus_balance=expected_omnibus_balance,
        expected_omnibus_details=expected_omnibus_details,
    )


def test_vsav_lacks_staff_omnibus_while_pse_on_inter():
    initial_vehicle_events = [
        vsav_back_home_event,
        pse_affected_as_pump_event,
    ]
    vehicle_event = vsav_lacks_staff_omnibus_event

    expected_omnibus_balance = OmnibusBalance(
        both_available=0,
        both_on_inter_san=0,
        vsav_on_inter_san=0,
        pse_on_inter_san=0,
        pse_on_inter_fire=1,
        only_pse_available=0,
    )

    expected_omnibus_details = [
        OmnibusDetail(
            vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "pse_on_inter_fire"
        )
    ]
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        vehicle_event=vehicle_event,
        expected_omnibus_balance=expected_omnibus_balance,
        expected_omnibus_details=expected_omnibus_details,
    )


def test_vsav_affected_but_pse_not_in_cache():
    initial_vehicle_events = []
    vehicle_event = make_vehicle_event(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_omni",
        omnibus=OmnibusInfos(
            partner_raw_vehicle_id=pse_raw_vehicle_id,
        ),
        status="selected",
    )

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        vehicle_event=vehicle_event,
        expected_omnibus_balance=None,
        expected_omnibus_details=None,
    )


def test_vsav_becomes_omnibus_and_is_affected_while_pse_at_home():

    vsav_no_omnibus = make_vehicle_event(
        raw_vehicle_id=vsav_raw_vehicle_id,
        role="vsav_solo",
        status="misc_unavailable",
        omnibus=None,
    )
    initial_vehicle_events = [vsav_no_omnibus, pse_as_vsav_home_event]

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=initial_vehicle_events,
        vehicle_event=vsav_affected_event,
        expected_omnibus_balance=OmnibusBalance(
            both_available=0,
            both_on_inter_san=0,
            vsav_on_inter_san=1,
            pse_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "vsav_on_inter_san"
            )
        ],
    )


def test_pse_downgraded_because_vsav_has_been_affected():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_affected_event, pse_as_pse_home_event],
        vehicle_event=pse_downgraded_event,
        expected_omnibus_balance=OmnibusBalance(
            both_available=0,
            both_on_inter_san=0,
            vsav_on_inter_san=1,
            pse_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "vsav_on_inter_san"
            )
        ],
    )


def test_pse_affected_on_inter_san():

    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_affected_event, pse_as_vsav_home_event],
        vehicle_event=pse_to_inter_san_event,
        expected_omnibus_balance=OmnibusBalance(
            both_available=0,
            both_on_inter_san=1,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "both_on_inter_san"
            )
        ],
    )


def test_vsav_home_while_pse_still_on_inter_san():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_affected_event, pse_to_inter_san_event],
        vehicle_event=vsav_back_home_event,
        expected_omnibus_balance=OmnibusBalance(
            both_available=0,
            both_on_inter_san=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=1,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "pse_on_inter_san"
            )
        ],
    )


def test_pse_joins_vsav_back_home():
    execute_update_omnibus_with_initialized_balance_assert_result(
        initial_vehicle_events=[vsav_back_home_event, pse_to_inter_san_event],
        vehicle_event=pse_back_home_event,
        expected_omnibus_balance=OmnibusBalance(
            both_available=1,
            both_on_inter_san=0,
            vsav_on_inter_san=0,
            pse_on_inter_san=0,
            pse_on_inter_fire=0,
            only_pse_available=0,
        ),
        expected_omnibus_details=[
            OmnibusDetail(
                vsav_raw_vehicle_id, pse_raw_vehicle_id, "STOU", "both_available"
            )
        ],
    )
