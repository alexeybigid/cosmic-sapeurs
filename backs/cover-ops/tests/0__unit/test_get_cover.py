from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import InMemoryVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import AvailabilityStoredInfos
from cover_ops.domain.use_cases.get_cover import GetCover
from tests.utils.availability_changed_event_factory import (
    make_availability_changed_event_entity,
)

from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.vehicle_event_data import RawVehicleId
from shared.helpers.date import DateStr


def test_get_cover_if_empty():
    counts_events_repo = InMemoryAvailabilityEventsRepository()
    counts_events_repo.availability_events = []
    getCover = GetCover(
        availability_events_repo=counts_events_repo,
        vehicle_cache=InMemoryVehicleCache(),
    )
    counts_entities = getCover.execute(hours=1)
    assert len(counts_entities) == 0


def test_get_cover():
    counts_events_repo = InMemoryAvailabilityEventsRepository()
    vsav_1_stou_19_sept = make_availability_changed_event_entity(
        area_availability=Availability(available=8),
        raw_vehicle_id="1",
        home_area="STOU",
        timestamp=DateStr("2020-09-19T13:00:00.0"),
        role="vsav_solo",
    )
    vsav_2_stou_26_sept = make_availability_changed_event_entity(
        area_availability=Availability(in_service=3),
        raw_vehicle_id="2",
        timestamp=DateStr("2020-09-26T13:00:00.0"),
        home_area="PARM",
        role="vsav_solo",
    )

    pse_3_CHPT_20_sept = make_availability_changed_event_entity(
        area_availability=Availability(available=2),
        raw_vehicle_id="3",
        timestamp=DateStr("2020-09-20T12:00:00.0"),
        home_area="CHPT",
        role="pse_solo",
    )
    vsav_1_STOU_20_sept = make_availability_changed_event_entity(
        area_availability=Availability(available=12),
        raw_vehicle_id="1",
        timestamp=DateStr("2020-09-20T13:00:00.0"),
        home_area="STOU",
        role="vsav_solo",
    )
    vsav_1_STOU_25_sept = make_availability_changed_event_entity(
        area_availability=Availability(available=3),
        raw_vehicle_id="1",
        timestamp=DateStr("2020-09-25T13:00:00.0"),
        home_area="STOU",
        role="vsav_solo",
    )

    counts_events_repo.availability_events = [
        vsav_2_stou_26_sept,
        vsav_1_stou_19_sept,
        vsav_1_STOU_25_sept,
        vsav_1_STOU_20_sept,
        pse_3_CHPT_20_sept,
    ]
    # Prepare cache from repo
    vehicle_cache = InMemoryVehicleCache()
    # _availabilities_by_area_by_role
    vehicle_cache._availabilities_by_area_by_role["STOU"][
        "vsav_solo"
    ] = AvailabilityStoredInfos(
        availability=vsav_1_STOU_25_sept.data.area_availability,
        latest_event_data=vsav_1_STOU_25_sept.data.latest_event_data,
    )
    vehicle_cache._availabilities_by_area_by_role["PARM"][
        "vsav_solo"
    ] = AvailabilityStoredInfos(
        availability=vsav_2_stou_26_sept.data.area_availability,
        latest_event_data=vsav_2_stou_26_sept.data.latest_event_data,
    )
    vehicle_cache._availabilities_by_area_by_role["CHPT"][
        "pse_solo"
    ] = AvailabilityStoredInfos(
        availability=pse_3_CHPT_20_sept.data.area_availability,
        latest_event_data=pse_3_CHPT_20_sept.data.latest_event_data,
    )
    # _bspp_availabilities_by_role
    vehicle_cache._bspp_availabilities_by_role["vsav_solo"] = AvailabilityStoredInfos(
        availability=vsav_1_STOU_25_sept.data.bspp_availability,
        latest_event_data=vsav_1_STOU_25_sept.data.latest_event_data,
    )
    vehicle_cache._bspp_availabilities_by_role["pse_solo"] = AvailabilityStoredInfos(
        availability=pse_3_CHPT_20_sept.data.bspp_availability,
        latest_event_data=pse_3_CHPT_20_sept.data.latest_event_data,
    )
    # _latest_vehicle_event_data_by_vehicle_id
    vehicle_cache._latest_vehicle_event_data_by_vehicle_id[
        RawVehicleId("1")
    ] = vsav_1_STOU_25_sept.data.latest_event_data
    vehicle_cache._latest_vehicle_event_data_by_vehicle_id[
        RawVehicleId("3")
    ] = pse_3_CHPT_20_sept.data.latest_event_data
    vehicle_cache._latest_vehicle_event_data_by_vehicle_id[
        RawVehicleId("2")
    ] = vsav_2_stou_26_sept.data.latest_event_data

    getCover = GetCover(
        availability_events_repo=counts_events_repo, vehicle_cache=vehicle_cache
    )
    actual_availability_changed_event_data = getCover.execute(hours=6 * 24)  # five days

    expected_availability_changed_event_data = [
        pse_3_CHPT_20_sept.data,
        vsav_1_STOU_20_sept.data,
        vsav_1_STOU_25_sept.data,
        vsav_1_STOU_25_sept.data,  # TODO : this one is a duplicate and should not be expected
        vsav_2_stou_26_sept.data,
        vsav_2_stou_26_sept.data,  # TODO : this one is a duplicate and should not be expected
    ]
    assert (
        len(actual_availability_changed_event_data) == 6
    )  # 4 # TODO : without the duplicates, this should be 4.
    assert (
        actual_availability_changed_event_data
        == expected_availability_changed_event_data
    )
