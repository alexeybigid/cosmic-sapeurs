from cover_ops.domain.ports.operation_cache import InMemoryOperationCache
from cover_ops.domain.ports.operation_events_repository import (
    InMemoryOperationEventsRepository,
    OngoingOpsCountByCauseByArea,
)
from cover_ops.domain.services.operation_event_service import OperationEventService
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_operation_events_repository import (
    ResyncCacheFromOperationEventsRepository,
)
from cover_ops.helpers.factories.operation_event_entity_factory import (
    make_operation_event_entity,
)
from tests.utils.assert_helpers import assert_subset_of

from shared.data_transfert_objects.operation_event_data import (
    RawOperationId,
    operation_closed,
    operation_has_all_vehicles_released,
    operation_has_first_affected_vehicle,
    operation_just_opened,
)
from shared.helpers.date import DateStr


def test_resync_cache_from_operations():
    operation_events_repo = InMemoryOperationEventsRepository()
    operation_cache = InMemoryOperationCache()
    operation_event_service = OperationEventService(
        operation_events_repo, operation_cache
    )
    resync_cache_from_operation_events_repository = (
        ResyncCacheFromOperationEventsRepository(
            operation_events_repo,
            operation_event_service,
        )
    )

    events_kwargs = [
        {
            "raw_operation_id": "1",
            "status": operation_just_opened,
            "cause": "victim",
            "address_area": "STOU",
        },
        {
            "raw_operation_id": "1",
            "status": "validated",
            "cause": "victim",
            "address_area": "STOU",
        },
        {
            "raw_operation_id": "2",
            "status": operation_just_opened,
            "cause": "victim",
            "address_area": "STOU",
        },
        {
            "raw_operation_id": "2",
            "status": operation_has_first_affected_vehicle,
        },
        {
            "raw_operation_id": "3",
            "status": operation_just_opened,
            "cause": "fire",
            "address_area": "STOU",
        },
        {
            "raw_operation_id": "4",
            "status": operation_just_opened,
            "cause": "fire",
            "address_area": "PARM",
        },
        {
            "raw_operation_id": "4",
            "status": operation_has_first_affected_vehicle,
            "cause": "fire",
            "address_area": "PARM",
        },
        {
            "raw_operation_id": "4",
            "status": operation_has_all_vehicles_released,
            "cause": "fire",
            "address_area": "PARM",
        },
        {
            "raw_operation_id": "5",
            "status": operation_closed,
        },
        # some operation without a cause
        {
            "raw_operation_id": "6",
            "status": operation_just_opened,
            "cause": None,
            "address_area": "STOU",
        },
        {
            "raw_operation_id": "6",
            "status": operation_has_first_affected_vehicle,
            "cause": None,
            "address_area": "STOU",
        },
    ]

    repo_operation_events = [
        make_operation_event_entity(
            timestamp=DateStr(f"2020-01-01T1{k}:00:00.000000"), **event_kwargs
        )
        for k, event_kwargs in enumerate(events_kwargs)
    ]

    operation_events_repo.operation_events = repo_operation_events
    resync_cache_from_operation_events_repository.execute()

    actual_operation_stored_infos_by_id = (
        operation_cache._operation_stored_infos_by_operation_id
    )
    assert len(actual_operation_stored_infos_by_id) == 3
    assert (
        actual_operation_stored_infos_by_id[RawOperationId("1")].status
        == operation_just_opened
    )
    assert (
        actual_operation_stored_infos_by_id[RawOperationId("2")].status
        == operation_has_first_affected_vehicle
    )
    assert (
        actual_operation_stored_infos_by_id[RawOperationId("3")].status
        == operation_just_opened
    )

    actual_ongoing_ops_counts_by_cause_by_area = (
        operation_cache._ongoing_ops_count_by_cause_by_area
    )

    expected_ongoing_ops_counts_by_cause_by_area: OngoingOpsCountByCauseByArea = {
        "victim": {"STOU": 1},
        "fire": {},
        "other": {},
    }

    for cause in expected_ongoing_ops_counts_by_cause_by_area.keys():
        assert_subset_of(
            actual_ongoing_ops_counts_by_cause_by_area[cause],
            expected_ongoing_ops_counts_by_cause_by_area[cause],
        )
