from setuptools import find_packages, setup

packages = find_packages(exclude=[])

setup(
    name="cover-ops",
    version="0.1",
    packages=packages,
    install_requires=[
        "tqdm",
        "pandas",
        "python-dotenv",
        "aiohttp",
        "aiohttp_cors",
        "dataclasses_jsonschema",
        "requests",
        "redis",
    ],
)
