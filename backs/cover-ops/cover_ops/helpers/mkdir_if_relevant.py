import os
from pathlib import Path


def mkdir_if_relevant(path: Path):
    if not os.path.isdir(path):
        os.mkdir(path)
