from typing import Optional, Union

from cover_ops.domain.entities.event_entities import VehicleEventEntity

from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    OmnibusInfos,
    RawVehicleId,
    VehicleRole,
    VehicleStatus,
)
from shared.factories.vehicle_event_factory import make_vehicle_event_data
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4


def make_vehicle_event_entity(
    uuid: str = None,
    # data: VehicleEventData = None,
    # Following args are for make_vehicle_event_data
    timestamp: DateStr = None,
    raw_vehicle_id: Union[RawVehicleId, str] = None,
    raw_operation_id: str = None,
    status: VehicleStatus = None,
    raw_status: str = None,
    home_area: Area = None,
    role: VehicleRole = None,
    vehicle_name: str = None,
    omnibus: Optional[OmnibusInfos] = None,
) -> VehicleEventEntity:
    vehicle_event_data = make_vehicle_event_data(
        timestamp=timestamp,
        raw_vehicle_id=raw_vehicle_id,
        raw_operation_id=raw_operation_id,
        status=status,
        raw_status=raw_status,
        home_area=home_area,
        role=role,
        vehicle_name=vehicle_name,
        omnibus=omnibus,
    )
    uuid = uuid or uuid4()
    return VehicleEventEntity(uuid=uuid, data=vehicle_event_data)
