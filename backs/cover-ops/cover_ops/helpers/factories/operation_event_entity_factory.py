from typing import Union

from cover_ops.domain.entities.event_entities import OperationEventEntity

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
    RawOperationId,
)
from shared.factories.operation_event_factory import make_operation_event_data
from shared.helpers.date import DateStr
from shared.helpers.uuid import uuid4


def make_operation_event_entity(
    uuid: str = None,
    # Following args are for make_vehicle_event_data
    timestamp: DateStr = None,
    raw_operation_id: Union[RawOperationId, str] = None,
    status: OperationStatus = None,
    address: str = None,
    address_area: Area = None,
    cause: OperationCause = None,
    raw_cause: str = None,
) -> OperationEventEntity:
    operation_event_data = make_operation_event_data(
        timestamp=timestamp,
        raw_operation_id=raw_operation_id,
        status=status,
        address=address,
        address_area=address_area,
        cause=cause,
        raw_cause=raw_cause,
    )
    uuid = uuid or uuid4()
    return OperationEventEntity(uuid=uuid, data=operation_event_data)
