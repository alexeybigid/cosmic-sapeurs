import os

from dotenv import load_dotenv

from shared.helpers.decorators import (
    ExceptionMode,
    exception_mode_options,
    make_exception_logger,
)
from shared.helpers.prepare_get_env_variable import prepare_get_env_variable

load_dotenv()

get_env_variable = prepare_get_env_variable(os.environ.get)


def get_exception_mode() -> ExceptionMode:
    return get_env_variable("EXCEPTION_MODE", exception_mode_options)


exception_logger = make_exception_logger(get_exception_mode())
