from typing import Callable, Dict, List, TypeVar

K = TypeVar("K")
T = TypeVar("T")


def make_typed_record(keys: List[K], factory: Callable[[], T]) -> Dict[K, T]:
    record = {}
    for k in keys:
        record[k] = factory()
    return record
