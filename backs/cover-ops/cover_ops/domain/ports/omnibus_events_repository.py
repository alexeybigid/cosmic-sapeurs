import abc
from typing import List, Optional

from cover_ops.domain.entities.event_entities import OmnibusBalanceChangedEventEntity

from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.ports.abstract_events_repository import AbstractSapeursEventsRepository


class AbstractOmnibusEventsRepository(AbstractSapeursEventsRepository, abc.ABC):
    @abc.abstractmethod
    def get_latest_event_data(self) -> Optional[OmnibusBalanceChangedEventData]:
        raise NotImplementedError


class InMemoryOmnibusEventsRepository(AbstractOmnibusEventsRepository):
    def __init__(self) -> None:
        self._omnibus_events: List[OmnibusBalanceChangedEventEntity] = []

    def _add(self, omnibus_event_entity: OmnibusBalanceChangedEventEntity):
        self._omnibus_events.append(omnibus_event_entity)

    def _match_uuid(self, uuid: str) -> Optional[OmnibusBalanceChangedEventEntity]:
        _matches = [event for event in self._omnibus_events if event.uuid == uuid]
        return _matches[0] if _matches else None

    def get_latest_event_data(self) -> Optional[OmnibusBalanceChangedEventData]:
        return self._omnibus_events[-1].data if self._omnibus_events else None

    # next methods are only for test purposes
    @property
    def omnibus_events(self) -> List[OmnibusBalanceChangedEventEntity]:
        return self._omnibus_events

    @omnibus_events.setter
    def omnibus_events(self, events: List[OmnibusBalanceChangedEventEntity]):
        self._omnibus_events = events
