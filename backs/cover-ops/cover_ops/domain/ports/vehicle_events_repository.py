import abc
from dataclasses import dataclass
from itertools import groupby
from typing import Dict, List, Optional, Tuple, Union

from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.helpers.make_typed_record import make_typed_record

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.vehicle_event_data import (
    Area,
    AvailabilityKind,
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
    vehicle_availability_kind_options,
    vehicle_role_options,
)
from shared.ports.abstract_events_repository import AbstractSapeursEventsRepository

VehicleEventEntitiesList = List[VehicleEventEntity]


@dataclass
class AvailabilityStoredInfos:
    availability: Availability
    latest_event_data: Union[VehicleEventData, None] = None

    @classmethod
    def from_dict(
        cls,
        input: Dict,
    ) -> "AvailabilityStoredInfos":

        latest_event_data_dict = input["latest_event_data"]
        latest_event_data = (
            VehicleEventData(**latest_event_data_dict)
            if latest_event_data_dict
            else None
        )

        return cls(
            availability=Availability(**input["availability"]),
            latest_event_data=latest_event_data,
        )


AvailabilityStoredInfoByRole = Dict[VehicleRole, AvailabilityStoredInfos]
AvailabilityStoredInfoByAreaByRole = Dict[Area, AvailabilityStoredInfoByRole]

VehicleEventDataById = Dict[RawVehicleId, VehicleEventData]
AvailabilityStoredInfosByVehicleId = Dict[RawVehicleId, AvailabilityStoredInfos]


class AbstractVehicleEventsRepository(
    AbstractSapeursEventsRepository,
    abc.ABC,
):
    @abc.abstractmethod
    def get_latest_event_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventEntity]:
        raise NotImplementedError

    def list_latest_events_by_availability_by_role_by_area_and_for_bspp(
        self,
    ) -> Tuple[
        Dict[Area, Dict[VehicleRole, Dict[AvailabilityKind, VehicleEventEntitiesList]]],
        Dict[VehicleRole, Dict[AvailabilityKind, VehicleEventEntitiesList]],
    ]:

        events_by_availability_by_role = make_typed_record(
            vehicle_role_options,
            lambda: make_typed_record(
                vehicle_availability_kind_options, make_vehicle_event_entities_list
            ),
        )
        events_by_availability_by_area_by_role = make_typed_record(
            bspp_area_options,
            lambda: make_typed_record(
                vehicle_role_options,
                lambda: make_typed_record(
                    vehicle_availability_kind_options,
                    make_vehicle_event_entities_list,
                ),
            ),
        )

        latest_events_by_raw_vehicle_id = self.get_latest_event_by_raw_vehicle_id()
        for (home_area, role, availability_kind), matching_latest_events in groupby(
            latest_events_by_raw_vehicle_id.values(),
            lambda event: (
                event.data.home_area,
                event.data.role,
                event.data.availability_kind,
            ),
        ):
            if home_area not in bspp_area_options:
                continue
            if role not in vehicle_role_options:
                continue

            list_matching_latest_events = list(matching_latest_events)

            events_by_availability_by_area_by_role[home_area][role][
                availability_kind
            ] += list_matching_latest_events
            events_by_availability_by_role[role][
                availability_kind
            ] += list_matching_latest_events

        return (
            events_by_availability_by_area_by_role,
            events_by_availability_by_role,
        )


def make_vehicle_event_entities_list() -> List[VehicleEventEntity]:
    return []


class InMemoryVehicleEventsRepository(AbstractVehicleEventsRepository):
    def __init__(self) -> None:
        self._vehicle_events: VehicleEventEntitiesList = []
        super().__init__()

    def get_all(self) -> VehicleEventEntitiesList:
        return self._vehicle_events

    def get_latest_event_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventEntity]:
        latest_events_by_raw_vehicle_id: Dict[RawVehicleId, VehicleEventEntity] = {}
        for raw_vehicle_id, group in groupby(
            sorted(
                self._vehicle_events,
                key=lambda event: event.data.raw_vehicle_id,
            ),
            lambda event: event.data.raw_vehicle_id,
        ):
            group_sorted_by_timestamp = list(
                sorted(
                    group,
                    key=lambda event: event.data.timestamp,
                ),
            )
            latest_event = group_sorted_by_timestamp[-1]
            latest_events_by_raw_vehicle_id[
                latest_event.data.raw_vehicle_id
            ] = latest_event
        return latest_events_by_raw_vehicle_id

    def _match_uuid(self, uuid: str) -> Optional[VehicleEventEntity]:
        _matches = [event for event in self._vehicle_events if event.uuid == uuid]
        return _matches[0] if _matches else None

    def _add(self, vehicle_event_entity: VehicleEventEntity):
        self._vehicle_events.append(vehicle_event_entity)

    # next methods are only for test purposes
    @property
    def vehicle_events(self) -> VehicleEventEntitiesList:
        return self._vehicle_events

    @vehicle_events.setter
    def vehicle_events(self, vehicle_events: List[VehicleEventEntity]):
        self._vehicle_events = vehicle_events
