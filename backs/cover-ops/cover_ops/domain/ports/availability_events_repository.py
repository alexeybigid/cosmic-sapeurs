import abc
from abc import abstractproperty
from datetime import timedelta
from itertools import groupby
from typing import Callable, List, Optional

from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity

from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.helpers.date import to_datetime
from shared.ports.abstract_events_repository import AbstractSapeursEventsRepository


class AbstractAvailabilityEventsRepository(AbstractSapeursEventsRepository):
    @abc.abstractmethod
    def get_availability_since(
        self, hours: Optional[int]
    ) -> List[AvailabilityChangedEventEntity]:
        raise NotImplementedError

    @abc.abstractmethod
    def list_latest_availability_event_data_by_vehicle_from_repo(
        self,
    ) -> List[AvailabilityChangedEventData]:
        raise NotImplementedError

    @abstractproperty
    def last_availability_event(self) -> Optional[AvailabilityChangedEventEntity]:
        raise NotImplementedError


def sorted_groupby(l: List, key_function: Callable):
    return groupby(
        sorted(l, key=key_function),
        key_function,
    )


class InMemoryAvailabilityEventsRepository(AbstractAvailabilityEventsRepository):
    def __init__(self) -> None:
        self._availability_events: List[AvailabilityChangedEventEntity] = []

    def _add(self, availability_changed_entity: AvailabilityChangedEventEntity):
        self._availability_events.append(availability_changed_entity)

    def get_availability_since(
        self, hours: Optional[int]
    ) -> List[AvailabilityChangedEventEntity]:
        if hours is None:
            return self._availability_events

        if not self._availability_events:
            return []

        last_timestamp = max(
            [
                availability_event.data.timestamp
                for availability_event in self._availability_events
            ]
        )

        date = to_datetime(last_timestamp) - timedelta(hours=hours)
        filtered = list(
            filter(
                lambda availability_event: to_datetime(
                    availability_event.data.timestamp
                )
                >= date,
                self._availability_events,
            )
        )

        if not len(filtered):
            return []

        filtered_sorted = sorted(filtered, key=lambda event: event.data.timestamp)

        return filtered_sorted

    def list_latest_availability_event_data_by_vehicle_from_repo(
        self,
    ) -> List[AvailabilityChangedEventData]:
        if not self._availability_events:
            return []

        latest_availability_event_data_by_vehicle = []

        for _, group in sorted_groupby(
            self._availability_events,
            lambda event: event.data.raw_vehicle_id,
        ):
            latest_availability_event_data_by_vehicle.append(
                get_latest_event(list(group)).data
            )
        return latest_availability_event_data_by_vehicle

    def _match_uuid(self, uuid: str) -> Optional[AvailabilityChangedEventEntity]:
        _matches = [event for event in self._availability_events if event.uuid == uuid]
        return _matches[0] if _matches else None

    # next methods are only for test purposes
    @property
    def availability_events(self) -> List[AvailabilityChangedEventEntity]:
        return self._availability_events

    @property
    def last_availability_event(self) -> Optional[AvailabilityChangedEventEntity]:
        return self._availability_events[-1] if self._availability_events else None

    @availability_events.setter
    def availability_events(self, events: List[AvailabilityChangedEventEntity]):
        self._availability_events = events


def sort_events_by_timestamp(
    events: List[AvailabilityChangedEventEntity],
) -> List[AvailabilityChangedEventEntity]:
    return list(
        sorted(
            events,
            key=lambda event: event.data.timestamp,
        ),
    )


def get_latest_event(
    events: List[AvailabilityChangedEventEntity],
) -> AvailabilityChangedEventEntity:
    return sort_events_by_timestamp(events)[-1]
