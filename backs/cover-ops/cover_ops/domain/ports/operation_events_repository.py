import abc
from dataclasses import dataclass
from itertools import groupby
from typing import Callable, Dict, List, Optional

from cover_ops.domain.entities.event_entities import OperationEventEntity

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
    RawOperationId,
    operation_closed,
    operation_has_first_affected_vehicle,
    operation_just_opened,
    operation_relevant_status_options,
)
from shared.helpers.logger import logger
from shared.ports.abstract_events_repository import AbstractSapeursEventsRepository

OperationEventEntitiesList = List[OperationEventEntity]

OngoingOpsCountByArea = Dict[Area, int]
OngoingOpsCountByCauseByArea = Dict[OperationCause, OngoingOpsCountByArea]


def sort_operation_events_by_timestamp(
    events: OperationEventEntitiesList,
) -> OperationEventEntitiesList:
    return list(
        sorted(
            events,
            key=lambda event: event.data.timestamp,
        ),
    )


@dataclass
class LatestOperationInfos:
    cause: OperationCause
    address_area: Area
    last_relevant_status: OperationStatus  # OperationRelevantStatus


def filter_operation_events(
    events: List[OperationEventEntity],
    predicate: Callable[[OperationEventEntity], bool],
) -> List[OperationEventEntity]:
    filtered_events = []
    for event in events:
        if predicate(event):
            filtered_events.append(event)
    return filtered_events


class AbstractOperationEventsRepository(
    AbstractSapeursEventsRepository,
    abc.ABC,
):
    @abc.abstractmethod
    def get_events_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationStatus]] = None,
    ) -> List[OperationEventEntity]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status(
        self,
    ) -> List[OperationEventEntity]:
        raise NotImplementedError

    def get_latest_operation_infos_for_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[LatestOperationInfos]:
        events_from_this_operation_with_relevant_status = (
            self.get_events_for_id_with_status_in(
                raw_operation_id, statuses=operation_relevant_status_options
            )
        )
        if not events_from_this_operation_with_relevant_status:
            return
        latest_event = sort_operation_events_by_timestamp(
            events_from_this_operation_with_relevant_status
        ).pop()
        if latest_event.data.status == operation_just_opened:
            opening_event = latest_event
        else:
            opening_events_from_this_operation = filter_operation_events(
                events=events_from_this_operation_with_relevant_status,
                predicate=lambda event: event.data.status == operation_just_opened,
            )
            if not opening_events_from_this_operation:
                return
            opening_event = opening_events_from_this_operation.pop()
        cause = opening_event.data.cause
        address_area = opening_event.data.address_area
        if not cause or not address_area:
            # Should not really happen ...
            logger.warn(
                f"Found in repo an opening event for operation #{raw_operation_id} with either address or no cause."
            )
            return

        return LatestOperationInfos(
            cause=cause,
            address_area=address_area,
            last_relevant_status=latest_event.data.status,
        )


class InMemoryOperationEventsRepository(AbstractOperationEventsRepository):
    def __init__(self) -> None:
        self._operation_events: OperationEventEntitiesList = []
        super().__init__()

    def get_all(self) -> OperationEventEntitiesList:
        return self._operation_events

    def get_events_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationStatus]] = None,
    ) -> List[OperationEventEntity]:
        events_with_relevant_status_for_this_operation = filter_operation_events(
            events=self._operation_events,
            predicate=lambda event: event.data.raw_operation_id == raw_operation_id
            and event.data.status in statuses
            if statuses
            else True,
        )
        return events_with_relevant_status_for_this_operation

    def get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status(
        self,
    ) -> List[OperationEventEntity]:
        operation_events_with_relevant_and_closing_status = filter_operation_events(
            self._operation_events,
            lambda event: event.data.status
            in [
                *operation_relevant_status_options,
                operation_closed,
            ],
        )
        ongoing_operation_latest_events: List[OperationEventEntity] = []

        ongoing_operation_latest_events = []
        for _, group in groupby(
            sorted(
                operation_events_with_relevant_and_closing_status,
                key=lambda event: event.data.raw_operation_id,
            ),
            lambda event: event.data.raw_operation_id,
        ):
            group_sorted_by_timestamp = sort_operation_events_by_timestamp(list(group))
            latest_event = group_sorted_by_timestamp[-1]
            if latest_event.data.status in [
                operation_just_opened,
                operation_has_first_affected_vehicle,
            ]:
                ongoing_operation_latest_events.append(latest_event)

        return list(
            sorted(
                ongoing_operation_latest_events,
                key=lambda event: event.data.timestamp,
            ),
        )

    def _match_uuid(self, uuid: str) -> Optional[OperationEventEntity]:
        _matches = [event for event in self._operation_events if event.uuid == uuid]
        return _matches[0] if _matches else None

    def _add(self, operation_event_entity: OperationEventEntity):
        self._operation_events.append(operation_event_entity)

    # next methods are only for test purposes
    @property
    def operation_events(self) -> OperationEventEntitiesList:
        return self._operation_events

    @operation_events.setter
    def operation_events(self, operation_events: OperationEventEntitiesList):
        self._operation_events = operation_events
