import abc
from typing import Dict, List, Optional

from cover_ops.domain.ports.vehicle_events_repository import (
    AvailabilityStoredInfoByAreaByRole,
    AvailabilityStoredInfoByRole,
    AvailabilityStoredInfos,
    VehicleEventDataById,
)
from cover_ops.helpers.make_typed_record import make_typed_record

from shared.data_transfert_objects.area import Area, bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusDetail,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
    vehicle_role_options,
)
from shared.helpers.decorators import timeit


class AbstractVehicleCache(abc.ABC):
    def __init__(self):
        self.initialize_cache()

    @abc.abstractmethod
    def set_availability_stored_infos_for_areas_for_roles(
        self,
        availability_stored_infos_by_area_by_role: AvailabilityStoredInfoByAreaByRole,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def set_availability_stored_infos_for_area_for_role(
        self,
        area: Area,
        role: VehicleRole,
        availability_stored_infos: AvailabilityStoredInfos,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        raise NotImplementedError

    @abc.abstractmethod
    def set_bspp_availability_stored_infos_for_role(
        self,
        role: VehicleRole,
        availability_stored_infos: AvailabilityStoredInfos,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        raise NotImplementedError

    @abc.abstractmethod
    def set_bspp_availability_stored_infos_for_roles(
        self, bspp_availability_stored_infos_by_role: AvailabilityStoredInfoByRole
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def remove_omnibus_given_vsav_id(self, vsav_id: RawVehicleId) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_omnibus_given_vsav_id(
        self,
        vsav_id: RawVehicleId,
    ) -> Optional[OmnibusDetail]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_omnibus_details(self) -> List[OmnibusDetail]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_omnibus_balance(self) -> OmnibusBalance:
        raise NotImplementedError

    @abc.abstractmethod
    def set_omnibus_balance(self, omnibus_balance: OmnibusBalance) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def set_latest_vehicle_event_data_for_vehicle_id(
        self, vehicle_event_data: VehicleEventData
    ):
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_vehicle_event_data_for_vehicle_id(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[VehicleEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_latest_vehicle_event_datas_for_area_for_role(
        self,
        area: Area,
        role: VehicleRole,
    ) -> List[VehicleEventData]:
        raise NotImplementedError

    @abc.abstractmethod
    def get_vehicle_event_data_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> List[VehicleEventData]:
        raise NotImplementedError

    @timeit("initialize_cache")
    def initialize_cache(self):
        bspp_availabilities_by_role = make_typed_record(
            vehicle_role_options,
            lambda: AvailabilityStoredInfos(availability=Availability()),
        )
        availabilities_by_area_by_role = make_typed_record(
            bspp_area_options,
            lambda: make_typed_record(
                vehicle_role_options,
                lambda: AvailabilityStoredInfos(availability=Availability()),
            ),
        )

        omnibus_balance = OmnibusBalance()

        self.set_bspp_availability_stored_infos_for_roles(bspp_availabilities_by_role)  # type: ignore
        self.set_availability_stored_infos_for_areas_for_roles(availabilities_by_area_by_role)  # type: ignore
        self.set_omnibus_balance(omnibus_balance)


class InMemoryVehicleCache(AbstractVehicleCache):
    _latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById
    _availabilities_by_area_by_role: AvailabilityStoredInfoByAreaByRole
    _bspp_availabilities_by_role: AvailabilityStoredInfoByRole
    _omnibus_balance: OmnibusBalance

    def __init__(self):
        self._latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById = {}
        super().__init__()
        self.omnibus_details: Dict[RawVehicleId, OmnibusDetail] = {}

    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        try:
            return self._availabilities_by_area_by_role[area][role]
        except KeyError:
            raise Exception  # TODO ?? Should not happen.

    def set_availability_stored_infos_for_area_for_role(
        self,
        area: Area,
        role: VehicleRole,
        availability_stored_infos: AvailabilityStoredInfos,
    ) -> None:
        self._availabilities_by_area_by_role[area][role] = availability_stored_infos

    def set_bspp_availability_stored_infos_for_role(
        self,
        role: VehicleRole,
        availability_stored_infos: AvailabilityStoredInfos,
    ) -> None:
        self._bspp_availabilities_by_role[role] = availability_stored_infos

    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        return self._bspp_availabilities_by_role[role]

    def set_bspp_availability_stored_infos_for_roles(
        self, stored_info_by_role: AvailabilityStoredInfoByRole
    ) -> None:
        self._bspp_availabilities_by_role = stored_info_by_role

    def set_availability_stored_infos_for_areas_for_roles(
        self, stored_info_by_area_by_role: AvailabilityStoredInfoByAreaByRole
    ) -> None:
        self._availabilities_by_area_by_role = stored_info_by_area_by_role

    def set_latest_vehicle_event_data_for_vehicle_id(
        self, vehicle_event_data: VehicleEventData
    ):
        self._latest_vehicle_event_data_by_vehicle_id[
            vehicle_event_data.raw_vehicle_id
        ] = vehicle_event_data

    def get_latest_vehicle_event_data_for_vehicle_id(
        self, raw_vehicle_id: RawVehicleId
    ) -> Optional[VehicleEventData]:
        return self._latest_vehicle_event_data_by_vehicle_id.get(raw_vehicle_id)

    def get_vehicle_event_data_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> List[VehicleEventData]:
        return [
            vehicle_event_data
            for vehicle_event_data in self._latest_vehicle_event_data_by_vehicle_id.values()
            if vehicle_event_data.raw_operation_id == raw_operation_id
        ]

    def get_latest_vehicle_event_datas_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> List[VehicleEventData]:
        return [
            vehicle_event_data
            for vehicle_event_data in self._latest_vehicle_event_data_by_vehicle_id.values()
            if vehicle_event_data.role == role and vehicle_event_data.home_area == area
        ]

    def get_omnibus_balance(self) -> OmnibusBalance:
        return self._omnibus_balance

    def set_omnibus_balance(self, omnibus_balance: OmnibusBalance) -> None:
        self._omnibus_balance = omnibus_balance

    def remove_omnibus_given_vsav_id(self, vsav_id: RawVehicleId) -> None:
        if vsav_id in self.omnibus_details:
            del self.omnibus_details[vsav_id]

    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        self.omnibus_details[omnibus_detail.vsav_id] = omnibus_detail

    def get_omnibus_given_vsav_id(
        self,
        vsav_id: RawVehicleId,
    ) -> Optional[OmnibusDetail]:
        return self.omnibus_details.get(vsav_id)

    def get_omnibus_details(self) -> List[OmnibusDetail]:
        return list(self.omnibus_details.values())
