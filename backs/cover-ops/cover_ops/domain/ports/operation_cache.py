import abc
from dataclasses import dataclass
from typing import Dict, List, Optional, Set

from cover_ops.domain.ports.operation_events_repository import (
    OngoingOpsCountByCauseByArea,
)

from shared.data_transfert_objects.area import Area, bspp_area_options
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationStatus,
    RawOperationId,
    operation_cause_options,
)


@dataclass
class OperationStoredInfos:
    status: OperationStatus
    latest_ongoing_op_changed_event_data: Optional[OngoingOpChangedEventData]

    @classmethod
    def from_dict(cls, d: dict) -> "OperationStoredInfos":
        latest_ongoing_op_changed_event_data_value = d.get(
            "latest_ongoing_op_changed_event_data"
        )
        return OperationStoredInfos(
            status=d["status"],
            latest_ongoing_op_changed_event_data=OngoingOpChangedEventData.from_dict(
                latest_ongoing_op_changed_event_data_value
            )
            if latest_ongoing_op_changed_event_data_value
            else None,
        )


OperationStoredInfosById = Dict[RawOperationId, OperationStoredInfos]


def default_ongoing_ops_count_by_cause_by_area_dict() -> OngoingOpsCountByCauseByArea:
    ongoing_ops_count_by_cause_by_area: OngoingOpsCountByCauseByArea = {}
    for operation_cause in operation_cause_options:
        ongoing_ops_count_by_cause_by_area[operation_cause] = {}
        for area in bspp_area_options:
            ongoing_ops_count_by_cause_by_area[operation_cause][area] = 0
    return ongoing_ops_count_by_cause_by_area


class AbstractOperationCache(abc.ABC):
    def __init__(self):
        self.clear_cache()

    @abc.abstractmethod
    def set_operation_stored_infos_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
        operation_stored_infos: OperationStoredInfos,
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_operation_stored_infos_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> OperationStoredInfos:
        raise NotImplementedError

    @abc.abstractmethod
    def set_operations_notified_from_vehicle_changes(
        self, operation_id: Set[RawOperationId]
    ) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        raise NotImplementedError

    @abc.abstractmethod
    def set_ongoing_ops_count_for_area_for_cause(
        self, area: Area, cause: OperationCause, count: int
    ):
        raise NotImplementedError

    @abc.abstractmethod
    def get_ongoing_ops_count_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> int:
        raise NotImplementedError

    @abc.abstractmethod
    def get_ongoing_ops_counts_for_cause(self, cause: OperationCause) -> List[int]:
        raise NotImplementedError

    @abc.abstractmethod
    def set_ongoing_ops_count_for_causes_for_areas(
        self, ongoing_ops_count_by_cause_by_area: OngoingOpsCountByCauseByArea
    ):
        raise NotImplementedError

    @abc.abstractmethod
    def get_all_operation_stored_infos(self) -> List[OperationStoredInfos]:
        raise NotImplementedError

    @abc.abstractmethod
    def remove_operation_stored_infos_for_operation_id(
        self, raw_operation_id: RawOperationId
    ):
        raise NotImplementedError

    def clear_cache(self):
        self.set_ongoing_ops_count_for_causes_for_areas(
            default_ongoing_ops_count_by_cause_by_area_dict()
        )


class InMemoryOperationCache(AbstractOperationCache):
    def __init__(self):
        self._operation_stored_infos_by_operation_id = {}
        self._operations_notified_from_vehicle_changes = set()
        super().__init__()

    def set_operation_stored_infos_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
        operation_stored_infos: OperationStoredInfos,
    ) -> None:
        self._operation_stored_infos_by_operation_id[
            raw_operation_id
        ] = operation_stored_infos

    def get_operation_stored_infos_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> Optional[OperationStoredInfos]:
        return self._operation_stored_infos_by_operation_id.get(raw_operation_id)

    def set_ongoing_ops_count_for_area_for_cause(
        self, area: Area, cause: OperationCause, count: int
    ):
        self._ongoing_ops_count_by_cause_by_area[cause][area] = count

    def get_ongoing_ops_count_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> int:
        try:
            return self._ongoing_ops_count_by_cause_by_area[cause][area]
        except KeyError:
            raise Exception(
                "Should not happen since we initialize with all area and casue options"
            )

    def set_operations_notified_from_vehicle_changes(
        self, operation_ids: Set[RawOperationId]
    ) -> None:
        self._operations_notified_from_vehicle_changes = operation_ids

    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        return self._operations_notified_from_vehicle_changes

    def set_ongoing_ops_count_for_causes_for_areas(
        self, ongoing_ops_count_by_cause_by_area: OngoingOpsCountByCauseByArea
    ):
        self._ongoing_ops_count_by_cause_by_area = ongoing_ops_count_by_cause_by_area

    def get_ongoing_ops_counts_for_cause(self, cause: OperationCause) -> List[int]:
        return list(self._ongoing_ops_count_by_cause_by_area[cause].values())

    def remove_operation_stored_infos_for_operation_id(
        self, raw_operation_id: RawOperationId
    ):
        if raw_operation_id in self._operation_stored_infos_by_operation_id:
            del self._operation_stored_infos_by_operation_id[raw_operation_id]

    def get_all_operation_stored_infos(self) -> List[OperationStoredInfos]:
        return list(self._operation_stored_infos_by_operation_id.values())
