import abc
from datetime import timedelta
from typing import List, Optional

from cover_ops.domain.entities.event_entities import OngoingOpChangedEventEntity

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationCause
from shared.helpers.date import to_datetime

OngoingOpChangedList = List[OngoingOpChangedEventEntity]


class AbstractOngoingOpEventsRepository(abc.ABC):
    def add(self, ongoing_op_changed_entity: OngoingOpChangedEventEntity):
        self._add(ongoing_op_changed_entity)

    @abc.abstractclassmethod
    def _add(self, ongoing_ops_changed_entity: OngoingOpChangedEventEntity) -> None:
        raise NotImplementedError

    @abc.abstractclassmethod
    def get_ongoing_ops_since(self, hours: int) -> List[OngoingOpChangedEventEntity]:
        raise NotImplementedError

    @abc.abstractclassmethod
    def get_latest_event_data_by_area_by_cause(
        self, area: Area, cause: OperationCause
    ) -> Optional[OngoingOpChangedEventData]:
        raise NotImplementedError


class InMemoryOngoingOpEventsRepository(AbstractOngoingOpEventsRepository):
    def __init__(self) -> None:
        self._ongoing_op_events: OngoingOpChangedList = []

    def _add(self, ongoing_op_changed_entity: OngoingOpChangedEventEntity):
        self._ongoing_op_events.append(ongoing_op_changed_entity)

    def get_ongoing_ops_since(self, hours: int) -> List[OngoingOpChangedEventEntity]:
        if not self._ongoing_op_events:
            return []

        last_timestamp = max(
            [
                availability_event.data.timestamp
                for availability_event in self._ongoing_op_events
            ]
        )
        date = to_datetime(last_timestamp) - timedelta(hours=hours)

        filtered = list(
            filter(
                lambda ongoing_op_event: to_datetime(ongoing_op_event.data.timestamp)
                >= date,
                self._ongoing_op_events,
            )
        )
        if not len(filtered):
            return []

        filtered_sorted = sorted(filtered, key=lambda event: event.data.timestamp)

        return filtered_sorted

    def get_latest_event_data_by_area_by_cause(
        self, area: Area, cause: OperationCause
    ) -> Optional[OngoingOpChangedEventData]:
        sorted_selection = sorted(
            [
                event
                for event in self.ongoing_op_events
                if event.data.address_area == area and event.data.cause == cause
            ],
            key=lambda event: event.data.timestamp,
        )
        return sorted_selection[-1].data if sorted_selection else None

    # next methods are only for test purposes
    @property
    def ongoing_op_events(self) -> OngoingOpChangedList:
        return self._ongoing_op_events

    @ongoing_op_events.setter
    def ongoing_op_events(self, events: OngoingOpChangedList):
        self._ongoing_op_events = events
