from dataclasses import dataclass
from typing import Dict, List, Optional

from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.entities.event_entities import OmnibusBalanceChangedEventEntity
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusBalanceChangedEventData,
    OmnibusBalanceKind,
    OmnibusDetail,
)
from shared.data_transfert_objects.vehicle_event_data import (
    AvailabilityKind,
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
    vehicle_role_options,
)
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import OmnibusBalanceChangedEvent, VehicleEvent


def infer_balance_key_for_mixed_availability(
    vsav_availability_kind: AvailabilityKind,
    pse_role: VehicleRole,
    pse_availability_kind: AvailabilityKind,
):
    if (
        pse_role == "pse_omni_san"
        and pse_availability_kind == "in_service"
        and vsav_availability_kind == "available"
    ):
        return "pse_on_inter_san"
    if (
        pse_role == "pse_omni_san"
        and pse_availability_kind == "available"
        and vsav_availability_kind == "in_service"
    ):
        return "vsav_on_inter_san"
    if (
        pse_role == "pse_omni_pump"
        and pse_availability_kind == "in_service"
        and vsav_availability_kind not in ["in_service", "available"]
    ):
        return "pse_on_inter_fire"
    if (
        pse_role == "pse_omni_pump"
        and pse_availability_kind == "available"
        and vsav_availability_kind not in ["in_service", "available"]
    ):
        return "only_pse_available"
    return  # dangling state


@dataclass
class OmnibusDatas:
    vsav_data: VehicleEventData
    pse_data: VehicleEventData


def infer_omnibus_datas(
    data_0: VehicleEventData, data_1: VehicleEventData
) -> OmnibusDatas:
    if data_0.role == "vsav_omni":
        return OmnibusDatas(data_0, data_1)
    return OmnibusDatas(data_1, data_0)


def infer_balance_key(omnibus_datas: OmnibusDatas) -> Optional[OmnibusBalanceKind]:
    vsav_availability_kind = omnibus_datas.vsav_data.availability_kind
    pse_availability_kind = omnibus_datas.pse_data.availability_kind

    if vsav_availability_kind == "in_service" and pse_availability_kind == "in_service":
        return "both_on_inter_san"

    if vsav_availability_kind == "available" and pse_availability_kind == "available":
        return "both_available"

    return infer_balance_key_for_mixed_availability(
        vsav_availability_kind=vsav_availability_kind,
        pse_availability_kind=pse_availability_kind,
        pse_role=omnibus_datas.pse_data.role,
    )


def get_matching_omnibus_detail(
    raw_vehicle_id: RawVehicleId, omnibus_details: List[OmnibusDetail]
):
    # TODO : Should be a Cache Method ?
    matching = [d for d in omnibus_details if raw_vehicle_id in [d.pse_id, d.vsav_id]]
    return matching[0] if matching else None


class UpdateOmnibus:
    def __init__(
        self,
        domain_event_bus: DomainEventBus,
        omnibus_events_repo: AbstractOmnibusEventsRepository,
        uuid: AbstractUuid,
        vehicle_cache: AbstractVehicleCache,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.omnibus_repo = omnibus_events_repo
        self.uuid = uuid
        self.vehicle_cache = vehicle_cache

    async def execute(self, event: VehicleEvent) -> None:

        if event.data.role not in vehicle_role_options:
            return

        previous_self_event_data = (
            self.vehicle_cache.get_latest_vehicle_event_data_for_vehicle_id(
                event.data.raw_vehicle_id
            )
        )

        if not previous_self_event_data:
            logger.warn(
                f"Could not retrieve event data from vehicle {event.data.raw_vehicle_id}"
            )
            return

        omnibus_details = self.vehicle_cache.get_omnibus_details()
        omnibus_infos = event.data.omnibus
        previous_omnibus_detail = get_matching_omnibus_detail(
            event.data.raw_vehicle_id, omnibus_details
        )

        if not omnibus_infos:
            if not previous_omnibus_detail:
                return
            # This vehicle was involved in an omnibus, but is not anymore. Will decrement for previous balance.
            detail_of_just_dissociated_omnibus = previous_omnibus_detail
            balance_key_of_just_dissociated_omnibus = (
                detail_of_just_dissociated_omnibus.balance_kind
            )
            new_omnibus_balance = self.apply_delta(
                self.vehicle_cache.get_omnibus_balance(),
                {balance_key_of_just_dissociated_omnibus: -1},
            )
            self.vehicle_cache.remove_omnibus_given_vsav_id(
                detail_of_just_dissociated_omnibus.vsav_id
            )
            await self.publish_and_set_new_balance_in_cache(
                event.data, new_omnibus_balance
            )
            return

        previous_partner_event_data = (
            self.vehicle_cache.get_latest_vehicle_event_data_for_vehicle_id(
                omnibus_infos.partner_raw_vehicle_id
            )
        )

        # This vehicle is involved in an omnibus.
        omnibus_balance = self.vehicle_cache.get_omnibus_balance()
        if not previous_partner_event_data:
            logger.warn(
                f"Could not retrieve event data from vehicle {omnibus_infos.partner_raw_vehicle_id}"
            )
            return

        current_event_datas = infer_omnibus_datas(
            event.data, previous_partner_event_data
        )
        current_balance_key = infer_balance_key(current_event_datas)
        if current_balance_key:
            # This vehicle is involved in an omnibus
            if previous_omnibus_detail:
                # This vehicle was involved in an omnibus before : remove and decrement
                omnibus_balance = self.apply_delta(
                    omnibus_balance,
                    {previous_omnibus_detail.balance_kind: -1},
                )
                self.vehicle_cache.remove_omnibus_given_vsav_id(
                    previous_omnibus_detail.vsav_id
                )
            omnibus_balance = self.apply_delta(
                omnibus_balance,
                {current_balance_key: +1},
            )
            self.vehicle_cache.add_omnibus(
                OmnibusDetail(
                    vsav_id=current_event_datas.vsav_data.raw_vehicle_id,
                    pse_id=current_event_datas.pse_data.raw_vehicle_id,
                    balance_kind=current_balance_key,
                    area=current_event_datas.vsav_data.home_area,
                )
            )
            await self.publish_and_set_new_balance_in_cache(event.data, omnibus_balance)

    async def publish_and_set_new_balance_in_cache(
        self, event_data: VehicleEventData, omnibus_balance: OmnibusBalance
    ):
        omnibus_balance_changed_event_data = OmnibusBalanceChangedEventData(
            timestamp=event_data.timestamp,
            omnibus_balance=omnibus_balance,
            details=list(self.vehicle_cache.get_omnibus_details()),
        )
        await self._publish_and_save(omnibus_balance_changed_event_data)
        self.vehicle_cache.set_omnibus_balance(omnibus_balance)

    async def _publish_and_save(self, data: OmnibusBalanceChangedEventData):
        uuid = self.uuid.make()
        await self.domain_event_bus.publish(
            OmnibusBalanceChangedEvent(
                uuid=uuid,
                data=data,
            )
        )
        self.omnibus_repo.add(
            OmnibusBalanceChangedEventEntity(
                uuid=uuid,
                data=data,
            )
        )

    @staticmethod
    def apply_delta(
        omnibus_balance: OmnibusBalance,
        delta: Dict[OmnibusBalanceKind, int],
    ) -> OmnibusBalance:
        return OmnibusBalance(
            both_available=omnibus_balance.both_available
            + delta.get("both_available", 0),
            both_on_inter_san=omnibus_balance.both_on_inter_san
            + delta.get("both_on_inter_san", 0),
            vsav_on_inter_san=omnibus_balance.vsav_on_inter_san
            + delta.get("vsav_on_inter_san", 0),
            pse_on_inter_san=omnibus_balance.pse_on_inter_san
            + delta.get("pse_on_inter_san", 0),
            pse_on_inter_fire=omnibus_balance.pse_on_inter_fire
            + delta.get("pse_on_inter_fire", 0),
            only_pse_available=omnibus_balance.only_pse_available
            + delta.get("only_pse_available", 0),
        )
