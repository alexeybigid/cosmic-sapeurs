from typing import List

from cover_ops.domain.entities.event_entities import (
    OperationEventEntity,
    VehicleEventEntity,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)

from shared.helpers.logger import logger
from shared.sapeurs_events import (
    Event,
    operation_changed_status,
    vehicle_changed_status,
)


class TransfertEventsBatchToRepositories:
    def __init__(
        self,
        vehicle_events_repo: AbstractVehicleEventsRepository,
        operation_events_repo: AbstractOperationEventsRepository,
    ) -> None:
        self.vehicle_events_repo = vehicle_events_repo
        self.operation_events_repo = operation_events_repo

    def execute(self, events: List[Event]):
        logger.info(f"Transfering {len(events)} events to repositories")
        for event in events:
            topic = event.topic
            if topic == vehicle_changed_status:
                self.vehicle_events_repo.add(
                    VehicleEventEntity(data=event.data, uuid=event.uuid)
                )
            elif topic == operation_changed_status:
                self.operation_events_repo.add(
                    OperationEventEntity(data=event.data, uuid=event.uuid)
                )
            else:
                logger.warn(
                    f"\n TransfertEventsBatchToRepositories expects vehicle or operation event. Event #{event.uuid} had topic {topic}. Skipping !"
                )

        logger.info("Transfert events batch to repositories finished.")
