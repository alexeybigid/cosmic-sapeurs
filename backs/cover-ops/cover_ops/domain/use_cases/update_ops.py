from typing import Optional

from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.entities.event_entities import OngoingOpChangedEventEntity
from cover_ops.domain.ports.ongoing_ops_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.services.operation_event_service import OperationEventService

from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import OngoingOpChangedEvent, OperationEvent


class UpdateOps:
    def __init__(
        self,
        ongoing_op_events_repo: AbstractOngoingOpEventsRepository,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
        operation_event_service: OperationEventService,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.ongoing_op_events_repo = ongoing_op_events_repo
        self.uuid = uuid
        self.operation_event_service = operation_event_service
        # # TODO : it's not the role of UpdateOps !
        # if resync_cache_from_operations:
        #     resync_cache_from_operations.execute()

    async def execute(self, event: OperationEvent) -> None:
        """Store event in repository

        Args:
            event (OperationEvent): event published on operation status change
        """
        ongoing_op_changed_event_data = self.operation_event_service.process_event(
            event.data
        )
        if ongoing_op_changed_event_data is None:
            return
        ongoing_ops_changed_event_entity = OngoingOpChangedEventEntity(
            uuid=self.uuid.make(), data=ongoing_op_changed_event_data
        )
        self.ongoing_op_events_repo.add(ongoing_ops_changed_event_entity)
        ongoing_ops_changed_event = OngoingOpChangedEvent(
            uuid=ongoing_ops_changed_event_entity.uuid,
            data=ongoing_op_changed_event_data,
        )
        await self.domain_event_bus.publish(ongoing_ops_changed_event)
