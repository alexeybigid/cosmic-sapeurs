from typing import List

from cover_ops.domain.ports.ongoing_ops_events_repository import (
    AbstractOngoingOpEventsRepository,
)

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationCause


class GetOps:
    def __init__(
        self, ongoing_op_events_repo: AbstractOngoingOpEventsRepository
    ) -> None:
        self.ongoing_op_events_repo = ongoing_op_events_repo

    def execute(self, hours: int) -> List[OngoingOpChangedEventData]:
        ongoing_ops_events_data_from_date = [
            event.data
            for event in self.ongoing_op_events_repo.get_ongoing_ops_since(hours)
        ]
        causes_to_fetch: List[OperationCause] = ["fire", "victim"]
        latest_ops_event_datas_per_area = []
        for area in bspp_area_options:
            for cause in causes_to_fetch:
                latest_event_data = (
                    self.ongoing_op_events_repo.get_latest_event_data_by_area_by_cause(
                        area, cause
                    )
                )
                if (
                    latest_event_data
                    and latest_event_data not in ongoing_ops_events_data_from_date
                ):
                    latest_ops_event_datas_per_area.append(latest_event_data)

        return latest_ops_event_datas_per_area + ongoing_ops_events_data_from_date
