from typing import List, Optional

from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.vehicle_event_data import vehicle_role_options


class GetCover:
    def __init__(
        self,
        availability_events_repo: AbstractAvailabilityEventsRepository,
        vehicle_cache: AbstractVehicleCache,
    ) -> None:
        self.availability_events_repo = availability_events_repo
        self.vehicle_cache = vehicle_cache

    def execute(self, hours: Optional[int]) -> List[AvailabilityChangedEventData]:

        availability_events_data_from_date = [
            event.data
            for event in self.availability_events_repo.get_availability_since(hours)
        ]

        if hours is None:  # In this case, we return ALL events
            return availability_events_data_from_date

        latest_availability_event_data_by_vehicle_from_repo = (
            self.availability_events_repo.list_latest_availability_event_data_by_vehicle_from_repo()
        )
        return sorted(
            availability_events_data_from_date
            + latest_availability_event_data_by_vehicle_from_repo,
            key=lambda event_data: event_data.timestamp,
        )
        # TODO : We should indeed filter duplicated events data, but the following implementation (in-memory) took too much time.
        # unique_availability_events_data = get_unique_availability_events_data(
        #     availability_events_data_from_date
        #     + latest_availability_event_data_by_vehicle_from_repo
        # )

        # return sorted(
        #     unique_availability_events_data, key=lambda event_data: event_data.timestamp
        # )


def partialy_equal(
    data_1: AvailabilityChangedEventData, data_2: AvailabilityChangedEventData
) -> bool:
    return (
        data_1.timestamp == data_2.timestamp
        and data_1.raw_vehicle_id == data_2.raw_vehicle_id
        and data_1.role == data_2.role
        and data_1.home_area == data_2.home_area
    )


def get_unique_availability_events_data(
    availability_events_data: List[AvailabilityChangedEventData],
) -> List[AvailabilityChangedEventData]:
    unique_availability_events_data = []
    for event_data in availability_events_data:
        if not any(
            partialy_equal(event_data, o) for o in unique_availability_events_data
        ):
            unique_availability_events_data.append(event_data)
    return unique_availability_events_data
