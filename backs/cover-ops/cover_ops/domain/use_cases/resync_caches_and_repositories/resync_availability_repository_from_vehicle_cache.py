from typing import List, Tuple

from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    AvailabilityStoredInfoByAreaByRole,
    AvailabilityStoredInfoByRole,
    VehicleEventDataById,
)

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import (
    Availability,
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.vehicle_event_data import vehicle_role_options
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid


class ResyncAvailabilityRepositoryFromVehicleCache:
    def __init__(
        self,
        availability_events_repo: AbstractAvailabilityEventsRepository,
        vehicle_cache: AbstractVehicleCache,
        uuid: AbstractUuid,
    ) -> None:
        self.availability_events_repo = availability_events_repo
        self.vehicle_cache = vehicle_cache
        self.uuid = uuid

    def execute(self):
        logger.info("Resync availability repository from vehicle cache.")
        # latest_vehicle_event_data_by_vehicle_id = (
        #     self.vehicle_cache.get_latest_vehicle_event_data_by_vehicle_id_deprecated()
        # )
        availability_events_data = self._list_availability_changed_event_data()
        self.add_availability_to_repository(availability_events_data)

    def add_availability_to_repository(
        self, availability_events_data: List[AvailabilityChangedEventData]
    ):
        for availability_event_data in availability_events_data:
            self.availability_events_repo.add(
                AvailabilityChangedEventEntity(
                    data=availability_event_data,
                    uuid=self.uuid.make(),
                )
            )

    def _list_availability_changed_event_data(
        self,
    ) -> List[AvailabilityChangedEventData]:
        """
        this method uses cache and should only be use for resync (not for listing)
        """
        list_availability_changed_event_data: List[AvailabilityChangedEventData] = []

        for role in vehicle_role_options:
            availability_info_for_bspp_for_role = (
                self.vehicle_cache.get_bspp_availability_stored_infos_for_role(role)
            )
            for area in bspp_area_options:
                availability_info_for_area_for_role = (
                    self.vehicle_cache.get_availability_stored_infos_for_area_for_role(
                        area, role
                    )
                )

                latest_event_data_for_role_for_area = (
                    self.vehicle_cache.get_latest_vehicle_event_datas_for_area_for_role(
                        area, role
                    )
                )

                for latest_event_data in latest_event_data_for_role_for_area:
                    if latest_event_data is not None:
                        availability_changed_event_data = AvailabilityChangedEventData(
                            bspp_availability=availability_info_for_bspp_for_role.availability,
                            area_availability=availability_info_for_area_for_role.availability,
                            home_area=area,
                            role=role,
                            timestamp=latest_event_data.timestamp,
                            raw_vehicle_id=latest_event_data.raw_vehicle_id,
                            latest_event_data=latest_event_data,
                            previous_raw_operation_id=None,
                        )
                        list_availability_changed_event_data.append(
                            availability_changed_event_data
                        )
        return list_availability_changed_event_data
