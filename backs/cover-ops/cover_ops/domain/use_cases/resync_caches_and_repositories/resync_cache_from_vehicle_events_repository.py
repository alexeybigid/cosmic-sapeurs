from collections import defaultdict
from itertools import chain
from typing import Tuple

from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
    AvailabilityStoredInfoByAreaByRole,
    AvailabilityStoredInfoByRole,
    AvailabilityStoredInfos,
    VehicleEventDataById,
)
from cover_ops.domain.use_cases.update_omnibus import OmnibusDatas, infer_balance_key
from cover_ops.helpers.make_typed_record import make_typed_record

from shared.data_transfert_objects.area import bspp_area_options
from shared.data_transfert_objects.availability_changed_event_data import Availability
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusDetail,
)
from shared.data_transfert_objects.vehicle_event_data import (
    VehicleRole,
    vehicle_role_options,
)
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid

vsav_omni: VehicleRole = "vsav_omni"


class ResyncCacheFromVehicleEventsRepository:
    def __init__(
        self,
        vehicle_events_repo: AbstractVehicleEventsRepository,
        vehicle_cache: AbstractVehicleCache,
        uuid: AbstractUuid,
    ) -> None:
        self.vehicle_events_repo = vehicle_events_repo
        self.vehicle_cache = vehicle_cache
        self.uuid = uuid

    def execute(self):
        logger.info("Resync cache from vehicle events repository started.")
        self.vehicle_cache.initialize_cache()
        (
            bspp_availability_info_by_role,
            availability_info_by_area_by_role,
        ) = self._resync_availability_by_area_and_bspp_by_role()
        latest_vehicle_event_data_by_vehicle_id = (
            self._resync_latest_vehicle_event_data_by_vehicle_id()
        )
        omnibus_balance = self._resync_omnibus_balance(
            latest_vehicle_event_data_by_vehicle_id
        )
        self._set_cache(
            latest_vehicle_event_data_by_vehicle_id,
            bspp_availability_info_by_role,
            availability_info_by_area_by_role,
            omnibus_balance,
        )
        logger.info("Resync cache from vehicle events repository finished.")

    def _resync_latest_vehicle_event_data_by_vehicle_id(
        self,
    ) -> VehicleEventDataById:
        latest_events_by_raw_vehicle_id = (
            self.vehicle_events_repo.get_latest_event_by_raw_vehicle_id()
        )
        latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById = {
            raw_vehicle_id: latest_event.data
            for (
                raw_vehicle_id,
                latest_event,
            ) in latest_events_by_raw_vehicle_id.items()
        }
        return latest_vehicle_event_data_by_vehicle_id

    def _resync_omnibus_balance(
        self, latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById
    ) -> OmnibusBalance:
        latest_vsav_omnibus_vehicle_event_data = [
            event_data
            for event_data in latest_vehicle_event_data_by_vehicle_id.values()
            if event_data.role == vsav_omni
        ]
        logger.info(
            f"[ResyncCacheFromVehicleEventsRepository._resync_omnibus_balance] Found {len(latest_vsav_omnibus_vehicle_event_data)} omnibus couples to resync from. "
        )
        omnibus_balance_values = defaultdict(lambda: 0)
        for vsav_omni_data in latest_vsav_omnibus_vehicle_event_data:
            pse_omni_data = latest_vehicle_event_data_by_vehicle_id.get(
                vsav_omni_data.omnibus.partner_raw_vehicle_id
            )
            if not pse_omni_data:
                logger.warn(
                    f"[ResyncCacheFromVehicleEventsRepository._resync_omnibus_balance] Could not find latest vehicle event data for {vsav_omni_data.omnibus.partner_raw_vehicle_id} "
                )
            else:
                omnibus_balance_key = infer_balance_key(
                    OmnibusDatas(vsav_data=vsav_omni_data, pse_data=pse_omni_data)
                )
                if omnibus_balance_key:
                    omnibus_balance_values[omnibus_balance_key] += 1
                    self.vehicle_cache.add_omnibus(
                        OmnibusDetail(
                            vsav_id=vsav_omni_data.raw_vehicle_id,
                            pse_id=pse_omni_data.raw_vehicle_id,
                            balance_kind=omnibus_balance_key,
                            area=vsav_omni_data.home_area,
                        )
                    )
                else:
                    logger.warn(
                        f"[ResyncCacheFromVehicleEventsRepository] Dangling state during omnibus resync : vsav #{vsav_omni_data.raw_vehicle_id} was {vsav_omni_data.availability_kind} ({vsav_omni_data.raw_status}) ; pse #{pse_omni_data.raw_vehicle_id} was {pse_omni_data.availability_kind} ({pse_omni_data.raw_status}) with role {pse_omni_data.role}"
                    )
        return OmnibusBalance(**omnibus_balance_values)

    def _resync_availability_by_area_and_bspp_by_role(
        self,
    ) -> Tuple[AvailabilityStoredInfoByRole, AvailabilityStoredInfoByAreaByRole]:
        (
            events_by_availability_by_area_by_role,
            events_by_availability_by_role,
        ) = (
            self.vehicle_events_repo.list_latest_events_by_availability_by_role_by_area_and_for_bspp()
        )

        bspp_availability_info_by_role: AvailabilityStoredInfoByRole = {}
        availability_info_by_area_by_role = make_typed_record(
            bspp_area_options,
            lambda: make_typed_record(
                vehicle_role_options,
                lambda: AvailabilityStoredInfos(availability=Availability()),
            ),
        )

        for role in vehicle_role_options:
            events_by_availability_for_role = events_by_availability_by_role[role]
            bspp_availability_info_by_role[role] = AvailabilityStoredInfos(
                availability=Availability(
                    available=len(events_by_availability_for_role["available"]),
                    unavailable=len(events_by_availability_for_role["unavailable"]),
                    in_service=len(events_by_availability_for_role["in_service"]),
                    recoverable=len(events_by_availability_for_role["recoverable"]),
                ),
            )
            for area in bspp_area_options:
                events_by_availability_for_area_for_role = (
                    events_by_availability_by_area_by_role[area][role]
                )

                all_events_for_area_for_role = list(
                    chain.from_iterable(
                        events_by_availability_for_area_for_role.values()
                    )
                )

                if all_events_for_area_for_role:
                    latest_event_for_area_for_role = sorted(
                        all_events_for_area_for_role,
                        key=lambda event: event.data.timestamp,
                    )[-1]

                    availability_info_by_area_by_role[area][
                        role
                    ] = AvailabilityStoredInfos(
                        availability=Availability(
                            available=len(
                                events_by_availability_for_area_for_role["available"]
                            ),
                            unavailable=len(
                                events_by_availability_for_area_for_role["unavailable"]
                            ),
                            in_service=len(
                                events_by_availability_for_area_for_role["in_service"]
                            ),
                            recoverable=len(
                                events_by_availability_for_area_for_role["recoverable"]
                            ),
                        ),
                        latest_event_data=latest_event_for_area_for_role.data,
                    )
        return bspp_availability_info_by_role, availability_info_by_area_by_role

    def _set_cache(
        self,
        latest_vehicle_event_data_by_vehicle_id: VehicleEventDataById,
        bspp_availability_info_by_role: AvailabilityStoredInfoByRole,
        availability_info_by_area_by_role: AvailabilityStoredInfoByAreaByRole,
        omnibus_balance: OmnibusBalance,
    ):
        for event_data in latest_vehicle_event_data_by_vehicle_id.values():
            self.vehicle_cache.set_latest_vehicle_event_data_for_vehicle_id(event_data)

        self.vehicle_cache.set_bspp_availability_stored_infos_for_roles(
            bspp_availability_info_by_role
        )
        self.vehicle_cache.set_availability_stored_infos_for_areas_for_roles(
            availability_info_by_area_by_role
        )
        self.vehicle_cache.set_omnibus_balance(omnibus_balance)
