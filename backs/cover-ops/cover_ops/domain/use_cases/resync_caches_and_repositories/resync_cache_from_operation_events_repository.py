from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.services.operation_event_service import OperationEventService

from shared.helpers.logger import logger


class ResyncCacheFromOperationEventsRepository:
    def __init__(
        self,
        operation_events_repo: AbstractOperationEventsRepository,
        operation_event_service: OperationEventService,
    ) -> None:
        self.operation_events_repo = operation_events_repo
        self.operation_event_service = operation_event_service

    def execute(self):
        logger.info("Resync cache from operation events repository started.")
        self.operation_event_service.operation_cache.clear_cache()
        operation_events = (
            self.operation_events_repo.get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status()
        )
        for operation_event in operation_events:
            self.operation_event_service.process_event(
                operation_event.data, is_resync=True
            )
        logger.info("Resync cache from operation events repository finished.")
