from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
)
from cover_ops.domain.ports.ongoing_ops_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_cache import AbstractOperationCache
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from cover_ops.domain.services.operation_event_service import OperationEventService
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_availability_repository_from_vehicle_cache import (
    ResyncAvailabilityRepositoryFromVehicleCache,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_operation_events_repository import (
    ResyncCacheFromOperationEventsRepository,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_cache_from_vehicle_events_repository import (
    ResyncCacheFromVehicleEventsRepository,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_ongoing_ops_repository_from_operation_cache import (
    ResyncOngoingOpsRepositoryFromOperationCache,
)

from shared.helpers.uuid import AbstractUuid


class ResyncCachesAndRepositories:
    def __init__(
        self,
        vehicle_cache: AbstractVehicleCache,
        operation_cache: AbstractOperationCache,
        vehicle_events_repo: AbstractVehicleEventsRepository,
        operation_events_repo: AbstractOperationEventsRepository,
        availability_events_repo: AbstractAvailabilityEventsRepository,
        ongoing_op_events_repo: AbstractOngoingOpEventsRepository,
        operation_event_service: OperationEventService,
        uuid: AbstractUuid,
    ) -> None:
        self.resync_cache_from_vehicle_events_repository = (
            ResyncCacheFromVehicleEventsRepository(
                vehicle_events_repo=vehicle_events_repo,
                vehicle_cache=vehicle_cache,
                uuid=uuid,
            )
        )
        self.resync_cache_from_operation_events_repository = (
            ResyncCacheFromOperationEventsRepository(
                operation_events_repo=operation_events_repo,
                operation_event_service=operation_event_service,
            )
        )
        self.resync_availability_repository_from_vehicle_cache = (
            ResyncAvailabilityRepositoryFromVehicleCache(
                uuid=uuid,
                vehicle_cache=vehicle_cache,
                availability_events_repo=availability_events_repo,
            )
        )
        self.resync_ongoing_ops_repository_from_operation_cache = (
            ResyncOngoingOpsRepositoryFromOperationCache(
                uuid=uuid,
                operation_cache=operation_cache,
                ongoing_op_events_repo=ongoing_op_events_repo,
            )
        )

    def execute(self):
        self.resync_cache_from_vehicle_events_repository.execute()
        self.resync_cache_from_operation_events_repository.execute()
        self.resync_availability_repository_from_vehicle_cache.execute()
        self.resync_ongoing_ops_repository_from_operation_cache.execute()
