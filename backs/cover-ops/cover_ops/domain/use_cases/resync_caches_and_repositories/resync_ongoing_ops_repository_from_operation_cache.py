from cover_ops.domain.entities.event_entities import OngoingOpChangedEventEntity
from cover_ops.domain.ports.ongoing_ops_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_cache import AbstractOperationCache

from shared.helpers.uuid import AbstractUuid


class ResyncOngoingOpsRepositoryFromOperationCache:
    def __init__(
        self,
        ongoing_op_events_repo: AbstractOngoingOpEventsRepository,
        operation_cache: AbstractOperationCache,
        uuid: AbstractUuid,
    ) -> None:
        self.ongoing_op_events_repo = ongoing_op_events_repo
        self.operation_cache = operation_cache
        self.uuid = uuid

    def execute(self):
        for (
            operation_stored_infos
        ) in self.operation_cache.get_all_operation_stored_infos():
            data = operation_stored_infos.latest_ongoing_op_changed_event_data
            if data:
                self.ongoing_op_events_repo.add(
                    OngoingOpChangedEventEntity(
                        data=data,
                        uuid=self.uuid.make(),
                    )
                )
