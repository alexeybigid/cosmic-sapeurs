from typing import TypeVar, Union

from cover_ops.domain.entities.event_entities import (
    OperationEventEntity,
    VehicleEventEntity,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)

from shared.cover_ops_topics import (
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
)
from shared.sapeurs_events import OperationEvent, VehicleEvent

InputVehicleEventType = TypeVar("InputVehicleEventType")
InputOperationEventType = TypeVar("InputOperationEventType")

operation_changed_status: TopicOperationChangedStatus = "operation_changed_status"
vehicle_changed_status: TopicVehicleChangedStatus = "vehicle_changed_status"


class AddEventsToRepositories:
    def __init__(
        self,
        operation_events_repo: AbstractOperationEventsRepository,
        vehicle_events_repo: AbstractVehicleEventsRepository,
    ):
        self.operation_events_repo = operation_events_repo
        self.vehicle_events_repo = vehicle_events_repo

    async def execute(self, event: Union[VehicleEvent, OperationEvent]):
        if isinstance(event, VehicleEvent):
            self.vehicle_events_repo.add(
                VehicleEventEntity(data=event.data, uuid=event.uuid)
            )
        elif isinstance(event, OperationEvent):
            self.operation_events_repo.add(
                OperationEventEntity(data=event.data, uuid=event.uuid)
            )
