from typing import Dict, Optional, Tuple

from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.ports.operation_cache import AbstractOperationCache
from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import VehicleEventDataById

from shared.data_transfert_objects.operation_event_data import (
    OperationEventData,
    OperationStatus,
    RawOperationId,
)
from shared.helpers.date import DateStr
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import AvailabilityChangedEvent, OperationEvent


class NotifyWhenVehicleAvailabilityChangedAffectsOperation:
    def __init__(
        self,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
        vehicle_cache: AbstractVehicleCache,
        operation_cache: AbstractOperationCache,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.uuid = uuid
        self.vehicle_cache = vehicle_cache
        self.operation_cache = operation_cache

    async def execute(
        self, availability_event: AvailabilityChangedEvent = None
    ) -> None:
        vehicle_event_data = availability_event.data.latest_event_data
        previous_raw_operation_id = availability_event.data.previous_raw_operation_id

        raw_operation_id = vehicle_event_data.raw_operation_id
        timestamp = vehicle_event_data.timestamp

        operations_notified_from_vehicle_changes = (
            self.operation_cache.get_operations_notified_from_vehicle_changes()
        )
        # previous
        if (
            previous_raw_operation_id is not None
            and previous_raw_operation_id != raw_operation_id
            and self.are_all_vehicle_released(
                previous_raw_operation_id,
            )
        ):
            if previous_raw_operation_id in operations_notified_from_vehicle_changes:
                operations_notified_from_vehicle_changes.remove(
                    previous_raw_operation_id
                )
                await self.publish_operation_event(
                    timestamp=timestamp,
                    raw_operation_id=previous_raw_operation_id,
                    status="all_vehicles_released",
                )

        if raw_operation_id is None:
            return

        # current
        if raw_operation_id not in operations_notified_from_vehicle_changes:
            if vehicle_event_data.availability_kind == "in_service":
                operations_notified_from_vehicle_changes.add(raw_operation_id)
                await self.publish_operation_event(
                    timestamp=timestamp,
                    raw_operation_id=raw_operation_id,
                    status="first_vehicle_affected",
                )

        elif self.are_all_vehicle_released(raw_operation_id):
            operations_notified_from_vehicle_changes.remove(raw_operation_id)
            await self.publish_operation_event(
                timestamp=timestamp,
                raw_operation_id=raw_operation_id,
                status="all_vehicles_released",
            )
        self.operation_cache.set_operations_notified_from_vehicle_changes(
            operations_notified_from_vehicle_changes
        )

    def are_all_vehicle_released(
        self,
        raw_operation_id: RawOperationId,
    ) -> bool:
        affected_vehicle_availability_kind = [
            vehicle_event_data.availability_kind
            for vehicle_event_data in self.vehicle_cache.get_vehicle_event_data_for_operation_id(
                raw_operation_id=raw_operation_id
            )
        ]
        return all(
            availability_kind != "in_service"
            for availability_kind in affected_vehicle_availability_kind
        )

    async def publish_operation_event(
        self,
        timestamp: DateStr,
        raw_operation_id: RawOperationId,
        status: OperationStatus,
    ):
        operation_changed_status_event = OperationEvent(
            uuid=self.uuid.make(),
            data=OperationEventData(
                address=None,
                address_area=None,
                cause=None,
                raw_cause=None,
                procedure=None,
                timestamp=timestamp,
                raw_operation_id=raw_operation_id,
                status=status,
            ),
        )
        await self.domain_event_bus.publish(operation_changed_status_event)
