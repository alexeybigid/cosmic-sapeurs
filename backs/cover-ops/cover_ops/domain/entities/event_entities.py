from dataclasses import dataclass

from dataclasses_jsonschema import JsonSchemaMixin

from shared.cover_ops_topics import (
    TopicAvailabilityChanged,
    TopicOmnibusBalanceChanged,
    TopicOngoingOpChanged,
    TopicOperationChangedStatus,
    TopicVehicleChangedStatus,
)
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import OperationEventData
from shared.data_transfert_objects.vehicle_event_data import VehicleEventData
from shared.events.event_entity import EventEntity
from shared.marshmallow_validate import with_from_dict_classmethod


@with_from_dict_classmethod()
@dataclass
class VehicleEventEntity(EventEntity[VehicleEventData]):
    # for IDE happyness
    data: VehicleEventData
    topic: TopicVehicleChangedStatus = "vehicle_changed_status"


@with_from_dict_classmethod()
@dataclass
class AvailabilityChangedEventEntity(
    JsonSchemaMixin, EventEntity[AvailabilityChangedEventData]
):
    # for IDE happyness
    data: AvailabilityChangedEventData
    topic: TopicAvailabilityChanged = "availabilityChanged"


@with_from_dict_classmethod()
@dataclass
class OperationEventEntity(EventEntity[OperationEventData]):
    # for IDE happyness
    data: OperationEventData
    topic: TopicOperationChangedStatus = "operation_changed_status"


@with_from_dict_classmethod()
@dataclass
class OngoingOpChangedEventEntity(
    JsonSchemaMixin, EventEntity[OngoingOpChangedEventData]
):
    # for IDE happyness
    data: OngoingOpChangedEventData
    topic: TopicOngoingOpChanged = "ongoingOpChanged"


@with_from_dict_classmethod()
@dataclass
class OmnibusBalanceChangedEventEntity(
    JsonSchemaMixin, EventEntity[OmnibusBalanceChangedEventData]
):
    data: OmnibusBalanceChangedEventData
    topic: TopicOmnibusBalanceChanged = "omnibusBalanceChanged"
