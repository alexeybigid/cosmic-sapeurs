from typing import Optional

from cover_ops.domain.ports.operation_cache import (
    AbstractOperationCache,
    OperationStoredInfos,
)
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
    LatestOperationInfos,
)

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    OperationEventData,
    OperationStatus,
    RawOperationId,
    operation_has_all_vehicles_released,
    operation_has_first_affected_vehicle,
    operation_just_opened,
)
from shared.helpers.date import DateStr
from shared.helpers.decorators import timeit
from shared.helpers.logger import logger


def increment_ongoing_ops(
    previous_area_ongoing_ops: int,
    previous_bspp_ongoing_ops: int,
    status: OperationStatus,
    cause: OperationCause,
    address_area: Area,
    raw_operation_id: RawOperationId,
    timestamp: DateStr,
) -> OngoingOpChangedEventData:
    area_ongoing_ops = previous_area_ongoing_ops + 1
    bspp_ongoing_ops = previous_bspp_ongoing_ops + 1
    return OngoingOpChangedEventData(
        bspp_ongoing_ops=bspp_ongoing_ops,
        area_ongoing_ops=area_ongoing_ops,
        address_area=address_area,
        cause=cause,
        raw_operation_id=raw_operation_id,
        status=status,
        timestamp=timestamp,
    )


def decrement_ongoing_ops(
    previous_area_ongoing_ops: int,
    previous_bspp_ongoing_ops: int,
    status: OperationStatus,
    cause: OperationCause,
    address_area: Area,
    raw_operation_id: RawOperationId,
    timestamp: DateStr,
) -> OngoingOpChangedEventData:
    area_ongoing_ops = previous_area_ongoing_ops - 1
    bspp_ongoing_ops = previous_bspp_ongoing_ops - 1

    return OngoingOpChangedEventData(
        bspp_ongoing_ops=bspp_ongoing_ops,
        area_ongoing_ops=area_ongoing_ops,
        address_area=address_area,
        cause=cause,
        raw_operation_id=raw_operation_id,
        status=status,
        timestamp=timestamp,
    )


def on_first_vehicle_affected_or_all_vehicles_released_estimate_ongoing_ops_changed_event_data(
    previous_area_ongoing_ops: int,
    previous_bspp_ongoing_ops: int,
    latest_status: OperationStatus,
    current_status: OperationStatus,
    cause: OperationCause,
    address_area: Area,
    raw_operation_id: RawOperationId,
    timestamp: DateStr,
) -> Optional[OngoingOpChangedEventData]:
    if current_status == operation_has_first_affected_vehicle:
        return increment_ongoing_ops(
            previous_area_ongoing_ops=previous_area_ongoing_ops,
            previous_bspp_ongoing_ops=previous_bspp_ongoing_ops,
            address_area=address_area,
            cause=cause,
            raw_operation_id=raw_operation_id,
            status=current_status,
            timestamp=timestamp,
        )

    else:  # all_vehicle_released
        if latest_status == current_status:
            # should not happen ...
            logger.warn(
                f"Something weird happen with operation #{raw_operation_id}: Received event {current_status} but latest status was {latest_status}. Skipping"
            )
            return
        return decrement_ongoing_ops(
            previous_area_ongoing_ops=previous_area_ongoing_ops,
            previous_bspp_ongoing_ops=previous_bspp_ongoing_ops,
            status=current_status,
            cause=cause,
            address_area=address_area,
            raw_operation_id=raw_operation_id,
            timestamp=timestamp,
        )


class OperationEventService:
    def __init__(
        self,
        operation_events_repo: AbstractOperationEventsRepository,
        operation_cache: AbstractOperationCache,
    ):
        self.operation_events_repo = operation_events_repo
        self.operation_cache = operation_cache

    @timeit("process operation event", min_duration=50)
    def process_event(
        self, event_data: OperationEventData, is_resync: bool = False
    ) -> Optional[OngoingOpChangedEventData]:
        current_status = event_data.status
        if current_status not in [
            operation_just_opened,
            operation_has_first_affected_vehicle,
            operation_has_all_vehicles_released,
        ]:
            return

        raw_operation_id = event_data.raw_operation_id

        if current_status == operation_just_opened:
            address_area = event_data.address_area
            cause = event_data.cause
            if cause is None or address_area is None:
                logger.warn(
                    f"Cause or address_area was None for operation #{raw_operation_id} "
                    f"with status {current_status}: cause: {cause}, address_area: {address_area}. Keeping in cache."
                )
                return

            operation_stored_infos = (
                self.operation_cache.get_operation_stored_infos_for_operation_id(
                    raw_operation_id
                )
            )

            if operation_stored_infos:
                previous_area_ongoing_ops = (
                    self.operation_cache.get_ongoing_ops_count_for_area_for_cause(
                        address_area, cause
                    )
                )
                previous_bspp_ongoing_ops = sum(
                    self.operation_cache.get_ongoing_ops_counts_for_cause(cause)
                )
                logger.warn(
                    "Opening event was not received first. Might increment lately."
                )
                ongoing_op_changed_event_data = increment_ongoing_ops(
                    previous_area_ongoing_ops=previous_area_ongoing_ops,
                    previous_bspp_ongoing_ops=previous_bspp_ongoing_ops,
                    address_area=address_area,
                    cause=cause,
                    raw_operation_id=raw_operation_id,
                    status=current_status,
                    timestamp=event_data.timestamp,
                )
            else:
                ongoing_op_changed_event_data = OngoingOpChangedEventData(
                    bspp_ongoing_ops=sum(
                        self.operation_cache.get_ongoing_ops_counts_for_cause(cause)
                    ),
                    area_ongoing_ops=self.operation_cache.get_ongoing_ops_count_for_area_for_cause(
                        address_area, cause
                    ),
                    address_area=address_area,
                    cause=cause,
                    raw_operation_id=raw_operation_id,
                    status=current_status,
                    timestamp=event_data.timestamp,
                )

                self.operation_cache.set_operation_stored_infos_for_operation_id(
                    raw_operation_id,
                    OperationStoredInfos(
                        status=current_status,
                        latest_ongoing_op_changed_event_data=ongoing_op_changed_event_data,
                    ),
                )

        else:  # first_vehicle_affected | all_vehicles_released
            latest_operation_info = self._get_latest_infos_from_operation_id(
                raw_operation_id,
            )

            if latest_operation_info is None:
                logger.warn(
                    f"Received status {current_status} for #{raw_operation_id} "
                    f"but no opening infos. Handle cache and return. "
                )
                if current_status == operation_has_first_affected_vehicle:
                    self.operation_cache.set_operation_stored_infos_for_operation_id(
                        raw_operation_id,
                        OperationStoredInfos(
                            status=current_status,
                            latest_ongoing_op_changed_event_data=None,
                        ),
                    )
                else:  # all vehicle released
                    self.operation_cache.remove_operation_stored_infos_for_operation_id(
                        raw_operation_id
                    )
                return

            # at this point, if current_status in [operation_has_first_affected_vehicle, operation_has_all_vehicles_released],
            # we know latest_operation_info is defined
            cause = latest_operation_info.cause
            address_area = latest_operation_info.address_area
            latest_status = latest_operation_info.last_relevant_status

            if not is_resync and current_status == latest_status:
                return
            ongoing_op_changed_event_data = on_first_vehicle_affected_or_all_vehicles_released_estimate_ongoing_ops_changed_event_data(
                previous_area_ongoing_ops=(
                    self.operation_cache.get_ongoing_ops_count_for_area_for_cause(
                        address_area, cause
                    )
                ),
                previous_bspp_ongoing_ops=sum(
                    self.operation_cache.get_ongoing_ops_counts_for_cause(cause)
                ),
                latest_status=latest_status,
                current_status=current_status,
                cause=cause,
                address_area=address_area,
                raw_operation_id=raw_operation_id,
                timestamp=event_data.timestamp,
            )
            if ongoing_op_changed_event_data:
                self.on_first_vehicle_affected_or_all_vehicles_released_update_cache(
                    ongoing_op_changed_event_data=ongoing_op_changed_event_data,
                )

        return ongoing_op_changed_event_data

    def _get_latest_infos_from_operation_id(
        self,
        raw_operation_id: RawOperationId,
    ) -> Optional[LatestOperationInfos]:
        cache_operation_stored_infos = (
            self.operation_cache.get_operation_stored_infos_for_operation_id(
                raw_operation_id
            )
        )
        if cache_operation_stored_infos is not None:
            last_ongoing_op_changed_event_data = (
                cache_operation_stored_infos.latest_ongoing_op_changed_event_data
            )

            if last_ongoing_op_changed_event_data is None:
                return

            cause = last_ongoing_op_changed_event_data.cause
            address_area = last_ongoing_op_changed_event_data.address_area
            status = last_ongoing_op_changed_event_data.status

            # at this point, we ensured cause and address_area are not None
            assert cause is not None
            assert address_area is not None
            return LatestOperationInfos(
                cause=cause, address_area=address_area, last_relevant_status=status
            )
        else:

            return self.operation_events_repo.get_latest_operation_infos_for_id(
                raw_operation_id
            )

    def on_first_vehicle_affected_or_all_vehicles_released_update_cache(
        self,
        ongoing_op_changed_event_data: OngoingOpChangedEventData,
    ):
        raw_operation_id = ongoing_op_changed_event_data.raw_operation_id

        self.operation_cache.set_ongoing_ops_count_for_area_for_cause(
            ongoing_op_changed_event_data.address_area,
            ongoing_op_changed_event_data.cause,
            ongoing_op_changed_event_data.area_ongoing_ops,
        )

        if ongoing_op_changed_event_data.status == operation_has_first_affected_vehicle:
            self.operation_cache.set_operation_stored_infos_for_operation_id(
                raw_operation_id,
                OperationStoredInfos(
                    status=ongoing_op_changed_event_data.status,
                    latest_ongoing_op_changed_event_data=ongoing_op_changed_event_data,
                ),
            )
        else:
            self.operation_cache.remove_operation_stored_infos_for_operation_id(
                raw_operation_id
            )
