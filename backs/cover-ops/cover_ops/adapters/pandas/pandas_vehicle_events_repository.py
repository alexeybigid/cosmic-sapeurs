from pathlib import Path
from typing import Dict

from cover_ops.adapters.pandas.pandas_events_repo import PandasSapeursEventsRepository
from cover_ops.domain.entities.event_entities import VehicleEventEntity
from cover_ops.domain.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from pandas import notna

from shared.data_transfert_objects.vehicle_event_data import (
    OmnibusInfos,
    RawVehicleId,
    VehicleEventData,
)


class PandasVehicleEventsRepository(
    AbstractVehicleEventsRepository,
    PandasSapeursEventsRepository[VehicleEventData],
):
    def __init__(self, csv_path: Path):

        PandasSapeursEventsRepository.__init__(
            self, csv_path=csv_path, EntityClass=VehicleEventEntity
        )

    def get_latest_event_by_raw_vehicle_id(
        self,
    ) -> Dict[RawVehicleId, VehicleEventEntity]:
        latest_event_by_raw_vehicle_id: Dict[RawVehicleId, VehicleEventEntity] = {}

        if self.df_with_flatten_data.empty:
            return latest_event_by_raw_vehicle_id

        for raw_vehicle_id, latest_event_series in (
            self.df_with_flatten_data.groupby("raw_vehicle_id")
            .apply(lambda s: s.sort_values("timestamp").iloc[-1])
            .iterrows()
        ):

            partner_raw_vehicle_id = latest_event_series[
                "omnibus.partner_raw_vehicle_id"
            ]
            omnibus = (
                OmnibusInfos(partner_raw_vehicle_id)
                if notna(partner_raw_vehicle_id)
                else None
            )

            latest_event_by_raw_vehicle_id[raw_vehicle_id] = VehicleEventEntity(
                uuid=latest_event_series.uuid,
                data=VehicleEventData(
                    raw_vehicle_id=raw_vehicle_id,
                    raw_operation_id=latest_event_series.raw_operation_id,
                    home_area=latest_event_series.home_area,
                    role=latest_event_series.role,
                    status=latest_event_series.status,
                    raw_status=latest_event_series.raw_status,
                    timestamp=latest_event_series.timestamp,
                    vehicle_name=latest_event_series.vehicle_name,
                    omnibus=omnibus,
                ),
            )

        return latest_event_by_raw_vehicle_id
