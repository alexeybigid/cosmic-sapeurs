from dataclasses import asdict
from typing import Dict

import pandas as pd
from cover_ops.domain.entities.event_entities import (
    EventEntity,
    OperationEventEntity,
    VehicleEventEntity,
)

from shared.cover_ops_topics import CoverOpsDomainTopic

__all__ = ["event_entity_to_series", "series_to_entity"]

topic_to_entity: Dict[CoverOpsDomainTopic, EventEntity] = {
    "vehicle_changed_status": VehicleEventEntity,
    "operation_changed_status": OperationEventEntity,
}


def event_entity_to_flat_series(event_entity: EventEntity) -> pd.Series:
    flat_event_entity_dict = asdict(event_entity.data)
    flat_event_entity_dict["uuid"] = event_entity.uuid
    # TODO : where is it used ? No topic in the Series ...
    return pd.Series(flat_event_entity_dict)


def event_entity_to_series(event_entity: EventEntity) -> pd.Series:
    event_entity_dict = event_entity.as_dict()
    return pd.Series(event_entity_dict)
