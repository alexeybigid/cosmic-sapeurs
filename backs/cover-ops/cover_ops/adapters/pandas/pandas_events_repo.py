import json
import os
from dataclasses import asdict
from pathlib import Path
from typing import Generic, Optional, TypeVar

import pandas as pd
from cover_ops.helpers.mkdir_if_relevant import mkdir_if_relevant
from pandas.io.json import json_normalize

from shared.events.event_entity import EventEntity
from shared.helpers.csv import writerow
from shared.helpers.decorators import timeit
from shared.helpers.logger import logger
from shared.ports.abstract_events_repository import AbstractSapeursEventsRepository

GenericEventData = TypeVar("GenericEventData")
GenericEventTopic = TypeVar("GenericEventTopic")


class InvalidEventRepositoryDataFrame(Exception):
    pass


def json_normalize_df_data(df):
    if df.empty:
        return df[["uuid"]]
    df_flatten_data = json_normalize(df.data)
    df_flatten_data[["uuid"]] = df.uuid
    df_flatten_data[["topic"]] = df.topic
    return df_flatten_data


class PandasSapeursEventsRepository(
    AbstractSapeursEventsRepository, Generic[GenericEventData]
):
    df: pd.DataFrame
    df_with_flatten_data: pd.DataFrame

    def __init__(self, csv_path: Path, EntityClass):
        self.csv_path = csv_path
        self._columns = ["uuid", "topic", "data"]
        if os.path.isfile(self.csv_path):
            self._from_csv(csv_path)
        else:
            mkdir_if_relevant(self.csv_path.parent)
            writerow(self.csv_path, self._columns)
            self.df = pd.DataFrame(columns=self._columns)
        self.EntityClass = EntityClass
        self.df_with_flatten_data = json_normalize_df_data(self.df)

    def _to_csv(self):
        # TODO : check that df.data is of type dict
        df_copy = self.df.copy()
        df_copy.data = df_copy.data.map(json.dumps)
        df_copy.to_csv(self.csv_path, index=False)

    def _from_csv(self, csv_path):
        self.df = pd.read_csv(csv_path)  # type: ignore
        self._check_columns_are_valid(self.df)
        self._deserialize_df_data_column()

    def _deserialize_df_data_column(self):
        self.df.data = self.df.data.map(json.loads)

    @timeit("pandas_repo_add", min_duration=50)
    def _add(self, event_entity: EventEntity[GenericEventData]) -> None:
        event_as_dict = event_entity.as_dict()
        new_row = pd.Series(event_as_dict)[self._columns]
        self.df_with_flatten_data = self.df_with_flatten_data.append(
            json_normalize_df_data(new_row), ignore_index=True
        )
        writerow(
            self.csv_path,
            [
                event_entity.uuid,
                event_entity.topic,
                json.dumps(asdict(event_entity.data)),
            ],
        )

    def _match_uuid(self, uuid: str) -> Optional[EventEntity[GenericEventData]]:
        _matches = self.df_with_flatten_data[self.df_with_flatten_data.uuid == uuid]
        if not _matches.empty:
            _matches_as_dict = _matches.iloc[0].to_dict()  # type: ignore
            uuid = _matches_as_dict.pop("uuid")
            topic = _matches_as_dict.pop("topic")
            return self.EntityClass.from_dict(
                {"uuid": uuid, "topic": topic, "data": _matches_as_dict}
            )

    def _check_columns_are_valid(self, df):
        df_columns = df.columns.to_list()
        if not df_columns == self._columns:
            message = f"Cannot initialize a PandasEventsRepository with columns {df_columns}, expecting {self._columns}"
            logger.warn(message)
            raise InvalidEventRepositoryDataFrame(message)
