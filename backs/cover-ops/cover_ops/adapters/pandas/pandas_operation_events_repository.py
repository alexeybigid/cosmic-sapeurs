from pathlib import Path
from typing import List, Optional

from cover_ops.adapters.pandas.pandas_events_repo import PandasSapeursEventsRepository
from cover_ops.domain.entities.event_entities import OperationEventEntity
from cover_ops.domain.ports.operation_events_repository import (
    AbstractOperationEventsRepository,
)

from shared.data_transfert_objects.operation_event_data import (
    OperationEventData,
    OperationStatus,
    RawOperationId,
    operation_closed,
    operation_has_first_affected_vehicle,
    operation_just_opened,
    operation_relevant_status_options,
)
from shared.helpers.decorators import timeit


def filter_df_for_status_and_operation_id(df, raw_operation_id, statuses):
    return df[
        (df.raw_operation_id == raw_operation_id)
        & (df.status.isin(statuses) if statuses else True)
    ].sort_values("timestamp")


class PandasOperationEventsRepository(
    AbstractOperationEventsRepository,
    PandasSapeursEventsRepository[OperationEventData],
):
    def __init__(self, csv_path: Path):
        PandasSapeursEventsRepository.__init__(
            self,
            csv_path=csv_path,
            EntityClass=OperationEventEntity,
        )

    def get_events_for_id_with_status_in(
        self,
        raw_operation_id: RawOperationId,
        statuses: Optional[List[OperationStatus]] = None,
    ) -> List[OperationEventEntity]:
        if self.df_with_flatten_data.empty:
            return []

        data_from_this_operation_for_those_statuses = (
            filter_df_for_status_and_operation_id(
                self.df_with_flatten_data, raw_operation_id, statuses
            )
        )

        if data_from_this_operation_for_those_statuses.empty:
            return []

        return [
            OperationEventEntity(
                uuid=last_row.uuid,
                data=OperationEventData(
                    raw_operation_id=last_row.raw_operation_id,
                    status=last_row.status,
                    address=last_row.address,
                    address_area=last_row.address_area,
                    timestamp=last_row.timestamp,
                    cause=last_row.cause,
                    raw_cause=last_row.raw_cause,
                    procedure=last_row.procedure,
                ),
            )
            for _, last_row in data_from_this_operation_for_those_statuses.iterrows()
        ]

    def get_sorted_latest_ongoing_operation_event_with_relevant_and_closing_status(
        self,
    ) -> List[OperationEventEntity]:
        if self.df_with_flatten_data.empty:
            return []
        relevant_df_with_flatten_data = self.df_with_flatten_data[
            self.df_with_flatten_data.status.isin(
                [*operation_relevant_status_options, operation_closed]
            )
        ]
        ongoing_operation_latest_events: List[OperationEventEntity] = []
        for _, group in relevant_df_with_flatten_data.groupby("raw_operation_id"):
            operation_row = group.sort_values("timestamp").iloc[-1]
            if operation_row.status in [
                operation_just_opened,
                operation_has_first_affected_vehicle,
            ]:
                operation_event_entity = OperationEventEntity(
                    uuid=operation_row.uuid,
                    data=OperationEventData(
                        timestamp=operation_row.timestamp,
                        status=operation_row.status,
                        address_area=operation_row.address_area,
                        address=operation_row.address,
                        cause=operation_row.cause,
                        raw_cause=operation_row.raw_cause,
                        raw_operation_id=operation_row.raw_operation_id,
                        procedure=operation_row.procedure,
                    ),
                )
                ongoing_operation_latest_events.append(operation_event_entity)

        return list(
            sorted(
                ongoing_operation_latest_events,
                key=lambda event: event.data.timestamp,
            ),
        )
