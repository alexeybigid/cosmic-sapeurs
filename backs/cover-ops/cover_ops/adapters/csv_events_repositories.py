import csv
import json
import os
from dataclasses import asdict
from pathlib import Path
from typing import List, cast

from cover_ops.domain.entities.event_entities import (
    AvailabilityChangedEventEntity,
    OmnibusBalanceChangedEventEntity,
    OngoingOpChangedEventEntity,
)
from cover_ops.domain.ports.availability_events_repository import (
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.omnibus_events_repository import (
    InMemoryOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_ops_events_repository import (
    InMemoryOngoingOpEventsRepository,
)
from cover_ops.helpers.mkdir_if_relevant import mkdir_if_relevant

from shared.cover_ops_topics import (
    TopicAvailabilityChanged,
    TopicOmnibusBalanceChanged,
    TopicOngoingOpChanged,
)
from shared.data_transfert_objects.availability_changed_event_data import (
    AvailabilityChangedEventData,
)
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalanceChangedEventData,
)
from shared.data_transfert_objects.ongoing_ops_changed_event_data import (
    OngoingOpChangedEventData,
)
from shared.helpers.csv import writerow
from shared.helpers.logger import logger


class CsvAvailabilityEventsRepository(InMemoryAvailabilityEventsRepository):
    def __init__(self, csv_path: Path) -> None:
        super().__init__()
        self.csv_path = csv_path
        if os.path.isfile(self.csv_path):
            self._availability_events = self._from_csv()
        else:
            mkdir_if_relevant(self.csv_path.parent)
            writerow(self.csv_path, ["uuid", "topic", "data"])

    def _add(self, event_entity: AvailabilityChangedEventEntity):
        super()._add(event_entity)
        writerow(
            self.csv_path,
            [
                event_entity.uuid,
                event_entity.topic,
                json.dumps(asdict(event_entity.data)),
            ],
        )

    def _from_csv(self) -> List[AvailabilityChangedEventEntity]:
        events = []
        with self.csv_path.open("r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                topic = row["topic"]
                expected_topic: TopicAvailabilityChanged = "availabilityChanged"
                if topic == expected_topic:
                    deserialized_data = json.loads(row["data"])
                    data = AvailabilityChangedEventData.from_dict(deserialized_data)
                    events.append(
                        AvailabilityChangedEventEntity(
                            uuid=row["uuid"],
                            topic=cast(TopicAvailabilityChanged, topic),
                            data=data,
                        )
                    )
                else:
                    logger.warn(
                        f"Found row with wrong topic {topic} in csv : {self.csv_path}"
                    )
        return events


class CsvOngoingOpEventsRepository(InMemoryOngoingOpEventsRepository):
    def __init__(self, csv_path: Path) -> None:
        super().__init__()
        self.csv_path = csv_path
        if os.path.isfile(self.csv_path):
            self._ongoing_op_events = self._from_csv()
        else:
            writerow(self.csv_path, ["uuid", "topic", "data"])

    def _add(self, event_entity: OngoingOpChangedEventEntity):
        super()._add(event_entity)
        writerow(
            self.csv_path,
            [
                event_entity.uuid,
                event_entity.topic,
                json.dumps(asdict(event_entity.data)),
            ],
        )

    def _from_csv(self) -> List[OngoingOpChangedEventEntity]:
        events = []
        with self.csv_path.open("r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                topic = row["topic"]
                expected_topic: TopicOngoingOpChanged = "ongoingOpChanged"
                if topic == expected_topic:
                    deserialized_data = json.loads(row["data"])
                    events.append(
                        OngoingOpChangedEventEntity(
                            uuid=row["uuid"],
                            topic=cast(TopicOngoingOpChanged, row["topic"]),
                            data=OngoingOpChangedEventData.from_dict(deserialized_data),
                        )
                    )
                else:
                    logger.warn(
                        f"Found row with wrong topic {topic} in csv : {self.csv_path}"
                    )
        return events


class CsvOmnibusEventsRepository(InMemoryOmnibusEventsRepository):
    def __init__(self, csv_path: Path) -> None:
        super().__init__()
        self.csv_path = csv_path
        if os.path.isfile(self.csv_path):
            self._ongoing_op_events = self._from_csv()
        else:
            writerow(self.csv_path, ["uuid", "topic", "data"])

    def _add(self, event_entity: OmnibusBalanceChangedEventEntity):
        super()._add(event_entity)
        writerow(
            self.csv_path,
            [
                event_entity.uuid,
                event_entity.topic,
                json.dumps(asdict(event_entity.data)),
            ],
        )

    def _from_csv(self) -> List[OmnibusBalanceChangedEventEntity]:
        events = []
        with self.csv_path.open("r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                topic = row["topic"]
                expected_topic: TopicOmnibusBalanceChanged = "omnibusBalanceChanged"
                if topic == expected_topic:
                    deserialized_data = json.loads(row["data"])
                    events.append(
                        OmnibusBalanceChangedEventEntity(
                            uuid=row["uuid"],
                            topic=cast(TopicOmnibusBalanceChanged, row["topic"]),
                            data=OmnibusBalanceChangedEventData.from_dict(
                                deserialized_data
                            ),
                        )
                    )
                else:
                    logger.warn(
                        f"Found row with wrong topic {topic} in csv : {self.csv_path}"
                    )
        return events
