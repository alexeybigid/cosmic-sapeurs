import json
from typing import List, Optional, Set

from cover_ops.adapters.redis_cache.redis_utils import redis_get_and_loads
from cover_ops.domain.ports.operation_cache import (
    AbstractOperationCache,
    OperationStoredInfos,
)
from cover_ops.domain.ports.operation_events_repository import (
    OngoingOpsCountByCauseByArea,
)
from redis import Redis

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.operation_event_data import (
    OperationCause,
    RawOperationId,
)
from shared.helpers.dataclass import dataclass_to_dict, dumps_dataclass

operation_stored_infos_for_key_prefix = "operation_stored_infos_for"
operations_notified_from_vehicle_changes_key = (
    "operations_notified_from_vehicle_changes"
)


class RedisOperationCache(AbstractOperationCache):
    def __init__(self, redis_instance: Redis):
        self.r = redis_instance
        super().__init__()

    def set_operation_stored_infos_for_operation_id(
        self,
        raw_operation_id: RawOperationId,
        operation_stored_infos: OperationStoredInfos,
    ) -> None:
        self.r.set(
            self.make_operation_stored_infos_for_operation_id_key(raw_operation_id),
            dumps_dataclass(operation_stored_infos),
        )

    def get_operation_stored_infos_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> Optional[OperationStoredInfos]:
        redis_value = self.r.get(
            self.make_operation_stored_infos_for_operation_id_key(raw_operation_id),
        )
        if redis_value:
            return OperationStoredInfos.from_dict(json.loads(redis_value))

    def set_ongoing_ops_count_for_area_for_cause(
        self, area: Area, cause: OperationCause, count: int
    ):
        redis_key = self.make_ongoing_ops_count_for_area_for_cause_key(area, cause)
        self.r.set(
            redis_key,
            count,
        )
        self.r.sadd(f"cause:{cause}", redis_key)

    def get_ongoing_ops_count_for_area_for_cause(
        self, area: Area, cause: OperationCause
    ) -> int:
        redis_value = self.r.get(
            self.make_ongoing_ops_count_for_area_for_cause_key(area, cause),
        )
        return int(redis_value.decode()) if redis_value else 0

    def set_ongoing_ops_count_for_causes_for_areas(
        self, ongoing_ops_count_by_cause_by_area: OngoingOpsCountByCauseByArea
    ):
        for (
            cause,
            ongoing_ops_count_by_area,
        ) in ongoing_ops_count_by_cause_by_area.items():
            for area, ongoing_ops_count in ongoing_ops_count_by_area.items():
                self.set_ongoing_ops_count_for_area_for_cause(
                    area, cause, ongoing_ops_count
                )
            # Notes:
            # ------
            # Could be with multiple set using mset, but actually not so
            # usefull, since we need to loop to map the keys anyway and
            # it's complicated to handle the tags of causes that way
            # hence, rather loop and call the unitary set method
            # mapping = {
            #     self.make_ongoing_ops_count_for_area_for_cause_key(
            #         area, cause
            #     ): ongoing_ops_count
            #     for area, ongoing_ops_count in ongoing_ops_count_by_area.items()
            # }
            # self.r.mset(mapping)

    def get_ongoing_ops_counts_for_cause(self, cause: OperationCause) -> List[int]:
        redis_matching_keys = self.r.sinter(f"cause:{cause}")
        return [
            int(count_as_binary.decode())
            for count_as_binary in self.r.mget(redis_matching_keys)
        ]

    def remove_operation_stored_infos_for_operation_id(
        self, raw_operation_id: RawOperationId
    ):
        self.r.delete(
            self.make_operation_stored_infos_for_operation_id_key(raw_operation_id)
        )

    def get_all_operation_stored_infos(self) -> List[OperationStoredInfos]:
        redis_matching_keys = self.r.keys(
            pattern=f"{operation_stored_infos_for_key_prefix}_*"
        )
        all_operation_stored_infos_serialized = self.r.mget(redis_matching_keys)
        return [
            OperationStoredInfos.from_dict(json.loads(serialized_infos))
            for serialized_infos in all_operation_stored_infos_serialized
        ]

    def set_operations_notified_from_vehicle_changes(
        self, operation_ids: Set[RawOperationId]
    ) -> None:
        self.r.set(
            operations_notified_from_vehicle_changes_key,
            json.dumps(list(operation_ids)),
        )

    def get_operations_notified_from_vehicle_changes(
        self,
    ) -> Set[RawOperationId]:
        value_in_cache = redis_get_and_loads(
            self.r, operations_notified_from_vehicle_changes_key
        )
        if not value_in_cache:
            return set()
        return set(value_in_cache)

    @staticmethod
    def make_ongoing_ops_count_for_area_for_cause_key(
        area: Area, cause: OperationCause
    ) -> str:
        return f"ongoing_ops_count_for_{area}_for_{cause}"

    @staticmethod
    def make_operation_stored_infos_for_operation_id_key(
        raw_operation_id: RawOperationId,
    ) -> str:
        return f"{operation_stored_infos_for_key_prefix}_{raw_operation_id}"
