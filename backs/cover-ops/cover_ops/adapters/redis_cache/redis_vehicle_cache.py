import json
from typing import Any, Dict, List, Optional, cast

from cover_ops.adapters.redis_cache.redis_utils import (
    redis_get_and_loads,
    redis_get_many_and_loads,
)
from cover_ops.domain.ports.vehicle_cache import AbstractVehicleCache
from cover_ops.domain.ports.vehicle_events_repository import (
    AvailabilityStoredInfoByAreaByRole,
    AvailabilityStoredInfoByRole,
    AvailabilityStoredInfos,
)
from redis import Redis

from shared.data_transfert_objects.area import Area
from shared.data_transfert_objects.omnibus_balance_changed_event_data import (
    OmnibusBalance,
    OmnibusDetail,
)
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
)
from shared.helpers.dataclass import dumps_dataclass

omnibus_balance_key = "omnibus_balance"
omnibus_detail_key = "omnibus_detail"


class RedisVehicleCache(AbstractVehicleCache):
    def __init__(self, redis_instance: Redis):
        self.r = redis_instance
        self.initialize_cache()

    def save_in_redis(self, key: str, value: str):
        self.r.set(
            key,
            value,
        )

    def get_availability_stored_infos_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        value_in_cache = redis_get_and_loads(
            self.r,
            self.make_availability_stored_infos_for_area_for_role_key(area, role),
        )
        return AvailabilityStoredInfos.from_dict(cast(Dict, value_in_cache))

    def set_availability_stored_infos_for_area_for_role(
        self,
        area: Area,
        role: VehicleRole,
        availability_stored_infos: AvailabilityStoredInfos,
    ) -> None:
        self.r.set(
            self.make_availability_stored_infos_for_area_for_role_key(area, role),
            dumps_dataclass(availability_stored_infos),
        )

    def get_bspp_availability_stored_infos_for_role(
        self, role: VehicleRole
    ) -> AvailabilityStoredInfos:
        value_in_cache = redis_get_and_loads(
            self.r,
            self.make_bspp_availability_stored_infos_for_role_key(role),
        )
        return AvailabilityStoredInfos.from_dict(cast(Any, value_in_cache))

    def set_bspp_availability_stored_infos_for_role(
        self,
        role: VehicleRole,
        availability_stored_infos: AvailabilityStoredInfos,
    ) -> None:
        self.r.set(
            self.make_bspp_availability_stored_infos_for_role_key(role),
            dumps_dataclass(availability_stored_infos),
        )

    def get_latest_vehicle_event_data_for_vehicle_id(
        self,
        vehicle_id: RawVehicleId,
    ) -> Optional[VehicleEventData]:
        redis_value = self.r.get(
            self.make_latest_vehicle_event_data_for_id_key(vehicle_id)
        )
        return (
            VehicleEventData.from_dict(
                json.loads(redis_value), skip_marshmallow_validation=True
            )
            if redis_value
            else None
        )

    def set_latest_vehicle_event_data_for_vehicle_id(
        self, vehicle_event_data: VehicleEventData
    ):
        vehicle_id = vehicle_event_data.raw_vehicle_id
        redis_key = self.make_latest_vehicle_event_data_for_id_key(vehicle_id)

        latest_stored_data_for_this_vehicle = (
            self.get_latest_vehicle_event_data_for_vehicle_id(vehicle_id)
        )

        self.r.set(
            redis_key,
            dumps_dataclass(vehicle_event_data),
        )

        if latest_stored_data_for_this_vehicle:
            previous_raw_operation_id = (
                latest_stored_data_for_this_vehicle.raw_operation_id
            )
            if previous_raw_operation_id:
                self.r.srem(f"raw_operation_id:{previous_raw_operation_id}", redis_key)

        self.r.sadd(f"role:{vehicle_event_data.role}", redis_key)
        self.r.sadd(f"home_area:{vehicle_event_data.home_area}", redis_key)

        current_raw_operation_id = vehicle_event_data.raw_operation_id
        if current_raw_operation_id is not None:
            self.r.sadd(f"raw_operation_id:{current_raw_operation_id}", redis_key)

    def get_latest_vehicle_event_datas_for_area_for_role(
        self, area: Area, role: VehicleRole
    ) -> List[VehicleEventData]:
        redis_matching_keys = self.r.sinter(f"role:{role}", f"home_area:{area}")
        latest_vehicle_event_datas_serialized = self.r.mget(redis_matching_keys)

        return [
            VehicleEventData.from_dict(json.loads(latest_vehicle_event_data_serialized))
            for latest_vehicle_event_data_serialized in latest_vehicle_event_datas_serialized
        ]

    def set_availability_stored_infos_for_areas_for_roles(
        self,
        availability_stored_infos_by_area_by_role: AvailabilityStoredInfoByAreaByRole,
    ) -> None:
        pipe = self.r.pipeline()
        for (
            area,
            availability_stored_infos_by_role,
        ) in availability_stored_infos_by_area_by_role.items():
            for (
                role,
                availability_stored_infos,
            ) in availability_stored_infos_by_role.items():
                pipe.set(
                    self.make_availability_stored_infos_for_area_for_role_key(
                        area, role
                    ),
                    dumps_dataclass(availability_stored_infos),
                )
        pipe.execute()

    def set_bspp_availability_stored_infos_for_roles(
        self, bspp_availability_stored_infos_by_role: AvailabilityStoredInfoByRole
    ) -> None:
        pipe = self.r.pipeline()
        for (
            role,
            availability_stored_infos,
        ) in bspp_availability_stored_infos_by_role.items():
            pipe.set(
                self.make_bspp_availability_stored_infos_for_role_key(role),
                dumps_dataclass(availability_stored_infos),
            )
        pipe.execute()

    def get_vehicle_event_data_for_operation_id(
        self, raw_operation_id: RawOperationId
    ) -> List[VehicleEventData]:
        redis_matching_keys = self.r.sinter(f"raw_operation_id:{raw_operation_id}")
        latest_vehicle_event_datas_serialized = self.r.mget(redis_matching_keys)

        return [
            VehicleEventData.from_dict(json.loads(latest_vehicle_event_data_serialized))
            for latest_vehicle_event_data_serialized in latest_vehicle_event_datas_serialized
        ]

    def get_omnibus_balance(self) -> OmnibusBalance:
        omnibus_balance_as_dict = redis_get_and_loads(self.r, omnibus_balance_key)
        return (
            OmnibusBalance.from_dict(omnibus_balance_as_dict)
            if omnibus_balance_as_dict
            else OmnibusBalance()
        )

    def set_omnibus_balance(self, omnibus_balance: OmnibusBalance) -> None:
        self.r.set(omnibus_balance_key, dumps_dataclass(omnibus_balance))

    def add_omnibus(self, omnibus_detail: OmnibusDetail) -> None:
        self.r.set(
            self.make_omnibus_detail_for_id_key(omnibus_detail.vsav_id),
            dumps_dataclass(omnibus_detail),
        )

    def get_omnibus_details(self) -> List[OmnibusDetail]:
        keys = self.r.keys(f"{omnibus_detail_key}*")
        details_as_dict = redis_get_many_and_loads(self.r, keys)
        return [OmnibusDetail(**detail_as_dict) for detail_as_dict in details_as_dict]

    def remove_omnibus_given_vsav_id(self, vsav_id: RawVehicleId) -> None:
        self.r.delete(self.make_omnibus_detail_for_id_key(vsav_id))

    def get_omnibus_given_vsav_id(
        self, vsav_id: RawVehicleId
    ) -> Optional[OmnibusDetail]:
        detail_as_dict = redis_get_and_loads(
            self.r, self.make_omnibus_detail_for_id_key(vsav_id=vsav_id)
        )
        return OmnibusDetail(**detail_as_dict) if detail_as_dict else None

    @staticmethod
    def make_availability_stored_infos_for_area_for_role_key(
        area: Area, role: VehicleRole
    ) -> str:
        return f"availability_stored_infos_for_{area}_for_{role}"

    @staticmethod
    def make_bspp_availability_stored_infos_for_role_key(role: VehicleRole) -> str:
        return f"bspp_availability_stored_infos_for_{role}"

    @staticmethod
    def make_latest_vehicle_event_data_for_id_key(vehicle_id: RawVehicleId) -> str:
        return f"latest_vehicle_event_data_for_{vehicle_id}"

    @staticmethod
    def make_omnibus_detail_for_id_key(vsav_id: RawVehicleId) -> str:
        return f"{omnibus_detail_key}_{vsav_id}"
