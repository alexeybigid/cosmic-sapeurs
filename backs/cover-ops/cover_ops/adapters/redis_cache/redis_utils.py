import json
import os
from typing import Any, List, Literal, Optional, Union

from redis import Redis


def set_variable_for_context(local_value, prod_value, CI_value):
    running_in_CI = os.environ.get("CI", False)
    if running_in_CI:
        return CI_value
    is_in_docker_compose = os.environ.get("DOCKER_COMPOSE", False) == "true"
    if is_in_docker_compose:
        return prod_value
    return local_value


redis_port: int = set_variable_for_context(
    local_value=6379, prod_value=6379, CI_value=6379
)
redis_host: Literal["localhost", "redis"] = set_variable_for_context(
    local_value="localhost", prod_value="redis", CI_value="redis"
)


def make_redis_instance() -> Redis:
    return Redis(host=redis_host, port=redis_port, db=0)


def deserialize_value_from_redis(serialized_value):
    return json.loads(serialized_value) if serialized_value else None


def redis_get_and_loads(r: Redis, key: str) -> Optional[Union[Any]]:
    serialized_value = r.get(key)
    return deserialize_value_from_redis(serialized_value)


def redis_get_many_and_loads(r: Redis, keys: List[str]) -> List[Any]:
    serialized_values = r.mget(keys)
    return [deserialize_value_from_redis(v) for v in serialized_values]
