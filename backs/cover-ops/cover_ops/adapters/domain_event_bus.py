from typing import Any, Callable, Coroutine, Literal

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.event_bus import InMemoryEventBus


class InvalidTopic(Exception):
    pass


TopicExternalEventsBecameReady = Literal["externalEventsBecameReady"]

EventCallback = Callable[[Any], Coroutine[Any, Any, Any]]

DomainEventBus = InMemoryEventBus[CoverOpsDomainTopic]
