import os
from dataclasses import dataclass
from typing import List, Literal

from cover_ops.helpers.exception_logger import get_exception_mode
from dotenv import load_dotenv

from shared.helpers.decorators import ExceptionMode
from shared.helpers.prepare_get_env_variable import prepare_get_env_variable

load_dotenv()

get_env_variable = prepare_get_env_variable(os.environ.get)


ProcessedEventsRepositories = Literal["CSV"]
processed_events_repositories_options: List[ProcessedEventsRepositories] = [
    "CSV",
]

CacheOption = Literal["IN_MEMORY", "REDIS"]
cache_options: List[CacheOption] = ["IN_MEMORY", "REDIS"]


@dataclass
class EnvironmentVariables:
    exception_mode: ExceptionMode
    processed_events_repositories: ProcessedEventsRepositories
    cache: CacheOption

    def __str__(self):
        return f"\
            exception_mode: {self.exception_mode} \n\
            processed_events_repositories: {self.processed_events_repositories} \n\
            cache: {self.cache} \n"


def get_env_variables() -> EnvironmentVariables:
    processed_events_repositories = get_env_variable(
        "PROCESSED_EVENTS_REPOSITORIES", processed_events_repositories_options
    )
    cache = get_env_variable("CACHE", cache_options)

    return EnvironmentVariables(
        exception_mode=get_exception_mode(),
        processed_events_repositories=processed_events_repositories,
        cache=cache,
    )
