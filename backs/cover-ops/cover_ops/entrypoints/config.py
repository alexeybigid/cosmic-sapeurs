import os
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Dict, Optional, Tuple

from cover_ops.adapters.csv_events_repositories import (
    CsvAvailabilityEventsRepository,
    CsvOmnibusEventsRepository,
    CsvOngoingOpEventsRepository,
)
from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.adapters.pandas.pandas_operation_events_repository import (
    PandasOperationEventsRepository,
)
from cover_ops.adapters.pandas.pandas_vehicle_events_repository import (
    PandasVehicleEventsRepository,
)
from cover_ops.adapters.redis_cache.redis_operation_cache import RedisOperationCache
from cover_ops.adapters.redis_cache.redis_utils import make_redis_instance
from cover_ops.adapters.redis_cache.redis_vehicle_cache import RedisVehicleCache
from cover_ops.domain.ports.availability_events_repository import (
    AbstractAvailabilityEventsRepository,
    InMemoryAvailabilityEventsRepository,
)
from cover_ops.domain.ports.omnibus_events_repository import (
    AbstractOmnibusEventsRepository,
)
from cover_ops.domain.ports.ongoing_ops_events_repository import (
    AbstractOngoingOpEventsRepository,
)
from cover_ops.domain.ports.operation_cache import (
    AbstractOperationCache,
    InMemoryOperationCache,
)
from cover_ops.domain.ports.vehicle_cache import (
    AbstractVehicleCache,
    InMemoryVehicleCache,
)
from cover_ops.domain.ports.vehicle_events_repository import (
    InMemoryVehicleEventsRepository,
)
from cover_ops.domain.services.operation_event_service import OperationEventService
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.get_cover import GetCover
from cover_ops.domain.use_cases.get_ops import GetOps
from cover_ops.domain.use_cases.notify_when_vehicle_availability_changed_affects_operation import (
    NotifyWhenVehicleAvailabilityChangedAffectsOperation,
)
from cover_ops.domain.use_cases.publish_converters_events_to_domain_event_bus import (
    PublishConvertersEventsToDomainEventBus,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_caches_and_repositories import (
    ResyncCachesAndRepositories,
)
from cover_ops.domain.use_cases.transfert_events_batch_to_repositories import (
    TransfertEventsBatchToRepositories,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.domain.use_cases.update_ops import UpdateOps
from cover_ops.entrypoints.environment_variables import (
    CacheOption,
    EnvironmentVariables,
    ProcessedEventsRepositories,
)
from cover_ops.entrypoints.subscribe_to_domain_events import subscribe_to_domain_events
from dotenv import load_dotenv

from shared.helpers.clock import RealClock
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid, RealUuid

load_dotenv()

vehicle_events_repo = InMemoryVehicleEventsRepository()
availability_events_repo = InMemoryAvailabilityEventsRepository()

clock = RealClock()
server_started_at = clock.get_now().replace(
    ":", " "
)  # replace ":" by " " for Windows compatibility


env_var_to_processed_events_repo: Dict[
    ProcessedEventsRepositories,
    Callable[
        [Optional[Tuple[Path, Path, Path]]],
        Tuple[
            AbstractAvailabilityEventsRepository,
            AbstractOngoingOpEventsRepository,
            AbstractOmnibusEventsRepository,
        ],
    ],
] = {
    "CSV": lambda csv_paths: (
        CsvAvailabilityEventsRepository(csv_paths[0]),
        CsvOngoingOpEventsRepository(csv_paths[1]),
        CsvOmnibusEventsRepository(csv_paths[2]),
    )
}


def get_redis_caches() -> Tuple[RedisVehicleCache, RedisOperationCache]:
    r = make_redis_instance()
    r.flushdb()
    return RedisVehicleCache(r), RedisOperationCache(r)


env_var_to_cache: Dict[
    CacheOption,
    Callable[
        [],
        Tuple[AbstractVehicleCache, AbstractOperationCache],
    ],
] = {
    "IN_MEMORY": lambda: (InMemoryVehicleCache(), InMemoryOperationCache()),
    "REDIS": get_redis_caches,
}

repo_and_cache_file_names = [
    "vehicle_events_repo.csv",
    "operation_events_repo.csv",
    "availability_events_repo.csv",
    "ongoing_op_events_repo.csv",
    "omnibus_events_repo.csv",
]


def infer_repository_latest_date(
    csv_base_path: Path,
) -> Optional[str]:
    p = csv_base_path.glob("**/*.csv")
    file_names = [x.name for x in p if x.is_file()]
    if not file_names:
        return None
    latest_server_started_at = max(
        [file_name.split("__")[0] for file_name in file_names]
    )

    repo_with_latest_date = [
        f"{latest_server_started_at}__{repo_name}"
        for repo_name in repo_and_cache_file_names
    ]

    if set(repo_with_latest_date).issubset(set(file_names)):
        return latest_server_started_at
    else:
        return


def infer_repository_and_cache_paths(
    csv_base_path: Path, current_server_started_at: str
):
    prefix_date = infer_repository_latest_date(csv_base_path)
    if prefix_date is None:
        prefix_date = current_server_started_at
        logger.info(
            f"No previous repositories to use. Create one with prefix {prefix_date}"
        )
    return tuple(
        [
            csv_base_path / f"{prefix_date}__{file_name}"
            for file_name in repo_and_cache_file_names
        ]
    )


@dataclass
class UseCases:
    update_cover: UpdateCover
    update_ops: UpdateOps
    update_omnibus: UpdateOmnibus
    notify_when_vehicle_availability_changed_affects_operation: NotifyWhenVehicleAvailabilityChangedAffectsOperation
    transfert_events_batch_to_repositories: TransfertEventsBatchToRepositories
    add_events_to_repositories: AddEventsToRepositories
    resync_caches_and_repositories: ResyncCachesAndRepositories  # Resync (processing)
    get_cover: GetCover
    get_ops: GetOps
    publish_converters_events_to_domain_event_bus: PublishConvertersEventsToDomainEventBus


class Config:
    vehicle_events_repo: PandasVehicleEventsRepository
    operation_events_repo: PandasOperationEventsRepository
    ongoing_op_events_repo: AbstractOngoingOpEventsRepository
    availability_events_repo: AbstractAvailabilityEventsRepository
    omnibus_events_repo: AbstractOmnibusEventsRepository
    vehicle_cache: AbstractVehicleCache
    operation_cache: AbstractOperationCache
    domain_event_bus: DomainEventBus
    uuid: AbstractUuid
    use_cases: UseCases

    def __init__(
        self, env: EnvironmentVariables, csv_base_path: Path = Path("data")
    ) -> None:
        # repo_infra = os.environ.get("REPOSITORIES")
        self.ENV = env
        logger.info(self.ENV)

        self.clock = clock
        self.domain_event_bus = DomainEventBus(clock)

        if not os.path.exists(csv_base_path):
            os.mkdir(csv_base_path)

        (
            vehicle_csv_path,
            operation_csv_path,
            availability_csv_path,
            ongoing_op_csv_path,
            omnibus_csv_path,
        ) = infer_repository_and_cache_paths(csv_base_path, server_started_at)
        self.vehicle_events_repo = PandasVehicleEventsRepository(vehicle_csv_path)
        self.operation_events_repo = PandasOperationEventsRepository(operation_csv_path)

        (
            self.availability_events_repo,
            self.ongoing_op_events_repo,
            self.omnibus_events_repo,
        ) = env_var_to_processed_events_repo[self.ENV.processed_events_repositories](
            (availability_csv_path, ongoing_op_csv_path, omnibus_csv_path)
        )
        self.uuid = RealUuid()

        self.vehicle_cache, self.operation_cache = env_var_to_cache[self.ENV.cache]()

        operation_event_service = OperationEventService(
            self.operation_events_repo, self.operation_cache
        )

        self._prepare_json_path = (
            f"data/{server_started_at}_latest_vehicle_changed_status_events.json"
        )

        self.use_cases = UseCases(
            update_cover=UpdateCover(
                availability_events_repo=self.availability_events_repo,
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                vehicle_cache=self.vehicle_cache,
            ),
            update_ops=UpdateOps(
                ongoing_op_events_repo=self.ongoing_op_events_repo,
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                operation_event_service=operation_event_service,
            ),
            update_omnibus=UpdateOmnibus(
                domain_event_bus=self.domain_event_bus,
                omnibus_events_repo=self.omnibus_events_repo,
                uuid=self.uuid,
                vehicle_cache=self.vehicle_cache,
            ),
            get_cover=GetCover(
                availability_events_repo=self.availability_events_repo,
                vehicle_cache=self.vehicle_cache,
            ),
            get_ops=GetOps(ongoing_op_events_repo=self.ongoing_op_events_repo),
            publish_converters_events_to_domain_event_bus=PublishConvertersEventsToDomainEventBus(
                domain_event_bus=self.domain_event_bus,
            ),
            notify_when_vehicle_availability_changed_affects_operation=NotifyWhenVehicleAvailabilityChangedAffectsOperation(
                domain_event_bus=self.domain_event_bus,
                uuid=self.uuid,
                vehicle_cache=self.vehicle_cache,
                operation_cache=self.operation_cache,
            ),
            transfert_events_batch_to_repositories=TransfertEventsBatchToRepositories(
                vehicle_events_repo=self.vehicle_events_repo,
                operation_events_repo=self.operation_events_repo,
            ),
            add_events_to_repositories=AddEventsToRepositories(
                vehicle_events_repo=self.vehicle_events_repo,
                operation_events_repo=self.operation_events_repo,
            ),
            resync_caches_and_repositories=ResyncCachesAndRepositories(
                uuid=RealUuid(),
                operation_event_service=operation_event_service,
                operation_events_repo=self.operation_events_repo,
                vehicle_events_repo=self.vehicle_events_repo,
                availability_events_repo=self.availability_events_repo,
                ongoing_op_events_repo=self.ongoing_op_events_repo,
                vehicle_cache=self.vehicle_cache,
                operation_cache=self.operation_cache,
            ),
        )

    def bus_subscribes_to_domain_events(self):
        subscribe_to_domain_events(
            self.domain_event_bus,
            self.use_cases.update_cover,
            self.use_cases.update_ops,
            self.use_cases.update_omnibus,
            self.use_cases.add_events_to_repositories,
            self.use_cases.notify_when_vehicle_availability_changed_affects_operation,
            self.use_cases.transfert_events_batch_to_repositories,
            self.use_cases.resync_caches_and_repositories,
        )
