from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.use_cases.add_events_to_repositories import (
    AddEventsToRepositories,
)
from cover_ops.domain.use_cases.notify_when_vehicle_availability_changed_affects_operation import (
    NotifyWhenVehicleAvailabilityChangedAffectsOperation,
)
from cover_ops.domain.use_cases.resync_caches_and_repositories.resync_caches_and_repositories import (
    ResyncCachesAndRepositories,
)
from cover_ops.domain.use_cases.transfert_events_batch_to_repositories import (
    TransfertEventsBatchToRepositories,
)
from cover_ops.domain.use_cases.update_cover import UpdateCover
from cover_ops.domain.use_cases.update_omnibus import UpdateOmnibus
from cover_ops.domain.use_cases.update_ops import UpdateOps
from tqdm import tqdm

from shared.helpers.logger import logger
from shared.sapeurs_events import (
    ConvertersCatchesUpEvent,
    ConvertersVehiclesInitEvent,
    OperationEvent,
    VehicleEvent,
)


def subscribe_to_vehicle_changed_status(
    domain_event_bus: DomainEventBus,
    update_cover: UpdateCover,
    update_omnibus: UpdateOmnibus,
    add_events_to_repositories: AddEventsToRepositories,
):
    async def update_omnibus_and_cover_then_add_to_repo(event: VehicleEvent):
        await update_omnibus.execute(event)
        await update_cover.execute(event)
        await add_events_to_repositories.execute(event)

    domain_event_bus.subscribe(
        "vehicle_changed_status", update_omnibus_and_cover_then_add_to_repo
    )


def subscribe_to_operation_changed_status(
    domain_event_bus: DomainEventBus,
    update_ops: UpdateOps,
    add_events_to_repositories: AddEventsToRepositories,
):
    async def update_ops_and_add_to_repo(event: OperationEvent):
        await update_ops.execute(event)
        await add_events_to_repositories.execute(event)

    domain_event_bus.subscribe("operation_changed_status", update_ops_and_add_to_repo)


def subscribe_to_converters_initialized_last_status_of_all_vehicles(
    domain_event_bus: DomainEventBus,
    transfert_events_batch_to_repositories: TransfertEventsBatchToRepositories,
    resync_caches_and_repositories: ResyncCachesAndRepositories,
):
    async def transfert_event_batch_and_resync_from_repo(
        event: ConvertersVehiclesInitEvent,
    ):
        transfert_events_batch_to_repositories.execute(event.data)
        resync_caches_and_repositories.execute()

    domain_event_bus.subscribe(
        "converters_initialized_last_status_of_all_vehicles",
        transfert_event_batch_and_resync_from_repo,
    )


def subscribe_to_converters_catches_up_accumulated_events(
    domain_event_bus: DomainEventBus,
):
    async def publish_catch_up_events_batch(event: ConvertersCatchesUpEvent):
        logger.info(f"\n\nPublishing catch up events batch (N={len(event.data)})...")
        for catch_up_event in tqdm(event.data):
            await domain_event_bus.publish(catch_up_event)

    domain_event_bus.subscribe(
        "converters_catches_up_accumulated_events",
        publish_catch_up_events_batch,
    )


def subscribe_to_domain_events(
    domain_event_bus: DomainEventBus,
    update_cover: UpdateCover,
    update_ops: UpdateOps,
    update_omnibus: UpdateOmnibus,
    add_events_to_repositories: AddEventsToRepositories,
    notify_when_vehicle_availability_changed_affects_operation: NotifyWhenVehicleAvailabilityChangedAffectsOperation,
    transfert_events_batch_to_repositories: TransfertEventsBatchToRepositories,
    resync_caches_and_repositories: ResyncCachesAndRepositories,
):
    subscribe_to_vehicle_changed_status(
        domain_event_bus,
        update_cover,
        update_omnibus,
        add_events_to_repositories,
    )

    subscribe_to_operation_changed_status(
        domain_event_bus, update_ops, add_events_to_repositories
    )

    subscribe_to_converters_initialized_last_status_of_all_vehicles(
        domain_event_bus,
        transfert_events_batch_to_repositories,
        resync_caches_and_repositories,
    )
    domain_event_bus.subscribe(
        "availabilityChanged",
        notify_when_vehicle_availability_changed_affects_operation.execute,
    )
    subscribe_to_converters_catches_up_accumulated_events(domain_event_bus)
