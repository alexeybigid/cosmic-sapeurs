import asyncio
from asyncio.events import AbstractEventLoop
from typing import Tuple, Union

import aiohttp_cors
from aiohttp import web
from aiohttp.web_app import Application
from cover_ops.entrypoints.backend_to_frontend import serialize_data
from cover_ops.entrypoints.config import Config
from cover_ops.entrypoints.environment_variables import get_env_variables
from cover_ops.entrypoints.socket_handlers import (
    SapeurWebSocketResponse,
    send_events_to_front,
)

from shared.helpers.dataclass import dataclass_to_dict
from shared.helpers.logger import logger
from shared.routes import make_url, sapeurs_routes
from shared.sapeurs_events import (
    OmnibusBalanceChangedEvent,
    OperationEvent,
    VehicleEvent,
)

web_json_response_missing_body = web.json_response(
    {"success": "false"}, status=400, reason="Missing body"
)


route_converted_event = make_url(sapeurs_routes.prefix, sapeurs_routes.converted_event)


def make_app(config: Config) -> Tuple[Application, AbstractEventLoop]:

    config.bus_subscribes_to_domain_events()

    routes = web.RouteTableDef()

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.hello))
    async def hello(request):
        logger.info("Received Hello Sapeur request")
        return web.json_response({"message": "Hello, world"})

    async def publish_in_domain(e: Union[VehicleEvent, OperationEvent]):
        await config.domain_event_bus.publish(e)

    maxHours = 4

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_cover))
    async def get_last_status_of_all_vehicles_route(request):
        hours = maxHours
        if request.body_exists:
            body = await request.json()
            hours = min(body.get("hours", maxHours), maxHours)
        availability_events_data = config.use_cases.get_cover.execute(hours=hours)
        serialized_events = [
            serialize_data(event_data) for event_data in availability_events_data
        ]
        return web.json_response(serialized_events)

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_ops))
    async def get_last_status_of_all_on_going_ops_route(request):
        hours = maxHours
        if request.body_exists:
            body = await request.json()
            hours = min(body.get("hours", maxHours), maxHours)
        on_going_op_events_data = config.use_cases.get_ops.execute(hours=hours)
        serialized_events = [
            serialize_data(event_data) for event_data in on_going_op_events_data
        ]
        return web.json_response(serialized_events)

    @routes.get(make_url(sapeurs_routes.prefix, sapeurs_routes.get_omnibus))
    async def get_last_omnibus_balance_route(request):
        omnibus_balance_changed_event = (
            config.omnibus_events_repo.get_latest_event_data()
        )
        return web.json_response(dataclass_to_dict(omnibus_balance_changed_event))

    @routes.post(route_converted_event)
    async def converted_event(request):
        if not request.body_exists:
            return web_json_response_missing_body
        sapeurs_event = await request.json()
        await config.use_cases.publish_converters_events_to_domain_event_bus.execute(
            sapeurs_event
        )
        return web.json_response({"success": "true"})

    @routes.get(make_url(sapeurs_routes.prefix, "ws"))
    async def websocket_handler(request):
        ws = send_events_to_front(
            SapeurWebSocketResponse(),
            config.domain_event_bus,
            topics=["availabilityChanged", "ongoingOpChanged", "omnibusBalanceChanged"],
        )
        await ws.prepare(request)
        await ws.run()
        return ws

    loop = asyncio.get_event_loop()
    app = web.Application()
    app.add_routes(routes)

    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
        },
    )

    for route in list(app.router.routes()):
        cors.add(route)
    return app, loop


def main():
    config = Config(env=get_env_variables())

    app, loop = make_app(config)
    runner = web.AppRunner(app)
    loop.run_until_complete(runner.setup())
    site = web.TCPSite(runner)
    loop.run_until_complete(site.start())
    loop.run_forever()
    # TODO : remember to resync repositories and caches when instantiating a new API service


if __name__ == "__main__":
    # execute only if run as a script
    main()
