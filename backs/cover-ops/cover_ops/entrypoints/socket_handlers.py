from typing import List, Union

import aiohttp
from aiohttp import web
from cover_ops.adapters.domain_event_bus import DomainEventBus
from cover_ops.domain.entities.event_entities import AvailabilityChangedEventEntity
from cover_ops.entrypoints.backend_to_frontend import backend_event_to_frontend_event

from shared.cover_ops_topics import CoverOpsDomainTopic
from shared.helpers.logger import logger
from shared.sapeurs_events import AvailabilityChangedEvent


class SapeurWebSocketResponse(web.WebSocketResponse):
    def __init__(self) -> None:
        super().__init__()

    async def send_to_front(
        self,
        event: Union[
            AvailabilityChangedEvent,
            AvailabilityChangedEventEntity,
        ],
    ):
        message = backend_event_to_frontend_event(event)
        message["topic"] = event.topic  # type: ignore
        await self.send_json(message)

    async def run(self):
        async for msg in self:
            logger.info(f"\n Incomming message : {msg.data}")
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == "close":
                    logger.info("Closing socket ...")
                    await self.close()
            elif msg.type == aiohttp.WSMsgType.ERROR:
                logger.info("ws connection closed with exception %s" % self.exception())
        logger.info("Websocket connection ran")


def send_events_to_front(
    ws: SapeurWebSocketResponse,
    domain_event_bus: DomainEventBus,
    topics: List[CoverOpsDomainTopic],
) -> SapeurWebSocketResponse:
    async def send_to_front_if_socket_open(e):
        if ws.closed:
            for topic in topics:
                domain_event_bus.unsubscribe(topic, send_to_front_if_socket_open)
            return
        await ws.send_to_front(e)

    for topic in topics:
        domain_event_bus.subscribe(topic, send_to_front_if_socket_open)
    return ws
