from typing import Any, Callable, Tuple

from adapters.abstract_scheduled_event_buses import AbstractScheduledEventBuses
from adapters.scheduled_event_bus import RandomScheduledEventBus, ScheduledEventBus
from domain.ports.publisher import AbstractPublisher

from shared.helpers.clock import AbstractClock


class RandomEventBuses(AbstractScheduledEventBuses):
    def __init__(
        self,
        clock: AbstractClock,
        vehicle_publisher: AbstractPublisher,
        operation_publisher: AbstractPublisher,
        make_vehicle_event: Callable,
        make_operation_event: Callable,
        **kwargs,
    ):
        self.make_vehicle_event = make_vehicle_event
        self.make_operation_event = make_operation_event

        super().__init__(
            clock=clock,
            vehicle_publisher=vehicle_publisher,
            operation_publisher=operation_publisher,
            **kwargs,
        )

    def _instantiate_event_buses(
        self,
        clock: AbstractClock,
        vehicle_publisher: AbstractPublisher,
        operation_publisher: AbstractPublisher,
        *,
        min_time_step: float,
        max_time_step: float,
    ) -> Tuple[ScheduledEventBus, ScheduledEventBus]:

        vehicle_events_bus = RandomScheduledEventBus(
            clock=clock,
            make_event=self.make_vehicle_event,
            min_time_step=min_time_step,
            max_time_step=max_time_step,
            publisher=vehicle_publisher,
        )
        operation_events_bus = RandomScheduledEventBus(
            clock=clock,
            make_event=self.make_operation_event,
            min_time_step=min_time_step,
            max_time_step=max_time_step,
            publisher=operation_publisher,
        )
        return vehicle_events_bus, operation_events_bus
