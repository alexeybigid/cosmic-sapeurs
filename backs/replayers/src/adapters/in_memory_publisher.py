from typing import List

from domain.ports.publisher import AbstractPublisher

from shared.external_event import ExternalEvent


class InMemoryPublisher(AbstractPublisher):
    def __init__(self) -> None:
        super().__init__()
        self._published_events: List[ExternalEvent] = []

    def publish_to_converters(self, event: ExternalEvent) -> None:
        self._published_events.append(event)

    # For test purposes only
    @property
    def published_events(self) -> List[ExternalEvent]:
        return self._published_events
