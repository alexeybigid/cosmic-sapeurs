from domain.ports.publisher import AbstractPublisher

from shared.adagio_event_dict_to_event import (
    adagio_intervention_topic_options,
    adagio_vehicle_topic_options,
)
from shared.external_event import ExternalEvent
from shared.helpers.logger import logger


class AdagioPublishers(AbstractPublisher):
    def __init__(
        self,
        vehicle_publisher: AbstractPublisher,
        intervention_publisher: AbstractPublisher,
    ):
        self.vehicle_publisher = vehicle_publisher
        self.intervention_publisher = intervention_publisher

    def publish_to_converters(self, event: ExternalEvent) -> None:
        """Publish event to converters app depending on topic"""
        if event.topic in adagio_intervention_topic_options:
            self.intervention_publisher.publish_to_converters(event)
        elif event.topic in adagio_vehicle_topic_options:
            self.vehicle_publisher.publish_to_converters(event)
        else:
            logger.warn(f"Cannot publish event with topic {event.topic}")
