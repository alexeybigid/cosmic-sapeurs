import ast
import json
import os
import random
from abc import abstractmethod
from typing import Callable, Dict, List, Optional, Union, cast

import pandas as pd
from domain.ports.publisher import AbstractPublisher
from pandas.api.types import is_datetime64_any_dtype as is_datetime

from shared.adagio_date import TopicsToReplay, topic_to_date_infos
from shared.adagio_topics import topic_adagio_event_options
from shared.cover_ops_topics import (
    CoverOpsDomainTopic,
    ExternalTopic,
    external_topic_options,
)
from shared.event_bus import InMemoryEventBus
from shared.external_event import ExternalEvent
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.clock import AbstractClock
from shared.helpers.date import DateStr, from_regex, to_datetime
from shared.helpers.get_and_set_in_dict import get_in_dict
from shared.helpers.logger import logger
from shared.pandas.pandas_date import to_timestamp
from shared.sapeurs_events import Event

ExternalEventBus = InMemoryEventBus[ExternalTopic]


class DataFrameError(Exception):
    pass


class Pause(Exception):
    pass


def deserialize_data_column(df: pd.DataFrame, inplace: bool = True) -> pd.DataFrame:
    if df.empty:
        return df
    if isinstance(df.data.values[0], dict):
        return df
    if not inplace:
        df = df.copy()
    try:
        df.data = df.data.map(ast.literal_eval)
    except:
        df.data = df.data.map(json.loads)
    return df


def _raise_if_df_empty(df: pd.DataFrame):
    if df.empty:
        raise DataFrameError("No event to replay. Check warnings.")


def _nan_to_none(df: pd.DataFrame) -> pd.DataFrame:
    df = df.where(df.notnull(), None)
    return df


def _timestamps_is_monotonic(df: pd.DataFrame, inplace: bool = True) -> pd.DataFrame:
    if not inplace:
        df = df.copy()
    if not df.timestamp.is_monotonic:
        df.sort_values("timestamp", inplace=True)
    return df


def _timestamps_is_datetime(df: pd.DataFrame, inplace: bool = True) -> pd.DataFrame:
    if not inplace:
        df = df.copy()
    if not is_datetime(df.timestamp):

        def _convert_timestamp_given_topic(
            series: pd.Series,
        ) -> Optional[DateStr]:
            date_format_in_data = topic_to_date_infos[
                series.topic
            ].date_format_regex_in_data
            try:
                return from_regex(series.timestamp, date_format_in_data)
            except Exception as e:
                logger.warn(
                    f"Cannot convert date {series.timestamp} to timestamp in topic {series.topic}, expected format {date_format_in_data}",
                )
                return None

        logger.warn(
            f"Can replay {len(df.dropna())} events out of {len(df)} (limitation due to timestamp incoherent format)"
        )
        df.timestamp = pd.to_datetime(df.apply(_convert_timestamp_given_topic, axis=1))
        df = df.loc[~df.timestamp.isna()]
    return df


def _has_required_columns(df: pd.DataFrame):
    required_columns = {"topic", "data"}
    columns = set(df.columns)
    missing_columns = required_columns - columns
    if missing_columns:
        raise DataFrameError(f"Some columns are missing : {missing_columns}")


def _set_timestamp_column(df: pd.DataFrame) -> pd.DataFrame:
    if "timestamp" in df:
        return df

    def _get_date_from_data_given_topic(series: pd.Series):
        date_path_in_data = topic_to_date_infos[series.topic].date_path_in_data
        return get_in_dict(date_path_in_data, series.data)

    df["timestamp"] = df.apply(_get_date_from_data_given_topic, axis=1)

    if df.timestamp.isnull().values.any():
        topics = set(df[df.timestamp.isna()].topic)
        expected_paths = [
            topic_to_date_infos[topic].date_path_in_data for topic in topics
        ]
        null_count = df.timestamp.isnull().sum()

        logger.warn(
            f"Some data are missing in event with topic in {topics}. Expected paths were {expected_paths}. Skipping those ({null_count}/{len(df)}). "
        )
        df = df.loc[~df.timestamp.isnull()]
        _raise_if_df_empty(df)
    return df


def _keep_known_topics_only(df: pd.DataFrame) -> pd.DataFrame:
    topic_options: List[TopicsToReplay] = cast(
        List[TopicsToReplay], external_topic_options
    ) + cast(
        List[TopicsToReplay],
        topic_adagio_event_options,
    )
    return df.loc[df.topic.isin(topic_options)]


def prepare_df_to_replay(df: pd.DataFrame) -> pd.DataFrame:
    df = deserialize_data_column(df)
    _has_required_columns(df)
    df = _keep_known_topics_only(df)
    df = _set_timestamp_column(df)
    df = _timestamps_is_datetime(df)
    df = _timestamps_is_monotonic(df)
    df = _nan_to_none(df)
    _raise_if_df_empty(df)
    return df


class ScheduledEventBus(ExternalEventBus):
    def __init__(
        self,
        clock: AbstractClock,
    ):
        super().__init__(clock=clock)

    @property
    def _keep_looping(self):
        return True

    @abstractmethod
    def start(self):
        raise NotImplementedError

    @abstractmethod
    async def _async_next(self):
        raise NotImplementedError

    async def async_loop(self):
        while self.clock.can_wake_up and self._keep_looping:
            await self._async_next()

    def get_events_between(
        self, start_date: DateStr = None, end_date: DateStr = None
    ) -> pd.DataFrame:
        logger.warn("'get_events_between' does nothing for this event Bus")
        return pd.DataFrame()

    def set_last_published_timestamp(self, date: DateStr):
        logger.warn("'set_last_published_timestamp' does nothing for this event Bus")


class RandomScheduledEventBus(ScheduledEventBus):
    def __init__(
        self,
        clock: AbstractClock,
        publisher: AbstractPublisher,
        *,
        make_event: Callable = make_vehicle_event,
        min_time_step: float,
        max_time_step: float,
    ):
        ScheduledEventBus.__init__(self, clock=clock)
        self.publisher = publisher

        self.make_event = make_event
        self.min_time_step = min_time_step
        self.max_time_step = max_time_step

    def start(self):
        pass

    async def _async_next(self):
        await self.clock.sleep(random.uniform(self.min_time_step, self.max_time_step))
        event = self.make_event(timestamp=self.clock.get_now())
        self.dispatch_event(event)

    def dispatch_event(self, event: ExternalEvent):
        self.publisher.publish_to_converters(event)


class ReplayedEventBus(ScheduledEventBus):
    def __init__(
        self,
        clock: AbstractClock,
        publisher: AbstractPublisher,
        *,
        csv_path: str = None,
        df: pd.DataFrame = None,
        time_step: float = 1.0,
        speed: float = 1,
        resync: bool = False,
        event_dict_to_event: Union[
            Callable[[Dict], Event], Callable[[Dict], ExternalEvent]
        ],
    ):
        ScheduledEventBus.__init__(self, clock)
        self.publisher = publisher
        self.df: pd.DataFrame
        if df is not None:
            df = df.copy()  # copy df to avoid mutating the input one
        else:
            if csv_path is None or not os.path.exists(csv_path):
                raise DataFrameError("Csv_path or DF should be provided")
            df = pd.read_csv(csv_path)

        self.df = prepare_df_to_replay(df)
        self.df.index = self.df.timestamp
        self.last_row_timestamp = self.df.index[-1]
        self.last_published_timestamp = self.df.timestamp[0]
        self.time_step = time_step
        self.speed = speed
        self.resync = resync
        self.step_index = 0
        self.timedelta_to_dispatch = pd.Timedelta(self.time_step * self.speed, unit="s")

        self.event_dict_to_event = event_dict_to_event

    def start(self):
        now = self.clock.get_now()
        self.offset = pd.Timestamp(now) - self.last_published_timestamp
        self._previous_now = to_datetime(now)

    async def _async_next(self):
        now = to_datetime(self.clock.get_now())
        ellapsed = now - self._previous_now
        logger.info(
            f"\n next step {self.step_index} - previous lasted : {ellapsed.total_seconds()} s"
        )
        self.step_index += 1
        if ellapsed.total_seconds() > self.time_step:
            logger.warn(
                f"\n --> --> Congestion ! You should set time_step greater than {ellapsed.total_seconds()} seconds. \n"
            )
        sleep_time = max(self.time_step - ellapsed.total_seconds(), 0)
        await self.clock.sleep(sleep_time)
        next_timestamp_begins = self.last_published_timestamp
        next_timestamp_ends = next_timestamp_begins + self.timedelta_to_dispatch
        rows_to_dispatch = self.df[next_timestamp_begins:next_timestamp_ends]

        logger.info(
            f" \n Dispatch {len(rows_to_dispatch)} events between {next_timestamp_begins} and {next_timestamp_ends}, in {self.time_step} seconds."
        )

        for _, series in rows_to_dispatch.iterrows():
            if self.resync:
                series["timestamp"] += self.offset
                # date_path_in_data = topic_to_date_infos[series.topic].date_path_in_data
                # date_format_in_data = topic_to_date_infos[
                #     series.topic
                # ].date_format_in_data
                # formatted_date_in_data = to_string(
                #     from_timestamp(series["timestamp"]), date_format_in_data
                # )
                # set_in_dict(date_path_in_data, series.data, formatted_date_in_data)
            self.dispatch_event(series)
        self._previous_now = now
        self.last_published_timestamp = next_timestamp_ends

    @property
    def _keep_looping(self) -> bool:
        return self.last_row_timestamp > self.last_published_timestamp

    @staticmethod
    def _to_timestamp_or_default(
        date: Union[DateStr, None], default: pd.Timestamp
    ) -> pd.Timestamp:
        if date:
            return to_timestamp(date)
        return default

    def get_events_between(
        self, start_date: DateStr = None, end_date: DateStr = None
    ) -> pd.DataFrame:
        start_date = self._to_timestamp_or_default(start_date, self.df.timestamp.min())
        end_date = self._to_timestamp_or_default(end_date, self.df.timestamp.max())
        rows = self.df.set_index("timestamp")[start_date:end_date].reset_index()
        return rows

    def set_last_published_timestamp(self, date: DateStr):
        self.last_published_timestamp = to_timestamp(date)

    def dispatch_event(self, event_series: pd.Series):
        topic: CoverOpsDomainTopic = event_series.topic
        topics = [*external_topic_options, *topic_adagio_event_options]
        if topic in topics:
            event_series.pop("timestamp")
            try:
                event = self.event_dict_to_event(event_series.to_dict())
                self.publisher.publish_to_converters(event)
            except Exception as e:
                logger.warn("\n \n Error: ", e)
                logger.warn(
                    f"\n Could not replay event with topic {topic} and data ",
                    event_series.data,
                )
