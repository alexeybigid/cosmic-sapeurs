import abc
from abc import abstractclassmethod
from typing import Tuple

from adapters.scheduled_event_bus import ScheduledEventBus
from domain.ports.publisher import AbstractPublisher

from shared.helpers.clock import AbstractClock


class AbstractScheduledEventBuses(ScheduledEventBus, abc.ABC):
    def __init__(
        self,
        clock: AbstractClock,
        vehicle_publisher: AbstractPublisher,
        operation_publisher: AbstractPublisher,
        **kwargs,
    ):
        super().__init__(clock)

        (
            self.vehicle_events_bus,
            self.operation_events_bus,
        ) = self._instantiate_event_buses(
            clock=clock,
            vehicle_publisher=vehicle_publisher,
            operation_publisher=operation_publisher,
            **kwargs,
        )

    @abstractclassmethod
    def _instantiate_event_buses(
        self,
        *,
        clock=AbstractClock,
        vehicle_publisher: AbstractPublisher,
        operation_publisher: AbstractPublisher,
        **kwargs,
    ) -> Tuple[ScheduledEventBus, ScheduledEventBus]:
        raise NotImplementedError

    def start(self):
        self.vehicle_events_bus.start()
        self.operation_events_bus.start()

    async def _async_next(self):
        await self.vehicle_events_bus._async_next()
        await self.operation_events_bus._async_next()

    @property
    def _keep_looping(self) -> bool:
        return (
            self.vehicle_events_bus._keep_looping
            or self.operation_events_bus._keep_looping
        )
