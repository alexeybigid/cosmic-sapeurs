import asyncio

from entrypoints.config import Config
from entrypoints.environment_variables import get_env_variables

config = Config(env=get_env_variables())

replayed_event_bus = config.replayed_event_bus

loop = asyncio.get_event_loop()


def main():
    replayed_event_bus.start()
    loop.create_task(replayed_event_bus.async_loop())
    loop.run_forever()


if __name__ == "__main__":
    # execute only if run as a script
    main()
