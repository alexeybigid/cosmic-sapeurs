from typing import Callable, Dict, Union

from adapters.adagio_publishers import AdagioPublishers
from adapters.http_publisher import HttpPublisher
from adapters.in_memory_publisher import InMemoryPublisher
from adapters.random_event_buses import RandomEventBuses
from adapters.replayed_event_buses import ReplayedEventBuses
from adapters.sapeurs_publishers import SapeursPublishers
from adapters.scheduled_event_bus import ReplayedEventBus, ScheduledEventBus
from domain.ports.publisher import AbstractPublisher
from entrypoints.environment_variables import (
    EnvironmentVariables,
    PublisherToConvertersOption,
    ScheduledEventBusOption,
)

from shared.adagio_event_dict_to_event import adagio_event_dict_to_event
from shared.factories.adagio.adagio_intervention_event_factory import (
    make_adagio_intervention_created_event,
)
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event,
)
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.clock import RealClock
from shared.helpers.logger import logger
from shared.routes import route_external_operation_event, route_external_vehicle_event
from shared.sapeurs_event_dict_to_event import sapeurs_event_dict_to_event

clock = RealClock()
replay_speed = 30


def prepare_env_var_to_scheduled_event_bus(
    vehicle_publisher: AbstractPublisher, operation_publisher: AbstractPublisher
) -> Dict[
    ScheduledEventBusOption,
    Union[Callable[[], ReplayedEventBus], Callable[[], RandomEventBuses]],
]:
    env_var_to_scheduled_event_bus: Dict[
        ScheduledEventBusOption,
        Union[Callable[[], ReplayedEventBus], Callable[[], RandomEventBuses]],
    ] = dict(
        RANDOM_SAPEURS=lambda: RandomEventBuses(
            clock=clock,
            min_time_step=0.1,
            max_time_step=0.5,
            make_operation_event=make_operation_event,
            make_vehicle_event=make_vehicle_event,
            vehicle_publisher=vehicle_publisher,
            operation_publisher=operation_publisher,
        ),
        RANDOM_ADAGIO=lambda: RandomEventBuses(
            clock=clock,
            min_time_step=0.1,
            max_time_step=0.5,
            make_operation_event=make_adagio_intervention_created_event,
            make_vehicle_event=make_adagio_vehicle_event,
            vehicle_publisher=vehicle_publisher,
            operation_publisher=operation_publisher,
        ),
        REPLAYED_SAPEURS=lambda: ReplayedEventBuses(
            clock=clock,
            publisher=SapeursPublishers(
                vehicle_publisher=vehicle_publisher,
                intervention_publisher=operation_publisher,
            ),
            vehicle_csv_path="./data/vehicle_events_to_replay.csv",
            operation_csv_path="./data/operation_events_to_replay.csv",
            time_step=4.0,
            resync=False,
            speed=replay_speed,
            event_dict_to_event=sapeurs_event_dict_to_event,
        ),
        REPLAYED_ADAGIO=lambda: ReplayedEventBuses(
            clock=clock,
            publisher=AdagioPublishers(
                vehicle_publisher=vehicle_publisher,
                intervention_publisher=operation_publisher,
            ),
            vehicle_csv_path="./data/adagio_vehicle_events_to_replay.csv",
            operation_csv_path="./data/adagio_intervention_events_to_replay.csv",
            time_step=4.0,
            resync=False,
            speed=replay_speed,
            event_dict_to_event=adagio_event_dict_to_event,
        ),
    )

    return env_var_to_scheduled_event_bus


class Config:
    replayed_event_bus: ScheduledEventBus

    def __init__(self, env: EnvironmentVariables) -> None:

        self.ENV = env
        logger.info(self.ENV)

        env_var_to_vehicle_publisher: Dict[
            PublisherToConvertersOption, Callable[[], AbstractPublisher]
        ] = dict(
            IN_MEMORY=lambda: InMemoryPublisher(),
            HTTP=lambda: HttpPublisher(
                f"{self.ENV.converters_url}/{route_external_vehicle_event}"
            ),
        )

        env_var_to_operation_publisher: Dict[
            PublisherToConvertersOption, Callable[[], AbstractPublisher]
        ] = dict(
            IN_MEMORY=lambda: InMemoryPublisher(),
            HTTP=lambda: HttpPublisher(
                f"{self.ENV.converters_url}/{route_external_operation_event}"
            ),
        )

        vehicle_publisher = env_var_to_vehicle_publisher[
            self.ENV.publisher_to_converters
        ]()
        operation_publisher = env_var_to_operation_publisher[
            self.ENV.publisher_to_converters
        ]()

        self.replayed_event_bus = prepare_env_var_to_scheduled_event_bus(
            vehicle_publisher, operation_publisher
        )[self.ENV.scheduled_event_bus]()
