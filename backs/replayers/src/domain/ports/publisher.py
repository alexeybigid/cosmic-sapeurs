import abc

from shared.external_event import ExternalEvent


class AbstractPublisher(abc.ABC):
    @abc.abstractmethod
    def publish_to_converters(self, event: ExternalEvent) -> None:
        """Publish event to converters app"""
        raise NotImplementedError
