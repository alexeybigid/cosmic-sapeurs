import ast
import json
from collections import defaultdict

import pandas as pd

from shared.exceptions import InvalidEventFormatError

df_intervention = pd.read_csv(
    "/Users/raph/Code/op18-enki/cosmic-sapeurs/backs/replayers/data/adagio_intervention_events_to_replay.csv"
)
df_vehicle = pd.read_csv(
    "/Users/raph/Code/op18-enki/cosmic-sapeurs/backs/replayers/data/adagio_vehicle_events_to_replay.csv"
)
df_intervention.data = df_intervention.data.map(ast.literal_eval)
df_vehicle.data = df_vehicle.data.map(ast.literal_eval)

from shared.adagio_event_dict_to_event import (
    adagio_intervention_event_as_dict_to_event,
    adagio_vehicle_event_as_dict_to_event,
)

# vehicle errors
vehicle_errors = defaultdict(lambda: 0.0)
vehicle_errors["total"] = len(df_vehicle)
for _, s in df_vehicle.iterrows():
    try:
        adagio_vehicle_event_as_dict_to_event(s.to_dict())
    except InvalidEventFormatError as e:
        vehicle_errors[str(e)] += 1
with open("vehicle-errors.json", "w") as file:
    json.dump(vehicle_errors, file)

# intervention errors
intervention_errors = defaultdict(lambda: 0.0)
intervention_errors["total"] = len(df_intervention)
for _, s in df_intervention.iterrows():
    try:
        adagio_intervention_event_as_dict_to_event(s.to_dict())
    except InvalidEventFormatError as e:
        intervention_errors[str(e)] += 1
with open("intervention-errors.json", "w") as file:
    json.dump(intervention_errors, file)


breakpoint()
