# Replayers 🇬🇧

This service allows us to "replay" events from CSV files, in order to mock the external system. 
We can choose the replay speed. Note that if the converters storage is in CSV mode, the files to replay are directly the stored CSV files. 

## Pre-Requisite

Go to the right folder: 
```
cd ./backs/replayers 

```
- Python 3
- CSV files with vehicle and operation events to replay, that should be placed in `data/adagio_vehicle_events_to_replay.csv` and `data/adagio_intervention_events_to_replay.csv`.
You may also use the sample ones, by copying them: 

```
cp data_sample/adagio_vehicle_events_to_replay.sample.csv data/adagio_vehicle_events_to_replay.csv

cp data_sample/adagio_intervention_events_to_replay.sample.csv data/adagio_intervention_events_to_replay.csv
```

## Install 

Create a virtual environment, install the packages and initialize a .env file. 
```
python3 -m venv venv 
source venv/bin/activate 
pip install -r requirements.txt
pip install ../shared
pip install -e ./src
cp .env.sample .env 
```

## Launch 
```
python src/entrypoints/server.py
```

## Tests
```
pytest tests 
``` 