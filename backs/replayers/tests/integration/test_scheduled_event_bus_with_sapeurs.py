import asyncio
from typing import Dict

import pytest
from adapters.in_memory_publisher import InMemoryPublisher
from adapters.scheduled_event_bus import DataFrameError, ReplayedEventBus
from pandas.api.types import is_datetime64_any_dtype as is_datetime
from tests.utils.create_df_events_to_dispatch import create_csv_events_to_dispatch

from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.clock import CustomClock
from shared.helpers.date import DateStr
from shared.sapeurs_events import VehicleEvent

csv_path = "tests/integration/temp_data/events_to_dispatch.csv"


event_dict_to_vehicle_event = VehicleEvent.from_dict

# def event_dict_to_operation_event(event_as_dict: Dict) -> OperationEvent:
#     return OperationEvent.from_dict(event_as_dict)


def test_fails_if_csv_not_found():
    clock = CustomClock()
    with pytest.raises(DataFrameError) as e:
        ReplayedEventBus(
            clock,
            publisher=InMemoryPublisher(),
            csv_path="notfound/file.csv",
            event_dict_to_event=event_dict_to_vehicle_event,
        )
    assert str(e.value) == "Csv_path or DF should be provided"

    with pytest.raises(DataFrameError) as e2:
        ReplayedEventBus(
            clock,
            publisher=InMemoryPublisher(),
            event_dict_to_event=event_dict_to_vehicle_event,
        )
    assert str(e2.value) == "Csv_path or DF should be provided"


def test_fails_if_csv_format_is_incorrect():
    clock = CustomClock()
    timestamps = [
        DateStr("2020-10-01T12:01:00.000000"),
        DateStr("2020-10-01T12:01:03.000000"),
        DateStr("2020-10-01T12:00:00.000000"),
    ]
    df_without_timestamp_in_data = create_csv_events_to_dispatch(
        csv_path, timestamps, make_event=make_vehicle_event
    )

    def del_timestamp(data: Dict) -> Dict:
        del data["timestamp"]
        return data

    df_without_timestamp_in_data.data = df_without_timestamp_in_data.data.apply(
        del_timestamp
    )
    with pytest.raises(DataFrameError) as e3:
        ReplayedEventBus(
            clock,
            publisher=InMemoryPublisher(),
            df=df_without_timestamp_in_data,
            event_dict_to_event=event_dict_to_vehicle_event,
        )
    assert str(e3.value) == "No event to replay. Check warnings."

    df_with_timestamp_as_str = create_csv_events_to_dispatch(
        csv_path,
        timestamps=["2020-10-01T12:01:00.000000"],
        make_event=make_vehicle_event,
    )
    event_bus = ReplayedEventBus(
        clock,
        publisher=InMemoryPublisher(),
        df=df_with_timestamp_as_str,
        event_dict_to_event=event_dict_to_vehicle_event,
    )
    assert is_datetime(event_bus.df.timestamp)
    assert event_bus.df.timestamp.is_monotonic

    timestamp_with_wrong_format_df = create_csv_events_to_dispatch(
        csv_path,
        timestamps=["wrong_timestamp", "2020-10-01T12:01:00.000000"],
        make_event=make_vehicle_event,
    )
    event_bus = ReplayedEventBus(
        clock,
        publisher=InMemoryPublisher(),
        df=timestamp_with_wrong_format_df,
        event_dict_to_event=event_dict_to_vehicle_event,
    )
    assert len(event_bus.df) == 1


def prepare_event_bus_and_spy(
    resync: bool, time_step: float, speed: float, clock: CustomClock = None
):
    timestamps = [
        DateStr("2020-10-01T12:01:00.000000"),
        DateStr("2020-10-01T12:01:03.000000"),
        DateStr("2020-10-01T12:00:00.000000"),
    ]

    df = create_csv_events_to_dispatch(
        csv_path, timestamps, make_event=make_vehicle_event
    )
    clock = clock or CustomClock()

    publisher = InMemoryPublisher()

    event_bus = ReplayedEventBus(
        clock,
        df=df,
        resync=resync,
        time_step=time_step,
        speed=speed,
        publisher=publisher,
        event_dict_to_event=event_dict_to_vehicle_event,
    )
    published_events = publisher.published_events

    event_bus.start()
    return event_bus, published_events, clock


async def _async_test_one_step(
    event_bus, published_events, expected_dispatched_events_timestamps
):
    # dispatch first event
    clock = event_bus.clock
    awaken_event = asyncio.Event()
    looping_task = asyncio.create_task(event_bus.async_loop())
    clock.set_awaken_event(awaken_event)
    await clock.wake_up()
    await looping_task
    assert [
        event.data.timestamp for event in published_events
    ] == expected_dispatched_events_timestamps


def test_provided_event_get_dispatched_correctly():

    speed = 1
    time_step = 1
    resync = False

    event_bus, published_events, _ = prepare_event_bus_and_spy(resync, time_step, speed)

    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            [
                "2020-10-01T12:00:00.000000",
            ],
        )
    )

    # Loop 60 times, ie : cursor should advance of 60 seconds
    for k in range(58):
        asyncio.run(
            _async_test_one_step(
                event_bus,
                published_events,
                [
                    "2020-10-01T12:00:00.000000",
                ],
            )
        )

    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            [
                "2020-10-01T12:00:00.000000",
                "2020-10-01T12:01:00.000000",
            ],
        )
    )


def test_streaming_speed():
    speed = 60
    time_step = 1
    resync = False

    event_bus, published_events, clock = prepare_event_bus_and_spy(
        resync, time_step, speed
    )

    clock.add_seconds(1)  # equivalent to 60 seconds

    expected_dispatched_events_timestamps = [
        "2020-10-01T12:00:00.000000",
        "2020-10-01T12:01:00.000000",
    ]
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )


# TODO : Decide if we get rid of resync for adagio ?
# => The only problem is when setting the date back in the event data after having resync
# def test_resync():
#     speed = 1
#     time_step = 1
#     resync = True

#     test_start_time = DateStr("2020-10-02T15:00:00.000000")

#     clock = CustomClock(test_start_time)
#     event_bus, published_events, clock = prepare_event_bus_and_spy(
#         resync, time_step, speed, clock=clock
#     )
#     clock.add_seconds(1)

#     expected_dispatched_events_timestamps = [test_start_time]
#     asyncio.run(
#         _async_test_one_step(
#             event_bus,
#             published_events,
#             expected_dispatched_events_timestamps,
#         )
#     )
