# Converters 🇫🇷 

Ce service vise à convertir les données brutes (provenant du système externe) au format sapeurs. Il s'agit d'une application Python indépendante, déployée donc indépendamment de Sapeurs-Backend. Les convertisseurs stockent les événements bruts avant de les convertir et de les envoyer à Sapeurs-Backend.

## Prérequis

Aller dans le dossier : 
```
cd backs/replayers
```

- Python 3
- Les fichiers CSV avec les évènements à rejouer doivent être :  `data/adagio_vehicle_events_to_replay.csv` et `data/adagio_intervention_events_to_replay.csv`.
Vous pouvez copier les fichiers d'exemple avec la commande suivante : 

```
cp data_sample/adagio_vehicle_events_to_replay.sample.csv data/adagio_vehicle_events_to_replay.csv

cp data_sample/adagio_intervention_events_to_replay.sample.csv data/adagio_intervention_events_to_replay.csv
```

## Installation 

Créer un environnement virtuel, installez les pacquets et initialisez le .env.  
```
python3 -m venv venv 
source venv/bin/activate 
pip install -r requirements.txt
pip install ../shared
pip install -e .
cp .env.sample .env 
```


## Lever le docker-compose de test et vérifier que les tests passent ! 
```
docker-compose -f ../../docker-compose.test.yml up --build 
pytest tests 
``` 
## Lancer l'application  
```
python converters/entrypoints/server.py
```
