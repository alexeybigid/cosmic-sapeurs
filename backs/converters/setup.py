from setuptools import find_packages, setup

packages = find_packages(exclude=[])

setup(
    name="converters",
    version="0.1",
    packages=packages,
)
