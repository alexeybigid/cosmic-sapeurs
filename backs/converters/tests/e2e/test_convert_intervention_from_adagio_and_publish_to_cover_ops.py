import copy
import json
from dataclasses import asdict

import pytest
from tests.utils.test_pg_url import test_pg_url

from converters.entrypoints.config import Config

# from tests.e2e.adagio_server_fixture import make_adagio_server_fixture
from converters.entrypoints.environment_variables import (
    EnvironmentVariables,
    Repositories,
)
from converters.entrypoints.server import (
    make_app,
    route_external_operation_event,
    route_external_vehicle_event,
)
from shared.factories.adagio.adagio_intervention_event_factory import (
    make_adagio_intervention_changed_status_event,
    make_adagio_intervention_created_event,
)
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event,
)

test_env = EnvironmentVariables(
    external_events_source="ADAGIO",
    publisher_to_cover_ops="IN_MEMORY",
    cover_ops_url="",
    converters_port=8081,
    external_system_gateway="CUSTOM",
    external_system_gateway_vehicles_url="foo:8080",
    repositories="PANDAS",
    pg_url=test_pg_url,
)

config = Config(test_env)


@pytest.fixture
def adagio_server_fixture(loop, aiohttp_client):
    """Start server for end-to-end tests"""
    app, loop, _ = make_app(config)
    return loop.run_until_complete(aiohttp_client(app))


async def test_convert_from_adagio_operation_events(
    adagio_server_fixture,
):
    # reset publisher events
    config.publisher.__init__()

    # Intervention created : Save and publish
    adagio_operation_just_opened_event_as_dict = asdict(
        make_adagio_intervention_created_event()
    )
    resp = await adagio_server_fixture.post(
        route_external_operation_event,
        data=json.dumps(adagio_operation_just_opened_event_as_dict),
    )
    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}

    assert len(config.publisher.published_events) == 1  # type: ignore (AbstractPublisher does not have this property)
    assert len(config.raw_operation_events_repo.df) == 1  # type: ignore (AbstractEventsRepo does not have this property)

    # Intervention changed status : Save but do not publish
    adagio_operation_changed_status_event_as_dict = asdict(
        make_adagio_intervention_changed_status_event()
    )

    resp = await adagio_server_fixture.post(
        route_external_operation_event,
        data=json.dumps(adagio_operation_changed_status_event_as_dict),
    )
    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}

    assert len(config.publisher.published_events) == 1  # type: ignore (AbstractPublisher does not have this property)
    assert len(config.raw_operation_events_repo.df) == 2  # type: ignore (AbstractEventsRepo does not have this property)
