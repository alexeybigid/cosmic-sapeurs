import ast
import json

import pytest

from converters.entrypoints.config import Config
from converters.entrypoints.environment_variables import EnvironmentVariables
from converters.entrypoints.server import (
    make_app,
    route_external_operation_event,
    route_external_vehicle_event,
)

test_env = EnvironmentVariables(
    external_events_source="SAPEURS",
    publisher_to_cover_ops="IN_MEMORY",
    cover_ops_url="",
    converters_port=8081,
    external_system_gateway="CUSTOM",
    external_system_gateway_vehicles_url="foo:8080",
    repositories="PANDAS",
)


config = Config(test_env)


@pytest.fixture
def identity_server_fixture(loop, aiohttp_client):
    """Start server for end-to-end tests"""

    app, loop, _ = make_app(config)
    return loop.run_until_complete(aiohttp_client(app))


async def test_convert_with_identity_and_publish_vehicle_event_to_cover_ops(
    identity_server_fixture,
):
    external_vehicle_event_as_dict = {
        "data": {
            "raw_vehicle_id": "1",
            "raw_operation_id": "3",
            "role": "vsav_solo",
            "raw_status": "parti",
            "status": "departed_to_intervention",
            "timestamp": "2020-12-10T12:30:00.853",
            "home_area": "STOU",
            "vehicle_name": "VSAV 201",
        },
        "uuid": "some_vehicle_id",
        "topic": "vehicle_changed_status",
    }

    resp = await identity_server_fixture.post(
        route_external_vehicle_event, data=json.dumps(external_vehicle_event_as_dict)
    )
    resp_json = await resp.json()

    assert resp.status == 200
    assert resp_json == {"success": "true"}


async def test_fails_when_no_body_is_provided(identity_server_fixture):
    resp = await identity_server_fixture.post(route_external_vehicle_event)

    assert resp.status == 400
    resp_json = await resp.json()
    assert resp_json == {"success": "false", "reason": "Missing body"}


async def test_fails_when_event_without_data_is_provided(identity_server_fixture):
    wrong_external_event = {"topic": "some_topic"}
    resp = await identity_server_fixture.post(
        route_external_vehicle_event, data=json.dumps(wrong_external_event)
    )

    assert resp.status == 400
    resp_json = await resp.json()
    assert resp_json["success"] == "false"
    assert ast.literal_eval(resp_json["reason"]) == ["Missing keys {'data'}"]


async def test_fails_when_event_with_wrong_topic_is_provided(identity_server_fixture):
    wrong_external_event = {"topic": "some_wrong_topic", "data": {}}
    resp = await identity_server_fixture.post(
        route_external_vehicle_event, data=json.dumps(wrong_external_event)
    )

    assert resp.status == 400
    resp_json = await resp.json()
    assert resp_json["success"] == "false"
    reason = ast.literal_eval(resp_json["reason"])
    assert reason["topic"] == ["Must be equal to vehicle_changed_status."]


async def test_fails_with_identity_when_event_with_wrong_data_is_provided(
    identity_server_fixture,
):
    wrong_external_event = {
        "data": {"raw_vehicle_id": "1"},
        "uuid": "some_vehicle_id",
        "topic": "vehicle_changed_status",
    }
    resp = await identity_server_fixture.post(
        route_external_vehicle_event, data=json.dumps(wrong_external_event)
    )
    assert resp.status == 400
    resp_json = await resp.json()

    assert resp_json["success"] == "false"
    assert ast.literal_eval(resp_json["reason"]) == {
        "data": {
            "raw_status": ["Missing data for required field."],
            "status": ["Missing data for required field."],
            "role": ["Missing data for required field."],
            "home_area": ["Missing data for required field."],
            "vehicle_name": ["Missing data for required field."],
        }
    }


async def test_convert_with_identity_and_publish_operation_event_to_cover_ops(
    identity_server_fixture,
):
    external_operation_event_as_dict = {
        "topic": "operation_changed_status",
        "uuid": "some_operation_id",
        "data": {
            "timestamp": "",
            "raw_operation_id": "2",
            "status": "opened",
            "address": "161 rue guillaume tell",
            "address_area": "CHPT",
            "cause": "victim",
            "raw_cause": "Feu de ...",
        },
    }
    resp = await identity_server_fixture.post(
        route_external_operation_event,
        data=json.dumps(external_operation_event_as_dict),
    )
    assert resp.status == 200
    resp_json = await resp.json()
    assert resp_json == {"success": "true"}
