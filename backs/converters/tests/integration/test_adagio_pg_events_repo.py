from sqlalchemy.engine import create_engine
from sqlalchemy.sql import select
from tests.utils.test_pg_url import test_pg_url

from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)
from converters.adapters.adagio.postgres.db import (
    initialize_db,
    raw_intervention_events_table,
    reset_db,
)


def prepare_db():
    engine = create_engine(
        test_pg_url,
        isolation_level="REPEATABLE READ",
    )
    reset_db(engine)
    initialize_db(engine)
    return engine


def test_adagio_pg_events_repo_for_serializable_event():
    engine = prepare_db()
    repo = AdagioPgEventsRepository(
        engine=engine,
        table=raw_intervention_events_table,
    )
    event_1 = dict(
        topic="foo_topic",
        data={
            "idEvent": 1,
            "status": {"id": 1, "label": "PARTI"},
            "timeGMT": "2020/01/03 23:03",
        },
    )

    repo.add(event_1)

    s = select([raw_intervention_events_table])
    records = engine.connect().execute(s).fetchall()

    assert len(records) == 1
    assert records[0][0] == "foo_topic"
    assert (
        records[0][1]
        == '{"idEvent": 1, "status": {"id": 1, "label": "PARTI"}, "timeGMT": "2020/01/03 23:03"}'
    )


# def test_adagio_csv_events_repo_for_unserializable_event():
#     now = "2020-12-15T11:30"
#     suffix = "test_error"
#     filename = f"{now}_{suffix}.csv"
#     csv_path = os.path.join(path, filename)
#     repo = AdagioCsvEventsRepository(now=DateStr(now), suffix=suffix, path=path)

#     class FooClass:
#         pass

#     unserializable_class = FooClass()
#     event = dict(
#         topic="foo_topic",
#         data={
#             "wrong_field": unserializable_class,
#         },
#     )

#     repo.add(event)
#     df = pd.read_csv(csv_path, delimiter=";")
#     assert df.data.values[0] == "Object of type FooClass is not JSON serializable"
