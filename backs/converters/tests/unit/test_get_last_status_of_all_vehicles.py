import asyncio

from tests.utils.converted_vehicle_samples import (
    example_adagio_vehicle_event_0,
    example_adagio_vehicle_event_0_converted,
    example_adagio_vehicle_event_1,
    example_adagio_vehicle_event_1_converted,
)

from converters.adapters.adagio.adagio_vehicle_event_mapper import (
    AdagioVehicleEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_events_repository import (
    InMemoryAdagioVehicleEventsRepository,
)
from converters.adapters.in_memory_publisher import InMemoryPublisher
from converters.domain.ports.external_system_gateway import CustomExternalSystemGateway
from converters.domain.use_cases.get_last_status_of_all_vehicles import (
    GetLastStatusOfAllVehicles,
)
from shared.helpers.uuid import CustomUuid
from shared.sapeurs_events import ConvertersVehiclesInitEvent


async def _async_test_get_last_status_of_all_vehicles():
    mapper_uuid = CustomUuid()
    mapper = AdagioVehicleEventMapper(uuid=mapper_uuid)
    external_sytem_gateway = CustomExternalSystemGateway()
    adagio_vehicle_event_repo = InMemoryAdagioVehicleEventsRepository()
    publisher = InMemoryPublisher()
    use_case_uuid = CustomUuid()
    get_last_status_of_all_vehicles = GetLastStatusOfAllVehicles(
        external_system_gateway=external_sytem_gateway,
        mapper=mapper,
        raw_vehicle_events_repo=adagio_vehicle_event_repo,
        publisher=publisher,
        uuid=use_case_uuid,
    )

    adagio_events_as_dict = [
        example_adagio_vehicle_event_0,
        example_adagio_vehicle_event_1,
    ]
    external_sytem_gateway.set_last_status_of_all_vehicles(adagio_events_as_dict)

    use_case_uuid.set_next_uuid("GetLastStatusOfAllVehicles_uuid")
    mapper_uuid.set_next_uuids(
        [
            example_adagio_vehicle_event_0_converted.uuid,
            example_adagio_vehicle_event_1_converted.uuid,
        ]
    )

    expected_event = ConvertersVehiclesInitEvent(
        data=[
            example_adagio_vehicle_event_0_converted,
            example_adagio_vehicle_event_1_converted,
        ],
        uuid="GetLastStatusOfAllVehicles_uuid",
    )

    await get_last_status_of_all_vehicles.execute()

    assert publisher.published_events == [expected_event]
    assert adagio_vehicle_event_repo.adagio_events[0] == {
        "topic": "provided_last_status_of_all_vehicles",
        "data": adagio_events_as_dict,
    }


def test_get_last_status_of_all_vehicles():
    asyncio.run(_async_test_get_last_status_of_all_vehicles())
