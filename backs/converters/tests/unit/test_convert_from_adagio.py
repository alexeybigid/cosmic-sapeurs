from typing import Optional

import pytest
from tests.utils.converted_vehicle_samples import example_adagio_vehicle_event_0

from converters.adapters.adagio.adagio_intervention_events_repository import (
    InMemoryAdagioInterventionEventsRepository,
)
from converters.adapters.adagio.adagio_operation_event_mapper import (
    AdagioOperationEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_event_mapper import (
    AdagioVehicleEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_events_repository import (
    InMemoryAdagioVehicleEventsRepository,
)
from converters.adapters.in_memory_publisher import InMemoryPublisher
from converters.domain.ports.accumulated_events_repo import (
    InMemoryAccumulatedEventsRepository,
)
from converters.domain.use_cases.convert_to_sapeurs_event import ConvertToSapeursEvent
from shared.adagio_intervention_events import AdagioInterventionEvent
from shared.exceptions import InvalidEventFormatError
from shared.factories.adagio.adagio_intervention_event_factory import (
    make_adagio_intervention_changed_status_event,
    make_adagio_intervention_created_event,
    make_adagio_intervention_reinforced_event,
)
from shared.factories.adagio.adagio_vehicle_event_factory import (
    make_adagio_vehicle_event,
)
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.date import DateStr
from shared.helpers.uuid import AbstractUuid, CustomUuid
from shared.sapeurs_events import OperationEvent, VehicleEvent


def prepare_convert_to_sapeurs_event(
    vehicle_mapper_uuid: Optional[AbstractUuid] = None,
    operation_mapper_uuid: Optional[AbstractUuid] = None,
):
    publisher = InMemoryPublisher()
    raw_vehicle_events_repo = InMemoryAdagioVehicleEventsRepository()
    raw_operation_events_repo = InMemoryAdagioInterventionEventsRepository()
    accumulated_events_repo = InMemoryAccumulatedEventsRepository()
    convert_to_sapeurs_event = ConvertToSapeursEvent(
        raw_vehicle_events_repo=raw_vehicle_events_repo,
        vehicle_mapper=AdagioVehicleEventMapper(
            uuid=vehicle_mapper_uuid or CustomUuid()
        ),
        operation_mapper=AdagioOperationEventMapper(
            uuid=operation_mapper_uuid or CustomUuid()
        ),
        publisher=publisher,
        accumulated_events_repo=accumulated_events_repo,
        raw_operation_events_repo=raw_operation_events_repo,
        uuid=CustomUuid(),
    )
    return (
        convert_to_sapeurs_event,
        publisher,
        raw_vehicle_events_repo,
        raw_operation_events_repo,
        accumulated_events_repo,
    )


def test_convert_correct_adagio_vehicle_event_to_sapeurs():
    vehicle_uuid = CustomUuid()

    (
        convert_to_sapeurs_event,
        publisher,
        raw_vehicle_events_repo,
        _,
        _,
    ) = prepare_convert_to_sapeurs_event(vehicle_mapper_uuid=vehicle_uuid)

    raw_adagio_vehicle_event = make_adagio_vehicle_event(
        id_mma=1,
        abrege_famille_mma_operationnelle="VRM",
        libelle_statut_operationnel="Sélection",
        abrege_statut_operationnel="S",
        timestamp="2020-12-09T17:32:18.897",
        id_intervention=0,
        libelle_affectation_operationnelle="LAND",
        immatriculation_bspp="VRM 102",
    )
    raw_adagio_vehicle_event_as_dict = raw_adagio_vehicle_event.as_dict()
    expected_uuid = "my_expected_uuid"
    vehicle_uuid.set_next_uuid(expected_uuid)

    converted_event = convert_to_sapeurs_event.execute_for_raw_vehicle_event(
        raw_adagio_vehicle_event_as_dict
    )

    expected_converted_event = make_vehicle_event(
        uuid=expected_uuid,
        # Following args are for make_vehicle_event_data
        timestamp=DateStr("2020-12-09T17:32:18.897"),
        raw_vehicle_id="1",
        raw_operation_id=None,
        status="selected",
        raw_status="Sélection",
        home_area="LAND",
        role="other",
        vehicle_name="VRM 102",
    )

    published_events = publisher.published_events

    assert len(raw_vehicle_events_repo.adagio_events) == 1
    assert raw_vehicle_events_repo.adagio_events[0] == raw_adagio_vehicle_event_as_dict
    assert len(published_events) == 1
    converted_event = published_events[0]
    assert converted_event == expected_converted_event


def test_convert_adagio_sdis_vehicle_event_to_sapeurs():
    (convert_to_sapeurs_event, publisher, _, _, _) = prepare_convert_to_sapeurs_event()

    raw_adagio_vehicle_event = make_adagio_vehicle_event(
        id_mma=1,
        abrege_famille_mma_operationnelle="VRM",
        abrege_statut_operationnel="S",
        timestamp="2020-12-09T17:32:18.897",
        id_intervention=0,
        libelle_affectation_operationnelle="MONDE",
    )
    raw_adagio_vehicle_event_as_dict = raw_adagio_vehicle_event.as_dict()
    convert_to_sapeurs_event.execute_for_raw_vehicle_event(
        raw_adagio_vehicle_event_as_dict
    )
    published_events = publisher.published_events
    assert published_events[0].data.home_area == "SDIS"


def test_convert_correct_adagio_intervention_event_to_sapeurs():
    # operation opened
    raw_adagio_intervention_created_event = make_adagio_intervention_created_event(
        motif="111",
        id_intervention=1,
        libelle_motif="Feu de ...",
        etat_intervention="09/12/2020 17:35:24",
        cstc="STOU",
        address="169 Bd de la Villette 75010 Paris",
        adagio_procedure="R",
    )
    expected_operation_uuid = "my_expected_operation_uuid"

    expected_sapeurs_intervention_created_event = make_operation_event(
        uuid=expected_operation_uuid,
        timestamp=DateStr("2020-12-09T17:35:24.000000"),
        raw_operation_id="1",
        status="opened",
        address="169 Bd de la Villette 75010 Paris",
        address_area="STOU",
        cause="fire",
        raw_cause="Feu de ...",
        procedure="red",
    )

    assert_adagio_operation_event_correctly_converted_to_sapeurs(
        raw_adagio_intervention_created_event,
        expected_sapeurs_intervention_created_event,
        expected_operation_uuid=expected_operation_uuid,
        expected_to_be_published=True,
    )
    # operation validated
    raw_adagio_intervention_validated_event = (
        make_adagio_intervention_changed_status_event(
            id_intervention=1,
            libelle_statut_intervention="Validation manuelle",
            groupe_horaire_modification_statut="2020-12-15T10:38:23.4534206+01:00",
        )
    )

    expected_sapeurs_intervention_validated_event = make_operation_event(
        uuid=expected_operation_uuid,
        timestamp=DateStr("2020-12-15T10:38:23.453420"),
        raw_operation_id="1",
        status="validated",
        address=None,
        address_area=None,
        cause=None,
        raw_cause=None,
    )

    assert_adagio_operation_event_correctly_converted_to_sapeurs(
        raw_adagio_intervention_validated_event,
        expected_sapeurs_intervention_validated_event,
        expected_operation_uuid=expected_operation_uuid,
        expected_to_be_published=False,
    )

    # intervention reinforced
    raw_adagio_intervention_reinforced_event = (
        make_adagio_intervention_reinforced_event(
            motif="111",
            id_intervention=1,
            libelle_motif="Feu de ...",
            etat_intervention="09/12/2020 17:35:24",
            cstc="STOU",
            address="169 Bd de la Villette 75010 Paris",
        )
    )

    expected_sapeurs_intervention_reinforced_event = make_operation_event(
        uuid=expected_operation_uuid,
        timestamp=DateStr("2020-12-09T17:35:24.000000"),
        raw_operation_id="1",
        status="reinforced",
        address="169 Bd de la Villette 75010 Paris",
        address_area="STOU",
        cause="fire",
        raw_cause="Feu de ...",
    )

    assert_adagio_operation_event_correctly_converted_to_sapeurs(
        raw_adagio_intervention_reinforced_event,
        expected_sapeurs_intervention_reinforced_event,
        expected_operation_uuid=expected_operation_uuid,
        expected_to_be_published=False,
    )

    # intervention closed
    raw_adagio_intervention_closed_event = (
        make_adagio_intervention_changed_status_event(
            id_intervention=1,
            libelle_statut_intervention="Cloture opération",
            groupe_horaire_modification_statut="2020-12-15T10:39:23.4534206+01:00",
        )
    )
    expected_sapeurs_intervention_closed_event = make_operation_event(
        uuid=expected_operation_uuid,
        timestamp=DateStr("2020-12-15T10:39:23.453420"),
        raw_operation_id="1",
        status="closed",
    )

    assert_adagio_operation_event_correctly_converted_to_sapeurs(
        raw_adagio_intervention_closed_event,
        expected_sapeurs_intervention_closed_event,
        expected_operation_uuid=expected_operation_uuid,
        expected_to_be_published=False,
    )


def test_received_incorrect_area_adagio_vehicle_event():
    (convert_to_sapeurs_event, _, _, _, _) = prepare_convert_to_sapeurs_event()

    erroneous_adagio_vehicle_event = make_adagio_vehicle_event(
        id_mma=1,
        abrege_famille_mma_operationnelle="VRM",
        abrege_statut_operationnel="S",
        timestamp="2020-12-09T17:32:18.897",
        id_intervention=-1,
        libelle_affectation_operationnelle="UNKNOWN",
    )
    erroneous_adagio_vehicle_event_as_dict = erroneous_adagio_vehicle_event.as_dict()
    with pytest.raises(InvalidEventFormatError):
        convert_to_sapeurs_event.execute_for_raw_vehicle_event(
            erroneous_adagio_vehicle_event_as_dict
        )


def test_cover_ops_is_unavailable():
    (
        convert_to_sapeurs_event,
        publisher,
        raw_vehicle_events_repo,
        _,
        accumulated_events_repo,
    ) = prepare_convert_to_sapeurs_event()
    publisher.set_error("Simulate Cover ops is down")

    convert_to_sapeurs_event.execute_for_raw_vehicle_event(
        example_adagio_vehicle_event_0
    )

    assert len(raw_vehicle_events_repo.adagio_events) == 1
    assert len(publisher.published_events) == 0
    assert len(accumulated_events_repo.events) == 1


def test_cover_ops_just_became_available():
    (
        convert_to_sapeurs_event,
        publisher,
        _,
        _,
        accumulated_events_repo,
    ) = prepare_convert_to_sapeurs_event()
    publisher.set_error("Simulate Cover ops is down")
    convert_to_sapeurs_event.execute_for_raw_vehicle_event(
        example_adagio_vehicle_event_0
    )
    assert len(accumulated_events_repo.events) == 1
    publisher.set_error(None)
    convert_to_sapeurs_event.execute_for_raw_vehicle_event(
        example_adagio_vehicle_event_0
    )
    assert len(accumulated_events_repo.events) == 0
    assert len(publisher.published_events) == 1
    published_event = publisher.published_events[0]
    assert published_event.topic == "converters_catches_up_accumulated_events"
    assert len(published_event.data) == 2


def assert_adagio_operation_event_correctly_converted_to_sapeurs(
    raw_adagio_event: AdagioInterventionEvent,
    expected_sapeurs_event: OperationEvent,
    *,
    expected_operation_uuid: str,
    expected_to_be_published: bool
):
    operation_uuid = CustomUuid()
    (
        convert_to_sapeurs_event,
        publisher,
        _,
        raw_operation_events_repo,
        _,
    ) = prepare_convert_to_sapeurs_event(operation_mapper_uuid=operation_uuid)

    operation_uuid.set_next_uuid(expected_operation_uuid)

    raw_adagio_event_as_dict = raw_adagio_event.as_dict()
    convert_to_sapeurs_event.execute_for_raw_operation_event(raw_adagio_event_as_dict)

    assert len(raw_operation_events_repo.adagio_events) == 1
    assert raw_operation_events_repo.adagio_events[0] == raw_adagio_event_as_dict

    published_events = publisher.published_events
    if expected_to_be_published:
        assert len(published_events) == 1
        assert published_events[0] == expected_sapeurs_event
    else:
        assert len(published_events) == 0
