from converters.adapters.identity_mappers import (
    OperationIdentityMapper,
    VehicleIdentityMapper,
)
from converters.adapters.in_memory_publisher import InMemoryPublisher
from converters.adapters.in_memory_raw_events_repo import (
    InMemoryExternalEventRepository,
)
from converters.domain.ports.accumulated_events_repo import (
    InMemoryAccumulatedEventsRepository,
)
from converters.domain.use_cases.convert_to_sapeurs_event import ConvertToSapeursEvent
from shared.factories.operation_event_factory import make_operation_event
from shared.factories.vehicle_event_factory import make_vehicle_event
from shared.helpers.uuid import CustomUuid


def prepare_convert_to_sapeurs_event():
    publisher = InMemoryPublisher()
    raw_vehicle_events_repo = InMemoryExternalEventRepository()
    raw_operation_events_repo = InMemoryExternalEventRepository()
    convert_to_sapeurs_event = ConvertToSapeursEvent(
        raw_vehicle_events_repo=raw_vehicle_events_repo,
        vehicle_mapper=VehicleIdentityMapper(),
        operation_mapper=OperationIdentityMapper(),
        publisher=publisher,
        accumulated_events_repo=InMemoryAccumulatedEventsRepository(),
        raw_operation_events_repo=raw_operation_events_repo,
        uuid=CustomUuid(),
    )
    return (
        convert_to_sapeurs_event,
        publisher,
        raw_vehicle_events_repo,
        raw_operation_events_repo,
    )


def test_convert_vehicle_event_with_identity():
    (
        convert_to_sapeurs_event,
        publisher,
        raw_vehicle_events_repo,
        _,
    ) = prepare_convert_to_sapeurs_event()

    vehicle_event = make_vehicle_event()
    vehicle_event_as_dict = vehicle_event.as_dict()
    convert_to_sapeurs_event.execute_for_raw_vehicle_event(vehicle_event_as_dict)

    assert publisher.published_events == [vehicle_event]
    assert raw_vehicle_events_repo.raw_events == [vehicle_event_as_dict]


def test_convert_operation_event_with_identity():
    (
        convert_to_sapeurs_event,
        publisher,
        _,
        raw_operation_events_repo,
    ) = prepare_convert_to_sapeurs_event()

    operation_event = make_operation_event()
    operation_event_as_dict = operation_event.as_dict()
    convert_to_sapeurs_event.execute_for_raw_operation_event(operation_event_as_dict)

    assert publisher.published_events == [operation_event]
    assert raw_operation_events_repo.raw_events == [operation_event_as_dict]
