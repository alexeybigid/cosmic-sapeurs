import asyncio
from asyncio.events import AbstractEventLoop
from typing import Tuple

import aiohttp_cors
from aiohttp import web
from aiohttp.web_app import Application

from converters.domain.use_cases.get_last_status_of_all_vehicles import (
    GetLastStatusOfAllVehicles,
)
from converters.entrypoints.config import Config
from converters.entrypoints.environment_variables import get_env_variables
from shared.exceptions import SapeursError
from shared.helpers.logger import logger
from shared.routes import (
    make_url,
    route_external_operation_event,
    route_external_vehicle_event,
    route_get_last_status_of_all_vehicles,
)

external_operation_events_count = 0
external_vehicle_events_count = 0


def handle_sapeurs_error(function_description: str, e: SapeursError):
    logger.warn(f"Exception raised when {function_description} : {str(e)}")
    return web.json_response(
        {"success": "false", "reason": str(e)},
        status=400,
        reason="Incorrect format",
    )


def make_app(
    config: Config,
) -> Tuple[Application, AbstractEventLoop, GetLastStatusOfAllVehicles]:
    (
        convert_to_sapeurs_event,
        get_last_status_of_all_vehicles,
    ) = config.use_cases()
    routes = web.RouteTableDef()
    # @routes.get(make_url(route_get_last_status_of_all_vehicles))
    # async def get_last_status_of_all_vehicles_endpoint(request):
    #     try:
    #         vehicle_events = await get_last_status_of_all_vehicles.execute()
    #         # return web.json_response(vehicle_events_as_dict)
    #     except SapeursError as e:
    #         return handle_sapeurs_error("getting last status of all vehicles", e)

    @routes.post(f"/{route_external_vehicle_event}")
    async def convert_vehicle_and_publish_to_cover_ops(request):
        global external_vehicle_events_count
        external_vehicle_events_count += 1

        if request.body_exists:
            external_raw_event = await request.json()
            try:
                convert_to_sapeurs_event.execute_for_raw_vehicle_event(
                    external_raw_event
                )
                return web.json_response({"success": "true"})
            except SapeursError as e:
                return handle_sapeurs_error("converting vehicle event", e)
        return web.json_response(
            {"success": "false", "reason": "Missing body"},
            status=400,
            reason="Missing body",
        )

    @routes.post(f"/{route_external_operation_event}")
    async def convert_operation_and_publish_to_cover_ops(request):
        if request.body_exists:
            external_raw_event = await request.json()

            global external_operation_events_count
            external_operation_events_count += 1

            try:
                convert_to_sapeurs_event.execute_for_raw_operation_event(
                    external_raw_event
                )
                return web.json_response({"success": "true"})
            except SapeursError as e:
                return handle_sapeurs_error("converting operation event", e)
        return web.json_response(
            {"success": "false", "reason": "Missing body"},
            status=400,
            reason="Missing body",
        )

    loop = asyncio.get_event_loop()
    app = web.Application()
    app.add_routes(routes)

    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
        },
    )

    for route in list(app.router.routes()):
        cors.add(route)

    return app, loop, get_last_status_of_all_vehicles


def main():
    config = Config(env=get_env_variables())
    app, loop, get_last_status_of_all_vehicles = make_app(config)
    loop.create_task(get_last_status_of_all_vehicles.execute())
    runner = web.AppRunner(app)
    loop.run_until_complete(runner.setup())
    site = web.TCPSite(runner, port=config.ENV.converters_port)
    loop.run_until_complete(site.start())

    loop.run_forever()


if __name__ == "__main__":
    # execute only if run as a script
    main()
