from typing import Callable, Dict, Optional, Tuple

from converters.adapters.adagio.adagio_external_system_gateway import (
    AdagioExternalSystemGateway,
)
from converters.adapters.adagio.adagio_intervention_events_repository import (
    PgAdagioInterventionEventsRepository,
)
from converters.adapters.adagio.adagio_operation_event_mapper import (
    AdagioOperationEventMapper,
)
from converters.adapters.adagio.adagio_vehicle_event_mapper import (
    AdagioVehicleEventMapper,
)
from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)
from converters.adapters.adagio.postgres.db import (
    initialize_db,
    raw_intervention_events_table,
    raw_vehicle_events_table,
)
from converters.adapters.http_publisher import HttpPublisher
from converters.adapters.identity_mappers import (
    OperationIdentityMapper,
    VehicleIdentityMapper,
)
from converters.adapters.in_memory_publisher import InMemoryPublisher
from converters.adapters.pandas_external_events_repo import (
    PandasExternalEventsRepository,
)
from converters.domain.ports.accumulated_events_repo import (
    AbstractAccumulatedEventsRepository,
    InMemoryAccumulatedEventsRepository,
)
from converters.domain.ports.event_mapper import (
    AbstractOperationEventMapper,
    AbstractVehicleEventMapper,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from converters.domain.ports.external_system_gateway import (
    AbstractExternalSystemGateway,
    CustomExternalSystemGateway,
)
from converters.domain.ports.publisher import AbstractPublisher
from converters.domain.use_cases.convert_to_sapeurs_event import ConvertToSapeursEvent
from converters.domain.use_cases.get_last_status_of_all_vehicles import (
    GetLastStatusOfAllVehicles,
)
from converters.entrypoints.environment_variables import (
    EnvironmentVariables,
    ExternalEventsSourceOption,
    ExternalSystemGatewayOption,
    PublisherToCoverOpsOption,
    Repositories,
)
from shared.helpers.clock import RealClock
from shared.helpers.logger import logger
from shared.helpers.uuid import RealUuid
from shared.ports.abstract_events_repository import AbstractEventsRepository
from shared.routes import sapeurs_routes

uuid = RealUuid()
server_started_at = (
    RealClock().get_now().replace(":", " ")
)  # replace ":" by " " for Windows compatibility

env_var_to_publisher: Dict[
    PublisherToCoverOpsOption,
    Callable[[str], AbstractPublisher],
] = dict(
    IN_MEMORY=lambda url: InMemoryPublisher(),
    HTTP=lambda url: HttpPublisher(url),
)

env_var_to_vehicle_mapper: Dict[
    ExternalEventsSourceOption, Callable[[], AbstractVehicleEventMapper]
] = dict(
    SAPEURS=lambda: VehicleIdentityMapper(),
    ADAGIO=lambda: AdagioVehicleEventMapper(uuid=uuid),
)
env_var_to_operation_mapper: Dict[
    ExternalEventsSourceOption, Callable[[], AbstractOperationEventMapper]
] = dict(
    SAPEURS=lambda: OperationIdentityMapper(),
    ADAGIO=lambda: AdagioOperationEventMapper(uuid=uuid),
)

env_var_to_external_system_gateway: Dict[
    ExternalSystemGatewayOption, Callable[[str], AbstractExternalSystemGateway]
] = dict(
    CUSTOM=lambda _: CustomExternalSystemGateway(),
    ADAGIO=lambda url: AdagioExternalSystemGateway(url),
)


def get_pandas_repositories(
    *_,
) -> Tuple[AbstractExternalEventsRepository, AbstractExternalEventsRepository]:
    raw_vehicle_events_repo = PandasExternalEventsRepository(
        csv_path=f"data/raw_events_repo/{server_started_at}_raw_vehicle_events_repo.csv"
    )
    raw_operation_events_repo = PandasExternalEventsRepository(
        csv_path=f"data/raw_events_repo/{server_started_at}_raw_operation_events_repo.csv",
    )
    return raw_vehicle_events_repo, raw_operation_events_repo


def get_pg_repositories(
    pg_url: str,
) -> Tuple[AbstractExternalEventsRepository, AbstractExternalEventsRepository]:
    from sqlalchemy.engine import create_engine

    engine = create_engine(
        pg_url,
        isolation_level="REPEATABLE READ",
    )
    initialize_db(engine)

    raw_vehicle_events_repo = AdagioPgEventsRepository(
        engine=engine,
        table=raw_vehicle_events_table,
    )
    raw_operation_events_repo = AdagioPgEventsRepository(
        engine=engine,
        table=raw_intervention_events_table,
    )
    return raw_vehicle_events_repo, raw_operation_events_repo


env_var_to_repositories: Dict[
    Repositories,
    Callable[
        [str],
        Tuple[AbstractExternalEventsRepository, AbstractExternalEventsRepository],
    ],
] = dict(PG=get_pg_repositories, PANDAS=get_pandas_repositories)


class Config:
    raw_vehicle_events_repo: AbstractExternalEventsRepository
    raw_operation_events_repo: AbstractExternalEventsRepository
    publisher: AbstractPublisher
    vehicle_mapper: AbstractVehicleEventMapper
    operation_mapper: AbstractOperationEventMapper
    external_event_repo: AbstractExternalEventsRepository
    accumulated_events_repo: AbstractAccumulatedEventsRepository

    def __init__(self, env: EnvironmentVariables) -> None:

        self.ENV = env
        logger.info(self.ENV)
        self.publisher = env_var_to_publisher[self.ENV.publisher_to_cover_ops](
            f"{self.ENV.cover_ops_url}"
        )
        self.vehicle_mapper = env_var_to_vehicle_mapper[
            self.ENV.external_events_source
        ]()
        self.operation_mapper = env_var_to_operation_mapper[
            self.ENV.external_events_source
        ]()

        (
            self.raw_vehicle_events_repo,
            self.raw_operation_events_repo,
        ) = env_var_to_repositories[self.ENV.repositories](
            self.ENV.pg_url or "No URL PROVIDED"
        )

        self.external_system_gateway = env_var_to_external_system_gateway[
            self.ENV.external_system_gateway
        ](self.ENV.external_system_gateway_vehicles_url or "No URL PROVIDED")
        self.accumulated_events_repo = InMemoryAccumulatedEventsRepository()

    def use_cases(self):
        return (
            ConvertToSapeursEvent(
                raw_vehicle_events_repo=self.raw_vehicle_events_repo,
                vehicle_mapper=self.vehicle_mapper,
                raw_operation_events_repo=self.raw_operation_events_repo,
                operation_mapper=self.operation_mapper,
                publisher=self.publisher,
                uuid=RealUuid(),
                accumulated_events_repo=self.accumulated_events_repo,
            ),
            GetLastStatusOfAllVehicles(
                raw_vehicle_events_repo=self.raw_vehicle_events_repo,
                mapper=self.vehicle_mapper,
                external_system_gateway=self.external_system_gateway,
                publisher=self.publisher,
                uuid=RealUuid(),
            ),
        )
