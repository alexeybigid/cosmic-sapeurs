from converters.domain.ports.event_mapper import AbstractVehicleEventMapper
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from converters.domain.ports.external_system_gateway import (
    AbstractExternalSystemGateway,
)
from converters.domain.ports.publisher import AbstractPublisher
from shared.exceptions import InvalidEventFormatError
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import ConvertersVehiclesInitEvent


class GetLastStatusOfAllVehicles:
    def __init__(
        self,
        external_system_gateway: AbstractExternalSystemGateway,
        mapper: AbstractVehicleEventMapper,
        raw_vehicle_events_repo: AbstractExternalEventsRepository,
        publisher: AbstractPublisher,
        uuid: AbstractUuid,
    ) -> None:
        self.external_system_gateway = external_system_gateway
        self.mapper = mapper
        self.raw_vehicle_events_repo = raw_vehicle_events_repo
        self.publisher = publisher
        self.uuid = uuid

    async def execute(self) -> None:

        external_events = self.external_system_gateway.get_last_status_of_all_vehicles()
        self.raw_vehicle_events_repo.add(
            {"topic": "provided_last_status_of_all_vehicles", "data": external_events}
        )

        converted_events = []
        for event in external_events:
            try:
                converted_event = self.mapper.convert(event)
                if converted_event.data.role != "other":
                    converted_events.append(converted_event)
            except InvalidEventFormatError as e:
                logger.warn(
                    f"\n \n Received event with invalid format: \n {str(e)}. \n Skipping event: \n {event},"
                )

        logger.info(
            f"Converted {len(converted_events)} events out of {len(external_events)}"
        )

        self.publisher.publish_to_cover_ops(
            ConvertersVehiclesInitEvent(
                data=converted_events,
                uuid=self.uuid.make(),
            )
        )
