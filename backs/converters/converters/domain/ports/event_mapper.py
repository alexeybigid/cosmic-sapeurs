import abc
from typing import Dict, Union

from shared.external_event import ExternalEventAsDict
from shared.sapeurs_events import OperationEvent, VehicleEvent


class AbstractVehicleEventMapper(abc.ABC):
    def __init__(self):
        pass

    def convert(self, raw_event: Union[Dict, ExternalEventAsDict]) -> VehicleEvent:
        raise NotImplementedError


class AbstractOperationEventMapper(abc.ABC):
    def __init__(self):
        pass

    def convert(self, raw_event: Union[Dict, ExternalEventAsDict]) -> OperationEvent:
        raise NotImplementedError
