import abc
from typing import Dict

from shared.ports.abstract_events_repository import AbstractEventsRepository


class AbstractExternalEventsRepository(AbstractEventsRepository):
    @abc.abstractclassmethod
    def _add(self, event: Dict) -> None:
        raise NotImplementedError
