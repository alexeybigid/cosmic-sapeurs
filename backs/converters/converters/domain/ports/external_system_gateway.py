import abc
import json
import os
from typing import Dict, List

from dotenv import load_dotenv

from shared.helpers.logger import logger


class AbstractExternalSystemGateway(abc.ABC):
    @abc.abstractmethod
    def get_last_status_of_all_vehicles() -> List[Dict]:
        raise NotImplementedError()


class CustomExternalSystemGateway(AbstractExternalSystemGateway):
    def __init__(self) -> None:
        load_dotenv()
        json_path = os.getenv("JSON_CUSTOM_EXTERNAL_SYSTEM_GATEWAY")
        if json_path is None:
            self._last_status_of_all_vehicles = []
            return

        try:
            with open(json_path) as file:
                self._last_status_of_all_vehicles = json.load(file)
        except Exception as e:
            logger.warn(f"Could not load json from {json_path} : {str(e)}")
            self._last_status_of_all_vehicles = []

    def get_last_status_of_all_vehicles(self) -> List[Dict]:
        return self._last_status_of_all_vehicles

    def set_last_status_of_all_vehicles(
        self, last_status_of_all_vehicles: List[Dict]
    ) -> None:
        self._last_status_of_all_vehicles = last_status_of_all_vehicles
