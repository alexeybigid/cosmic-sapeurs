import re
from typing import Dict, Optional, cast

from converters.adapters.adagio.mapping_dictionnaries.adagioProcedure_to_Procedure import (
    AdagioProcedure_to_Procedure,
)
from converters.domain.ports.event_mapper import AbstractOperationEventMapper
from shared.adagio_date import (
    adagio_etat_intervention_regex,
    adagio_groupe_horaire_modification_statut,
)
from shared.adagio_event_dict_to_event import adagio_intervention_event_dict_to_event
from shared.adagio_intervention_events import (
    AdagioInterventionChangedStatusEventData,
    AdagioInterventionCreatedEventData,
    AdagioInterventionReinforcedEventData,
    AdagioProcedure,
)
from shared.data_transfert_objects.operation_event_data import (
    OperationEventData,
    RawOperationId,
)
from shared.exceptions import InvalidEventFormatError
from shared.helpers.date import from_regex
from shared.helpers.logger import logger
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import OperationEvent

from .mapping_dictionnaries.libelleStatutIntervention_to_OperationStatus import (
    libelleStatutIntervention_to_OperationStatus,
)


# TODO : check where it's used : did not break any tests despite the fact that event.data may not be of right type
class AdagioOperationEventMapper(AbstractOperationEventMapper):
    def __init__(self, uuid: AbstractUuid):
        super().__init__()
        self.uuid = uuid

    def convert(self, raw_event_as_dict: Dict) -> Optional[OperationEvent]:

        topic = raw_event_as_dict.get("topic")
        raw_event = adagio_intervention_event_dict_to_event(raw_event_as_dict)

        if topic == "adagio_intervention_created":
            return self.on_intervention_created(
                cast(AdagioInterventionCreatedEventData, raw_event.data)
            )
        elif topic == "adagio_intervention_reinforced":
            return self.on_intervention_reinforced(
                cast(AdagioInterventionReinforcedEventData, raw_event.data)
            )
        elif topic == "adagio_intervention_changed_status":
            return self.on_intervention_changed_status(
                cast(AdagioInterventionChangedStatusEventData, raw_event.data)
            )
        else:
            messages = f"Received intervention event with unknown topic {topic}"
            logger.warn(messages)
            raise InvalidEventFormatError(messages)

    @staticmethod
    def is_motif_fire(motif: str):
        # all motif starting with 1 are fire
        return re.findall("^1\d{2}", motif)

    @staticmethod
    def is_motif_victim(motif: str):
        # all motif starting with 3 are victim
        return re.findall("^3\d{2}", motif)

    def on_intervention_created(
        self, event_data: AdagioInterventionCreatedEventData
    ) -> OperationEvent:
        adagio_procedure = event_data.OdeResume.Procedure
        ode_resume = event_data.OdeResume
        raw_cause = ode_resume.LibelleMotif
        motif = ode_resume.Motif

        if self.is_motif_fire(motif):
            cause = "fire"
        elif self.is_motif_victim(motif):
            cause = "victim"
        else:
            cause = "other"

        procedure = AdagioProcedure_to_Procedure[adagio_procedure]

        sapeurs_timestamp = from_regex(
            ode_resume.EtatIntervention,
            adagio_etat_intervention_regex,
        )
        return OperationEvent(
            uuid=self.uuid.make(),
            data=OperationEventData(
                raw_cause=raw_cause,
                raw_operation_id=RawOperationId(str(ode_resume.IdIntervention)),
                address=ode_resume.Adresse.LibelleComplet
                if ode_resume.Adresse
                else None,
                address_area=ode_resume.Cstc.Libelle,
                cause=cause,
                status="opened",
                timestamp=sapeurs_timestamp,
                procedure=procedure,
            ),
        )

    def on_intervention_reinforced(
        self, event_data: AdagioInterventionReinforcedEventData
    ) -> OperationEvent:
        adagio_procedure = event_data.OdeResume.Procedure
        ode_resume = event_data.OdeResume
        raw_cause = ode_resume.LibelleMotif

        motif = ode_resume.Motif

        if self.is_motif_fire(motif):
            cause = "fire"
        elif self.is_motif_victim(motif):
            cause = "victim"
        else:
            cause = "other"

        sapeurs_timestamp = from_regex(
            ode_resume.EtatIntervention,
            adagio_etat_intervention_regex,
        )

        procedure = AdagioProcedure_to_Procedure[adagio_procedure]
        return OperationEvent(
            uuid=self.uuid.make(),
            data=OperationEventData(
                raw_cause=raw_cause,
                raw_operation_id=RawOperationId(str(ode_resume.IdIntervention)),
                address=ode_resume.Adresse.LibelleComplet
                if ode_resume.Adresse
                else None,
                address_area=ode_resume.Cstc.Libelle,
                cause=cause,
                status="reinforced",
                timestamp=sapeurs_timestamp,
                procedure=procedure,
            ),
        )

    def on_intervention_changed_status(
        self, event_data: AdagioInterventionChangedStatusEventData
    ) -> OperationEvent:
        intervention = event_data.Intervention
        libelle_statut_intervention = (
            event_data.StatutIntervention.LibelleStatutIntervention
        )
        sapeurs_timestamp = from_regex(
            event_data.GroupeHoraireModificationStatut,
            adagio_groupe_horaire_modification_statut,
        )
        return OperationEvent(
            uuid=self.uuid.make(),
            data=OperationEventData(
                raw_cause=None,
                raw_operation_id=RawOperationId(str(intervention.IdIntervention)),
                address=None,
                address_area=None,
                cause=None,
                status=libelleStatutIntervention_to_OperationStatus[
                    libelle_statut_intervention
                ],
                timestamp=sapeurs_timestamp,
                procedure=None,
            ),
        )
