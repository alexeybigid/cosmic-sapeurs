from sqlalchemy import Column, MetaData, String, Table
from sqlalchemy.dialects.postgresql import JSONB

metadata = MetaData()

raw_vehicle_events_table = Table(
    "raw_vehicle_events",
    metadata,
    Column("topic", String(255)),
    Column("data", JSONB),
)

raw_intervention_events_table = Table(
    "raw_intervention_events",
    metadata,
    Column("topic", String(255)),
    Column("data", JSONB),
)


def initialize_db(engine):
    metadata.create_all(engine)


def reset_db(engine):
    metadata.drop_all(engine)
