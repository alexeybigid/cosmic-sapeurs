from typing import List

from converters.adapters.adagio.adagio_csv_events_repository import (
    AdagioCsvEventsRepository,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.adagio_vehicle_events import AdagioVehicleChangedStatusEvent
from shared.helpers.date import DateStr


class InMemoryAdagioVehicleEventsRepository(AbstractExternalEventsRepository):
    def __init__(self):
        self._adagio_events: List[AdagioVehicleChangedStatusEvent] = []

    def _add(self, event: AdagioVehicleChangedStatusEvent) -> None:
        self._adagio_events.append(event)

    # For test purpose only
    @property
    def adagio_events(self):
        return self._adagio_events


class CsvAdagioVehicleEventsRepository(AdagioCsvEventsRepository):
    def __init__(self, now: DateStr):
        super().__init__(now=now, suffix="raw_vehicle_events")
