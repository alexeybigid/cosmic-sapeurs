from typing import Dict

from converters.domain.ports.event_mapper import AbstractVehicleEventMapper
from shared.adagio_date import adagio_date_statut_operationnel_mma
from shared.adagio_event_dict_to_event import adagio_vehicle_event_dict_to_event
from shared.data_transfert_objects.operation_event_data import RawOperationId
from shared.data_transfert_objects.vehicle_event_data import (
    OmnibusInfos,
    RawVehicleId,
    VehicleEventData,
)
from shared.helpers.date import DateStr, from_regex
from shared.helpers.uuid import AbstractUuid
from shared.sapeurs_events import VehicleEvent

from .mapping_dictionnaries.abregeFamilleMMA_to_VehicleRole import (
    abregeFamille_to_VehicleRole,
)
from .mapping_dictionnaries.abregeStatutOperationnel_to_VehicleStatus import (
    AbregeStatutOperationnel_to_VehicleStatus,
)
from .mapping_dictionnaries.libelleObjetGeo_to_area import libelleObjetGeo_to_area


class AdagioVehicleEventMapper(AbstractVehicleEventMapper):
    def __init__(self, uuid: AbstractUuid):
        super().__init__()
        self.uuid = uuid

    def convert(self, raw_event_as_dict: Dict) -> VehicleEvent:

        adagio_vehicle_event = adagio_vehicle_event_dict_to_event(raw_event_as_dict)

        event_data = adagio_vehicle_event.data
        raw_date = event_data.DateStatutOperationnelMMA
        formated_date = from_regex(raw_date, adagio_date_statut_operationnel_mma)
        # formated_date = event_data.DateStatutOperationnelMMA.replace(" ", "")
        # if not is_datestr_format(formated_date):
        #     raise InvalidEventFormatError(
        #         f"Received incorrect string format {formated_date}, expected {date_format}."
        #     )
        libelleStatutOperationnel = event_data.LibelleStatutOperationnel.strip()
        raw_operation_id_as_string = str(event_data.IdIntervention)

        if raw_operation_id_as_string in ["-1", "0"]:
            raw_operation_id = None
        else:
            raw_operation_id = RawOperationId(raw_operation_id_as_string)

        is_ep6_omnibus = (
            event_data.Omnibus
            and event_data.InformationsOmnibus.EstOmnibusEpm6Classique
        )
        # --------------------------------------------

        role = abregeFamille_to_VehicleRole(
            event_data.AbregeFamilleMMAOperationnelle,
            event_data.AbregeFamilleMMAOriginelle,
            is_ep6_omnibus,
        )

        return VehicleEvent(
            uuid=self.uuid.make(),
            data=VehicleEventData(
                home_area=libelleObjetGeo_to_area[
                    event_data.LibelleAffectationOperationnelle
                ],
                raw_operation_id=raw_operation_id,
                raw_status=libelleStatutOperationnel,
                raw_vehicle_id=RawVehicleId(str(event_data.IdMMA)),
                role=role,
                status=AbregeStatutOperationnel_to_VehicleStatus[
                    event_data.AbregeStatutOperationnel
                ],
                timestamp=DateStr(formated_date),
                vehicle_name=event_data.ImmatriculationBSPP,
                omnibus=OmnibusInfos(
                    partner_raw_vehicle_id=RawVehicleId(
                        str(event_data.InformationsOmnibus.IdMMAOmnibus)
                    ),
                )
                if is_ep6_omnibus
                else None,
            ),
        )
