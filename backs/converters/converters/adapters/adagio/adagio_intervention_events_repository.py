from typing import List

from converters.adapters.adagio.adagio_csv_events_repository import (
    AdagioCsvEventsRepository,
)
from converters.adapters.adagio.postgres.adagio_pg_events_repository import (
    AdagioPgEventsRepository,
)
from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.adagio_intervention_events import AdagioInterventionEvent
from shared.helpers.date import DateStr


class InMemoryAdagioInterventionEventsRepository(AbstractExternalEventsRepository):
    def __init__(self):
        self._adagio_events: List[AdagioInterventionEvent] = []

    def _add(self, event: AdagioInterventionEvent) -> None:
        self._adagio_events.append(event)

    # For test purpose only
    @property
    def adagio_events(self):
        return self._adagio_events


class CsvAdagioInterventionEventsRepository(AdagioCsvEventsRepository):
    def __init__(self, now: DateStr):
        super().__init__(now=now, suffix="raw_intervention_events")


class PgAdagioInterventionEventsRepository(AdagioPgEventsRepository):
    pass
