import json
import os
from typing import Dict

from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.helpers.date import DateStr


class AdagioCsvEventsRepository(AbstractExternalEventsRepository):
    def __init__(self, now: DateStr, suffix: str, path: str = "data/adagio"):
        os.makedirs(path, exist_ok=True)
        self.csv_path = os.path.join(path, f"{now}_{suffix}.csv")
        self._add_row("topic", "data")

    def _add(self, event: Dict) -> None:
        topic = event["topic"]
        try:
            serialized_data = json.dumps(event["data"])
        except Exception as e:
            serialized_data = str(e)
        self._add_row(topic, serialized_data)

    def _add_row(self, first_field: str, second_field: str):
        with open(self.csv_path, "a") as file:
            file.write(f"{first_field};{second_field}\n")
