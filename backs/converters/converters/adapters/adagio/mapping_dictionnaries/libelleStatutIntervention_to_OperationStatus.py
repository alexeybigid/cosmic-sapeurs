from typing import Dict

from shared.adagio_intervention_events import AdagioLibelleStatutIntervention
from shared.data_transfert_objects.operation_event_data import OperationStatus

libelleStatutIntervention_to_OperationStatus: Dict[
    AdagioLibelleStatutIntervention, OperationStatus
] = {
    "Début": "opened",
    "Envoi": "sent",
    "Réception": "received",
    "Validation manuelle": "validated",
    "Validation automatique": "validated",
    "Annulation": "closed",
    "Fin": "finished",
    "En cours": "ongoing",
    "Rapports  rédigés": "finished",
    "Cloture automatique": "closed",
    "Cloture intervention": "closed",
    "Cloture opération": "closed",
}
