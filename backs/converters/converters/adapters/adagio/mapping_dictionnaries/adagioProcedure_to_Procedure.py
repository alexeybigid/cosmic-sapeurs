from typing import Dict

from shared.adagio_intervention_events import AdagioProcedure
from shared.data_transfert_objects.operation_event_data import Procedure

AdagioProcedure_to_Procedure: Dict[AdagioProcedure, Procedure] = {
    "R": "red",
    "O": "orange",
    "B": "white",
    "H": "external_mission",
    "V": "green",
}
