import csv
import os
from typing import Dict

import pandas as pd

from converters.domain.ports.external_events_repo import (
    AbstractExternalEventsRepository,
)
from shared.exceptions import InvalidEventFormatError
from shared.helpers.logger import logger


class InvalidEventRepositoryDataFrame(Exception):
    pass


class PandasExternalEventsRepository(AbstractExternalEventsRepository):
    df: pd.DataFrame

    def __init__(
        self,
        csv_path: str,
    ):
        self.csv_path = csv_path
        csv_path_exists = os.path.isfile(csv_path)
        self._columns = ["topic", "data"]

        if csv_path_exists:
            self._from_csv(self.csv_path)
        else:
            os.makedirs(os.path.dirname(self.csv_path), exist_ok=True)
            self.df = pd.DataFrame(columns=self._columns)
            self._to_csv()

    def _from_csv(self, csv_path):
        self.df = pd.read_csv(csv_path)
        self._check_columns_are_valid(self.df)
        self._standardize_columns_order()

    def _to_csv(self):
        self.df.to_csv(self.csv_path, index=False)

    def _add(self, event_as_dict: Dict) -> None:
        missing_keys = set(self._columns) - set(event_as_dict.keys())
        if missing_keys:
            message = f"Missing keys {missing_keys}"
            logger.warn(message)
            raise InvalidEventFormatError(message)
        new_row = pd.Series(event_as_dict)[self._columns]
        self.df = self.df.append(new_row, ignore_index=True)
        self._writerows(new_row)

    def _writerows(self, row: pd.Series):
        with open(self.csv_path, "a", newline="\n") as file:
            writer = csv.writer(file, delimiter=",")
            writer.writerow(row)

    def add_rows(self, rows: pd.DataFrame):
        self.df = pd.concat([self.df, rows], axis=0)
        self._to_csv()

    def _check_columns_are_valid(self, df):
        df_columns = df.columns.to_list()
        if not set(df_columns) == set(self._columns):
            message = f"Cannot initialize a PandasEventsRepository with columns {df_columns}, expecting {self._columns}"
            logger.warn(message)
            raise InvalidEventRepositoryDataFrame(message)

    def _standardize_columns_order(self):
        self.df = self.df[self._columns]
        self._to_csv()
