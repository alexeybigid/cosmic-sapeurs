from typing import Dict

from converters.domain.ports.event_mapper import (
    AbstractOperationEventMapper,
    AbstractVehicleEventMapper,
)
from shared.sapeurs_events import OperationEvent, VehicleEvent


class VehicleIdentityMapper(AbstractVehicleEventMapper):
    def convert(self, raw_event_as_dict: Dict) -> VehicleEvent:
        return VehicleEvent.from_dict(raw_event_as_dict)


class OperationIdentityMapper(AbstractOperationEventMapper):
    def convert(self, raw_event_as_dict: Dict) -> OperationEvent:
        return OperationEvent.from_dict(raw_event_as_dict)
