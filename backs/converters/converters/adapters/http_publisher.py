import json

import requests

from converters.domain.ports.publisher import AbstractPublisher
from shared.routes import sapeurs_routes
from shared.sapeurs_events import Event


class HttpPublisher(AbstractPublisher):
    def __init__(self, http_client_url: str) -> None:
        super().__init__()
        self.http_client_url = http_client_url

    def publish_to_cover_ops(self, event: Event) -> None:
        event_as_json = json.dumps(event.as_dict())
        requests.post(
            f"{self.http_client_url}/{sapeurs_routes.converted_event}",
            data=event_as_json
            # json.dumps({str(k): "toto" for k in range(1000000)}),
        )

    @property
    def can_publish(self) -> bool:
        try:
            requests.post(
                f"{self.http_client_url}/{sapeurs_routes.ping}",
            )
            return True
        except requests.exceptions.ConnectionError:
            return False
