import os

from dotenv import load_dotenv

from shared.helpers.decorators import exception_mode_options, make_exception_logger
from shared.helpers.prepare_get_env_variable import prepare_get_env_variable

load_dotenv()

get_env_variable = prepare_get_env_variable(os.environ.get)


def get_exception_mode():
    return get_env_variable("EXCEPTION_MODE", exception_mode_options, "RAISE")


exception_logger = make_exception_logger(get_exception_mode())
